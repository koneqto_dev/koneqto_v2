import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdviceRoutingModule } from './advice-routing.module';
import { ExpertAdvicesComponent } from './expert-advices/expert-advices.component';

@NgModule({
  declarations: [ExpertAdvicesComponent],
  imports: [
    CommonModule,
    AdviceRoutingModule
  ]
})
export class AdviceModule { }
