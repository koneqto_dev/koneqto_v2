import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpertAdvicesComponent } from './expert-advices.component';

describe('ExpertAdvicesComponent', () => {
  let component: ExpertAdvicesComponent;
  let fixture: ComponentFixture<ExpertAdvicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpertAdvicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpertAdvicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
