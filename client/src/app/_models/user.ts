﻿export class User {
    id: number;
    username: string;
    password: string;
    firstName: string;
    lastName: string;
    mobile: string;
    email: string;
    roleid:number;
    country:any
    state:any
    city:any
    functional_area:any
    dob:any
    gender:any
    currentworkingstatus:any
    phonecode:any;
   // lastName: string;
}

export class Country {
    constructor(public country_id: number,public sortname:string,public country_name: string,public phonecode:number) { }
  }

  export class City {
    constructor(public city_id: number, public city_name: string, public state_id: number) { }
  }
  export class State {
    constructor(public state_id: number, public state_name: string, public country_id: number
    ) { }
  }
  

export interface userProfile{
//user_profile_id serial,
//user_registration_id integer references unic_sol.lu_user_registration (user_registration_id),

Resume_Headline : string,
Current_Designation : string,
Current_Company : string,
Current_Location : string,
Preferred_Location : string,
Functional_Area : string,
Role : string,
Industry : string,
Date_of_Birth : string,
Gender : string,
Total_Experience : string,
Annual_Salary : string,
Highest_Degree : string,
Phone : string,
Email : string,
Permanent_Address : string,
Hometown : string,
Pin_Code : string,
Marital_Status : string,
Key_Skills : string,
selectedfnarea:any,
selectedrole:any
walkin:any

}

// export class educationaldetails{
//     //education_id serial,
// qualification : string;
// Specialization : string;
// University_or_Institute : string;
// year :number;
// grading_system : string;
// marks :number;
// medium : string;
// natureof_job : string;

// yearlist:any=[
//     {
//     id:1,
//     year:2012
// },
// {
//     id:2,
//     year:2012
// },
// {
//     id:3,
//     year:2012
// },
// {
//     id:4,
//     year:2012
// },
// {
//     id:5,
//     year:2012
// },
// {
//     id:6,
//     year:2012
// },
// {
//     id:7,
//     year:2012
// },
// {
//     id:8,
//     year:2012
// },
// {
//     id:9,
//     year:2012
// },
// {
//     id:10,
//     year:2012
// },
// {
//     id:11,
//     year:2012
// }

// ];

// }

export class jobposting{
    //education_id serial,
jobtitle : string;
companyname : string;
expyear : string;
expmonths:string;
location :number;
minsalaryl : string;
minsalaryt:string;
maxsalaryt:number;
maxsalary :number;
natureofjob : string;
functionalarea : string;
educationdetails : string;
keyskills : string;
description : string;

}
export class Languages {
    loginuserid: number;
    language_id: number;
    language_name: string;
    is_speak: string;
    is_read: string;
    is_write: string;
}
export class Skill {
    loginuserid: number;
    skill_name: string;
    version: string;
    last_used: string;
    exp_in_years:string;
    exp_in_months:string;
}



export class educationaldetails{
    //education_id serial,
    constructor(
    user_registration_id:string,
qualification : string,
Specialization : string,
University_or_Institute : string,
year :number,
grading_system : string,
marks :number,
medium : string,
natureof_job : string,
education_type:string
    ){}
}
export class interview
{
    title:string;
    schedule_date:string;
    schedule_time:string;
    location:string;
    interviewer:string;
    type:string;
    mode:string;
    notify_me:string;
    is_active:string;
    created_by:string;
    created_date:string;
    modify_by:string;
    modify_date:string;
}