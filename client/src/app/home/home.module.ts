import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { AboutComponent } from './about/about.component';

import { ContactusComponent } from './contactus/contactus.component';
import { CookiesComponent } from './cookies/cookies.component';

import { PrivacyComponent } from './privacy/privacy.component';

import { TermsAndConditionsComponent } from './terms-and-conditions/terms-and-conditions.component';

import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { GetappComponent } from './getapp/getapp.component';

@NgModule({
  declarations: [AboutComponent, ContactusComponent, CookiesComponent, PrivacyComponent,  TermsAndConditionsComponent, PageNotFoundComponent, GetappComponent],
  imports: [
    CommonModule,
    HomeRoutingModule
  ]
})
export class HomeModule { }
