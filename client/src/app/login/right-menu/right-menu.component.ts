import { Component, OnInit, TemplateRef } from '@angular/core';
import { AuthenticationService, UserService, FriendService, walkinService } from '../../services/index';

import { environment } from '../../../environments/environment';
import { ToastrService } from 'ngx-toastr';
import {SimpleCrypt} from "ngx-simple-crypt";
// import { BsModalService } from 'ngx-bootstrap/modal';
// import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-right-menu',
  templateUrl: './right-menu.component.html',
  styleUrls: ['./right-menu.component.css']
})
export class RightMenuComponent implements OnInit {

 
  model: any;
  friendslist: any = {};
  sendreq: any = {};
  invite: any={};
  message:string;
 
  constructor(private friendservice: FriendService, private ToastrService: ToastrService, private authenticationService:AuthenticationService ,private walkinService:walkinService,
    // private modalService: BsModalService,
    private userservice: UserService,private spinnerService: Ng4LoadingSpinnerService) { }
  Apiurl = environment.Apiurl
  emparray:any=[]
  emppresarray:any=[]
  presentdesignation: any = []

  // modalRef: BsModalRef;
  openModal1(profileoverview: TemplateRef<any>) {
    // this.modalRef = this.modalService.show(profileoverview,
    //   Object.assign({}, { class: 'gray modal-lg' })
    // );
  }
  skilldetails: any = []
  cur_des: any = []
  skillsarraydisplay: any = []
  previousdesignation: any = []
  result: any
  proskillsarray: any = []
  proskillsarraydisplay: any = []
  picsArr: any = []
  Imagejson: any = {};
  ApiUrl = environment.Apiurl
  imagesArr1: any = [];
  galleryOptions: NgxGalleryOptions[];
  graduniversity: any = []
  gradsarray: any = []
  pgarray: any = []
  pguniversity: any = []
  phdarray: any = []
  phduniversity: any = []
  languagesdisplay: any = []
  closeModal(template: TemplateRef<any>) {

    // this.modalService.hide(1);
  }
  openresume(profileoverview, user_registration_id) {
    debugger
    this.openModal1(profileoverview);

    this.userservice.getprofiledetails1(user_registration_id).subscribe((data) => {

      this.skilldetails = [];
      this.cur_des = [];
      this.skillsarraydisplay = [];
      this.previousdesignation = [];
      this.presentdesignation = [];
      //this.profileData = data[0];
      this.result = data[0];


      if (this.result.presentemp_details != null) {
        for (let i = 0; i < this.result.presentemp_details.length; i++) {
          this.presentdesignation.push(this.result.presentemp_details[i].designation.split(':')[1])

        }
      }

      if (this.result.previousemp_details != null) {
        for (let i = 0; i < this.result.previousemp_details.length; i++) {
          this.previousdesignation.push(this.result.previousemp_details[i].designation.split(':')[1])

        }
      }
      if (this.result.project_details) {

        for (let j = 0; j < this.result.project_details.length; j++) {
          var splitarray: any = []
          if (this.result.project_details[j].skills) {
            for (var i = 0; i < this.result.project_details[j].skills.length; i++) {
              splitarray.push(this.result.project_details[j].skills[i].split(':'))

            }
          }
          this.proskillsarray.push(splitarray)


        }
        for (let i = 0; i < this.proskillsarray.length; i++) {
          var display = [];
          let j = 0;
          while (j < this.proskillsarray[i].length) {

            let sep = ''
            if (this.proskillsarray[i][j + 1]) {
              sep = ','
            }
            display.push(this.proskillsarray[i][j][1] + sep)
            j++

          }
          this.proskillsarraydisplay.push(display)


        }
        // for (let i = 0; i < this.result.project_details.length; i++) {

        //   this.skill.push(JSON.parse(this.result.project_details[i].skills))

        // }

      }

      //-------------------------------------------------------------------------------------------------------------------
      for (let j = 0; j < this.result.skill_details.length; j++) {
        this.skilldetails.push(this.result.skill_details[j][0].split(':'))

      }

      let count = 0
      // for(let i=0;i<this.skilldetails.length;i++){
      while (count < this.skilldetails.length) {


        let sep = ''
        if (this.skilldetails[count + 1]) {
          sep = ','
        }
        // display.push(this.skilldetails[i][1]+sep)         
        this.skillsarraydisplay.push(this.skilldetails[count][1] + sep)
        // sep=''
        count++
      }
      // if (this.result.achievement_details != null) {
      //   for (var j = 0; j < this.result.achievement_details.length; j++) {
      //     this.picsArr = this.result.achievement_details[j].image;
      //     if (this.picsArr != null) {
      //       for (var i = 0; i < this.picsArr.length - 1; i++) {
      //         this.picsstring[j] = this.picsArr.split(' ')

      //       }
      //     }

      //   }

      // }

      if (this.result.achievement_details != null) {
        for (var j = 0; j < this.result.achievement_details.length; j++) {
          // let imagesArr:any=[];
          let imagesArr: any = [];
          this.picsArr = this.result.achievement_details[j].image;

          // if (this.picsArr != null) {
          //   for (var i = 0; i < this.picsArr.length - 1; i++) {


          //     this.picsstring[j] = this.picsArr.split(' ')

          //   }

          // }

          if (this.picsArr.length > 0) {
            for (var k = 0; k < this.picsArr.length; k++) {


              this.Imagejson =
                {
                  small: this.ApiUrl + '/achievements/' + this.picsArr[k],
                  medium: this.ApiUrl + '/achievements/' + this.picsArr[k],
                  big: this.ApiUrl + '/achievements/' + this.picsArr[k]
                }

              imagesArr.push(this.Imagejson);
            }

            //}
            this.imagesArr1.push(imagesArr)
          } else {
            this.imagesArr1.push(null)
          }
        }
      }
      this.galleryOptions = [
        {
          width: '400px',
          height: '300px',
          thumbnailsColumns: 4,
          imageAnimation: NgxGalleryAnimation.Slide
        },
        // max-width 800
        {
          breakpoint: 800,
          width: '100%',
          height: '600px',
          imagePercent: 80,
          thumbnailsPercent: 20,
          thumbnailsMargin: 20,
          thumbnailMargin: 20
        },
        // max-width 400
        {
          breakpoint: 400,
          preview: false
        }
      ];

      //university_or_institute
      if (this.result.grad_details != null) {
        // for (let i = 0; i < this.result.grad_details.length; i++) {
        //   this.graduniversity.push(JSON.parse(this.result.grad_details[i].university_or_institute)[0].name)
        // }
        for (let j = 0; j < this.result.grad_details.length; j++) {
          var splitarray: any = []
          if (this.result.grad_details[j].university_or_institute) {
            for (var i = 0; i < this.result.grad_details[j].university_or_institute.length; i++) {
              splitarray.push(this.result.grad_details[j].university_or_institute[i].split(':'))

            }
          }
          this.gradsarray.push(splitarray)


        }
        for (let i = 0; i < this.gradsarray.length; i++) {
          var display = []
          let j = 0
          while (j < this.gradsarray[i].length) {

            let sep = ''
            if (this.gradsarray[i][j + 1]) {
              sep = ','
            }
            display.push(this.gradsarray[i][j][1] + sep)
            j++

          }
          this.graduniversity.push(display)


        }
      }


      if (this.result.pg_details != null) {
        // for (let i = 0; i < this.result.pg_details.length; i++) {
        //   this.pguniversity.push(JSON.parse(this.result.pg_details[i].university_or_institute)[0].name)
        // }
        for (let j = 0; j < this.result.pg_details.length; j++) {
          var splitarray: any = []
          if (this.result.pg_details[j].university_or_institute) {
            for (var i = 0; i < this.result.pg_details[j].university_or_institute.length; i++) {
              splitarray.push(this.result.pg_details[j].university_or_institute[i].split(':'))

            }
          }
          this.pgarray.push(splitarray)


        }
        for (let i = 0; i < this.pgarray.length; i++) {
          var display = []
          let j = 0
          while (j < this.pgarray[i].length) {

            let sep = ''
            if (this.pgarray[i][j + 1]) {
              sep = ','
            }
            display.push(this.pgarray[i][j][1] + sep)
            j++

          }
          this.pguniversity.push(display)


        }
      }

      if (this.result.phd_details != null) {
        // for (let i = 0; i < this.result.phd_details.length; i++) {
        //   this.phduniversity.push(JSON.parse(this.result.phd_details[i].university_or_institute)[0].name)
        // }
        for (let j = 0; j < this.result.phd_details.length; j++) {
          var splitarray: any = []
          if (this.result.phd_details[j].university_or_institute) {
            for (var i = 0; i < this.result.phd_details[j].university_or_institute.length; i++) {
              splitarray.push(this.result.phd_details[j].university_or_institute[i].split(':'))

            }
          }
          this.phdarray.push(splitarray)


        }
        for (let i = 0; i < this.phdarray.length; i++) {
          var display = []
          let j = 0
          while (j < this.phdarray[i].length) {

            let sep = ''
            if (this.phdarray[i][j + 1]) {
              sep = ','
            }
            display.push(this.phdarray[i][j][1] + sep)
            j++

          }
          this.phduniversity.push(display)


        }
      }

      if (this.result.language_known != null) {
        for (let i = 0; i < this.result.language_known.length; i++) {
          this.languagesdisplay.push(JSON.parse(this.result.language_known[i].language_name))

        }

      }

      if (this.result.current_designation != null) {
        // for (let i = 0; i < this.result.current_designation.length; i++) {
        this.cur_des.push(this.result.current_designation.split(':')[1])
        // }

      }


    })

  }
  



  loadsuggestedfrnds() {
    
        
    this.friendservice.loadsuggestedfrnds()
      .subscribe(
        data => {
          
          this.friendslist = data;
       
         this.friendservice.connectionrefresh1()
         for (let j = 0; j < this.friendslist.length; j++) {

          if (this.friendslist[j].current_designation) {

            
            this.emppresarray.push(this.friendslist[j].current_designation.split(':')[1])
     


          }
          else{
            this.emppresarray.push(null)
          }
         
          // if (this.emparray[j]) {
            
          //   this.emppresarray.push(this.emparray[j][1])
           
          // }
        
        }

        },
        error => {
          this.ToastrService.error("Problem in Loading", "Friends");


        });
  }
//var show = myShows[Math.floor(Math.random() * friendslist.length)];

sendfriendrequest(reqid: any) {
    
  this.sendreq.user_registration_id = reqid; 
  this.spinnerService.show(); 
  this.friendservice.sendfriendrequest(this.sendreq)

    .subscribe(
      data => {  
   
        if(data.message[0].message == 1){
    
        this.loadsuggestedfrnds()
        this.ToastrService.success("Request Send Sucessfully", "Connect");
        this.loadsuggestedfrnds();
        this.spinnerService.hide();     
        }
        else{
          this.ToastrService.info("You have exceeded your connection requests limit for the day", "Connect");
        }
      },
      error => {
        //this.alertService.error(error);

      });
    
   
}
  errmsg:any
  sendemail(invite: any) {
    
    this.friendservice.invitemail(invite)     
      .subscribe(
        data => {
          if(data.message == 'Sucessfull'){
            this.ToastrService.success("Invite Friend Sucessful", "Mail");

          }
          
        //  this.invite = {};   
            },
        error => {
          
                })
      this.errmsg=false;
     this.invite = {}; 
  }

  validate(event: any) {

  
    / alert(event.keyCode) /
      const pattern = /^[A-Za-z' ']+$/;
    
      let inputChar = String.fromCharCode(event.charCode);
      if (event.keyCode != 8 && !pattern.test(inputChar)) {
        event.preventDefault();
      }
      if(event.keyCode==32 && this.invite.name.trim('').length==0)
      {
        event.preventDefault();  
      }
    }
  Keyname(event: any) {

    //alert(this.t)
        
        const pattern = /^[A-Za-z]+$/;    
        
        let inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
          event.preventDefault();
        }
      }
      keyPress(event: any) {
    
        
        const pattern = /[0-9]/;
    
        let inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
          event.preventDefault();
        }
      } 
    

  //     friendsearch(job_title) {
  //       
  //   if(job_title != undefined){
  //   this.friendservice.friendsearch(job_title)
  //     .subscribe(
  //       data => {
  //         
  //         this.friendslist = data  
        
  //     })
  //   }
  //   else{
  //     this.ToastrService.info("Enter your friends name", "Friend search");
  //   }
  // }


      
  user: any
  ngOnInit() {
   var simpleCrypt = new SimpleCrypt();  
   var usernameencrypt=localStorage.getItem('loginusername')
 var username = simpleCrypt.decode("my-key",usernameencrypt);
    this.user = username
    this.loadsuggestedfrnds()
    this.friendservice.connectRefresh.subscribe(
      data=>
      {
        
        this.loadsuggestedfrnds()
        
      }
    );

  }

}