import { Component, OnInit, TemplateRef, ViewChild, ElementRef } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';
// import { BsModalService } from 'ngx-bootstrap/modal';
// import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { AuthenticationService, UserService, walkinService } from '../../services/index';
import { SimpleCrypt } from "ngx-simple-crypt";
import { environment } from '../../../environments/environment';
import { FileUploader } from 'ng2-file-upload';
import { ToastrService } from 'ngx-toastr';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery';
import { Router, NavigationEnd } from "@angular/router";
// import { DeviceDetectorService } from 'ngx-device-detector';

@Component({
  selector: 'app-left-menu',
  templateUrl: './left-menu.component.html',
  styleUrls: ['./left-menu.component.css']
})
export class LeftMenuComponent implements OnInit {
  fnarealist: any;
  fnarea: any;
  countries: any;
  states: any;
  year: any;
  courses: any;
  @ViewChild('content') content: ElementRef
  public loginusername: any;
  RoleID: string
  isrecruter: boolean = false;
  sscdetails: any = {}
  interdetails: any = {};
  graduation: any = {};
  pg: any = {};
  phd: any = {};
  project: any = {};
  imgsrc : any;
  userId: string;
  username: string;
  presentdesignation:any = []
  // public adapter: ChatAdapter;

  userId1 = localStorage.getItem('loginuserid')
  simpleCrypt = new SimpleCrypt();
  loginuserid1 = this.simpleCrypt.decode("my-key", this.userId1);
  public uploader: FileUploader = new FileUploader({ url: environment.Apiurl + '/profile/profileimageuploads/?loginuserid=' + this.loginuserid1 + '&AuthToken=' + localStorage.getItem('AuthToken') });
  constructor(
    // private modelservice: BsModalService,
     private ToastrService: ToastrService,
    private AuthenticationService: AuthenticationService, private serve: UserService, private walkinservice: walkinService, private router: Router,
    // private deviceService: DeviceDetectorService
    ) {
    // var simpleCrypt = new SimpleCrypt();
    this.epicFunction();
  
  }

  
  deviceInfo:any
  isMobile:any
  epicFunction() {
    ///console.log('hello `Home` component');
    // this.deviceInfo = this.deviceService.getDeviceInfo();
    // this.isMobile = this.deviceService.isMobile();
    // const isTablet = this.deviceService.isTablet();
    // const isDesktopDevice = this.deviceService.isDesktop();
    //console.log(this.deviceInfo);
    
//  alert(this.deviceInfo.device)
 
 
    //console.log(isMobile);  // returns if the device is a mobile device (android / iPhone / windows-phone etc)
    //console.log(isTablet);  // returns if the device us a tablet (iPad etc)
    //console.log(isDesktopDevice); // returns if the app is running on a Desktop browser.
  }
  
  
  result: any = {}
  source: any;
  specialElementHandlers:any
  message: any;
  // modalRef: BsModalRef;

  profileImage: any;
  imageChangedEvent: any = '';
  image_valid: any;
  ApiUrl = environment.Apiurl

  images: any
  pics: string;
  picsArr: any;
  picsstring: any = [];
  previousdesignation : any= []
  Imagejson: any = {};
  imagesArr1: any = [];
  galleryOptions: NgxGalleryOptions[];
  //image check

  images_array: any = []
  profileImage1: any = []
  url = ''
 
  imageType: string
  errImageType: boolean
  profileimage = {
    type: '',
    img_data: ''
  }
  check(file: any) {

    this.imageType = file.target.files[0].type

    this.profileimage.type = this.imageType.replace("image/", ".")

    if (file.target.files.length) {


      if (this.imageType == "image/jpeg" || this.imageType == "image/jpg" || this.imageType == "image/png" || this.imageType == "image/gif") {
        var reader = new FileReader();
        reader.readAsDataURL(file.target.files[0]); // read file as data url
        reader.onload = (ev: any) => { // called once readAsDataURL is completed
          this.profileimage.img_data = ev.target.result;

          this.profileimage.img_data = this.profileimage.img_data.replace("data:" + this.imageType + ";base64,", "");

          this.errImageType = false
          this.onUpload()

        }


      }
      else {
        this.errImageType = true;
  
        this.profileimage = { type: '', img_data: '' }
        this.image = ''
        this.ToastrService.error("Inavalid Image Format");
  
      }

    }


    
  }
  image: any
  onUpload() {



    if (this.profileimage.img_data != '' && !this.errImageType) {

      this.serve.SaveProfile(this.profileimage).subscribe(data => {
        this.getimage()
        this.ToastrService.success("Profile image uploaded Successfully");
       
      }
      )
      // this.imgsrc = this.profileImage
     
      this.closeModal()
    }
    else {
      this.image_valid = true
    }
  }

  openresume1(template: TemplateRef<any>) {
    //this.modalRef = this.modalService.show(template);
    // this.modalRef = this.modelservice.show(
    //   template,
    //   Object.assign({}, { class: 'gray modal-lg' })
    // );
  }
  openprofile(template: TemplateRef<any>) {
    //this.modalRef = this.modalService.show(template);
    // this.modalRef = this.modelservice.show(
    //   template,
    //   Object.assign({}, { class: 'gray modal-lg' })
    // );
  }
  // closeModal(imageenlarge: TemplateRef<any>) {
  //   this.modalRef.hide();
  // }
  openmodal(imageenlarge: TemplateRef<any>) {
    //this.modalRef = this.modalService.show(template);
    // this.modalRef = this.modelservice.show(
    //   imageenlarge,
    //   Object.assign({}, { class: 'gray modal-md' })
    // );
  }


  loadtodaywalkins: any;
  formattedate: any;
  formatDate(date: any) {

    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;

    if (day.length < 2) day = '0' + day;

    this.formattedate = [year, month, day].join('-')
    return this.formattedate;
  }
  //var date = new Date();
  todaydate() {
    ;
    let created_on = this.formatDate(new Date());

    this.walkinservice.loadwalkins()
      .subscribe(
        data => {

          this.loadtodaywalkins = data;


        },
        error => {
          //this.alertService.error(error);

        });
  }




  skill: any = []
  loginuserid: any;

  openModal(editprofiletemplate: TemplateRef<any>) {
    this.profileImage = ''

    this.imageChangedEvent = null
    // this.modalRef = this.modelservice.show(editprofiletemplate);
    this.image_valid = false;

    // this.modalRef = this.modelservice.show(
    //   Object.assign({
    //     backdrop: 'static',
    //     keyboard: false
    //   }, { class: 'gray modal-lg' })
    // );

  }
  closeModal() {


    // this.modalRef.hide();

  }

  pskills: any = []
  a: any = []
  languagesdisplay: any = [];
  cur_des: any = []
  gradsarray: any = []
  graduniversity: any = []
  pgarray: any = []
  pguniversity: any = [];
  phdarray: any = []
  phduniversity: any = [];
  proskillsarray: any = []
  proskillsarraydisplay: any = []
  skillsarray: any = []
  skillsarraydisplay: any = []
  skilldetails: any = []

  getimage() {
    this.serve.getImage().subscribe(data => {
      // this.profileimage = JSON.parse(JSON.stringify(data[0]))
      this.profileImage = data[0].image


    })
  }

  confirmation(res_confirm: boolean) {
    if (res_confirm) {
      this.router.navigate(['/profile-dashboard']);
      // this.modalRef.hide()
    }
    else {
      // this.modalRef.hide()
      /*       if (this.result.role == 3) {
              this.router.navigate(['home/posts']);
            }
            else {
              this.router.navigate(['home/connectionposts']);
            } */
    }
  }
  // confirm: BsModalRef
  // confirmtemplate=confirm

  confirmtemplate: TemplateRef<any>
  @ViewChild('confirmtemplate') ctemplate: ElementRef;
  handleLoad(template) {
    this.ctemplate
    // this.modalRef = this.modelservice.show(
    //   this.ctemplate,
    //   Object.assign({
    //     backdrop: 'static',
    //     keyboard: false
    //   }, { class: 'gray modal-md' })
    // );
    
  }
  confirmtemplate2: TemplateRef<any>
  @ViewChild('confirmtemplate2') ctemplate2: ElementRef;
  handleLoad2(confirmtemplate) {
    this.ctemplate2
    // this.modalRef = this.modelservice.show(
    //   this.ctemplate2,
    //   Object.assign({
    //   //  backdrop: 'static',
    //   //  keyboard: false
    //   }, { class: 'gray modal-md' })
    // );
    
  }
  
  count: any
  hidetemp: boolean = true;
  pref_loc1:any=[]
  pref_loc2:any=[]
  openresume(template: TemplateRef<any>) {
    this.hidetemp = false;
    this.skillsarraydisplay = []
    this.skilldetails = []
    //this.modalRef = this.modalService.show(template);
    this.serve.getprofiledetails(this.loginuserid1).subscribe(data => {

      this.result = data[0];
      // this.reimage2 = "https://c2.staticflickr.com/2/1574/25734996011_637430f5d8_c.jpg";
      // this.reimage2 = "http://35.200.188.66:3000/events/images/201811710.jpeg";


      //current_designation
      if (this.result.current_designation) {
        this.cur_des=[]
        this.cur_des.push(this.result.current_designation.split(':')[1])
      }

      //pref_location
      if (this.result.preferred_location) {
        //var display = []
        this.pref_loc1 = [];
        for (let p = 0; p < this.result.preferred_location.length; p++) {
          this.pref_loc1.push(this.result.preferred_location[p].split(':'))

        }
        //this.pref_loc1.push(display)
      }

      if (this.pref_loc1) {
        this.pref_loc2 = []
        for (let p1 = 0; p1 < this.pref_loc1.length; p1++) {
          if (this.pref_loc1[p1 + 1]) {
            this.pref_loc2.push(this.pref_loc1[p1][1])
          }
          else {
            this.pref_loc2.push(this.pref_loc1[p1][1] + '.')
          }
          //this.pref_loc2.push(this.pref_loc2)
        }

      }

      //language_known

      if (this.result.language_known) {
        this.languagesdisplay = []
        // for (let i = 0; i < this.result[0].language_known.length; i++) {

        //   this.languagesdisplay.push(JSON.parse(this.userProfile[0].language_known[i].language_name))

        // }
        for (let i = 0; i < this.result.language_known.length; i++) {
          this.languagesdisplay.push(JSON.parse(this.result.language_known[i].language_name))
        }
      }

      // for()
      //university_or_institute
      if (this.result.grad_details) {

        for (let j = 0; j < this.result.grad_details.length; j++) {
          var splitarray: any = []
          if (this.result.grad_details[j].university_or_institute) {
            for (var i = 0; i < this.result.grad_details[j].university_or_institute.length; i++) {
              splitarray.push(this.result.grad_details[j].university_or_institute[i].split(':'))

            }
          }
          this.gradsarray.push(splitarray)


        }
        for (let i = 0; i < this.gradsarray.length; i++) {
          var display = []
          let j = 0
          while (j < this.gradsarray[i].length) {

            let sep = ''
            if (this.gradsarray[i][j + 1]) {
              sep = ','
            }
            display.push(this.gradsarray[i][j][1] + sep)
            j++

          }
          this.graduniversity.push(display)


        }
        // for (let i = 0; i < this.result.project_details.length; i++) {

        //   this.skill.push(JSON.parse(this.result.project_details[i].skills))

        // }
      }

      if (this.result.pg_details) {
        for (let j = 0; j < this.result.pg_details.length; j++) {
          var splitarray: any = []
          if (this.result.pg_details[j].university_or_institute) {
            for (var i = 0; i < this.result.pg_details[j].university_or_institute.length; i++) {
              splitarray.push(this.result.pg_details[j].university_or_institute[i].split(':'))

            }
          }
          this.pgarray.push(splitarray)

        }
        for (let i = 0; i < this.pgarray.length; i++) {
          var display = []
          let j = 0
          while (j < this.pgarray[i].length) {

            let sep = ''
            if (this.pgarray[i][j + 1]) {
              sep = ','
            }
            display.push(this.pgarray[i][j][1] + sep)
            j++

          }
          this.pguniversity.push(display)

        }
      }
      if (this.result.phd_details) {
        for (let j = 0; j < this.result.phd_details.length; j++) {
          var splitarray: any = []
          if (this.result.phd_details[j].university_or_institute) {
            for (var i = 0; i < this.result.phd_details[j].university_or_institute.length; i++) {
              splitarray.push(this.result.phd_details[j].university_or_institute[i].split(':'))

            }
          }
          this.phdarray.push(splitarray)

        }
        for (let i = 0; i < this.phdarray.length; i++) {
          var display = []
          let j = 0
          while (j < this.phdarray[i].length) {

            let sep = ''
            if (this.phdarray[i][j + 1]) {
              sep = ','
            }
            display.push(this.phdarray[i][j][1] + sep)
            j++

          }
          this.phduniversity.push(display)


        }
      }

      //key_skills
      if (this.result.skill_details) {
        for (let j = 0; j < this.result.skill_details.length; j++) {
          this.skilldetails.push(this.result.skill_details[j][0].split(':'))
        }
      }
      let count = 0
      // for(let i=0;i<this.skilldetails.length;i++){
      while (count < this.skilldetails.length) {
        let sep = '.'
        if (this.skilldetails[count + 1]) {
          sep = ', '
        }
        // display.push(this.skilldetails[i][1]+sep)         
        this.skillsarraydisplay.push(this.skilldetails[count][1] + sep)
        // sep=''
        count++

      }

      //presentdesignation
      if (this.result.presentemp_details != null) {
        this.presentdesignation=[]
        for (let i = 0; i < this.result.presentemp_details.length; i++) {
          this.presentdesignation.push(this.result.presentemp_details[i].designation.split(':')[1])
        }
      }

      //previousdesignation
      if (this.result.previousemp_details != null) {
        this.previousdesignation=[]
        for (let i = 0; i < this.result.previousemp_details.length; i++) {
          this.previousdesignation.push(this.result.previousemp_details[i].designation.split(':')[1])
        }
      }

      if (this.result.project_details) {

        for (let j = 0; j < this.result.project_details.length; j++) {
          var splitarray: any = []
          if (this.result.project_details[j].skills) {
            for (var i = 0; i < this.result.project_details[j].skills.length; i++) {
              splitarray.push(this.result.project_details[j].skills[i].split(':'))

            }
          }
          this.proskillsarray.push(splitarray)


        }
        for (let i = 0; i < this.proskillsarray.length; i++) {
          var display = []
          let j = 0
          while (j < this.proskillsarray[i].length) {

            let sep = ''
            if (this.proskillsarray[i][j + 1]) {
              sep = ','
            }
            display.push(this.proskillsarray[i][j][1] + sep)
            j++

          }
          this.proskillsarraydisplay.push(display)


        }
        // for (let i = 0; i < this.result.project_details.length; i++) {

        //   this.skill.push(JSON.parse(this.result.project_details[i].skills))

        // }
      }
    }, error => {

      //this.alertService.error(error);
    })

    // this.modalRef = this.modelservice.show(
    //   template,
    //   Object.assign({
    //     backdrop: 'static',
    //     keyboard: false
    //   }, { class: 'gray modal-lg' })
    // );
  }
  closeResumeTemplate(template2: TemplateRef<any>) {
    this.hidetemp = true;
    // this.modalRef.hide();
  }
  ngOnInit() {

    if(localStorage.getItem('loginuserid')!=null &&  this.isMobile=='true' ){
      //alert("plz login in web or mobile app")
      this.handleLoad2(this.ctemplate2)
    }

    // var simpleCrypt = new SimpleCrypt(); 
    // var userId = localStorage.getItem('loginuserid')
     var simpleCrypt = new SimpleCrypt();
    // var loginuserid = simpleCrypt.decode("my-key", userId);


   

    this.AuthenticationService.currentMessage.subscribe(message => this.message = message);
    var username1 = localStorage.getItem('loginusername')
    var username = simpleCrypt.decode("my-key", username1);

    this.loginusername = username;
    this.serve.getImage().subscribe(data2 => {
     
      // this.profileimage = JSON.parse(JSON.stringify(data[0]))
      this.profileImage = data2[0].image


    })

    // alert(this.loginusername)
    // this.loginuserid = simpleCrypt.decode("my-key", localStorage.getItem('loginuserid'));

    this.serve.getprofiledetails(this.loginuserid1).subscribe(data => {

      this.result = data[0];

      this.skillsarraydisplay = []
     
      this.serve.getlogincount(this.loginuserid1).subscribe(data1 => {

        this.count = data1;

        if (this.count[0].lastlogin_time % 10 == 0) {

          if (this.result.role == 3) {
            if (this.result.skill_details == null || this.result.educational_details == null) {
              this.handleLoad(this.ctemplate)
            }
          }
          else {
            if (this.result.presentemp_details == null || this.result.skill_details == null || this.result.educational_details == null) {
              this.handleLoad(this.ctemplate)
            }
          }
        }

      })



      if (this.result.presentemp_details != null) {
        for (let i = 0; i < this.result.presentemp_details.length; i++) {
          this.presentdesignation.push(this.result.presentemp_details[i].designation.split(':')[1])
        }
     
      }

      if (this.result.previousemp_details != null) {
        for (let i = 0; i < this.result.previousemp_details.length; i++) {
          this.previousdesignation.push(this.result.previousemp_details[i].designation.split(':')[1])

        }
      }
      if (this.result.project_details) {

        for (let j = 0; j < this.result.project_details.length; j++) {
          var splitarray: any = []
          if (this.result.project_details[j].skills) {
            for (var i = 0; i < this.result.project_details[j].skills.length; i++) {
              splitarray.push(this.result.project_details[j].skills[i].split(':'))
            }
          }
          this.proskillsarray.push(splitarray)
        }
        for (let i = 0; i < this.proskillsarray.length; i++) {
          var display = []
          let j = 0
          while (j < this.proskillsarray[i].length) {

            let sep = ''
            if (this.proskillsarray[i][j + 1]) {
              sep = ','
            }
            display.push(this.proskillsarray[i][j][1] + sep)
            j++

          }
          this.proskillsarraydisplay.push(display)

        }
        // for (let i = 0; i < this.result.project_details.length; i++) {

        //   this.skill.push(JSON.parse(this.result.project_details[i].skills))

        // }
      }

      if (this.result.skill_details) {
        for (let j = 0; j < this.result.skill_details.length; j++) {
          this.skilldetails.push(this.result.skill_details[j][0].split(':'))
        }
      }
      let count = 0
      // for(let i=0;i<this.skilldetails.length;i++){
      while (count < this.skilldetails.length) {


        let sep = ''
        if (this.skilldetails[count + 1]) {
          sep = ','
        }
        // display.push(this.skilldetails[i][1]+sep)         
        this.skillsarraydisplay.push(this.skilldetails[count][1] + sep)
        // sep=''
        count++
      }
      //-------------------------------------------------------------------------------------------------------------------

      if (this.result.achievement_details != null) {
        for (var j = 0; j < this.result.achievement_details.length; j++) {
          this.picsArr = this.result.achievement_details[j].image;
          // if (this.picsArr != null) {
          //   for (var i = 0; i < this.picsArr.length - 1; i++) {
          //     this.picsstring[j] = this.picsArr.split(' ')

          //   }
          // }

        }

      }

      if (this.result.achievement_details != null) {
        for (var j = 0; j < this.result.achievement_details.length; j++) {
          // let imagesArr:any=[];
          let imagesArr: any = [];
          this.picsArr = this.result.achievement_details[j].image;

          // if (this.picsArr != null) {
          //   for (var i = 0; i < this.picsArr.length - 1; i++) {


          //     this.picsstring[j] = this.picsArr.split(' ')

          //   }

          // }

          if (this.picsArr.length>0) {
            for (var k = 0; k < this.picsArr.length; k++) {


              this.Imagejson =
                {
                  small: this.ApiUrl + '/achievements/' + this.picsArr[k],
                  medium: this.ApiUrl + '/achievements/' + this.picsArr[k],
                  big: this.ApiUrl + '/achievements/' + this.picsArr[k]
                }

              imagesArr.push(this.Imagejson);
              }
            
          
          //}
          this.imagesArr1.push(imagesArr)
            }else{
              this.imagesArr1.push(null)
            }
        }
      }
      this.galleryOptions = [
        {
          width: '400px',
          height: '300px',
          thumbnailsColumns: 4,
          imageAnimation: NgxGalleryAnimation.Slide
        },
        // max-width 800
        {
          breakpoint: 800,
          width: '100%',
          height: '600px',
          imagePercent: 80,
          thumbnailsPercent: 20,
          thumbnailsMargin: 20,
          thumbnailMargin: 20
        },
        // max-width 400
        {
          breakpoint: 400,
          preview: false
        }
      ];
      if (this.result.language_known) {
        for (let i = 0; i < this.result.language_known.length; i++) {
          this.languagesdisplay.push(JSON.parse(this.result.language_known[i].language_name))
        }
      }
      //university_or_institute
      //   if(this.result.grad_details){
      //   for (let i = 0; i < this.result.grad_details.length; i++) {
      //     this.graduniversity.push(JSON.parse(this.result.grad_details[i].university_or_institute)[0].name)
      //   }
      // }
      if (this.result.grad_details) {

        for (let j = 0; j < this.result.grad_details.length; j++) {
          var splitarray: any = []
          if (this.result.grad_details[j].university_or_institute) {
            for (var i = 0; i < this.result.grad_details[j].university_or_institute.length; i++) {
              splitarray.push(this.result.grad_details[j].university_or_institute[i].split(':'))

            }
          }
          this.gradsarray.push(splitarray)

        }
        for (let i = 0; i < this.gradsarray.length; i++) {
          var display = []
          let j = 0
          while (j < this.gradsarray[i].length) {

            let sep = ''
            if (this.gradsarray[i][j + 1]) {
              sep = ','
            }
            display.push(this.gradsarray[i][j][1] + sep)
            j++

          }
          this.graduniversity.push(display)

        }
        // for (let i = 0; i < this.result.project_details.length; i++) {

        //   this.skill.push(JSON.parse(this.result.project_details[i].skills))

        // }
      }
      if (this.result.pg_details) {
        for (let j = 0; j < this.result.pg_details.length; j++) {
          var splitarray: any = []
          if (this.result.pg_details[j].university_or_institute) {
            for (var i = 0; i < this.result.pg_details[j].university_or_institute.length; i++) {
              splitarray.push(this.result.pg_details[j].university_or_institute[i].split(':'))

            }
          }
          this.pgarray.push(splitarray)

        }
        for (let i = 0; i < this.pgarray.length; i++) {
          var display = []
          let j = 0
          while (j < this.pgarray[i].length) {

            let sep = ''
            if (this.pgarray[i][j + 1]) {
              sep = ','
            }
            display.push(this.pgarray[i][j][1] + sep)
            j++

          }
          this.pguniversity.push(display)

        }
      }
      if (this.result.phd_details) {
        for (let j = 0; j < this.result.phd_details.length; j++) {
          var splitarray: any = []
          if (this.result.phd_details[j].university_or_institute) {
            for (var i = 0; i < this.result.phd_details[j].university_or_institute.length; i++) {
              splitarray.push(this.result.phd_details[j].university_or_institute[i].split(':'))

            }
          }
          this.phdarray.push(splitarray)

        }
        for (let i = 0; i < this.phdarray.length; i++) {
          var display = []
          let j = 0
          while (j < this.phdarray[i].length) {

            let sep = ''
            if (this.phdarray[i][j + 1]) {
              sep = ','
            }
            display.push(this.phdarray[i][j][1] + sep)
            j++

          }
          this.phduniversity.push(display)

        }
      }

      if (this.result.current_designation) {
this.cur_des=[]
        // let objson = JSON.parse(this.result.current_designation)

        // for (let i = 0; i < objson.length; i++) {
          this.cur_des.push(this.result.current_designation.split(':')[1])
        // }
      }
    })


    var rollId1 = localStorage.getItem('RoleID')
    var roleID = simpleCrypt.decode("my-key", rollId1);
    this.RoleID = eval(roleID)
    if (this.RoleID == "5") {
      this.isrecruter = true;
    }
  }
  selectedvalue(value: any) {

    this.AuthenticationService.changeMessage(value)
  }
  newMessage() {
    this.AuthenticationService.changeMessage("Hello from Sibling")
  }

}