import { Component, OnInit, ViewContainerRef, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Params, Router, NavigationExtras } from '@angular/router';
//import { AuthenticationService, UserService } from '../../../_services/index';
import { AuthenticationService, UserService, LoginAuthService } from '../../services/index';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ToastrService } from 'ngx-toastr';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.css']
})
export class ForgotpasswordComponent implements OnInit {


  model: any = {};
  emexsit: boolean = false;

  message: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private userService: UserService,
    private LoginAuthService: LoginAuthService,
    private toastr1: ToastrService,
    private spinnerService: Ng4LoadingSpinnerService




  ) { }

  //forgotpassword
  sendemail(femail:any) {
    
    if (this.model.femail && this.emexsit == true) {
      this.LoginAuthService.sendmail(this.model.femail)
        .subscribe(
          data => {
            // alert(data.message)
            this.spinnerService.show();
            if (data.message == "Email sent sucessfully") {
              
              this.spinnerService.hide();
              this.router.navigate(['/mailmessage']);
            
              // this.emailCheck(femail)
              // this.toastr1.error("Email sent sucessfully", "Email sent..! ");
            }
            else {
              this.toastr1.error("Problem at the server", "Email not sent..! ");
            }
          },
          error => {
            
            this.toastr1.error("Problem at the server", "Email not sent..! ");
          })
    }

  }

  ngOnInit() {

  }

  login() {
    this.router.navigate(['']);
  }

  email:any
  emailCheck(event:any) {
    this.email = event.target.value

    this.emexsit = false
    this.LoginAuthService.emailCheck(this.email).subscribe(data => {
      data = eval(JSON.stringify(data[0].count))
     
      if (data == 1) {
        this.emexsit = true

      }
      else {
        this.emexsit = false
      }

    })

  }

}

