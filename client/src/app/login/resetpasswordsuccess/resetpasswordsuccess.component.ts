import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router, NavigationExtras } from '@angular/router';
@Component({
  selector: 'app-resetpasswordsuccess',
  templateUrl: './resetpasswordsuccess.component.html',
  styleUrls: ['./resetpasswordsuccess.component.css']
})
export class ResetpasswordsuccessComponent implements OnInit {

  
  constructor(  private route: ActivatedRoute,
    private router: Router,) { }

  ngOnInit() {
  }


  login(){
    this.router.navigate([''])
  } 

}
