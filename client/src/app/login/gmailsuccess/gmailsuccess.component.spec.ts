import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GmailsuccessComponent } from './gmailsuccess.component';

describe('GmailsuccessComponent', () => {
  let component: GmailsuccessComponent;
  let fixture: ComponentFixture<GmailsuccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GmailsuccessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GmailsuccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
