
import { Component, OnInit, ViewContainerRef, TemplateRef, ViewChild, OnDestroy, OnChanges } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute, Params, Router, NavigationExtras } from '@angular/router';
import { AuthenticationService, UserService, JobpostService, LoginAuthService } from '../../services/index'
import { Observable } from 'rxjs/Rx';
// import { AuthService } from 'angularx-social-login';
// import { FacebookLoginProvider, GoogleLoginProvider, LinkedInLoginProvider } from 'angularx-social-login';
// import { SocialUser } from 'angularx-social-login';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { AppComponent } from '../../app.component';
import { ToastrService } from 'ngx-toastr';


import { Idle, DEFAULT_INTERRUPTSOURCES } from '@ng-idle/core';
import { Keepalive } from '@ng-idle/keepalive';
// import { BsModalService } from 'ngx-bootstrap';
//import { BsModalService } from 'ngx-bootstrap';


@Component({
  selector: 'app-loginpage',
  templateUrl: './loginpage.component.html',
  styleUrls: ['./loginpage.component.css']
})
export class LoginpageComponent implements OnInit {
  // private user: SocialUser;
  private loggedIn: boolean;
  loading: boolean = false;
  mexsit: boolean = false;
  emexsit: boolean = false;
  public show: boolean = false;
  fnarealist: Observable<any[]>;
  public buttonName: any = 'Show';
  public loginname: any = 'login';
  public forgotpasswordname: any;
  public fshow: boolean = false;
  public username: any;
  public password: any;
  model: any = {};
  empmodel: any = {}
  message: string;
  title: string;
  locationlist: any;
  gani: any
  uid: any = []
  closeBtnName: string;
  list: any[] = [];
  roleid: number;
  message1: string;
  public sociallogin: boolean = false;
  countries: any = [];
  countries1: any = []
  states: any = [];
  states1: any = [];
  cities: any = [];
  item: any;
  RoleID: string;
  // Object.data: any =[];
  idleState = 'Not started.';
  timedOut = false;
  lastPing?: Date = null;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private userService: UserService,
    // private authService: AuthService,
    private spinnerService: Ng4LoadingSpinnerService,
    private AppComponent: AppComponent,
    // private modalService: BsModalService  ,
    private toastr1: ToastrService,
    private LoginAuthService: LoginAuthService,
    private idle: Idle, private keepalive: Keepalive
  ) {
    // sets an idle timeout of 5 seconds, for testing purposes.
    // idle.setIdle(1800);
    // sets a timeout period of 5 seconds. after 10 seconds of inactivity, the user will be considered timed out.
    // idle.setTimeout(5);
    // sets the default interrupts, in this case, things like clicks, scrolls, touches to the document
    // idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);

    //   idle.onIdleEnd.subscribe(() => this.idleState = 'No longer idle.');
    //   idle.onTimeout.subscribe(() => {
    //    localStorage.clear();
    //    // localStorage.clear();
    //    this.router.navigate(['']);
    //     this.idleState = 'Timed out!';
    //     this.timedOut = true;
    //   });
    //   idle.onIdleStart.subscribe(() => this.idleState = 'You\'ve gone idle!');
    //   idle.onTimeoutWarning.subscribe((countdown) => this.idleState = 'You will time out in ' + countdown + ' seconds!');
    //   // toastr1.info("Session Out","Login Again")

    //   // sets the ping interval to 15 seconds
    //   keepalive.interval(15);

    //   keepalive.onPing.subscribe(() => this.lastPing = new Date());
    //   this.timedOut = false;
    //   this.reset();
  }



  // reset() {
  //   this.idle.watch();
  //   this.idleState = 'Started.';
  //   this.timedOut = false;
  //   localStorage.clear();
  //   localStorage.clear();
  //   this.modalService.hide(1);
  //   this.router.navigate(['']);
  // }
  // registration() {
  //   this.router.navigate(['/registration'])
  // }
  // forgotpassword() {
  //   this.router.navigate(['/forgetpwd'])
  // }
  userlogin(username, password: any) {

    // this.spinnerService.show();
    this.message = "";
    username = this.model.username.toLowerCase()
    this.authenticationService.login(username.trim(), this.model.password)
      .subscribe(
        user => {

          // this.spinnerService.hide();
          // this.AppComponent.loader(true);

          if (user.message == "Password Not exists") {
            this.message = "Wrong password";
          }
          if (user.message == "User not exist") {
            this.message = user.message;
          }

          if (user.message == "User already exist") {
            if (user.roleid == "3") {
              this.router.navigate(['home/posts']);
              this.saveip()
            }
            if (user.roleid == "5") {
              this.router.navigate(['home/connectionposts']);
              this.saveip()
            }
          }

        },
        error => {
          // this.AppComponent.loader(true);

        });



  }
  register() {
debugger
    this.router.navigate(['/registration']);
  }
  Keyname(event: any) {
    //alert(this.t)

    const pattern = /[^\s]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }
  keyPress(event: any) {

    const pattern = /[0-9\+\-\ ]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }



  socialid: string;
  sid: any = {};
  email: any
  users: any
  count: any
  gmailuser: any = []


  hello1: boolean = false
  ////////////////////////////////////////////////////////////////////////////////////

  getIp() {

    this.LoginAuthService.getIpAddress().subscribe(data => {
      // alert(JSON.stringify(data))
      this.UserIp = JSON.stringify(data);


    });
  }
  UserIp: any
  saveip() {

    this.LoginAuthService.saveIpAddress(this.UserIp)
      .subscribe(
        error => {
        })
  }

  ngOnInit() {

    // this.modalService.hide(1);
    this.getIp();

    ////////////////////////////////
    if (localStorage.getItem('RoleID') == "Xg==") {
      this.router.navigate(['home/posts']);
    }
    else if (localStorage.getItem('RoleID') == "WA==") {
      this.router.navigate(['home/connectionposts']);
    }


  //  
  }

  emplyeeregistration(empmodel: any) {

    if ((localStorage.getItem('currentUser') != null)) {
      this.router.navigate(['home/connectionposts']);
    }
    this.LoginAuthService.employeeresgistration(this.empmodel)
      .subscribe(
        data => {

        },
        error => {
          // this.AppComponent.loader(false);
          // this.AppComponent.loader(false);
        });
  }


  // signInWithGoogle(): void {

  //   this.sociallogin = true;
  //   this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
  // }

  // signWithFB(): void {

  //   this.sociallogin = true;
  //   this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
  // }

  // signOut(): void {

  //   this.authService.signOut();

  // }
}
