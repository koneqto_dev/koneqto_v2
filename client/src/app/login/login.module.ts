import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginRoutingModule } from './login-routing.module';
import { LoginpageComponent } from './loginpage/loginpage.component';
import { RegistrationComponent } from './registration/registration.component';
import { VerifyemailComponent } from './verifyemail/verifyemail.component';
import { GmailsuccessComponent } from './gmailsuccess/gmailsuccess.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { ResetpasswordsuccessComponent } from './resetpasswordsuccess/resetpasswordsuccess.component';
import { MainContentComponent } from './main-content/main-content.component';
import { HeaderComponent } from './header/header.component';
import { LeftMenuComponent } from './left-menu/left-menu.component';
import { RightMenuComponent } from './right-menu/right-menu.component';
import { MailmessageComponent } from './mailmessage/mailmessage.component';
import { SuccessmessageComponent } from './successmessage/successmessage.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime'
import { BsModalService, TabsModule } from 'ngx-bootstrap';
import { AvatarModule } from 'ngx-avatar';
import { NgxGalleryModule } from 'ngx-gallery'
import { PDFExportModule } from '@progress/kendo-angular-pdf-export';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { AuthenticationService, UserService, LoginAuthService, JobpostService, walkinService, FriendService } from '../services';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Routes, RouterModule } from '@angular/router';
import { MainPageComponent } from './main-page/main-page.component';
import { PostsComponent } from './posts/posts.component';

@NgModule({
  declarations: [
    LoginpageComponent,
    RegistrationComponent,
    VerifyemailComponent,
    GmailsuccessComponent,
    ForgotpasswordComponent,
    ResetpasswordsuccessComponent,
    MainContentComponent,
    HeaderComponent,
    LeftMenuComponent,
    RightMenuComponent,
    MailmessageComponent,
    SuccessmessageComponent,
    MainPageComponent,
    PostsComponent],
  imports: [
    CommonModule,
    LoginRoutingModule,
    HttpClientModule,
    FormsModule,
    HttpModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    AvatarModule,
    NgxGalleryModule,
    PDFExportModule,
    Ng4LoadingSpinnerModule,
    TabsModule.forRoot(),
    AvatarModule,

  ],
  providers: [AuthenticationService, UserService, LoginAuthService, BsModalService, JobpostService,walkinService,FriendService],

})
export class LoginModule { }
