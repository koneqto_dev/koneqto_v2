import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginpageComponent } from './loginpage/loginpage.component';
import { FormsModule } from '@angular/forms';
import { MainContentComponent } from './main-content/main-content.component';
import { RegistrationComponent } from './registration/registration.component';
import { TodayWalkinsComponent } from '../jobs/today-walkins/today-walkins.component';
import { MainPageComponent } from './main-page/main-page.component';
import { PostsComponent } from './posts/posts.component';
import { JobsModule } from '../jobs/jobs.module';

const routes1: Routes = [];

@NgModule({
  imports: [RouterModule.forChild(routes1)],
  exports: [RouterModule, FormsModule]
})
export class LoginRoutingModule { }
