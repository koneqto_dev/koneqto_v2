import { Component, OnInit } from '@angular/core';
import {  AuthenticationService ,UserService} from '../../services/index';
import { Inject, HostListener } from "@angular/core";
import { DOCUMENT } from "@angular/platform-browser";

@Component({
  selector: 'app-main-content',
  templateUrl: './main-content.component.html',
  styleUrls: ['./main-content.component.css']
})
export class MainContentComponent implements OnInit {
  message:any;
  public fixed: boolean = false; 
  public fixed1: boolean = false; 
  constructor(private AuthenticationService:AuthenticationService,@Inject(DOCUMENT) private doc: Document) { }
  @HostListener("window:scroll", [])
  onWindowScroll() {
    var d = document.documentElement;
    var offset = d.scrollTop;
    //var height = d.offsetHeight;
    var widhight = d.scrollTop + window.innerHeight;
    if(widhight < 1000){
    if (offset > 150) {
      this.fixed = true;
    }
    else if (this.fixed && offset < 150) {
      this.fixed = false;
  }
  }else{
    if (offset > 150) {
      this.fixed1 = true;
    }
    else if (this.fixed && offset < 150) {
      this.fixed1 = false;
  }
  }
}

value:any;
  ngOnInit() {
    
    this.AuthenticationService.currentMessage.subscribe(message => this.message = message)
    this.value=this.message;
  }

}
