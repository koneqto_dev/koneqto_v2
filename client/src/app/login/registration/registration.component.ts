import { Component, OnInit, ViewContainerRef, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Params, Router, NavigationExtras } from '@angular/router';
import { AuthenticationService, JobpostService, UserService, LoginAuthService } from '../../services/index';
// import { AuthService } from "angularx-social-login";
// import { SocialUser } from "angularx-social-login";
import { TabDirective } from 'ngx-bootstrap/tabs';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { AppComponent } from '../../app.component';
import { ToastrService } from 'ngx-toastr';
// import { BsModalService } from 'ngx-bootstrap/modal';
// import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { TabsetComponent } from 'ngx-bootstrap';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
 
  // private user: SocialUser;
  private loggedIn: boolean;
  loading: boolean = false;
  mexsit: boolean = false;
  uexsit: boolean = false;
  uexsitemp: boolean = false
  emexsit1: boolean = false;
  emexsit: boolean = false;

  // modalRef: BsModalRef;
  public show: boolean = false;
  fnarealists: any = []
  public buttonName: any = 'Show';
  public loginname: any = 'login';
  public forgotpasswordname: any;
  public fshow: boolean = false;
  public username: any;
  public password: any;
  model: any = {};
  empmodel: any = {}
  message: string;
  title: string;
  locationlist: any;
  closeBtnName: string;
  list: any[] = [];
  roleid: number;
  message1: string;
  public sociallogin: boolean = false;
  countries: any = [];
  states: any = [];
  states1: any = [];
  cities: any = [];
  cities1: any = [];
  Gender: any = {}
  item: any
  id: any;
  socialid: string;
  socialCheck: any = [];
  details: any = [];
  today :any= new Date()
  date :any= this.today.getFullYear() - 18 + "/" + this.today.getMonth() + "/" + this.today.getDate()

  dateArr: any = this.date.split("/")
  max = new Date(this.dateArr[0], this.dateArr[1], this.dateArr[2])
  min = new Date(1900, 1, 1);
  upexsit: boolean;


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private userService: UserService,
    private jobpostservice: JobpostService,
    // private authService: AuthService,
    private spinnerService: Ng4LoadingSpinnerService,
    private AppComponent: AppComponent,
    private LoginAuthService: LoginAuthService,
    // private modalService: BsModalService,
    private toastr1: ToastrService,

  ) { }

  states2: any
  onuserstate(country_id) {

    // this.model.city=null 
    this.LoginAuthService.loadstatesdropdown(country_id)
      .subscribe(
        data => {
          this.states = data;
        },
        error => {
          //this.alertService.error(error);            
        });

    this.LoginAuthService.getphonebyid(country_id)
      .subscribe(
        data => {
          this.states2 = data;
          this.model.phonecode = this.states2[0].phonecode
          this.CountryCode = this.states2[0].phonecode
        },
        error => {

        });

  }
  country_id1:any


  onSelect2(state_id:any) {
    this.LoginAuthService.loadcitydropdown(state_id)
      .subscribe(
        data => {
          this.cities = data;
        },
        error => {
        });
  }
  //for emp
  states3: any
  states4: any
  onSelect1(country_id:any) {

    this.empmodel.city1 = null
    this.LoginAuthService.loadstatesdropdown(country_id)
      .subscribe(
        data => {
          this.states4 = data;
        },
        error => {

        });
    this.LoginAuthService.getphonebyid(country_id)
      .subscribe(
        data => {
          this.states3 = data;
          this.empmodel.phonecode = this.states3[0].phonecode
          this.CountryCode = this.states3[0].phonecode
        },
        error => {
        });

  }
  //for emp
  onSelect4(state_id:any) {
    this.LoginAuthService.loadcitydropdown(state_id)
      .subscribe(
        data => {
          this.cities1 = data;
        },
        error => {


        });
  }
  login() {

    this.router.navigate([''])
    // this.authService.signOut();
    localStorage.clear();
  }
  @ViewChild('staticTabs') staticTabs: TabsetComponent;
  selectTab(tab_id: number) {

    this.roleid = tab_id;
  }
  onSelect(data: TabDirective): void {
  }
  datecheck(event:any) {


    const pattern = /^(((0[1-9]|[12][0-9]|30)[-/]?(0[13-9]|1[012])|31[-/]?(0[13578]|1[02])|(0[1-9]|1[0-9]|2[0-8])[-/]?02)[-/]?[0-9]{4}|29[-/]?02[-/]?([0-9]{2}(([2468][048]|[02468][48])|[13579][26])|([13579][26]|[02468][048]|0[0-9]|1[0-6])00))$/;
    let inputChar = String.fromCharCode(event.charCode)
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }
  openModalverifyotp(otpverifytemplate: TemplateRef<any>) {
    const mailPatern = /^[a-z0-9._%+-]+@(?!gmail.com)(?!google.com)(?!yahoo.com)(?!hotmail.com)(?!ymail.com)(?!yahoomail.com)[a-z0-9.-]+\.[a-z]{3,3}$/;
    let inputChar = String.fromCharCode(this.empmodel.Companymailid.charCode)

    // this.modalRef = this.modalService.show(otpverifytemplate, { class: 'modal-dialog-centered'});

    // this.otpvarify=true;
    if (this.empmodel.companyname && this.empmodel.Companywebsite && this.empmodel.country1 && this.empmodel.state1 && this.empmodel.tcc &&
      this.empmodel.city1 && this.empmodel.hrname && this.empmodel.Companymailid && this.empmodel.Designation && this.empmodel.cphonenumber
      && this.empmodel.empPassword && this.empmodel.RetypePassword && this.empmodel.gender && !this.emexsit && !this.mexsit && !this.uexsit && mailPatern && mailPatern.test(this.empmodel.Companymailid)) {
      // if(mailPatern.test(inputChar)){&& mailPatern.test(inputChar  .test(inputChar)

      if (this.empmodel.empPassword == this.empmodel.RetypePassword) {
        this.otp = '';
        // this.modalRef = this.modalService.show(otpverifytemplate, { class: 'modal-dialog-centered' });
        //  }
      }
    }
  }
  openModal(otptemplate: TemplateRef<any>) {
    var pattern = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{3,3}$/;
    this.model.username = this.model.username.toLowerCase();

    // this.modalRef = this.modalService.show( otptemplate, { class: 'modal-dialog-centered'} ); && !this.mexsit


    // this.otpvarify=true;
    //this.modalRef = this.modalService.show(otptemplate);

    if (this.model.firstName && this.model.lastName && this.model.username && this.model.mobile && this.model.email
      && this.model.password && this.model.repassword && this.model.dob && this.model.country &&
      this.model.state && this.model.functional_area && this.model.gender && this.model.currentworkingstatus && this.model.tc && !this.mexsit && !this.emexsit1 && !this.uexsit && pattern.test(this.model.email)) {

      if (this.model.password == this.model.repassword) {
        this.otp = '';
        // this.modalRef = this.modalService.show(otptemplate, { class: 'modal-dialog-centered' });
      }
    }
  }
  CountryCode: any;
  register() {
    
    this.count = 0
    var newuser = this.model;
    this.model.username = this.model.username.toLowerCase();
    newuser.errmsg = false;
    if (newuser.password != newuser.repassword) {
      newuser.errmsg = true;
    }
    else {
      
      this.model.roleid = this.roleid;
      // this.model.mobile = "+" + this.model.phonecode +"-"+ this.model.mobile;
      if (this.model.firstName && this.model.lastName && this.model.username && this.model.mobile && this.model.email
        && this.model.password && this.model.repassword && this.model.dob && this.model.country &&
        this.model.state && this.model.functional_area && this.model.gender && this.model.currentworkingstatus && this.model.tc) {
        if (!this.uexsit) {
          this.sendotp()
        } else {
          // alert()
        }

      }
    }
  }
  empregister() {

    
    var newuser1 = this.empmodel;
    this.empmodel.hrname = this.empmodel.hrname.toLowerCase();
    newuser1.errmsg = false;
    if (newuser1.empPassword != newuser1.RetypePassword) {
      newuser1.errmsg = true;
    }
    else {
      

      if (this.empmodel.companyname && this.empmodel.Companywebsite && this.empmodel.country1 && this.empmodel.state1 && this.empmodel.tcc &&
        this.empmodel.city1 && this.model.functional_area1 && this.empmodel.hrname && this.empmodel.Companymailid && this.empmodel.firstName && this.empmodel.Designation && this.empmodel.cphonenumber
        && this.empmodel.empPassword && this.empmodel.RetypePassword, this.empmodel.gender && !this.uexsit) {
        this.sendotp()
      } else {
        // alert();
      }

    }
  }
  sessionId:any
  count: number = 0
  sendotp() {
    
    this.LoginAuthService.sendotp1(this.mobile, this.CountryCode)
      .subscribe(
        data => {
          this.count = this.count + 1
          
          this.sessionId = data.sessionId;

        },
        error => {
          //


        });
    // } else {
    //   // alert(); }


  }
  Keyname(event: any) {

    //alert(this.t)


    const pattern = /^[A-Za-z' ']+$/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }
  

  otp: any;
  otpvarify: any;
  verifyotp(otp:any) {
    
    this.otpvarify = false
    
    if (this.otp != '') {
      this.LoginAuthService.varifyotp2(this.otp, this.mobile, this.CountryCode, this.sessionId)
        .subscribe(
          data => {
debugger
            if (data.matchResult == 'OTP Matched') {
              this.otpvarify = true;
              // this.modalRef.hide();
              this.sessionId = ''
              if (this.model.email) {
                // this.model.fbuser=this.fbuser
                this.LoginAuthService.create(this.model)
                  .subscribe(
                    data => { 
                      debugger
                      this.toastr1.success('Registration ', 'Sucessfull!');
                      if (data.message == 'Registration completed successfully')    {
                        // this.sessionId=''
                        
                        this.model = {}
                        this.router.navigate(['/sucessmessages']);

                        this.model = {}
                      }
                      else if (data.message == 'registrationmailsent') {
                        // this.sessionId=''
                        this.model = {}
                        
                        this.router.navigate(['/gmailsucess']);

                      }
                      this.model = ''

                    })
              }
              if (this.empmodel.Companymailid) {                
                this.LoginAuthService.employeeresgistration(this.empmodel)
                  .subscribe(
                    data => {
                    

                      // this.modalRef.hide();
                      if (data.message == 'Registration completed successfully') {
                        this.router.navigate(['/sucessmessages'])
                        //this.authService.signOut();
                        this.empmodel = {}
                      }


                    },

                    error => {


                      // this.AppComponent.loader(false);

                    });

              }

              this.toastr1.success("Mobile Number Verified Sucessfully", "Yeah..! Registered");
              // this.modalRef.hide()
            }
            else {
              this.otpvarify = false;
              this.toastr1.error("Your mobile number is not verified", "Not Registered..! ");
            }
            //this.otp = data;

          },
          error => {
            //

          });
    }
  }
  keyPress(event: any) {

    this.count = 0


    const pattern = /[0-9]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }





  Keyname3(event: any) {
    const pattern = /[a-zA-z' ']/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
    if (event.keyCode == 32 && this.empmodel.companyname.trim('').length == 0) {
      event.preventDefault();

    }
  }
  Keyname1(event: any) {




    // const pattern = /[a-zA-z' ']/;
    const pattern = /[^\s]/;


    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }
  Keyname2(event: any) {
    const pattern = /[^\s]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  pswdpattrn(event: any) {
    const pattern = /(?=.*\d)(?=.*[!@#$%*])(?=.*[A-Z]).{8,}/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();

    }

  }











  Firstnamecheck(event: any) {
    const pattern = /^[A-Za-z' ']+$/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
    if (event.keyCode == 32 && this.model.firstName.trim('').length == 0) {
      event.preventDefault();

    }
  }


  Lastnamecheck(event: any) {
    const pattern = /^[A-Za-z' ']+$/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
    if (event.keyCode == 32 && this.model.lastName.trim('').length == 0) {
      event.preventDefault();

    }
  }
  DesignationCheck(event: any) {

    const pattern = /^[A-Za-z' ']+$/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
    if (event.keyCode == 32 && this.empmodel.Designation.trim('').length == 0) {
      event.preventDefault();

    }
  }

  loadcountry() {
    this.LoginAuthService.loadcountrydropdown1()
      .subscribe(
        data => {


          this.countries = data;

          // alert("loadyeardropdown loaded sucessfully");

        },
        error => {
          //this.alertService.error(error);

        });
  }
  loadgender() {
    this.LoginAuthService.loadgender()
      .subscribe(
        data => {


          this.Gender = data;

        },
        error => {
          //this.alertService.error(error);

        });
  }
  loadfunctionalarea() {
    this.LoginAuthService.loadfunctinalarea()
      .subscribe(
        data => {


          this.fnarealists = data;

        },
        error => {
        });
  }
  fbuser: boolean = false;
  
  ngOnInit() {
    
    // this.authService.authState.subscribe((user) => {

    //   if (user != null) {

        
    //     this.model = user;
      
    //     // this.emialUser = true;
    //     if(user.email == undefined){
    //       this.fbuser = false;
         
    //     }
    //     else {
    //       this.fbuser = true;
        
    //     }
    //     user.name.split(" ")
    //     this.model.firstName = this.model.name.split(" ")[0]
    //     this.model.lastName = user.name.split(" ")[1]
        
    //    //  this.model.email=user.email;

    //     this.id = user.id;
    //     //  this.socialuserCheck(this.id)


    //     this.loadcountry();
    //     this.loadgender();
    //     this.loadfunctionalarea();
        
    //   }

    //   else {
     
    //     this.loadcountry();
    //     this.loadgender();
    //     this.loadfunctionalarea();


    //   }
    // });
  }
  mobile:any
  checkphone(e:any) {
    this.count = 0
  
    this.mobile = e.target.value

    this.mexsit = false
    if (e.target.value.length == 10) {
      this.LoginAuthService.checkmobile_no(this.mobile).subscribe(data => {

        if (data[0].count == 1) {
          this.mexsit = true
        }
        else {
          this.mexsit = false
        }
      });
    }
    else { }
  }
  email:any
  emailCheck1(event:any) {
    this.email = event.target.value

    this.emexsit1 = false
    this.LoginAuthService.emailCheck(this.email).subscribe(data => {
      data = eval(JSON.stringify(data[0].count))

      if (data == 1) {
        this.emexsit1 = true

      }
      else {
        this.emexsit1 = false
      }

    })

  }
  emailCheck(event:any) {
    this.email = event.target.value

    this.emexsit = false
    this.LoginAuthService.emailCheck(this.email).subscribe(data => {
      data = eval(JSON.stringify(data[0].count))

      if (data == 1) {
        this.emexsit = true

      }
      else {
        this.emexsit = false
      }

    })

  }
  userNameCheck: string
  userNameChecking(event:any) {
    this.userNameCheck = event.target.value.toLowerCase();
    // this.userNameCheck = event.target.value
    this.uexsit = false
    this.LoginAuthService.userNameChecking(this.userNameCheck).subscribe(data => {

      data = eval((JSON.stringify(data[0].count)))



      if (data >= 1) {
        this.uexsit = true;
      }
      else {
        this.uexsit = false;
      }
    });
  }
  userNameCheckingemp(event:any) {
    this.userNameCheck = event.target.value.toLowerCase();
    // this.userNameCheck = event.target.value
    this.uexsitemp = false
    this.LoginAuthService.userNameChecking(this.userNameCheck).subscribe(data => {

      data = eval((JSON.stringify(data[0].count)))



      if (data >= 1) {
        this.uexsitemp = true;
      }
      else {
        this.uexsitemp = false;
      }
    });
  }
  searhjob(p_skills, p_location, p_experience, p_max_salary) {




  }
  // signInWithGoogle(): void {


  //   this.sociallogin = true;
  //   this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);

  // }
  // signInWithFB(): void {


  //   this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
  // }
  // signOut(): void {


  //   this.authService.signOut();
  //   localStorage.clear();

  // }



}