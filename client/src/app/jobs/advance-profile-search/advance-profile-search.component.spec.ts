import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvanceProfileSearchComponent } from './advance-profile-search.component';

describe('AdvanceProfileSearchComponent', () => {
  let component: AdvanceProfileSearchComponent;
  let fixture: ComponentFixture<AdvanceProfileSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvanceProfileSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvanceProfileSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
