import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { JobsRoutingModule } from './jobs-routing.module';
import { TodayWalkinsComponent } from './today-walkins/today-walkins.component';
import { OtherLocationsComponent } from './other-locations/other-locations.component';
import { OtherCategoryComponent } from './other-category/other-category.component';
import { SkillWiseComponent } from './skill-wise/skill-wise.component';
import { ConnetionJobpostsComponent } from './connetion-jobposts/connetion-jobposts.component';
import { RatingByJobseekersComponent } from './rating-by-jobseekers/rating-by-jobseekers.component';
import { AdvanceSearchComponent } from './advance-search/advance-search.component';
import { AdvanceProfileSearchComponent } from './advance-profile-search/advance-profile-search.component';
import { Routes, RouterModule } from '@angular/router';

@NgModule({
  declarations: [TodayWalkinsComponent, OtherLocationsComponent, OtherCategoryComponent, SkillWiseComponent, ConnetionJobpostsComponent, RatingByJobseekersComponent, AdvanceSearchComponent, AdvanceProfileSearchComponent],
  imports: [
    CommonModule,
    JobsRoutingModule
  ],
})
export class JobsModule { }
