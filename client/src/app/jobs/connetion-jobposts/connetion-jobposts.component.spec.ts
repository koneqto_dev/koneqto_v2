import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConnetionJobpostsComponent } from './connetion-jobposts.component';

describe('ConnetionJobpostsComponent', () => {
  let component: ConnetionJobpostsComponent;
  let fixture: ComponentFixture<ConnetionJobpostsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConnetionJobpostsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnetionJobpostsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
