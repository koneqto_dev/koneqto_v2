import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TodayWalkinsComponent } from './today-walkins/today-walkins.component';

const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class JobsRoutingModule { }
