import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RatingByJobseekersComponent } from './rating-by-jobseekers.component';

describe('RatingByJobseekersComponent', () => {
  let component: RatingByJobseekersComponent;
  let fixture: ComponentFixture<RatingByJobseekersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RatingByJobseekersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RatingByJobseekersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
