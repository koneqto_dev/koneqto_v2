import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SkillWiseComponent } from './skill-wise.component';

describe('SkillWiseComponent', () => {
  let component: SkillWiseComponent;
  let fixture: ComponentFixture<SkillWiseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SkillWiseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SkillWiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
