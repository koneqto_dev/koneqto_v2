import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TodayWalkinsComponent } from './today-walkins.component';

describe('TodayWalkinsComponent', () => {
  let component: TodayWalkinsComponent;
  let fixture: ComponentFixture<TodayWalkinsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TodayWalkinsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodayWalkinsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
