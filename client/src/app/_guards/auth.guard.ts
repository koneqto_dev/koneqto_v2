﻿import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { SimpleCrypt } from "ngx-simple-crypt";

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router) { }


    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        
        var simpleCrypt = new SimpleCrypt();
        var loginuserid1 = localStorage.getItem('loginuserid')        
        var roleId = simpleCrypt.decode("my-key", localStorage.getItem('RoleID'));
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        if (loginuserid) {
            // logged in so return true
            
            return true;
        }

        else if (eval(roleId) == 3) {
            return true;
        }
        else {
            // not logged in so redirect to login page with the return url
            this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
            return false;
        }

    }
}
@Injectable()
export class AuthGuardRole implements CanActivate {

    constructor(private router: Router) { }


    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        var simpleCrypt = new SimpleCrypt();
        var loginuserid1 = localStorage.getItem('loginuserid')       
        var roleId = simpleCrypt.decode("my-key", localStorage.getItem('RoleID'));
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);

        if (eval(roleId) == 5) {
            // logged in so return true
            return true;
        }
        else {


            // not logged in so redirect to login page with the return url
            this.router.navigate(['/404'], { queryParams: { returnUrl: state.url } });
            return false;
        }

    }
}