import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CharityRoutingModule } from './charity-routing.module';
import { CharityComponent } from './charity/charity.component';

@NgModule({
  declarations: [CharityComponent],
  imports: [
    CommonModule,
    CharityRoutingModule
  ]
})
export class CharityModule { }
