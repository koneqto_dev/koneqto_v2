import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime'


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { AuthenticationService } from './services';
import { FormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { NgIdleKeepaliveModule } from '@ng-idle/keepalive';
import { JobsModule } from './jobs/jobs.module';
import { LoginModule } from './login/login.module';
import { HeaderComponent } from './login/header/header.component';
//  import {BsModalService, BsModalRef , TabsModule } from 'ngx-bootstrap'


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ToastrModule.forRoot(),
    NgIdleKeepaliveModule.forRoot(), JobsModule, LoginModule
    // TabsModule.forRoot()
  ],

  providers: [
    // BsModalRef, BsModalService,
    // AuthServiceConfig,
    //AuthenticationService,AuthService
  ],
  exports: [HeaderComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
