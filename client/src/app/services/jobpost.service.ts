import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { userProfile, educationaldetails, jobposting, Skill } from '../_models/index';
import { Http, Headers, Response, URLSearchParams, RequestOptions } from '@angular/http';
import { User } from '../_models/index';
import { Observable } from 'rxjs/Rx';
import { Subject } from 'rxjs/Subject';
import { environment } from '../../environments/environment'
import { SimpleCrypt } from "ngx-simple-crypt";
import 'rxjs/add/operator/map';
// var loginuserid1=localStorage.getItem('c-user')
// var loginuserid=(atob(loginuserid1));
// var loginuserid=localStorage.getItem('loginuserid')

@Injectable()
export class JobpostService {

  public mixedRefresh = new Subject<any>();
  public adviceRefresh = new Subject<any>();
  public jobRefresh = new Subject<any>();
  public eventRefresh = new Subject<any>();
  public charityRefresh = new Subject<any>();
  public jobRefreshRec = new Subject<any>();

  
  profilevisibilityenable(loginuserid, value): Observable<any> {

    return this.http
      .post(
        this.BaseApiurl +
        "/jobposting/profilevisibilityenable/?AuthToken=" +
        localStorage.getItem('AuthToken'),
        {
          loginuserid: eval(loginuserid),
          value: value
        }
      )
      .map(this.extractData)
      .catch(this.handleError);
  }
  advicerefresh(advice) {
    this.adviceRefresh.next(advice);
  }
  mixedrefresh(mixed) {
    this.mixedRefresh.next(mixed);
  }
  jobrefresh(job) {
    this.jobRefresh.next(job);
  }
  jobrefreshrec(job) {
    this.jobRefreshRec.next(job);
  }
  eventrefresh(event) {
    this.eventRefresh.next(event);
  }
  charityrefresh(charity) {
    this.charityRefresh.next(charity);
  }

  BaseApiurl = environment.Apiurl;
  AuthToken = localStorage.getItem('AuthToken')
  constructor(private http: HttpClient) { }
  // dataArray: any[] = [];


  getexpertAdviceonly(): Observable<any> {
    var loginuserid1 = localStorage.getItem('loginuserid')
    var simpleCrypt = new SimpleCrypt();
    var loginuserid = simpleCrypt.decode("my-key", loginuserid1);;

    return this.http.post(this.BaseApiurl + '/jobposting/getexpertadviceonly/?AuthToken=' + localStorage.getItem('AuthToken'), {
      p_registration_id: eval(loginuserid),


    })

      .map(this.extractData)
      .catch(this.handleError);
  }
  // deleteexpertAdviceonly(id) {
  //   


  //   return this.http.post(this.BaseApiurl + '/jobposting/deleteexpertAdviceonly/?AuthToken=' + this.AuthToken,{
  //     Id:id,


  //   })

  //     .map(this.extractData)
  //     .catch(this.handleError);
  // }


  getallmixedposts(loginuserid: any, search: any): Observable<any> {

    return this.http.post(this.BaseApiurl + '/jobposting/getallmixedposts/?AuthToken=' + localStorage.getItem('AuthToken'),
      {
        loginuserid: eval(loginuserid),
        search: search
      })
      .map(this.extractData)
      .catch(this.handleError);
  }


  jobpost(jobpost: any, jobpostid): Observable<any> {
    var loginuserid1 = localStorage.getItem('loginuserid')
    var simpleCrypt = new SimpleCrypt();
    var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
    var username1 = localStorage.getItem('loginusername')
    var username = simpleCrypt.decode("my-key", username1);

    return this.http.post(this.BaseApiurl + '/jobposting/jobpost?AuthToken=' + localStorage.getItem('AuthToken'),
      {
        name:username,
        loginuserid: eval(loginuserid),
        jobtitle: jobpost.jobtitle,
        natureofjob: jobpost.natureofjob,
        companyname: jobpost.companyname,
        functionalarea: jobpost.functionalarea,
        educationdetails: jobpost.educationdetails,
        keyskills: jobpost.keyskills,
        optionalskills: jobpost.optionalkills,
        country: jobpost.country,
        location: jobpost.location,
        state: jobpost.state,
        fresher: jobpost.fresher,
        minexp: jobpost.minexp,
        maxexp: jobpost.maxexp,
        maxsalary: jobpost.maxsalary,

        minsalary: jobpost.minsalary,

        gender: jobpost.gender,
        noticeperiod: jobpost.noticeperiod,
        vacancy: jobpost.vacancy,
        startdate: jobpost.startdate,
        enddate: jobpost.enddate,
        phy_status: jobpost.phy_status,
        contactperson: jobpost.contactperson,
        contactno: jobpost.contactno,

        description: jobpost.description,
        certification: jobpost.certification,
        specialmention: jobpost.specialmention,
        venue: jobpost.venue,
        photo: jobpost.photo,
        video: jobpost.video,
        interviewdetails: jobpost.interviewprocedure,
        first_question: jobpost.first_question,
        second_question: jobpost.second_question,
        third_question: jobpost.third_question,
        fourth_question: jobpost.fourth_question,
        fifth_question: jobpost.fifth_question,
        jobpostid: jobpostid,
        currency: jobpost.curency,
        locality: jobpost.locality,
        salarytype:jobpost.salaryType
      })

      .map(this.extractData)
      .catch(this.handleError);

  }
  deleteexpertAdviceonly(id) : Observable<any>{



    return this.http.post(this.BaseApiurl + '/jobposting/deleteexpertAdviceonly/?AuthToken=' + localStorage.getItem('AuthToken'), {
      Id: id,


    })

      .map(this.extractData)
      .catch(this.handleError);
  }

  //var loginuserid1 = localStorage.getItem('loginuserid')
  editjobpost(jobpost: any, jobpostid) : Observable<any>{
    var loginuserid1 = localStorage.getItem('loginuserid')
    var simpleCrypt = new SimpleCrypt();
    var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
    return this.http.post(this.BaseApiurl + '/jobposting/updatejob?AuthToken=' + localStorage.getItem('AuthToken'),
      {
        loginuserid: eval(loginuserid),

        optional_skills: jobpost.optional_skills,
        maxsalary: jobpost.maxsalary,
        minsalary: jobpost.minsalary,
        gender: jobpost.gender,
        notice_period: jobpost.notice_period,
        enddate: jobpost.tilldate,
        phy_status: jobpost.physicalstatus,
        contactperson: jobpost.contactperson,
        // photo: jobpost.photo,
        // video: jobpost.video,
        jobpostid: jobpostid,


      })
      .map(this.extractData)
      .catch(this.handleError);

  }
  getphysicalstatus(): Observable<any> {


    return this.http.get(this.BaseApiurl + '/jobposting/getphysicalstatus' + '&AuthToken=' + localStorage.getItem('AuthToken'))
      .map(this.extractData)
      .catch(this.handleError);

  }



  getmyposts(loginuserid: any,jobidred:any,search: any,psize:any,plength:any): Observable<any> {
debugger
    return this.http.post(this.BaseApiurl + '/jobposting/getmyposts/?AuthToken=' + localStorage.getItem('AuthToken'),
      {
        loginuserid: eval(loginuserid),
        jobid:jobidred,
        search: search,
        pagesize:psize,
        plength:plength
      })
      .map(this.extractData)
      .catch(this.handleError);
  }

  // getmyposts(loginuserid, jobidred) {


  //   return this.http.get(this.BaseApiurl + '/jobposting/getmyposts/?loginuserid=' + eval(loginuserid) + '&jobid=' + jobidred + '&AuthToken=' + localStorage.getItem('AuthToken'),

  //   )
  //     .map(this.extractData)
  //     .catch(this.handleError);

  // }
  savecharity(postcharity: any): Observable<any> {
    var loginuserid1 = localStorage.getItem('loginuserid')
    var simpleCrypt = new SimpleCrypt();
    var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
    var username1 = localStorage.getItem('loginusername')
    var username = simpleCrypt.decode("my-key", username1);


    return this.http.post(this.BaseApiurl + '/jobposting/postcharity?AuthToken=' + localStorage.getItem('AuthToken'), {
      loginusername: username,
      charityname: postcharity.charityname,
      charitycategory: postcharity.charitycategory,
      address: postcharity.address,
      charityidproof: postcharity.charityidproof,
      mobilenumber: postcharity.mobilenumber,

      startdate: postcharity.charitystartdate,
      enddate: postcharity.charityenddate,
      charityfund: postcharity.charityfund,
      termsandconditions: postcharity.termsandconditions,
      charitydescription: postcharity.charitydescription,

      volunteersnames: postcharity.volunteersnames,
      volunteersmobile: postcharity.volunteersmobile,


      charitysubcategory: postcharity.charitysubcategory,
      account_number: postcharity.account_number,
      account_name: postcharity.account_name,
      account_type: postcharity.account_type,
      ifsc_code: postcharity.ifsc_code,
      firstname: postcharity.firstname,
      bankname: postcharity.bankname,
      supporting_image: postcharity.supporting_image,
      type:postcharity.image_type,

      loginuserid: eval(loginuserid)


    })
      .map(this.extractData)
      .catch(this.handleError);

  }
  loadcharitycategories(): Observable<any> {

    return this.http.get(this.BaseApiurl + '/jobposting/loadcharitycategories/?AuthToken=' + localStorage.getItem('AuthToken'))
      .map(this.extractData)
      .catch(this.handleError);
  }
  // /?AuthToken=' + localStorage.getItem('AuthToken')
  loadsubcharitydropdown(charity_category_id): Observable<any> {

    return this.http.get(this.BaseApiurl + '/jobposting/loadsubcharitydropdown/?charity_category_id=' + charity_category_id + '&AuthToken=' + localStorage.getItem('AuthToken'))
      .map(this.extractData)
      .catch(this.handleError);
  }
  getallcharityposts(): Observable<any> {

    return this.http.get(this.BaseApiurl + '/jobposting/getallcharityposts/?AuthToken=' + localStorage.getItem('AuthToken'))
      .map(this.extractData)
      .catch(this.handleError);
  }

  getmycharityposts(): Observable<any> {
    var loginuserid1 = localStorage.getItem('loginuserid')
    var simpleCrypt = new SimpleCrypt();
    var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
    return this.http.get(this.BaseApiurl + '/jobposting/getmycharityposts/?loginuserid=' + eval(loginuserid) + '&AuthToken=' + localStorage.getItem('AuthToken'))
      .map(this.extractData)
      .catch(this.handleError);
  }

  getallcharitypostsview(charity_id): Observable<any> {

    return this.http.get(this.BaseApiurl + '/jobposting/getallcharitypostsview/?charity_id=' + charity_id + '&AuthToken=' + localStorage.getItem('AuthToken'))
      .map(this.extractData)
      .catch(this.handleError);
  }

  getcharitydetails(): Observable<any> {
    var loginuserid1 = localStorage.getItem('loginuserid')
    var simpleCrypt = new SimpleCrypt();
    var loginuserid = simpleCrypt.decode("my-key", loginuserid1);

    return this.http.get(this.BaseApiurl + '/jobposting/getcharitydetails/?loginuserid=' + eval(loginuserid) + '&AuthToken=' + localStorage.getItem('AuthToken'))
      .map(this.extractData)
      .catch(this.handleError);
  }

  savedonaters(postdonate: any): Observable<any> {
    var loginuserid1 = localStorage.getItem('loginuserid')
    var simpleCrypt = new SimpleCrypt();
    var loginuserid = simpleCrypt.decode("my-key", loginuserid1);

    return this.http.post(this.BaseApiurl + '/jobposting/savedonaters?AuthToken=' + localStorage.getItem('AuthToken'),
      {
        loginuserid: eval(loginuserid),
        donatorsname: postdonate.donatorsname,
        donatorsmobilenumber: postdonate.donatorsmobilenumber,
        donatoremail: postdonate.donatoremail,
        donatoraddress: postdonate.donatoraddress,
        donationamount: postdonate.donationamount,
        frequentlydonation: postdonate.frequentlydonation,
        supportingfor: postdonate.supportingfor,
        comments: postdonate.comments
      })
      .map(this.extractData)
      .catch(this.handleError);

  }
  editcharity(charity: any): Observable<any> {
    var loginuserid1 = localStorage.getItem('loginuserid')
    var simpleCrypt = new SimpleCrypt();
    var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
    return this.http.post(this.BaseApiurl + '/jobposting/updatecharity/?AuthToken=' + localStorage.getItem('AuthToken'),
      {
        loginuserid: eval(loginuserid),
        firstname: charity.firstname,
        charity_name: charity.charity_name,
        charity_mobile_no: charity.charity_mobile_no,
        charity_help_start_date: charity.charity_help_start_date,
        charity_help_end_date: charity.charity_help_end_date,
        charity_address: charity.charity_address,
        volunteers_name: charity.volunteers_name,
        volunteers_mobile: charity.volunteers_mobile,
        account_name: charity.account_name,
        account_number: charity.account_number,
        account_type: charity.account_type,
        ifsc_code: charity.ifsc_code,
        charity_description: charity.charity_description,
        charity_id: charity.charity_id,
        bankname: charity.bankname



      })
      .map(this.extractData)
      .catch(this.handleError);

  }



  Deletecharity(charity_id: any): Observable<any> {
    var loginuserid1 = localStorage.getItem('loginuserid')
    var simpleCrypt = new SimpleCrypt();
    var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
    return this.http.post(this.BaseApiurl + '/jobposting/deletecharity?AuthToken=' + localStorage.getItem('AuthToken'), {
      loginuserid: eval(loginuserid),
      charity_id: charity_id
    }
    )
      .map(this.extractData)
      .catch(this.handleError);

  }



  Applyjob(job: any, answer): Observable<any> {
    var loginuserid1 = localStorage.getItem('loginuserid')
    var loginusername1 = localStorage.getItem('firstname')
    var simpleCrypt = new SimpleCrypt();
    var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
    var loginusername = simpleCrypt.decode("my-key", loginusername1);
    return this.http.post(this.BaseApiurl + '/jobposting/applyjob?AuthToken=' + localStorage.getItem('AuthToken'), {
      loginuserid: eval(loginuserid),
      jobid: job.posting_id,
      registration_id: job.registration_id, first: answer.firstanswer, second: answer.secondanswer, third: answer.thirdanswer, fourth: answer.fourthanswer, fifth: answer.fifthanswer,
      jobtitle: job.job_title,
      loginusername: loginusername,
      description: job.summary,
      mandatoryskills: job.skill
    }
    )
      .map(this.extractData)
      .catch(this.handleError);

  }



  Deletejob(jobid: any,jobimage:any,jobvideo:any) : Observable<any>{
    var loginuserid1 = localStorage.getItem('loginuserid')
    var simpleCrypt = new SimpleCrypt();
    var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
    return this.http.post(this.BaseApiurl + '/jobposting/deletejob?AuthToken=' + localStorage.getItem('AuthToken'), {
      loginuserid: eval(loginuserid),
      jobid: jobid,
      image:jobimage,
      video:jobvideo
    }
    )
      .map(this.extractData)
      .catch(this.handleError);

  }


  //jobs in other category
  getmypostingsinothrcat(p_registration_id: any, p_category: any): Observable<any> {

    return this.http.post(this.BaseApiurl + '/jobposting/getmypostingsinothrcat?AuthToken=' + localStorage.getItem('AuthToken'), { p_registration_id: p_registration_id, p_category: p_category })
      .map(this.extractData)
      .catch(this.handleError);
  }

  //getjobs othrlocation
  getmypostingsinothrloc(p_registration_id: any, p_location: any): Observable<any> {
    
    if (p_location == undefined) {
      p_location = 0;
    }

    return this.http.post(this.BaseApiurl + '/jobposting/getmypostingsinothrloc?AuthToken=' + localStorage.getItem('AuthToken'), { p_registration_id: p_registration_id, p_location: p_location })
      .map(this.extractData)
      .catch(this.handleError);
  }

  ///////////////////////////////////////////////  Events Services  ///////////////////////////////////////////////////////

  //posting events service
  saveevent(postevent: any): Observable<any> {
    var loginuserid1 = localStorage.getItem('loginuserid')
    var simpleCrypt = new SimpleCrypt();
    var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
    var username = localStorage.getItem('loginusername')
    var loginusername = simpleCrypt.decode("my-key", username);

    return this.http.post(this.BaseApiurl + '/jobposting/saveevent?AuthToken=' + localStorage.getItem('AuthToken'), {
      eventid: postevent.eventid,
      p_registration_id: eval(loginuserid),
      eventname: postevent.eventname,
      eventfunctionalarea: postevent.eventfunctionalarea,
      eventtype: postevent.eventtype,
      country: postevent.country,
      eventlocation: postevent.eventlocation,
      state: postevent.state,
      eventlocality: postevent.eventlocality,
      eventstartdate: postevent.eventstartdate,
      eventenddate: postevent.eventenddate,
      eventdescription: postevent.eventdescription,
      eventaddress: postevent.eventaddress,
      eventimage: postevent.eventimage,
      eimageextType: postevent.eimageextType,
      eventvideo: postevent.eventvideo,
      evideoextType: postevent.evideoextType,
      name:loginusername
    })
      .map(this.extractData)
      .catch(this.handleError);
  }

  //loadeventtypes
  loadeventtypes() : Observable<any>{

    return this.http.get(this.BaseApiurl + '/jobposting/loadeventtypes/?AuthToken=' + localStorage.getItem('AuthToken'))
      .map(this.extractData)
      .catch(this.handleError);
  }

  //loadcityinevents
  // loadcityinevents() {

  //   return this.http.get(this.BaseApiurl + '/jobposting/loadcityinevents/?AuthToken=' + localStorage.getItem('AuthToken'))
  //     .map(this.extractData)
  //     .catch(this.handleError);
  // }


  //loadeventlocalities
  // loadeventlocalities() {

  //   return this.http.get(this.BaseApiurl + '/jobposting/loadeventlocalities1/?AuthToken=' + localStorage.getItem('AuthToken'))
  //     .map(this.extractData)
  //     .catch(this.handleError);
  // }


  // getExpertAdvice(psize: any, plength: any) {
  //   var simpleCrypt = new SimpleCrypt();
  //   var loginuserid = simpleCrypt.decode("my-key", this.loginuserid1);
  //   
  //   return this.http.post(this.BaseApiurl + '/jobposting/getexpertadvice/?AuthToken=' + this.AuthToken, {
  //     p_registration_id: eval(loginuserid),
  //     pagesize: psize,
  //     pagelength: plength

  //   })

  //     .map(this.extractData)
  //     .catch(this.handleError);
  // }

  //Getting latest events
  allsavedevents(psize: any, plength: any) : Observable<any>{
    var loginuserid1 = localStorage.getItem('loginuserid')
    var simpleCrypt = new SimpleCrypt();
    var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
    return this.http.post(this.BaseApiurl + '/jobposting/allsavedevents/?AuthToken=' + localStorage.getItem('AuthToken'), {
      p_registration_id: eval(loginuserid),
      pagesize: psize,
      pagelength: plength

    })
      .map(this.extractData)
      .catch(this.handleError);
  }

  //interested in event click
  interestedinevent(allsavedeventslist): Observable<any> {
    
    var loginuserid1 = localStorage.getItem('loginuserid')
    var simpleCrypt = new SimpleCrypt();
    var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
    var p_registration_id = eval(loginuserid)
    var username = localStorage.getItem('firstname')
    var loginusername = simpleCrypt.decode("my-key", username);
    return this.http.post(this.BaseApiurl + '/jobposting/interestedinevent/?AuthToken=' + localStorage.getItem('AuthToken'), {
      registration_id: allsavedeventslist.registration_id,
      loginusername: loginusername,
      p_registration_id: p_registration_id,
      event_id: allsavedeventslist.event_id,
      event_image: allsavedeventslist.event_image,
      event_name: allsavedeventslist.event_name,
      event_description: allsavedeventslist.event_description,
      event_location: allsavedeventslist.event_location,
      event_start_date: allsavedeventslist.event_start_date,
      event_end_date: allsavedeventslist.event_end_date,
      event_type: allsavedeventslist.event_type,
      functional_area: allsavedeventslist.functional_area,
      event_address: allsavedeventslist.event_address,

      type: 'event'
    })
      .map(this.extractData)
      .catch(this.handleError);
  }



  //uninterested in event click
  uninterestedinevent(allsavedeventslist) : Observable<any>{
    var loginuserid1 = localStorage.getItem('loginuserid')
    var simpleCrypt = new SimpleCrypt();
    var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
    var p_registration_id = eval(loginuserid)
    return this.http.post(this.BaseApiurl + '/jobposting/uninterestedinevent/?AuthToken=' + localStorage.getItem('AuthToken'), {
      p_registration_id: p_registration_id,
      registration_id: allsavedeventslist.registration_id,
      event_id: allsavedeventslist.event_id,
      type: 'event',
      active: false
    })
      .map(this.extractData)
      .catch(this.handleError);
  }


  Report(id: any, type: any, registration_id: any, description: any):  Observable<any> {
    
    var loginuserid1 = localStorage.getItem('loginuserid')
    var simpleCrypt = new SimpleCrypt();
    var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
    var p_registration_id = eval(loginuserid)
    var username = localStorage.getItem('loginusername')
    var loginusername = simpleCrypt.decode("my-key", username);
    
    return this.http.post(this.BaseApiurl + '/jobposting/report/?AuthToken=' + localStorage.getItem('AuthToken'), {

      p_registration_id: registration_id,
      id: id,
      type: type,
      description: description,
      loginuserid: loginuserid
    })
      .map(this.extractData)
      .catch(this.handleError);
  }


  //Getting my events
  mysavedevents() : Observable<any>{

    var loginuserid1 = localStorage.getItem('loginuserid')
    var simpleCrypt = new SimpleCrypt();
    var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
    var p_registration_id = eval(loginuserid)

    return this.http.get(this.BaseApiurl + '/jobposting/mysavedevents/?p_registration_id=' + p_registration_id + '&AuthToken=' + localStorage.getItem('AuthToken'))
      .map(this.extractData)
      .catch(this.handleError);
  }

  //Deleting my event
  deletemyevent(event_id,eventimage,eventvideo): Observable<any> {

    return this.http.post(this.BaseApiurl + '/jobposting/deletemyevent/?AuthToken=' + localStorage.getItem('AuthToken'), { event_id: event_id ,eventimage:eventimage,eventvideo:eventvideo})
      .map(this.extractData)
      .catch(this.handleError);
  }

  //Getting interestedinevents
  getappliedevents(): Observable<any> {
    var loginuserid1 = localStorage.getItem('loginuserid')
    var simpleCrypt = new SimpleCrypt();
    var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
    var p_registration_id = eval(loginuserid)

    return this.http.get(this.BaseApiurl + '/jobposting/getappliedevents/?p_registration_id=' + p_registration_id + '&AuthToken=' + localStorage.getItem('AuthToken'))
      .map(this.extractData)
      .catch(this.handleError);
  }

  //Getting interested users for my events
  getmyeventappliedusers(event_id): Observable<any> {

    var p_type_id = event_id;

    return this.http.get(this.BaseApiurl + '/jobposting/getmyeventappliedusers/?p_type_id=' + p_type_id + '&AuthToken=' + localStorage.getItem('AuthToken'))
      .map(this.extractData)
      .catch(this.handleError);
  }



  homesavedevents(): Observable<any> {
    var loginuserid1 = localStorage.getItem('loginuserid')
    var simpleCrypt = new SimpleCrypt();
    var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
    var p_registration_id = eval(loginuserid)
    var idtosend = 0;

    return this.http.get(this.BaseApiurl + '/jobposting/savedevents/?p_registration_id=' + idtosend + '&AuthToken=' + localStorage.getItem('AuthToken'))
      .map(this.extractData)
      .catch(this.handleError);
  }




  getappliedjobs(jobid) : Observable<any>{
    var loginuserid1 = localStorage.getItem('loginuserid')
    var simpleCrypt = new SimpleCrypt();
    var loginuserid = simpleCrypt.decode("my-key", loginuserid1);

    return this.http.get(this.BaseApiurl + '/jobposting/getApplyjob/?loginuserid=' + eval(loginuserid) + '&jobid=' + jobid + '&AuthToken=' + localStorage.getItem('AuthToken'))
      .map(this.extractData)
      .catch(this.handleError);

  }
  getjobapplicantstatus(jobid, registration_id): Observable<any> {
    var loginuserid1 = localStorage.getItem('loginuserid')
    var simpleCrypt = new SimpleCrypt();
    var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
    return this.http.get(this.BaseApiurl + '/jobposting/getjobapplicantstatus/?loginuserid=' + loginuserid + '&jobid=' + jobid + '&registration_id=' + eval(registration_id) + '&AuthToken=' + localStorage.getItem('AuthToken'))
      .map(this.extractData)
      .catch(this.handleError);
  }


  geteventsearch(eventsearch): Observable<any> {
    var loginuserid1 = localStorage.getItem('loginuserid')
    var simpleCrypt = new SimpleCrypt();
    var loginuserid = simpleCrypt.decode("my-key", loginuserid1);

    var p_registration_id = eval(loginuserid)

    return this.http.get(this.BaseApiurl + '/jobposting/geteventsearch/?p_skills=' + eventsearch.p_skills + '&p_location=' + eventsearch.p_location + '&p_event_date=' + eventsearch.p_event_date + '&AuthToken=' + localStorage.getItem('AuthToken'))
      .map(this.extractData)
      .catch(this.handleError);
  }



  split_array

searchjob(loginuserid: any,searchskills:any, psize: any, plength: any): Observable<any> {

this.split_array=[];
if (searchskills) {
  this.split_array = searchskills.split(',')
}

if (this.split_array) {
  if (this.split_array[0] == 'undefined' ||this.split_array[0] == ''  ) {
    this.split_array[0] = null;
  }
  if (this.split_array[1] == 'undefined' || this.split_array[1] == '') {
    this.split_array[1] = null;
  }
  if (this.split_array[2] == 'undefined' || this.split_array[2] == '' ) {
    this.split_array[2] = null;
  }
  if (this.split_array[3] == 'undefined' || this.split_array[1] == '') {
    this.split_array[3] = null;
  }
}

    return this.http.post(this.BaseApiurl + '/jobposting/searchid/?AuthToken=' + localStorage.getItem('AuthToken'), {
      p_registration_id: loginuserid,skill_name01: this.split_array[0], skill_name02: this.split_array[1],skill_name03: this.split_array[2],skill_name04: this.split_array[3],pagesize: psize, pagelength: plength
    })
      .map(this.extractData)
      .catch(this.handleError);
  }

  // job search by id for mobile api
  jobsearchid_android(p_posting_id: any): Observable<any> {
    return this.http.post(this.BaseApiurl + '/jobposting/searchandroidid/?AuthToken=' + localStorage.getItem('AuthToken'), {
      p_posting_id: p_posting_id
    })
        .map(this.extractData)
        .catch(this.handleError);
    }

  // jobsearchid(loginuserid: any, p_jobid: any) {
  //   return this.http.post(this.BaseApiurl + '/jobposting/searchid/?AuthToken=' + localStorage.getItem('AuthToken'), {
  //     p_registration_id: loginuserid,
  //     p_jobid: p_jobid
  //   })
  //     .map(this.extractData)
  //     .catch(this.handleError);
  // }




  getjobappliedusers(jobid, type): Observable<any> {
    var loginuserid1 = localStorage.getItem('loginuserid')
    var simpleCrypt = new SimpleCrypt();
    var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
    return this.http.post(this.BaseApiurl + '/jobposting/getjobappliedusers/?AuthToken=' + localStorage.getItem('AuthToken'), {
      p_registration_id: eval(loginuserid),
      jobid: jobid, type: type
    })
      .map(this.extractData)
      .catch(this.handleError);
  }

  //Expert advice posting.....


  postAdvice(expadviceall): Observable<any> {
    var loginuserid2 = localStorage.getItem('loginuserid')
    var simpleCrypt = new SimpleCrypt();
    var loginuserid = simpleCrypt.decode("my-key", loginuserid2);

    return this.http.post(this.BaseApiurl + '/jobposting/advice/?AuthToken=' + localStorage.getItem('AuthToken'), {
      p_registration_id: eval(loginuserid),
      advice_options: expadviceall.advice_options,
      functional_area: expadviceall.functional_area,
      advice_message: expadviceall.advice_message
    })
      .map(this.extractData)
      .catch(this.handleError);
  }


  updateAdvice(expadviceall) : Observable<any>{


    return this.http.post(this.BaseApiurl + '/jobposting/updateadvice/?AuthToken=' + localStorage.getItem('AuthToken'), {

      advice_options: expadviceall.advice_options,
      functional_area: expadviceall.functional_area,
      advice_message: expadviceall.advice_message,
      id: expadviceall.id
    })
      .map(this.extractData)
      .catch(this.handleError);
  }



  //Get Expert Advice.......
  getExpertAdvice(psize: any, plength: any): Observable<any> {
    var loginuserid1 = localStorage.getItem('loginuserid')
    var simpleCrypt = new SimpleCrypt();
    var loginuserid = simpleCrypt.decode("my-key", loginuserid1);;

    return this.http.post(this.BaseApiurl + '/jobposting/getexpertadvice/?AuthToken=' + localStorage.getItem('AuthToken'), {
      p_registration_id: eval(loginuserid),
      pagesize: psize,
      pagelength: plength

    })

      .map(this.extractData)
      .catch(this.handleError);
  }




  //////////////////////get applied

  getApplied(jobid): Observable<any> {


    return this.http.get(this.BaseApiurl + '/jobposting/getapplied/?jobid=' + jobid + '&AuthToken=' + localStorage.getItem('AuthToken'))
      .map(this.extractData)
      .catch(this.handleError)

  }
  //short listing
  shorlisted(jobid, element, email_id, first_name, job_title,mobile_no) : Observable<any>{

    return this.http.post(this.BaseApiurl + '/jobposting/shortlist/?AuthToken=' + localStorage.getItem('AuthToken'), {
      jobid: jobid,
      element: element,
      email: email_id,
      name: first_name,
      jobtitle: job_title,
      mobile_no:mobile_no
    })
  }

  //get shortlisted


  shortlistedList(jobid): Observable<any> {


    return this.http.get(this.BaseApiurl + '/jobposting/getshortlist/?jobid=' + jobid + '&AuthToken=' + localStorage.getItem('AuthToken'))
      .map(this.extractData)
      .catch(this.handleError)

  }
  scheduledList(jobid): Observable<any> {


    return this.http.get(this.BaseApiurl + '/jobposting/scheduledlist/?jobid=' + jobid + '&AuthToken=' + localStorage.getItem('AuthToken'))
      .map(this.extractData)
      .catch(this.handleError)

  }
  progressed(jobid) : Observable<any>{


    return this.http.get(this.BaseApiurl + '/jobposting/progressed/?jobid=' + jobid + '&AuthToken=' + localStorage.getItem('AuthToken'))
      .map(this.extractData)
      .catch(this.handleError)

  }
  changeStatus(selected_user, jobid, jobtitle, status,mobile_no,name): Observable<any> {
    
    return this.http.post(this.BaseApiurl + '/jobposting/changestatus/?AuthToken=' + localStorage.getItem('AuthToken'),
     {
        user: selected_user,
         jobid: jobid, jobtitle: jobtitle,
          status: status ,
          mobile_no:mobile_no,
          name:name,
          title:jobtitle

        })


      .map(this.extractData)
      .catch(this.handleError);


  }

  nextRoundPost(interview, schedule_user, schedulejobid, schedulejobtitle,mobile_no,first_name): Observable<any> {
    var loginuserid1 = localStorage.getItem('loginuserid')
    var simpleCrypt = new SimpleCrypt();
    var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
    return this.http.post(this.BaseApiurl + '/jobposting/nextround/?AuthToken=' + localStorage.getItem('AuthToken'), {
      title: schedulejobtitle,
      schedule_date: interview.scheduledate2,
      schedule_time: interview.scheduletime2,
      interviewer: interview.interviewer,
      type: interview.type,
      location: interview.location,
      mode: interview.mode,
      notify_me: interview.notify_me,
      created_by: eval(loginuserid),
      schedule_user: schedule_user,
      schedulejobid: schedulejobid,
      status: interview.status,
      mobile_no:mobile_no,
      name:first_name

    })


      .map(this.extractData)
      .catch(this.handleError);


  }



  schedulepost(interview, schedule_user, schedulejobid, schedulejobtitle,mobile_no,name): Observable<any> {
    
    var loginuserid1 = localStorage.getItem('loginuserid')
    var simpleCrypt = new SimpleCrypt();
    var loginuserid = simpleCrypt.decode("my-key", loginuserid1);

    return this.http.post(this.BaseApiurl + '/jobposting/schedule/?AuthToken=' + localStorage.getItem('AuthToken'), {
      title: schedulejobtitle,
      schedule_date: interview.scheduledate1,
      schedule_time: interview.scheduletime1,
      interviewer: interview.interviewer,
      type: interview.type,
      location: interview.location,
      mode: interview.mode,
      notify_me: interview.notify_me,
      created_by: eval(loginuserid),
      schedule_user: schedule_user,
      schedulejobid: schedulejobid,
      mobile_no:mobile_no,
      name:name
    })


      .map(this.extractData)
      .catch(this.handleError);


  }
  getcity(country_id): Observable<any> {
    return this.http.get(this.BaseApiurl + '/jobposting/city/?country_id=' + country_id + '&AuthToken=' + localStorage.getItem('AuthToken'))
      .map(this.extractData)
      .catch(this.handleError);
  }



  getexpyear(): Observable<any> {
    return this.http.get(this.BaseApiurl + '/profile/loadexpyears/?AuthToken=' + localStorage.getItem('AuthToken'))
      .map(this.extractData)
      .catch(this.handleError);

  }

  natureofjob(): Observable<any> {

    return this.http.get(this.BaseApiurl + '/jobposting/natureofjob/?AuthToken=' + localStorage.getItem('AuthToken'))
      .map(this.extractData)
      .catch(this.handleError);

  }
  getfunctinalareadata() : Observable<any>{
    return this.http.get(this.BaseApiurl + '/profile/getfunctinalareadata/?AuthToken=' + localStorage.getItem('AuthToken'))
      .map(this.extractData)
      .catch(this.handleError);



  }
  loadcountrydropdown(): Observable<any> {

    return this.http.get(this.BaseApiurl + '/profile/loadcountrydropdown/?AuthToken=' + localStorage.getItem('AuthToken'))
      .map(this.extractData)
      .catch(this.handleError);
  }
  loadnoticeperiod(): Observable<any> {


    return this.http.get(this.BaseApiurl + '/profile/loadnoticeperiod/?AuthToken=' + localStorage.getItem('AuthToken'))
      .map(this.extractData)
      .catch(this.handleError);
  }
  getallcourses() : Observable<any>{
    return this.http.get(this.BaseApiurl + '/profile/allcourses/?AuthToken=' + localStorage.getItem('AuthToken'))
      .map(this.extractData)
      .catch(this.handleError);
  }

  getallcourses1(coursename): Observable<any> {
    return this.http.get(this.BaseApiurl + '/profile/allcourses1/?coursename=' + coursename + '&AuthToken=' + localStorage.getItem('AuthToken'))
      .map(this.extractData)
      .catch(this.handleError);
  }

  getSkills1(fn_area_id): Observable<any> {
    
    return this.http.get(this.BaseApiurl + '/profile/getskillnames1/?AuthToken=' + localStorage.getItem('AuthToken') + '&fn_area_id=' + fn_area_id)
      .map(this.extractData)
      .catch(this.handleError);
  }

  getSkills2(fn_area_id, name): Observable<any> {
    
    return this.http.get(this.BaseApiurl + '/profile/getskillnames2/?AuthToken=' + localStorage.getItem('AuthToken') + '&fn_area_id=' + fn_area_id + '&name=' + name)
      .map(this.extractData)
      .catch(this.handleError);
  }

  getStatebyid(country_id): Observable<any> {
    return this.http.get(this.BaseApiurl + '/jobposting/state/?country_id=' + country_id + '&AuthToken=' + localStorage.getItem('AuthToken'))
      .map(this.extractData)
      .catch(this.handleError);
  }

  getcitybyid(state_id): Observable<any> {
    return this.http.get(this.BaseApiurl + '/jobposting/city/?state_id=' + state_id + '&AuthToken=' + localStorage.getItem('AuthToken'))
      .map(this.extractData)
      .catch(this.handleError);
  }

  getcitybyid1(state_id, name): Observable<any> {
    return this.http.get(this.BaseApiurl + '/jobposting/city1/?state_id=' + state_id + '&name=' + name + '&AuthToken=' + localStorage.getItem('AuthToken'))
      .map(this.extractData)
      .catch(this.handleError);
  }

  getLocalitybyid1(city_id): Observable<any> {
    return this.http.get(this.BaseApiurl + '/jobposting/loadeventlocalities1/?city_id=' + city_id + '&AuthToken=' + localStorage.getItem('AuthToken'))
      .map(this.extractData)
      .catch(this.handleError);
  }

  getLocalitybyid2(city_id, name) : Observable<any>{
    return this.http.get(this.BaseApiurl + '/jobposting/loadeventlocalities2/?city_id=' + city_id + '&name=' + name + '&AuthToken=' + localStorage.getItem('AuthToken'))
      .map(this.extractData)
      .catch(this.handleError);
  }

  // getall cities by stateid for  mobile api
  getcitybyidformobileapi(state_id) : Observable<any>{
    return this.http.get(this.BaseApiurl + '/jobposting/mobileapicity/?state_id=' + state_id + '&AuthToken=' + localStorage.getItem('AuthToken'))
      .map(this.extractData)
      .catch(this.handleError);
  }
  
  // getall cities for  mobile api
  getallcitiesformobileapi(): Observable<any> {
    return this.http.get(this.BaseApiurl + '/jobposting/getallcitiesformobileapi/?AuthToken=' + localStorage.getItem('AuthToken'))
      .map(this.extractData)
      .catch(this.handleError);
  }
// getall localities for  mobile api
  getLocalitybyidformobileapi(city_id): Observable<any> {
    return this.http.get(this.BaseApiurl + '/jobposting/mobileapilocalities/?city_id=' + city_id + '&AuthToken=' + localStorage.getItem('AuthToken'))
      .map(this.extractData)
      .catch(this.handleError);
  }
// getall skills for  mobile api
 getallskillsformobileapi(fn_area_id): Observable<any> {
    
    return this.http.get(this.BaseApiurl + '/jobposting/mobileapiskills/?AuthToken=' + localStorage.getItem('AuthToken') + '&fn_area_id=' + fn_area_id)
      .map(this.extractData)
      .catch(this.handleError);
  }
 

  getanswer(user_registration_id, posting_id): Observable<any> {

    return this.http.get(this.BaseApiurl + '/jobposting/getanswer/?p_registration_id=' + user_registration_id + '&posting_id=' + posting_id + '&AuthToken=' + localStorage.getItem('AuthToken'))
      .map(this.extractData)
      .catch(this.handleError);
  }


  getVideoResume(user_registration_id): Observable<any> {

    return this.http.get(this.BaseApiurl + '/profile/getvideoresume/?loginuserid=' + user_registration_id + '&AuthToken=' + localStorage.getItem('AuthToken'))
      .map(this.extractData)
      .catch(this.handleError);
  }
  checkApplicants(jobid, type): Observable<any> {
    return this.http.get(this.BaseApiurl + '/jobposting/checkapplicants/?jobid=' + jobid + '&type=' + type + '&AuthToken=' + localStorage.getItem('AuthToken'))
      .map(this.extractData)
      .catch(this.handleError);
  }
  loadsalarytype(jobtype): Observable<any>
  {
      return this.http.get(this.BaseApiurl + '/jobposting/loadsalarytype/?jobtype=' + jobtype  + '&AuthToken=' + localStorage.getItem('AuthToken') )
      .map(this.extractData)
      .catch(this.handleError);
  }



  checkReport(posting_id:number,registration_id:number,loginuserid:number): Observable<any>
  {
    
    return this.http.get(this.BaseApiurl + '/jobposting/checkreport/?jobid=' + posting_id + '&registration_id='+ registration_id + '&loginuserid=' + loginuserid + '&AuthToken=' + localStorage.getItem('AuthToken'))
    .map(this.extractData)
    .catch(this.handleError);
  }



  private extractData(res: Response) {

    let body = res;
    //  this.dataArray = body || [];

    return body || [];
  }

  //handleError
  private handleError(error: any) {

    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return Observable.throw(errMsg);
  }


}

