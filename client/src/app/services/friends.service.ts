import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { userProfile, educationaldetails, jobposting } from '../_models/index';
import { Http, Headers, Response, URLSearchParams, RequestOptions } from '@angular/http';
import { User } from '../_models/index';
import { Observable } from 'rxjs/Rx';
import { Subject } from 'rxjs/Subject';
import { environment } from '../../environments/environment'
import { SimpleCrypt } from "ngx-simple-crypt";
// var loginuserid1=localStorage.getItem('c-user')
// var loginuserid=(atob(loginuserid1));
// var loginuserid=localStorage.getItem('loginuserid')
// var loginuserid=localStorage.getItem('loginuserid')

@Injectable()
export class FriendService {
  public connectRefresh = new Subject<any>();
  public connectRefresh1= new Subject<any>();
  public connectRefresh2= new Subject<any>();
  BaseApiurl = environment.Apiurl
  constructor(private http: HttpClient) { }

  AuthToken = localStorage.getItem('AuthToken')
  //var loginuserid1 = localStorage.getItem('loginuserid')


  connectionrefresh1()
  {
    this.connectRefresh1.next();
  } 
  connectionrefresh2()
  {
    this.connectRefresh2.next();
  }

    // loadconnections

    loadconnections(search: any): Observable<any> {  
     
      return this.http.post(this.BaseApiurl + '/friendservice/loadconnections/?AuthToken=' + localStorage.getItem('AuthToken'), {
        search:search
      })
        .map(this.extractData)
        .catch(this.handleError);
    }
  //viewProfilefriend
  viewProfilefriend(user_reg_id): Observable<any> {
    return this.http.get(this.BaseApiurl + '/friendservice/viewProfilefriend/?user_reg_id=' + user_reg_id + '&AuthToken=' + localStorage.getItem('AuthToken'))
      .map(this.extractData)
      .catch(this.handleError);

  }
  connectrefresh()
  {
    this.connectRefresh.next();
  }
  //loadfindconnections
  loadfindconnections(): Observable<any> {  
    var loginuserid1 = localStorage.getItem('loginuserid')
    var simpleCrypt = new SimpleCrypt();
    var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
    var p_registration_id = eval(loginuserid)
    
    return this.http.get(this.BaseApiurl + '/friendservice/loadfindconnections/?p_registration_id=' + p_registration_id + '&AuthToken=' + localStorage.getItem('AuthToken'))
      .map(this.extractData)
      .catch(this.handleError);
  }

// /loadallfriends
loadallfriends(): Observable<any> {
  
  var loginuserid1 = localStorage.getItem('loginuserid')
  var simpleCrypt = new SimpleCrypt();
  var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
  var p_registration_id = eval(loginuserid)
  
  return this.http.get(this.BaseApiurl + '/friendservice/loadfrnds/?p_registration_id=' + p_registration_id + '&AuthToken=' + localStorage.getItem('AuthToken'))
    .map(this.extractData)
    .catch(this.handleError);
}
  //loadsuggestedfriends  
  loadsuggestedfrnds(): Observable<any> {
    
    var loginuserid1 = localStorage.getItem('loginuserid')
    var simpleCrypt = new SimpleCrypt();
    var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
    var p_registration_id = eval(loginuserid)
    
    return this.http.get(this.BaseApiurl + '/friendservice/loadsuggestedfrnds/?p_registration_id=' + p_registration_id + '&AuthToken=' + localStorage.getItem('AuthToken'))
      .map(this.extractData)
      .catch(this.handleError);
  }
  //loadnotification
  loadnotification(): Observable<any> {
    
    var loginuserid1 = localStorage.getItem('loginuserid')
    var simpleCrypt = new SimpleCrypt();
    var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
    var p_registration_id = eval(loginuserid)
   
    return this.http.get(this.BaseApiurl + '/friendservice/loadreqnotif/?p_registration_id=' + p_registration_id + '&AuthToken=' + localStorage.getItem('AuthToken'))
      .map(this.extractData)
      .catch(this.handleError);
  }
  //loadreqfriends
  loadreqfriends() : Observable<any>{
    
    
    var loginuserid1 = localStorage.getItem('loginuserid')
    var simpleCrypt = new SimpleCrypt();
    var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
    var p_registration_id = eval(loginuserid)
   
    return this.http.get(this.BaseApiurl + '/friendservice/loadreqfriends/?p_registration_id=' + p_registration_id + '&AuthToken=' + localStorage.getItem('AuthToken'))
      .map(this.extractData)
      .catch(this.handleError);
  }
  // acceptFriend request
  friendrequeststatus(requestsent): Observable<any> {
    
    var loginuserid1 = localStorage.getItem('loginuserid')
    //p_request_to,p_request_from,p_is_accepted,p_is_rejected
    var simpleCrypt = new SimpleCrypt();
    var loginuserid = simpleCrypt.decode("my-key",loginuserid1);
    var loginusername1 = localStorage.getItem('firstname')
    var loginusername = simpleCrypt.decode("my-key", loginusername1);
    requestsent.p_request_from = requestsent.user_registration_id;
    requestsent.p_request_to = eval(loginuserid);
    return this.http.post(this.BaseApiurl + '/friendservice/friendrequeststatus?AuthToken=' + localStorage.getItem('AuthToken'), {
      p_request_to: requestsent.p_request_to,
      p_request_from: requestsent.p_request_from,
      p_is_accepted: requestsent.p_is_accepted,
      p_is_rejected: requestsent.p_is_rejected,
      loginusername: loginusername,


    })

      .map(this.extractData)
      .catch(this.handleError);
  }
  // declineFriend request
  friendrequeststatus1(requestsent): Observable<any> {
    
    var loginuserid1 = localStorage.getItem('loginuserid')
    //p_request_to,p_request_from,p_is_accepted,p_is_rejected
    var simpleCrypt = new SimpleCrypt();
    var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
    var loginusername1 = localStorage.getItem('firstname')
    var loginusername = simpleCrypt.decode("my-key", loginusername1);
    requestsent.p_request_from = requestsent.user_registration_id;
    requestsent.p_request_to = eval(loginuserid);
    return this.http.post(this.BaseApiurl + '/friendservice/friendrequeststatus1?AuthToken=' + localStorage.getItem('AuthToken'), {
      p_request_to: requestsent.p_request_to,
      p_request_from: requestsent.p_request_from,
      p_is_accepted: requestsent.p_is_accepted,
      p_is_rejected: requestsent.p_is_rejected,
      loginusername: loginusername,


    })

      .map(this.extractData)
      .catch(this.handleError);
  }
  //sendfriendrequest
  sendfriendrequest(request):Observable<any> {
    
    
    var loginuserid1 = localStorage.getItem('loginuserid')
    var simpleCrypt = new SimpleCrypt();
    var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
    request.p_request_to = request.user_registration_id;
    request.p_request_from = eval(loginuserid);


    return this.http.post(this.BaseApiurl + '/friendservice/sendfriendrequest?AuthToken=' + localStorage.getItem('AuthToken'), {
      p_request_to: request.p_request_to,
      p_request_from: request.p_request_from,


    })

      .map(this.extractData)
      .catch(this.handleError);

  }
  //sendfriendrequest for recuiter
  sendfriendreq1(req: any): Observable<any> {
    return this.http.post(this.BaseApiurl + '/friendservice/sendfriendrequest?AuthToken=' + localStorage.getItem('AuthToken'),
      {
        p_request_to: req.p_request_to,
        p_request_from: req.p_request_from,
        AuthToken: localStorage.getItem('AuthToken')
      })
      .map(this.extractData)
      .catch(this.handleError);

  }
  //loadaccepted friends
  loadacceptedfriends(): Observable<any> {
    
    var loginuserid1 = localStorage.getItem('loginuserid')
    var simpleCrypt = new SimpleCrypt();
    var loginuserid = simpleCrypt.decode("my-key",loginuserid1);
    var p_registration_id = eval(loginuserid)
    
    return this.http.get(this.BaseApiurl + '/friendservice/loadacceptedfriends/?p_registration_id=' + p_registration_id + '&AuthToken=' + localStorage.getItem('AuthToken'))
      .map(this.extractData)
      .catch(this.handleError);
  }
  //invite mail
  invitemail(invite: any):Observable<any> {
    var simpleCrypt = new SimpleCrypt();
    var userId = localStorage.getItem('loginuserid')
    var loginuserid = simpleCrypt.decode("my-key", userId);
    var usernamedecrypted = localStorage.getItem('firstname')
    var username = simpleCrypt.decode("my-key", usernamedecrypted);
    
    return this.http.post<any>(this.BaseApiurl + '/friendservice/invitemail?AuthToken=' + localStorage.getItem('AuthToken'),
      {
        loginuserid: eval(loginuserid),
        loginusername: username,
        name: invite.name,
        mail: invite.mail,
        mobile: invite.mobile
      })
      .map(this.extractData)
      .catch(this.handleError);

  }
  //getfriendsjobposts
  getfriendjobposts(psize, plength, loginuserid): Observable<any> {

    
    return this.http.post<any>(this.BaseApiurl + '/friendservice/getfriendjobposts?AuthToken=' + localStorage.getItem('AuthToken'),
      {
        psize: psize, plength: plength, loginuserid: loginuserid

      })
      .map(this.extractData)
      .catch(this.handleError);

  }
  //friendsearch
  friendsearch(job_title: any): Observable<any> {
    var loginuserid1 = localStorage.getItem('loginuserid')
    var simpleCrypt = new SimpleCrypt();
    var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
    
  
    return this.http.post(this.BaseApiurl + '/friendservice/friendsearch?AuthToken='+localStorage.getItem('AuthToken'),
     {
       job_title: job_title,
       loginuserid: eval(loginuserid),
      })
      .map(this.extractData)
      .catch(this.handleError);
  }
  private extractData(res: Response) {
    
    let body = res;
    return body || [];
  }

  //handleError
  private handleError(error: any) {
    
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}