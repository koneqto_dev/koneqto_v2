import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
//import { userProfile, educationaldetails, jobposting} from '../_models/index';
import { User } from '../_models/index';
import { Observable } from 'rxjs/Rx';
import { Subject } from 'rxjs/Subject';
import { environment } from '../../environments/environment';
import { SimpleCrypt } from "ngx-simple-crypt";
// var loginuserid1=localStorage.getItem('c-user')
// var loginuserid=(atob(loginuserid1));
// var loginuserid=localStorage.getItem('loginuserid')

@Injectable()
export class ratingService {
  //var loginuserid1 = localStorage.getItem('loginuserid')
  AuthToken = localStorage.getItem('AuthToken')

  constructor(private http: HttpClient) { }
  BaseApiurl = environment.Apiurl
  loadcompanylist() : Observable<any>{
    var loginuserid1 = localStorage.getItem('loginuserid')
    var simpleCrypt = new SimpleCrypt();
    var loginuserid = simpleCrypt.decode("my-key", loginuserid1);

    var p_registration_id = eval(loginuserid)
   
    return this.http.get(this.BaseApiurl + '/rating/loadcompanylist/?p_registration_id=' + p_registration_id + '&AuthToken=' + localStorage.getItem('AuthToken'))
      .map(this.extractData)
      .catch(this.handleError);
  }
  loadcandidatelist() : Observable<any>{
    
    var loginuserid1 = localStorage.getItem('loginuserid')
    var simpleCrypt = new SimpleCrypt();
    var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
    var p_registration_id = eval(loginuserid)
  
    return this.http.get(this.BaseApiurl + '/rating/loadcandidatelist/?p_registration_id=' + p_registration_id + '&AuthToken=' + localStorage.getItem('AuthToken'))
      .map(this.extractData)
      .catch(this.handleError);
  }
  //user_registration_id,company_registration_id,aspect1,aspect2,aspect3,aspect4,aspect5,description
  saverating(crate) : Observable<any>{
    
    return this.http.post(this.BaseApiurl + '/rating/saverating?AuthToken=' + localStorage.getItem('AuthToken'), {
      user_registration_id: crate.user_registration_id,
      company_registration_id: crate.company_registration_id,
      aspect1: crate.aspect1,
      aspect2: crate.aspect2,
      aspect3: crate.aspect3,
      aspect4: crate.aspect4,
      aspect5: crate.aspect5,
      description: crate.description
    })
      .map(this.extractData)
      .catch(this.handleError);


  }
  saveuserrating(rating): Observable<any> {
    var loginuserid1 = localStorage.getItem('loginuserid')
    var simpleCrypt = new SimpleCrypt();
    var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
    
    return this.http.post(this.BaseApiurl + '/rating/saveuserrating/?AuthToken=' + localStorage.getItem('AuthToken'),
      {
        tech_rating: rating.tech_rating,
        info_rating: rating.info_rating,
        behaviour_rating: rating.behaviour_rating,
        etiquette_rating: rating.etiquette_rating,
        communication_rating: rating.communication_rating,
        expectations: rating.expectations,
        overall_exp: rating.overallexp,
        user: rating.user,
        jobid: rating.jobid,
        recruiter_id: eval(loginuserid)
      })
      .map(this.extractData)
      .catch(this.handleError);


  }

  getuserrating(posting_id,applieduser_id): Observable<any> {
    var loginuserid1 = localStorage.getItem('loginuserid')
    var simpleCrypt = new SimpleCrypt();
    var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
    console.log(applieduser_id,"applieduser_id sevice")
    return this.http.post(this.BaseApiurl + '/rating/getuserrating/?AuthToken=' + localStorage.getItem('AuthToken'),
      {
        posting_id: posting_id,
        loginuserid: applieduser_id
      })
      .map(this.extractData)
      .catch(this.handleError);


  }

  recruiterRating(infrarec,hostrec,timerec,arrangerec,overall_exprec,infracomp,hostcomp,timecomp,arrangecomp,overall_expcomp,jobid,posted_user_id,loginuserid,rating_for,id,r_created_date,c_created_date,p_rated_for_comp): Observable<any> {
    return this.http.post(this.BaseApiurl + '/rating/recruiterrating/?AuthToken=' + localStorage.getItem('AuthToken'),
      {
        infrastructurerec:infrarec,hostpitalityrec:hostrec,waiting_timerec:timerec,arrangementrec:arrangerec,overall_exprec:overall_exprec,jobid:jobid,recruiter_id:posted_user_id,
        loginuserid:loginuserid,rating_for:rating_for,id:id,r_created_date:r_created_date,c_created_date:c_created_date,p_rated_for_comp:p_rated_for_comp,
        infracomp:infracomp,hostcomp:hostcomp,timecomp:timecomp,arrangecomp:arrangecomp,overall_expcomp:overall_expcomp
      })
      .map(this.extractData)
      .catch(this.handleError);


  }
  
  loadallreviews(job): Observable<any> {
    
   
    return this.http.post(this.BaseApiurl + '/rating/loadallreviews/?AuthToken=' + localStorage.getItem('AuthToken'),
      {
        jobid:job.jobid,loginuserid:job.loginuserid
        
      })
      .map(this.extractData)
      .catch(this.handleError);


  }
  l
  private extractData(res: Response) {
    
    let body = res;

    return body || [];
  }

  //handleError
  private handleError(error: any) {
    
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
 
}

