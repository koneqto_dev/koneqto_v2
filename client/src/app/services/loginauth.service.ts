
import 'rxjs/add/operator/map'
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { userProfile, educationaldetails, jobposting } from '../_models/index';
import { Http, Headers, Response, URLSearchParams, RequestOptions } from '@angular/http';
import { User } from '../_models/index';
import { Observable } from 'rxjs/Rx';
import { Subject } from 'rxjs/Subject';
import { environment } from '../../environments/environment'
import { SimpleCrypt } from "ngx-simple-crypt";



@Injectable()
export class LoginAuthService {

  BaseApiurl = environment.Apiurl;
  constructor(private http: HttpClient) { }


  // this.http.get('https://jsonip.com/').subscribe(data => {
  //   });
  //Get IP Adress using http://freegeoip.net/json/?callback  --> https://api.ipify.org?format=json -->http://api.ipify.org/?format=jsonp&callback=JSONP_CALLBACK
  getIpAddress(): Observable<any> {
    
    return this.http.get('https://api.ipify.org?format=json')
      .map(this.extractData)
      .catch(this.handleError);
  }

  saveIpAddress(UserIp): Observable<any> {

    let simpleCrypt = new SimpleCrypt();
    // Loginuserid         
    var loginuserid1 = localStorage.getItem('loginuserid')
    var loginuserid = simpleCrypt.decode("my-key", loginuserid1);;
    return this.http.post(this.BaseApiurl + '/loginauth/saveIp', { UserIp: UserIp, loginuserid: loginuserid })
      .map(this.extractData)
      .catch(this.handleError);

  }







  //User Registration
  create(user: User):Observable<any> {
    
    var mo =  user.mobile;
    //http://localhost:5000/api/authenticate
    // (this.BaseApiurl+'/api/authenticate', { username: username, password: password })
    // userfname:user.firstName,userlname:user.lastName,usermobile:user.mobile,useremail:user.email,useruname:user.username,userpwd:user.password});
    var a = user.email;
    var b = user.firstName;
    var c = user.id;
    var d = user.lastName;
    var e = user.mobile;
    var f = user.password;
    var g = user.username;
    var h = user.country;
    var i = user.state;
    var j = user.city;
    var k = user.functional_area;
    var l = user.dob;
    var m = user.gender;
    var n = user.currentworkingstatus;
   
    return this.http.post(this.BaseApiurl + '/loginauth/newregistration/', {
      userfname: user.firstName, userlname: user.lastName, usermobile: mo, useremail: user.email, useruname: user.username, userpwd: user.password, roleid: user.roleid,
       country: user.country, state: user.state, city: user.city, functional_area: user.functional_area, id: user.id, dob: user.dob, gender: user.gender, 
       currentworkingstatus: user.currentworkingstatus
    })
      .map(this.extractData)
      .catch(this.handleError);

  }
  //Employee Registration
  employeeresgistration(employeemodel: any):Observable<any> {
    
    var mo = employeemodel.cphonenumber;
    return this.http.post(this.BaseApiurl + '/loginauth/empregistration/', {
      userfname: employeemodel.companyname, useruname: employeemodel.hrname, usermobile: mo, useremail: employeemodel.Companymailid, company_websie: employeemodel.Companywebsite, userpwd: employeemodel.empPassword, roleid: employeemodel.roleid,name:employeemodel.firstName, hr_designation: employeemodel.Designation, country: employeemodel.country1, state: employeemodel.state1, city: employeemodel.city1, gender: employeemodel.gender, functional_area: employeemodel.functional_area1
    })
      .map(this.extractData)
      .catch(this.handleError);

  }

  //verify email
  emailverify(id) : Observable<any>{
    return this.http.post(this.BaseApiurl + '/loginauth/verifyemail', { id: id })
      .map(this.extractData)
      .catch(this.handleError);
  }
  //sendotp
  sendotp(phonenumber, CountryCode): Observable<any> {
    


    return this.http.get('http://2factor.in/API/V1/96ab36da-f79d-11e8-a895-0200cd936042/SMS/' + CountryCode + phonenumber + '/AUTOGEN')
      // return this.http.post(this.BaseApiurl + '/api/mobileotp',{phonenumber:phonenumber})
      .map(this.extractData)
      .catch(this.handleError);
  }
  //verifyotp
  varifyotp(otp, mobilenumber, CountryCode): Observable<any> {


    return this.http.get('https://2factor.in/API/V1/96ab36da-f79d-11e8-a895-0200cd936042/SMS/VERIFY-2/' + CountryCode + mobilenumber + '/' + otp)
      .map(this.extractData)
      .catch(this.handleError);

  }
  sendotp1(phonenumber, CountryCode):Observable<any> {
    
  

    return this.http.post(this.BaseApiurl + '/loginauth/sendotp/', { CountryCode: CountryCode, phonenumber: phonenumber })
      // return this.http.post(this.BaseApiurl + '/api/mobileotp',{phonenumber:phonenumber})
      .map(this.extractData)
      .catch(this.handleError);
  }

  // varifyotp2(otp, mobile, CountryCode) {


  //   return this.http.post(this.BaseApiurl+'/loginauth/verifyotp/', { CountryCode:CountryCode,mobilenumber:mobile ,otp:otp})
  //     .map(this.extractData)
  //     .catch(this.handleError);

  // }


  varifyotp2(otp, mobile, CountryCode, sessionId):Observable<any>{
    

    return this.http.get(this.BaseApiurl + '/loginauth/verifyotp/?CountryCode=' + CountryCode + '&mobilenumber=' + mobile + '&otp=' + otp + '&sessionId=' + sessionId)
      .map(this.extractData)
      .catch(this.handleError);


  }

  varifyotp5(otp, mobilenumber, CountryCode): Observable<any> {
    return this.http.get('https://2factor.in/API/V1/96ab36da-f79d-11e8-a895-0200cd936042/SMS/VERIFY-2/' + CountryCode + mobilenumber + '/' + otp)
      .map(this.extractData)
      .catch(this.handleError);
  }






  //checksocialid
  checksocialid(socialid, email): Observable<any> {

    return this.http.post<any>(this.BaseApiurl + '/loginauth/checksocialid', { socialid: socialid, email: email })
      .map(user => {

        if (user.message != 'no socialid') {
          let simpleCrypt = new SimpleCrypt();
          // Loginuserid         
          var userId = JSON.stringify(user.user_id).toString();
          let encodedId = simpleCrypt.encode("my-key", userId);
          localStorage.setItem('loginuserid', encodedId);
          //AuthToken    
          var AuthToken = user.AuthToken;
          localStorage.setItem('AuthToken', AuthToken);
          //loginusername 
          var userName = JSON.stringify(user.username);
          let encodedName = simpleCrypt.encode("my-key", userName);
          localStorage.setItem('loginusername', encodedName);
          //Roleid            
          var RoleId = JSON.stringify(user.roleid).toString();
          let encodedrId = simpleCrypt.encode("my-key", RoleId);
          localStorage.setItem('RoleID', encodedrId);
          localStorage.setItem('currentUser', JSON.stringify(user));
        }
        return user;

      })

  }




  loadcountrydropdown1(): Observable<any> {

    return this.http.get(this.BaseApiurl + '/loginauth/loadcountrydropdown')
      .map(this.extractData)
      .catch(this.handleError);
  }
  loadstatesdropdown(country_id) : Observable<any>{

    return this.http.get(this.BaseApiurl + '/loginauth/loadstatesdropdown/?country_id=' + country_id)
      .map(this.extractData)
      .catch(this.handleError);
  }
  loadcitydropdown(state_id) : Observable<any>{

    return this.http.get(this.BaseApiurl + '/loginauth/loadcitydropdown/?state_id=' + state_id)
      .map(this.extractData)
      .catch(this.handleError);
  }
  loadgender(): Observable<any> {

    return this.http.get(this.BaseApiurl + '/loginauth/loadgender/')
      .map(this.extractData)
      .catch(this.handleError);
  }
  loadfunctinalarea(): Observable<any> {

    return this.http.get(this.BaseApiurl + '/loginauth/loadfunctinalareadata')
      .map(this.extractData)
      .catch(this.handleError);
  }
  getphonebyid(country_id): Observable<any> {

    return this.http.get(this.BaseApiurl + '/loginauth/phonecode/?country_id=' + country_id)
      .map(this.extractData)
      .catch(this.handleError);
  }
  emailCheck(email):Observable<any> {


    return this.http.get(this.BaseApiurl + '/loginauth/email/?email=' + email)
      .map(this.extractData)
      .catch(this.handleError);
  }
  userNameChecking(userNameCheck):Observable<any> {

    return this.http.get(this.BaseApiurl + '/loginauth/userNameCheck/?username=' + userNameCheck)
      .map(this.extractData)
      .catch(this.handleError);
  }
  checkmobile_no(mobile): Observable<any> {

    return this.http.get(this.BaseApiurl + '/loginauth/checkmobile/?mobile=' + mobile)
      .map(this.extractData)
      .catch(this.handleError);
  }

  sendmail(email: any):Observable<any> {

    return this.http.post<any>(this.BaseApiurl + '/loginauth/sendemail', { email: email })
      .map(this.extractData)
      .catch(this.handleError);
  }
  savenewpassowrd(model: any, id: any): Observable<any> {

    return this.http.post<any>(this.BaseApiurl + '/loginauth/Savenewpassword', { pass: model.password, id: id })
      .map(this.extractData)
      .catch(this.handleError);
  }

  //extractData
  private extractData(res: Response) {

    let body = res;
    return body || [];
  }
  //handleError
  private handleError(error: any) {

    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}