import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { userProfile, educationaldetails, jobposting } from '../_models/index';
import { User } from '../_models/index';
import { Observable } from 'rxjs/Rx';
import { Subject } from 'rxjs/Subject';
import { environment } from '../../environments/environment'
import { SimpleCrypt } from "ngx-simple-crypt";

@Injectable()
export class walkinService {

  constructor(private http: HttpClient) { }
  BaseApiurl = environment.Apiurl;

  AuthToken = localStorage.getItem('AuthToken')


  loadwalkins() : Observable<any>{

    //var p_registration_id=localStorage.getItem('loginuserid')

    return this.http.get(this.BaseApiurl + '/walkin/loadwalkins/?AuthToken=' + localStorage.getItem('AuthToken'))
      .map(this.extractData)
      .catch(this.handleError);
  }
  sendmypost(sendjob): Observable<any> {
    debugger
    return this.http.post(this.BaseApiurl + '/walkin/sendmypost/?AuthToken=' + localStorage.getItem('AuthToken'), {
       postedjobid: sendjob.postedjobid,
        recruiter_id: sendjob.recruiter_id,
        recruiter_name:sendjob.recruiter_name,
         profile_id: sendjob.profile_id,
         send_job_description:sendjob.sendjobdescription })
      .map(this.extractData)
      .catch(this.handleError);
  }



  searchwalkin(walkin) : Observable<any>{

    if (walkin.value == 0) {
      walkin.value = undefined;
    }
    if (walkin.skill_name == 'undefined') {
      walkin.skill_name = '';
    }
    if (walkin.functionalarea == 'undefined') {
      walkin.functionalarea = '';
    }
    return this.http.post(this.BaseApiurl + '/walkin/walkinsearch/?AuthToken=' + localStorage.getItem('AuthToken'), { skill_name: walkin.skill_name, location: walkin.value, functionalarea: walkin.functionalarea })
      .map(this.extractData)
      .catch(this.handleError);
  }


  loadadvancejobs(): Observable<any> {

    var loginuserid1 = localStorage.getItem('loginuserid')
    var simpleCrypt = new SimpleCrypt();
    var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
    return this.http.get(this.BaseApiurl + '/walkin/loadadvancejobs/?loginuserid=' + loginuserid + '&AuthToken=' + localStorage.getItem('AuthToken'))
      .map(this.extractData)
      .catch(this.handleError);

  }



  // viewProfile(p_user_registration_id) {

  //   return this.http.get(this.BaseApiurl + '/walkin/profileoverview/?p_user_registration_id=' + p_user_registration_id + '&AuthToken=' + localStorage.getItem('AuthToken'))
  //     .map(this.extractData)
  //     .catch(this.handleError);

  // }


  split_array
  advanceJobsearch(opr) : Observable<any>{
    this.split_array = []

    var loginuserid1 = localStorage.getItem('loginuserid')
    var simpleCrypt = new SimpleCrypt();
    var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
    if (opr.skill_name) {
      this.split_array = opr.skill_name.split(',')
    }


    if (opr.firstname == 'undefined') {
      opr.firstname = '';
    }

    // if (opr.skill_name == 'undefined') {
    //   opr.skill_name = '';
    // }
    if (this.split_array) {
      if (this.split_array[0] == 'undefined' || this.split_array[0] == '') {
        this.split_array[0] = null;
      }
      if (this.split_array[1] == 'undefined' || this.split_array[1] == '') {
        this.split_array[1] = null;
      }
      if (this.split_array[2] == 'undefined' || this.split_array[2] == '') {
        this.split_array[2] = null;
      }
      if (this.split_array[3] == 'undefined' || this.split_array[1] == '') {
        this.split_array[3] = null;
      }
    }
    if (opr.skill_name1 == 'undefined') {
      opr.skill_name1 = '';
    }

    if (opr.current_location == 'undefined') {
      opr.current_location = '';
    }
    if (opr.total_exp_in_yrs == 'undefined') {
      opr.total_exp_in_yrs = '';
    }
    if (opr.gender == 'undefined') {
      opr.gender = '';
    }
    if (opr.preferred_location == 'undefined') {
      opr.preferred_location = '';
    }
    if (opr.marital_status == 'undefined') {
      opr.marital_status = '';
    }
    if (opr.employment_type == 'undefined') {
      opr.employment_type = '';
    }
    if (opr.notice_period == 'undefined') {
      opr.notice_period = '';
    }
    if (opr.qualification == 'undefined') {
      opr.qualification = '';
    }
    if (opr.type_of_interview == 'undefined') {
      opr.type_of_interview = '';
    }
    if (opr.year == 'undefined') {
      opr.year = '';
    }
    if (opr.locality == 'undefined') {
      opr.locality = '';
    }
    if (opr.physical_status == 'undefined') {
      opr.physical_status = '';
    }

    return this.http.post(this.BaseApiurl + '/walkin/advancejobsearch/?loginuserid=' + loginuserid + '&AuthToken=' + localStorage.getItem('AuthToken'),
      {
        firstname: opr.firstname, skill_name01: this.split_array[0], skill_name02: this.split_array[1], skill_name03: this.split_array[2], skill_name04: this.split_array[3], skill_name1: opr.skill_name1, current_location: opr.current_location, total_exp_in_yrs: opr.total_exp_in_yrs, gender: opr.gender,
        preferred_location: opr.preferred_location, marital_status: opr.marital_status, employment_type: opr.employment_type, notice_period: opr.notice_period, qualification: opr.qualification, type_of_interview: opr.type_of_interview, year: opr.year, physical_status: opr.physical_status, locality: opr.locality
      })

      .map(this.extractData)
      .catch(this.handleError);


  }
  // advanceJobsearch(opr) {
  //   var loginuserid1 = localStorage.getItem('loginuserid')
  //   var simpleCrypt = new SimpleCrypt();
  //   var loginuserid = simpleCrypt.decode("my-key", loginuserid1);

  //   if (opr.firstname == 'undefined') {
  //     opr.firstname = '';
  //   }

  //   if (opr.skill_name == 'undefined') {
  //     opr.skill_name = '';
  //   }

  //   if (opr.skill_name1 == 'undefined') {
  //     opr.skill_name1 = '';
  //   }

  //   if (opr.current_location == 'undefined') {
  //     opr.current_location = '';
  //   }
  //   if (opr.total_exp_in_yrs == 'undefined') {
  //     opr.total_exp_in_yrs = '';
  //   }
  //   if (opr.gender == 'undefined') {
  //     opr.gender = '';
  //   }
  //   if (opr.preferred_location == 'undefined') {
  //     opr.preferred_location = '';
  //   }
  //   if (opr.marital_status == 'undefined') {
  //     opr.marital_status = '';
  //   }
  //   if (opr.employment_type == 'undefined') {
  //     opr.employment_type = '';
  //   }
  //   if (opr.notice_period == 'undefined') {
  //     opr.notice_period = '';
  //   }
  //   if (opr.qualification == 'undefined') {
  //     opr.qualification = '';
  //   }
  //   if (opr.type_of_interview == 'undefined') {
  //     opr.type_of_interview = '';
  //   }
  //   if (opr.year == 'undefined') {
  //     opr.year = '';
  //   }
  //   if (opr.locality == 'undefined') {
  //     opr.locality = '';
  //   }
  //   if (opr.physical_status == 'undefined') {
  //     opr.physical_status = '';
  //   }
  //   return this.http.post(this.BaseApiurl + '/walkin/advancejobsearch/?loginuserid=' + loginuserid+ '&AuthToken=' + localStorage.getItem('AuthToken'),
  //     {
  //       firstname: opr.firstname, skill_name: opr.skill_name,skill_name1:opr.skill_name1, current_location: opr.current_location, total_exp_in_yrs: opr.total_exp_in_yrs, gender: opr.gender,
  //       preferred_location: opr.preferred_location, marital_status: opr.marital_status, employment_type: opr.employment_type, notice_period: opr.notice_period, qualification: opr.qualification, type_of_interview: opr.type_of_interview, year: opr.year, physical_status: opr.physical_status,locality:opr.locality
  //     })
  //     .map(this.extractData)
  //     .catch(this.handleError);
  // }



  loadjobsinotherloc() : Observable<any>{

    // var p_registration_id=localStorage.getItem('loginuserid')
    var p_registration_id = localStorage.getItem('loginuserid')
    var simpleCrypt = new SimpleCrypt();
    var loginuserid = simpleCrypt.decode("my-key", p_registration_id);

    return this.http.get(this.BaseApiurl + '/walkin/loadjobsiAnotherloc/?p_user_registration_id=' + eval(loginuserid) + '&AuthToken=' + localStorage.getItem('AuthToken'))
      .map(this.extractData)
      .catch(this.handleError);
  }

  oprsearch(opr) : Observable<any>{

    // var p_registration_id=localStorage.getItem('loginuserid');
    var p_registration_id = localStorage.getItem('loginuserid')
    var simpleCrypt = new SimpleCrypt();
    var loginuserid = simpleCrypt.decode("my-key", eval(loginuserid));

    return this.http.post(this.BaseApiurl + '/walkin/oprsearch?AuthToken=' + localStorage.getItem('AuthToken'), { p_user_registration_id: p_registration_id, skills: opr.skillcompany, location: opr.location, experience: opr.experience })
      .map(this.extractData)
      .catch(this.handleError);
  }

  advanceprofileSearch(mailid: any, phone: any): Observable<any> {

    var loginuserid1 = localStorage.getItem('loginuserid')
    var simpleCrypt = new SimpleCrypt();
    var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
    return this.http.post(this.BaseApiurl + '/walkin/advanceprofileSearch/?loginuserid=' + loginuserid + '&AuthToken=' + localStorage.getItem('AuthToken'), { mailid: mailid, phone: phone })
      .map(this.extractData)
      .catch(this.handleError);
  }



  private extractData(res: Response) {

    let body = res;

    return body || [];
  }

  //handleError
  private handleError(error: any) {

    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return Observable.throw(errMsg);
  }


}