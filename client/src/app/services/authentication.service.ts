﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders,HttpClientModule } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { config } from '../_models/index';
import 'rxjs/add/operator/map'
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subject } from 'rxjs/Subject';
import { environment } from '../../environments/environment';
import { SimpleCrypt } from "ngx-simple-crypt";

@Injectable()
export class AuthenticationService {
    constructor(private http: HttpClient) { }
    AuthToken = localStorage.getItem('AuthToken')

    private messageSource = new BehaviorSubject(1);
    currentMessage = this.messageSource.asObservable();

    BaseApiurl = environment.Apiurl;

    changeMessage(message: any) {

        this.messageSource.next(message)
    }


    private notify = new Subject<any>();
    /**
     * Observable string streams
     */
    notifyObservable$ = this.notify.asObservable();



    public notifyOther(data: any) {
        if (data) {
            this.notify.next(data);
        }
    }
    login(username: string, password: string): Observable<any> {


        return this.http.post<any>(this.BaseApiurl + '/loginauth/authenticate/', { username: username, password: password })
            .map(user => {
                // login successful if there's a jwt token in the response

                if (user.message == 'User already exist') {


                    let simpleCrypt = new SimpleCrypt();
                    var serverdata = JSON.parse(user.data);
                    var metadata = serverdata[0];
                    // var user_id=metadata.user_id
                    //var sessionUserID=(btoa(user_id))
                    //  localStorage.setItem('c-user',sessionUserID)
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
           
                    let encodedName = simpleCrypt.encode("my-key", metadata.username);
                    if(metadata.company_name==null){                  
                    let firstname=simpleCrypt.encode("my-key", metadata.first_name);
                    let lastname=simpleCrypt.encode("my-key", metadata.last_name);
                    user.first_name=metadata.first_name
                    user.last_name= metadata.last_name
                    localStorage.setItem('firstname', firstname);
                    localStorage.setItem('lastname', lastname);
                    }
                    if(metadata.company_name){
                        
                    let companyname=simpleCrypt.encode("my-key", metadata.company_name);
                    user.company_name=metadata.company_name
                    localStorage.setItem('companyname', companyname);

                   
                    let firstname=simpleCrypt.encode("my-key", metadata.first_name);
                    user.first_name=metadata.first_name
                    localStorage.setItem('firstname',firstname);
                    }
                    let decodedUsername = simpleCrypt.decode("my-key", encodedName);

                    var UserId = metadata.user_id.toString();
                    let encodedId = simpleCrypt.encode("my-key", UserId);
                    var rollId = metadata.role_id.toString();
                    let encodedRollId = simpleCrypt.encode("my-key", rollId);
                    let decodedRollID = simpleCrypt.decode("my-key", encodedRollId);
                    
                    localStorage.setItem('RoleID', encodedRollId);
                    localStorage.setItem('loginuserid', encodedId);
                    localStorage.setItem('loginusername', encodedName);
                    
                    localStorage.setItem('currentUser', JSON.stringify(user));
                   
                    localStorage.setItem('AuthToken', user.AuthToken);

               
                    
                    // localStorage.setItem('RoleID', metadata.role_id);
                }

                return user;
            });
    }


    sociallogin(firstname: string, lastname: string, email: string, apikey: string, photourl: any) : Observable<any>{

        return this.http.post<any>(this.BaseApiurl + 'api/sociallogin', { firstname: firstname, lastname: lastname, apikey: apikey, email: email, photourl: photourl })
            .map(user => {
                // login successful if there's a jwt token in the response
                if (user) {

                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(user));
                }

                return user;
            });

    }
    searchjob(p_skills: any, p_location: any, p_experience: any, p_max_salary: any): Observable<any> {

        return this.http.post<any>(this.BaseApiurl + '/api/searchjob?AuthToken=' + localStorage.getItem('AuthToken'), { p_skills: p_skills, p_location: p_location, p_experience: p_experience, p_max_salary: p_max_salary })
            .map(this.extractData)
            .catch(this.handleError);
    }

    logout() {
        // remove user from local storage to log user out\
        //this.authService.signOut();
        localStorage.removeItem('currentUser');
        // localStorage.removeItem('loginuserid');
        localStorage.removeItem('RoleID')
        localStorage.removeItem('loginuserid')
        localStorage.removeItem('loginusername')
        localStorage.clear();
        localStorage.clear();

    }
    private extractData(res: Response) {

        let body = res;
        //  this.dataArray = body || [];

        return body || [];
    }
    //handleError
    private handleError(error: any) {

        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg);
        return Observable.throw(errMsg);
    }
}