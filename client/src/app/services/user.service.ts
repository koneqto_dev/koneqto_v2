﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { userProfile, educationaldetails } from '../_models/index';
import { Http, Headers, Response, URLSearchParams, RequestOptions } from '@angular/http';
import { User, config } from '../_models/index';
import { Observable } from 'rxjs/Rx';
import { Subject } from 'rxjs/Subject';
import { Options } from 'selenium-webdriver/firefox';
import { environment } from '../../environments/environment';
import { SimpleCrypt } from "ngx-simple-crypt";
// var loginuserid1=localStorage.getItem('c-user')
//var loginuserid=(atob(loginuserid1));
//var loginuserid=localStorage.getItem('loginuserid')

//export var loginuserid=decrypted decodedUserID

@Injectable()
export class UserService {
    //  simpleCrypt = new SimpleCrypt();

    constructor(private http: Http
    ) { }
    dataArray: any[] = [];
    simpleCrypt = new SimpleCrypt();
    AuthToken = localStorage.getItem('AuthToken');
    BaseApiurl = environment.Apiurl
    //  username=localStorage.getItem('loginusername')
    //  loginusername = this.simpleCrypt.decode("my-key",this.username);
    //var loginuserid1 = localStorage.getItem('loginuserid')
    // loginuserid1
    //  simpleCrypt = new SimpleCrypt();
    // loginuserid=localStorage.getItem('loginuserid')
    // loginuserid = this.simpleCrypt.decode("my-key",this.decodedUserID);
    getById(id: number) : Observable<any>{
        return this.http.get(this.BaseApiurl + '/api/users/' + id + '&AuthToken=' + localStorage.getItem('AuthToken'));
    }

    // loginuserid(userId){
    //     this.loginuserid1=userId;
    // }

    // create(user: User) {

    //     //http://localhost:5000/api/authenticate
    //     // (this.BaseApiurl+'/api/authenticate', { username: username, password: password })
    //     // userfname:user.firstName,userlname:user.lastName,usermobile:user.mobile,useremail:user.email,useruname:user.username,userpwd:user.password});
    //     var a = user.email;
    //     var b = user.firstName;
    //     var c = user.id;
    //     var d = user.lastName;
    //     var e = user.mobile;
    //     var f = user.password;
    //     var g = user.username;
    //     var h = user.country;
    //     var i = user.state;
    //     var j = user.city;
    //     var k = user.functional_area;
    //     var l = user.dob;
    //     var m = user.gender;
    //     var n = user.currentworkingstatus;



    //     return this.http.post(this.BaseApiurl + '/api/newregistration', { userfname: user.firstName, userlname: user.lastName, usermobile: user.mobile, useremail: user.email, useruname: user.username, userpwd: user.password, roleid: user.roleid, country: user.country, state: user.state, city: user.city, functional_area: user.functional_area,id:user.id, dob: user.dob, gender: user.gender, currentworkingstatus: user.currentworkingstatus })

    //         .map(this.extractData)
    //         .catch(this.handleError);

    // }
    onlinestatus() : Observable<any>{
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        // var a = model.password;

        return this.http.post(this.BaseApiurl + '/profile/onlinestatus/?AuthToken=' + localStorage.getItem('AuthToken'), { loginuserid: eval(loginuserid) })
            .map(this.extractData)
            .catch(this.handleError)
    }
    orgdetails(org: any): Observable<any> {
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);


        return this.http.post(this.BaseApiurl + '/profile/postCompanyDetails?AuthToken=' + localStorage.getItem('AuthToken'), {
            loginuserid: eval(loginuserid),
            current_company: org.current_company,
            overview_company: org.overview_company,
            established_year: org.established_year,
           
            type_company: org.type_company,
            key_persons: org.ceo_directors_cfo,
            noof_employees: org.number_ofemployees,
            financial_info: org.financial_info,
            office_address: org.office_address,
            locations_any: org.locations_otherplaces,
            cmmi_level: org.cmmi_level,
            prestigious_projects: org.prestigious_projects,
            awards: org.awards_recognitions,
            certifications: org.cetifications,
            clients: org.client,
            participations: org.any_participations_globally
        })

    }
    displaycompanydetails(): Observable<any> {
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);


        return this.http.get(this.BaseApiurl + '/profile/displaycompanydetails/?loginuserid=' + eval(loginuserid) + '&AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);
    }
    //profile services
    saveprofile(userProfile: any) : Observable<any>{
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);


        return this.http.post(this.BaseApiurl + '/api/ProfileSanpshot/?AuthToken=' + localStorage.getItem('AuthToken'), { loginuserid: eval(loginuserid), Resume_Headline: userProfile.resume_headline, current_Location: userProfile.current_location, Current_Company: userProfile.current_company, current_designation: userProfile.current_designation, Preferred_Location: userProfile.preferred_location, Total_Experience: userProfile.total_experience, Annual_Salary: userProfile.annual_salary, Industry: userProfile.industry, Functional_Area: userProfile.functional_area, Role: userProfile.role, Date_of_Birth: userProfile.date_of_birth, Gender: userProfile.gender, Phone: userProfile.phone, Email: userProfile.email, Permanent_Address: userProfile.permanent_address, Hometown: userProfile.hometown, Pin_Code: userProfile.pin_code, Marital_Status: userProfile.marital_status, Key_Skills: userProfile.key_skills })
            .map(this.extractData)
            .catch(this.handleError);
    }
    //socialusercheck
    socialuserCheck(id): Observable<any> {
        return this.http.get(this.BaseApiurl + '/profile/socialusercheck/?id=' + id + '&AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);
    }
    // //verify email
    // emailverify(id) {
    //     return this.http.post(this.BaseApiurl + '/profile/verifyemail?AuthToken=' + this.AuthToken, { id: id })
    //         .map(this.extractData)
    //         .catch(this.handleError);
    // }



    getregistartiondetails(loginuserid): Observable<any> {


        // let options = new RequestOptions({ headers: headers });
        return this.http.get(this.BaseApiurl + '/profile/getregistartiondetails/?loginuserid=' + loginuserid + '&AuthToken=' + localStorage.getItem('AuthToken'))

            .map(this.extractData)
            .catch(this.handleError);
    }



    changePassword(model: any) : Observable<any>{
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        // var a = model.password;

        return this.http.post(this.BaseApiurl + '/profile/changepassword/?AuthToken=' + localStorage.getItem('AuthToken'), { loginuserid: eval(loginuserid), oldpassword: model.oldpassword, password: model.password })
            .map(this.extractData)
            .catch(this.handleError);
    }


    // sendotp(phonenumber,CountryCode) {
    //            
    //  

    //     return this.http.get('http://2factor.in/API/V1/ff7e30e8-d069-11e8-a895-0200cd936042/SMS/'+CountryCode+phonenumber+'/AUTOGEN' )
    //     // return this.http.post(this.BaseApiurl + '/api/mobileotp',{phonenumber:phonenumber})
    //         .map(this.extractData)
    //         .catch(this.handleError);
    // }

    // varifyotp(otp,mobilenumber,CountryCode) {
    //     
    //    

    //     return this.http.get('https://2factor.in/API/V1/ff7e30e8-d069-11e8-a895-0200cd936042/SMS/VERIFY3/'+CountryCode+mobilenumber+'/'+otp )
    //     .map(this.extractData)
    //     .catch(this.handleError);

    // }

    checksocialid(socialid, email_id): Observable<any> {


        return this.http.get(this.BaseApiurl + '/api/checksocialid/?socialid=' + socialid + '&email_id=' + email_id)
            .map(this.extractData)
            .catch(this.handleError);
    }

    getphonecode() : Observable<any>{
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);

        return this.http.get(this.BaseApiurl + '/profile/getphonecode/?loginuserid=' + eval(loginuserid) + '&AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);
    }

    getinterviewmode(): Observable<any> {

        return this.http.get(this.BaseApiurl + '/profile/getinterviewmode/?AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);

    }

    getinterviewtype(): Observable<any> {

        return this.http.get(this.BaseApiurl + '/profile/getinterviewtype/?AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);

    }

    getfunctinalareadata() : Observable<any>{


        return this.http.get(this.BaseApiurl + '/profile/getfunctinalareadata/?AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);
    }
    getprofiledetails(loginuserid) : Observable<any>{
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var viewer_id = simpleCrypt.decode("my-key", loginuserid1);

        // let options = new RequestOptions({ headers: headers });
        return this.http.get(this.BaseApiurl + '/profile/getprofiledetails/?loginuserid=' + loginuserid + '&AuthToken=' + localStorage.getItem('AuthToken') + '&viewer_id=' + viewer_id)

            .map(this.extractData)
            .catch(this.handleError);
    }

    getprofiledetails1(userid) : Observable<any>{

        var loginuserid1 = localStorage.getItem('loginuserid')
        var loginusername = localStorage.getItem('loginusername')
        var simpleCrypt = new SimpleCrypt();
        var viewer_id = simpleCrypt.decode("my-key", loginuserid1);
        var loginusername = simpleCrypt.decode("my-key", loginusername);

        return this.http.get(this.BaseApiurl + '/profile/getprofiledetails/?loginuserid=' + userid + '&AuthToken=' + localStorage.getItem('AuthToken') + '&viewer_id=' + viewer_id + '&loginusername=' + loginusername)
            .map(this.extractData)
            .catch(this.handleError);
    }


    // getoldemail() {
    //     var simpleCrypt = new SimpleCrypt();       
    //     var loginuserid = simpleCrypt.decode("my-key",this.loginuserid1);

    //     return this.http.get(this.BaseApiurl + '/profile/getOldemail/?loginuserid=' + loginuserid)
    //         .map(this.extractData)
    //         .catch(this.handleError);
    // }

    getCurrentMobile() : Observable<any>{
        var simpleCrypt = new SimpleCrypt();
        var loginuserid2 = localStorage.getItem('loginuserid')
        var loginuserid = simpleCrypt.decode("my-key", loginuserid2);

        return this.http
            .get(this.BaseApiurl + '/profile/getcurrentMobile/?loginuserid=' + eval(loginuserid) + '&AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);
    }
    checkmobile_no(mobile) : Observable<any>{

        return this.http.get(this.BaseApiurl + '/api/checkmobile/?mobile=' + mobile + '&AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);
    }

    changeMobile(newmobile): Observable<any> {
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        var username1 = localStorage.getItem('firstname')
        var username = simpleCrypt.decode("my-key", username1);

        return this.http.post(this.BaseApiurl + '/profile/changeMobile/?AuthToken=' + localStorage.getItem('AuthToken'), { newmobile: newmobile, loginuserid: eval(loginuserid), loginusername: username })
            .map(this.extractData)
            .catch(this.handleError);
    }

    Changeemail(newemail) : Observable<any>{
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        return this.http.post(this.BaseApiurl + '/api/changeemail/?AuthToken=' + localStorage.getItem('AuthToken'), { newemail: newemail, loginuserid: eval(loginuserid) })
            .map(this.extractData)
            .catch(this.handleError);
    }


    loadcountrydropdown(): Observable<any> {

        return this.http.get(this.BaseApiurl + '/profile/loadcountrydropdown/?AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);
    }

    loadstates(): Observable<any> {

        return this.http.get(this.BaseApiurl + '/profile/loadstates/?AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);
    }
    loadstatesbyname(event1): Observable<any> {

        return this.http.get(this.BaseApiurl + '/profile/loadstatesbyname/?AuthToken=' + localStorage.getItem('AuthToken')+'&statename='+event1)
            .map(this.extractData)
            .catch(this.handleError);
    }
    loadcities(name): Observable<any> {
        
        return this.http.get(this.BaseApiurl + '/profile/loadcities/?AuthToken=' + localStorage.getItem('AuthToken') + '&name=' + name)
            .map(this.extractData)
            .catch(this.handleError);
    }
    loadstatesdropdown(country_id): Observable<any> {

        return this.http.get(this.BaseApiurl + '/profile/loadstatesdropdown/?country_id=' + country_id
            + '&AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);
    }

    loadcitydropdown(state_id): Observable<any> {

        return this.http.get(this.BaseApiurl + '/profile/loadcitydropdown/?state_id=' + state_id + '&AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);
    }

    loadlocalities(): Observable<any> {

        return this.http.get(this.BaseApiurl + '/profile/loadareas/?AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);
    }

    getallLocalities(name): Observable<any>{
        return this.http.get(this.BaseApiurl + '/profile/loadalllocalities/?name=' + name + '&AuthToken=' + localStorage.getItem('AuthToken'))
        .map(this.extractData)
        .catch(this.handleError);
    }
    loadnoticeperiod(): Observable<any> {

        return this.http.get(this.BaseApiurl + '/profile/loadnoticeperiod/?AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);
    }

    loadexpyears(): Observable<any> {

        return this.http.get(this.BaseApiurl + '/profile/loadexpyears/?AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);
    }

    loadexpmonths(): Observable<any> {

        return this.http.get(this.BaseApiurl + '/profile/loadexpmonths/?AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);
    }
    loadmaritalstatus(): Observable<any> {

        return this.http.get(this.BaseApiurl + '/profile/loadmaritalstatus/?AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);
    }

    loadgender() : Observable<any>{

        return this.http.get(this.BaseApiurl + '/profile/loadgender/?AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);
    }
    loadjobtype(): Observable<any> {

        return this.http.get(this.BaseApiurl + '/profile/loadjobtype/?AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);
    }

    loadsalinlakhs() : Observable<any>{

        return this.http.get(this.BaseApiurl + '/profile/loadsalinlakhs/?AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);
    }
    loadsalinthousands(): Observable<any> {

        return this.http.get(this.BaseApiurl + '/profile/loadsalinthousands/?AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);
    }
    loademptype(): Observable<any> {

        return this.http.get(this.BaseApiurl + '/profile/loademptype/?AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);
    }
    loaddesiredshift() : Observable<any>{

        return this.http.get(this.BaseApiurl + '/profile/loaddesiredshift/?AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);
    }
    loaddesignations() : Observable<any>{

        return this.http.get(this.BaseApiurl + '/profile/loaddesignations/?AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);
    }

    loadbloodgroups() : Observable<any>{

        return this.http.get(this.BaseApiurl + '/profile/loadbloodgroups/?AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);
    }
    loadphysicalstatus(): Observable<any> {

        return this.http.get(this.BaseApiurl + '/profile/loadphysicalstatus/?AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);
    }
    loadfresher_or_exp(): Observable<any> {

        return this.http.get(this.BaseApiurl + '/profile/loadstatus/?AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);
    }
    //getadvice options

    getadviceoptionsdata() : Observable<any>{
        return this.http.get(this.BaseApiurl + '/profile/getadviceoptionsdata/?AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);

    }
    getroledata(fn_area_id: any): Observable<any> {
        let headers = new Headers();
        headers.append('Token', localStorage.getItem('AuthToken'));
        let options = new RequestOptions({ headers: headers });
        return this.http.get(this.BaseApiurl + '/profile/getroledata/?fn_area_id=' + fn_area_id + '&AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);

    }
    getlocationadata(): Observable<any> {
        let headers = new Headers();
        headers.append('Token', localStorage.getItem('AuthToken'));
        let options = new RequestOptions({ headers: headers });
        return this.http.get(this.BaseApiurl + '/profile/getstatesdata/?AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);

    }
    Addvideoresume(video): Observable<any> {
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        var username1 = localStorage.getItem('loginusername')
        var username = simpleCrypt.decode("my-key", username1);
        return this.http.post(this.BaseApiurl + '/profile/addvideoresume/?AuthToken=' + localStorage.getItem('AuthToken'), { loginuserid: eval(loginuserid), loginusername: username, video: video.video })
            .map(this.extractData)
            .catch(this.handleError);
    }

    DeleteVideoResume(videoid) : Observable<any>{
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);

        return this.http.post(this.BaseApiurl + '/profile/deletevideoresume/?AuthToken=' + localStorage.getItem('AuthToken'), { loginuserid: eval(loginuserid), videoid: videoid })
            .map(this.extractData)
            .catch(this.handleError);
    }

    Getvideoresume(): Observable<any> {
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        return this.http.get(this.BaseApiurl + '/profile/getvideoresume/?loginuserid=' + loginuserid + '&AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);
    }
    Addgallery(gallery): Observable<any> {
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        var username1 = localStorage.getItem('loginusername')
        var username = simpleCrypt.decode("my-key", username1);
        return this.http.post(this.BaseApiurl + '/profile/addgallery/?AuthToken=' + localStorage.getItem('AuthToken'), { loginuserid: eval(loginuserid), loginusername: username, name: gallery.name, image: gallery.image, filetype: gallery.filetype })
            .map(this.extractData)
            .catch(this.handleError);
    }
    getlogincount(loginuserid): Observable<any> {


        // var loginuserid1 = localStorage.getItem('loginuserid')
        // var simpleCrypt = new SimpleCrypt();
        // var loginuserid = simpleCrypt.decode("my-key", loginuserid1);

        return this.http.get(this.BaseApiurl + '/profile/getlogincount/?loginuserid=' + loginuserid + '&AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);
    }
    GetGallery(): Observable<any> {
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);

        return this.http.get(this.BaseApiurl + '/profile/getgallery/?loginuserid=' + eval(loginuserid) + '&AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);
    }
    Deletegallery(id: any, image: any): Observable<any> {
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        return this.http.post(this.BaseApiurl + '/profile/deletegallery/?AuthToken=' + localStorage.getItem('AuthToken'), { loginuserid: eval(loginuserid), id: id, image: image })
            .map(this.extractData)
            .catch(this.handleError);
    }
    Deleteprofileimage(profile_image): Observable<any>{
     
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        return this.http.post(this.BaseApiurl + '/profile/deleteprofileimage/?AuthToken=' + localStorage.getItem('AuthToken'), { loginuserid: eval(loginuserid),profile_image: profile_image })
            .map(this.extractData)
            .catch(this.handleError);
    }
    Addachievementdetails(achievement): Observable<any> {

        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        var username1 = localStorage.getItem('loginusername')
        var username = simpleCrypt.decode("my-key", username1);
        return this.http.post(this.BaseApiurl + '/profile/addachievementdetails/?AuthToken=' + localStorage.getItem('AuthToken'), { loginuserid: eval(loginuserid), loginusername: username, name: achievement.name, description: achievement.description, image: achievement.image, filetype: achievement.filetype })
            .map(this.extractData)
            .catch(this.handleError);
    }
    Getachievementdetails() : Observable<any>{
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);

        return this.http.get(this.BaseApiurl + '/profile/getachievementdetails/?loginuserid=' + eval(loginuserid) + '&AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);
    }
    Editachievementdetails(achievement: any): Observable<any> {
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        return this.http.post(this.BaseApiurl + '/profile/editachievementdetails/?AuthToken=' + localStorage.getItem('AuthToken'), { loginuserid: eval(loginuserid), id: achievement.id, name: achievement.name, description: achievement.description })

            .map(this.extractData)
            .catch(this.handleError);
    }

    Deleteachievementdetails(id: any, image: any): Observable<any> {
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        return this.http.post(this.BaseApiurl + '/profile/deleteachievementdetails/?AuthToken=' + localStorage.getItem('AuthToken'), { loginuserid: eval(loginuserid), id: id, image: image })
            .map(this.extractData)
            .catch(this.handleError);
    }
    //ADD certifications
    Addcertifications(certifications): Observable<any> {
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        return this.http.post(this.BaseApiurl + '/profile/Addcertifications/?AuthToken=' + localStorage.getItem('AuthToken'), {
            loginuserid: eval(loginuserid), id: certifications.id, certification_name: certifications.certification_name,
            certification_body: certifications.certification_body,
            year: certifications.year
        })
            .map(this.extractData)
            .catch(this.handleError);
    }



    //Get certifications
    loadcertifications(): Observable<any> {
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        return this.http.get(this.BaseApiurl + '/profile/getcertifications/?loginuserid=' + eval(loginuserid) + '&AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);
    }


    // delete certifications 
    deletecertifications(id: any): Observable<any> {
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        return this.http.post(this.BaseApiurl + '/profile/deletecertifications/?AuthToken=' + localStorage.getItem('AuthToken'), { loginuserid: eval(loginuserid), id: id })
            .map(this.extractData)
            .catch(this.handleError);
    }
    Addprojectdetails(project: any): Observable<any> {
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        return this.http.post(this.BaseApiurl + '/profile/addprojectdetails/?AuthToken=' + localStorage.getItem('AuthToken'), { loginuserid: eval(loginuserid), name: project.name, team: project.team, description: project.description, duration: project.duration, skills: project.skills })
            .map(this.extractData)
            .catch(this.handleError);

    }
    Editprojectdetails(project: any): Observable<any> {
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        return this.http.post(this.BaseApiurl + '/profile/editprojectdetails/?AuthToken=' + localStorage.getItem('AuthToken'), { loginuserid: eval(loginuserid), id: project.id, name: project.name, team: project.team, description: project.description, duration: project.duration, skills: project.skills })
            .map(this.extractData)
            .catch(this.handleError);
    }

    Getallprojects(): Observable<any> {
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        return this.http.get(this.BaseApiurl + '/profile/getallprojects?loginuserid=' + eval(loginuserid) + '&AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);
    }

    DeleteProjectById(id: any): Observable<any> {
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        return this.http.post(this.BaseApiurl + '/profile/deleteprojectbyid/?AuthToken=' + localStorage.getItem('AuthToken'), { loginuserid: eval(loginuserid), id: id })
            .map(this.extractData)
            .catch(this.handleError);
    }
    Addlanguagesdetails(languages: any) : Observable<any>{
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        return this.http.post(this.BaseApiurl + '/profile/addlanguagesdetails/?AuthToken=' + localStorage.getItem('AuthToken'), { loginuserid: eval(loginuserid), language_name: languages.language_name, is_speak: languages.is_speak, is_read: languages.is_read, is_write: languages.is_write })

            .map(this.extractData)
            .catch(this.handleError);
    }
    Editlanguagesdetails(languages: any): Observable<any> {
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        return this.http.post(this.BaseApiurl + '/profile/editlanguagesdetails/?AuthToken=' + localStorage.getItem('AuthToken'), { loginuserid: eval(loginuserid), language_id: languages.language_id, language_name: languages.language_name, is_speak: languages.is_speak, is_read: languages.is_read, is_write: languages.is_write })

            .map(this.extractData)
            .catch(this.handleError);
    }
    Editskillsdetails(skill: any): Observable<any> {
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        return this.http.post(this.BaseApiurl + '/profile/editskillsdetails/?AuthToken=' + localStorage.getItem('AuthToken'), { loginuserid: eval(loginuserid), skill_name: skill.skill_name, version: skill.version, last_used: skill.last_used, exp_in_years: skill.exp_in_years, exp_in_months: skill.exp_in_months, skill_id: skill.skill_id })

            .map(this.extractData)
            .catch(this.handleError);
    }
    getskillnames(name): Observable<any> {
        
        return this.http.get(this.BaseApiurl + '/profile/getskillnames/?AuthToken=' + localStorage.getItem('AuthToken') + '&name=' + name)
            .map(this.extractData)
            .catch(this.handleError);
    }

// get all skills for mobile api
    getallskillnames() : Observable<any>{
        
        return this.http.get(this.BaseApiurl + '/profile/getallskillnames/?AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);
    }


    GetlanguagesdetailsById(): Observable<any> {
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        return this.http.get(this.BaseApiurl + '/profile/getlanguagesdetailsbyid?loginuserid=' + eval(loginuserid) + '&AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);
    }
    Getlanguagenames() : Observable<any>{

        return this.http.get(this.BaseApiurl + '/profile/getlanguagenames/?AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);
    }
    Getallskills(): Observable<any> {
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        return this.http.get(this.BaseApiurl + '/profile/getallskills?loginuserid=' + eval(loginuserid) + '&AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);
    }
    Addempdetails1(Empdetails1: any): Observable<any> {
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        return this.http.post(this.BaseApiurl + '/profile/addpresentempdetails/?AuthToken=' + localStorage.getItem('AuthToken'), { loginuserid: eval(loginuserid), organization_name: Empdetails1.organization_name, designation: Empdetails1.designation, from_date: Empdetails1.from_date, description: Empdetails1.description, notice_period: Empdetails1.notice_period, employment: Empdetails1.employment, employment_type: Empdetails1.employment_type })
            .map(this.extractData)
            .catch(this.handleError);

    }
    Getcourses(): Observable<any> {

        return this.http.get(this.BaseApiurl + '/api/courses/?AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);

    }


    editprofiledetails(model): Observable<any> {
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);

        return this.http.post(this.BaseApiurl + '/profile/edit/?AuthToken=' + localStorage.getItem('AuthToken'), {
            loginuserid: eval(loginuserid), resume_headline: model.resume_headline,
            preferred_location: model.preferred_location, permanent_address: model.permanent_address, hometown: model.hometown, pin_code: model.pin_code,
            marital_status: model.marital_status, prefered_jobtype: model.prefered_jobtype, pan_no: model.pan_no, passportno: model.passport_no, aadhar_no: model.aadhar_no,
            currentsalary: model.currentsalary,
            expectedsalary_min: model.expectedsalary_min, expectedsalary_max: model.expectedsalary_max, employment_type: model.employment_type,
            desired_shift: model.desired_shift,
            alternate_contact_number: model.alternate_contact_number, blood_group: model.blood_group, hobbies: model.hobbies, physical_status: model.physical_status,
            total_exp_in_yrs: model.total_exp_in_yrs, total_exp_in_months: model.total_exp_in_months, date_of_birth: model.date_of_birth, locality: model.locality,functional_area:model.fn_area_id,fresher_or_exp:model.fresher_or_experience
        })
            .map(this.extractData)
            .catch(this.handleError);
    }

    getphonebyid(country_id): Observable<any> {
        return this.http.get(this.BaseApiurl + '/jobposting/phonecode/?country_id=' + country_id + '&AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);
    }
    Addempdetails2(Empdetails2: any): Observable<any> {
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        return this.http.post(this.BaseApiurl + '/profile/addpreviousempdetails/?AuthToken=' + localStorage.getItem('AuthToken'), { loginuserid: eval(loginuserid), organization_name: Empdetails2.organization_name, designation: Empdetails2.designation, from_date: Empdetails2.from_date, to_date: Empdetails2.to_date, description: Empdetails2.description, employment: Empdetails2.employment, employment_type: Empdetails2.employment_type })
            .map(this.extractData)
            .catch(this.handleError);

    }

    Addempdetails3(Empdetails3: any) : Observable<any>{
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        return this.http.post(this.BaseApiurl + '/profile/addpreviousempdetails/?AuthToken=' + localStorage.getItem('AuthToken'), { loginuserid: eval(loginuserid), other_emp: Empdetails3.other_emp, employment: Empdetails3.employment, employment_type: Empdetails3.employment_type })
            .map(this.extractData)
            .catch(this.handleError);

    }
    Getpresentdetails() : Observable<any>{
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        return this.http.get(this.BaseApiurl + '/profile/getpresentemp/?loginuserid=' + eval(loginuserid) + '&employment=present' + '&AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);

    }

    Getpreviousdetails() : Observable<any>{
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        return this.http.get(this.BaseApiurl + '/profile/getpreviousemp/?loginuserid=' + eval(loginuserid) + '&employment=previous' + '&AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);

    }

    Getotherdetails() : Observable<any>{
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        return this.http.get(this.BaseApiurl + '/profile/getotheremp/?loginuserid=' + eval(loginuserid) + '&employment=other' + '&AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);

    }
    Editpresentemp(emp: any): Observable<any> {
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        return this.http.post(this.BaseApiurl + '/profile/editpresentemp/?AuthToken=' + localStorage.getItem('AuthToken'), { loginuserid: eval(loginuserid), organization_name: emp.organization_name, designation: emp.designation, from_date: emp.from_date, to_date: emp.to_date, description: emp.description, notice_period: emp.notice_period, other_emp: emp.other_emp, employment: emp.employment, id: emp.id, employment_type: emp.employment_type })
            //return this.http.post(this.BaseApiurl+'/profile/addlanguagesdetails', {loginuserid:loginuserid,languages:languages})
            .map(this.extractData)
            .catch(this.handleError);
    }

    Editpreviousemp(emp: any): Observable<any> {
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        return this.http.post(this.BaseApiurl + '/profile/editpreviousemp/?AuthToken=' + localStorage.getItem('AuthToken'), { loginuserid: eval(loginuserid), organization_name: emp.organization_name, designation: emp.designation, from_date: emp.from_date, to_date: emp.to_date, description: emp.description, employment: emp.employment, id: emp.id, employment_type: emp.employment_type })

            .map(this.extractData)
            .catch(this.handleError);
    }

    Editotheremp(emp: any): Observable<any> {
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        return this.http.post(this.BaseApiurl + '/profile/editotheremp/?AuthToken=' + localStorage.getItem('AuthToken'), { loginuserid: eval(loginuserid), other_emp: emp.other_emp, employment: emp.employment, id: emp.id, employment_type: emp.employment_type })

            .map(this.extractData)
            .catch(this.handleError);
    }
    DeletepresentempById(emp: any): Observable<any> {
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        return this.http.post(this.BaseApiurl + '/profile/deletepresentempbyid/?AuthToken=' + localStorage.getItem('AuthToken'), { loginuserid: eval(loginuserid), id: emp.id, organization_name: emp.organization_name, designation: emp.designation, notice_period: emp.notice_period })
            .map(this.extractData)
            .catch(this.handleError);
    }

    DeletepreviousempById(empid: any): Observable<any> {
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        return this.http.post(this.BaseApiurl + '/profile/deletepreviousempbyid/?AuthToken=' + localStorage.getItem('AuthToken'), { loginuserid: eval(loginuserid), id: empid })
            .map(this.extractData)
            .catch(this.handleError);
    }
    DeleteotherempById(empid: any): Observable<any> {
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        return this.http.post(this.BaseApiurl + '/profile/deleteotherempbyid/?AuthToken=' + localStorage.getItem('AuthToken'), { loginuserid: eval(loginuserid), id: empid })
            .map(this.extractData)
            .catch(this.handleError);
    }

    DeleteskillsById(skill: any) : Observable<any>{
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        return this.http.post(this.BaseApiurl + '/profile/deleteskillsbyid/?AuthToken=' + localStorage.getItem('AuthToken'), { loginuserid: eval(loginuserid), skill_id: skill.skill_id, skill_name: skill.skill_name })
            .map(this.extractData)
            .catch(this.handleError);
    }

    DeletelanguagesdetailsById(language_id: any): Observable<any> {
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        return this.http.post(this.BaseApiurl + '/profile/deletelanguagesdetailsbyid/?AuthToken=' + localStorage.getItem('AuthToken'), { loginuserid: eval(loginuserid), language_id: language_id })
            .map(this.extractData)
            .catch(this.handleError);
    }

    Addskillsdetails(skill: any) : Observable<any>{
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        return this.http.post(this.BaseApiurl + '/profile/addskilldetails/?AuthToken=' + localStorage.getItem('AuthToken'), { loginuserid: eval(loginuserid), skill_name: skill.skill_name, version: skill.version, exp_in_years: skill.exp_in_years, exp_in_months: skill.exp_in_months, last_used: skill.last_used })
            .map(this.extractData)
            .catch(this.handleError);
    }

    AddEducationdetails(educationdetail) : Observable<any>{
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        // qualification : educationaldetails.qualification,Specialization :educationaldetails. Specialization,university_or_institute :educationaldetails.university_or_institute ,year :educationaldetails.year,grading_system :educationaldetails.grading_system ,marks :educationaldetails.marks :number,medium : educationaldetails.medium 
        // natureof_job 


        var loginid = loginuserid

        return this.http.post(this.BaseApiurl + '/api/AddEducationdetails/?AuthToken=' + localStorage.getItem('AuthToken'), { loginuserid: eval(loginid), qualification: educationdetail.qualification, specialization: educationdetail.specialization, university_or_institute: educationdetail.university_or_institute, year: educationdetail.year, grading_system: educationdetail.grading_system, marks: educationdetail.marks, medium: educationdetail.medium, natureof_job: educationdetail.natureof_job, education_type: educationdetail.education_type })
            .map(this.extractData)
            .catch(this.handleError);
    }
    Getsscdetails(): Observable<any> {
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        return this.http.get(this.BaseApiurl + '/api/getsscdetails?loginuserid=' + eval(loginuserid) + '&education_type=1' + '&AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);
    }
    getsscdetails(user_registration_id: any): Observable<any> {
        return this.http.get(this.BaseApiurl + '/api/getsscdetails?loginuserid=' + user_registration_id + '&education_type=1' + '&AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);
    }

    Getinterdetails(): Observable<any> {
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        return this.http.get(this.BaseApiurl + '/api/getinterdetails?loginuserid=' + eval(loginuserid) + '&education_type=2' + '&AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);
    }
    Getgraduationdetails(): Observable<any> {
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        return this.http.get(this.BaseApiurl + '/api/getgraduationdetails?loginuserid=' + eval(loginuserid) + '&education_type=3' + '&AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);
    }
    Getpgdetails(): Observable<any> {
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        return this.http.get(this.BaseApiurl + '/api/getpgdetails?loginuserid=' + eval(loginuserid) + '&education_type=4' + '&AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);
    }
    Getphddetails(): Observable<any> {
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        return this.http.get(this.BaseApiurl + '/api/getphddetails?loginuserid=' + eval(loginuserid) + '&education_type=5' + '&AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);
    }
    Editsscdetails(ssc: any) : Observable<any>{
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        return this.http.post(this.BaseApiurl + '/api/editsscdetails/?AuthToken=' + localStorage.getItem('AuthToken'), { loginuserid: eval(loginuserid), qualification: ssc.qualification, year: ssc.year, medium: ssc.medium, marks: ssc.marks, education_id: ssc.education_id, education_type: ssc.education_type })

            .map(this.extractData)
            .catch(this.handleError);
    }

    Editinterdetails(inter: any): Observable<any> {
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        return this.http.post(this.BaseApiurl + '/api/editinterdetails/?AuthToken=' + localStorage.getItem('AuthToken'), { loginuserid: eval(loginuserid), qualification: inter.qualification, year: inter.year, medium: inter.medium, marks: inter.marks, education_id: inter.education_id, education_type: inter.education_type })

            .map(this.extractData)
            .catch(this.handleError);
    }
    Editgraduationdetails(graduation: any): Observable<any> {
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        return this.http.post(this.BaseApiurl + '/api/editgraduationdetails/?AuthToken=' + localStorage.getItem('AuthToken'), { loginuserid: eval(loginuserid), qualification: graduation.qualification, specialization: graduation.specialization, university_or_institute: graduation.university_or_institute, year: graduation.year, grading_system: graduation.grading_system, marks: graduation.marks, medium: graduation.medium, natureof_job: graduation.natureof_job, education_id: graduation.education_id, education_type: graduation.education_type })

            .map(this.extractData)
            .catch(this.handleError);
    }

    Editpgdetails(pg: any): Observable<any> {
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        return this.http.post(this.BaseApiurl + '/api/editpgdetails/?AuthToken=' + localStorage.getItem('AuthToken'), { loginuserid: eval(loginuserid), qualification: pg.qualification, specialization: pg.specialization, university_or_institute: pg.university_or_institute, year: pg.year, grading_system: pg.grading_system, marks: pg.marks, medium: pg.medium, natureof_job: pg.natureof_job, education_id: pg.education_id, education_type: pg.education_type })

            .map(this.extractData)
            .catch(this.handleError);
    }

    Editphddetails(phd: any): Observable<any> {
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        return this.http.post(this.BaseApiurl + '/api/editphddetails/?AuthToken=' + localStorage.getItem('AuthToken'), { loginuserid: eval(loginuserid), qualification: phd.qualification, specialization: phd.specialization, university_or_institute: phd.university_or_institute, year: phd.year, grading_system: phd.grading_system, marks: phd.marks, medium: phd.medium, natureof_job: phd.natureof_job, education_id: phd.education_id, education_type: phd.education_type })

            .map(this.extractData)
            .catch(this.handleError);
    }
    DeletesscById(education_id: any): Observable<any> {
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        return this.http.post(this.BaseApiurl + '/api/deletesscbyid/?AuthToken=' + localStorage.getItem('AuthToken'), { loginuserid: eval(loginuserid), education_id: education_id })
            .map(this.extractData)
            .catch(this.handleError);
    }
    DeleteinterById(education_id: any) : Observable<any>{
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        return this.http.post(this.BaseApiurl + '/api/deleteinterbyid/?AuthToken=' + localStorage.getItem('AuthToken'), { loginuserid: eval(loginuserid), education_id: education_id })
            .map(this.extractData)
            .catch(this.handleError);
    }
    DeletegraduationById(education_id: any): Observable<any> {
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        return this.http.post(this.BaseApiurl + '/api/deletegraduationbyid/?AuthToken=' + localStorage.getItem('AuthToken'), { loginuserid: eval(loginuserid), education_id: education_id })
            .map(this.extractData)
            .catch(this.handleError);
    }

    DeletepgById(education_id: any): Observable<any> {
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        return this.http.post(this.BaseApiurl + '/api/deletepgbyid/?AuthToken=' +localStorage.getItem('AuthToken'), { loginuserid: eval(loginuserid), education_id: education_id })
            .map(this.extractData)
            .catch(this.handleError);
    }
    DeletephdById(education_id: any): Observable<any> {
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        return this.http.post(this.BaseApiurl + '/api/deletephdbyid/?AuthToken=' + localStorage.getItem('AuthToken'), { loginuserid: eval(loginuserid), education_id: education_id })
            .map(this.extractData)
            .catch(this.handleError);
    }
    Retriveedudetails(): Observable<any> {
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);

        return this.http.get(this.BaseApiurl + '/api/retrivedudetails/?userid=' + eval(loginuserid) + '&AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);
    }
    loadyeardropdown(): Observable<any> {


        return this.http.get(this.BaseApiurl + '/api/loadyeardropdown/?AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);
    }
    loadstudiesdropdown(): Observable<any> {


        return this.http.get(this.BaseApiurl + '/api/loadstudiesdropdown/?AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);
    }

    loaduniversitiesdropdown(university): Observable<any> {
        return this.http.get(this.BaseApiurl + '/api/loaduniversitiesdropdown/?university=' + university + '&AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);
    }
// get all university dropdown for mobile api
    loadalluniversitiesdropdown(): Observable<any> {
        return this.http.get(this.BaseApiurl + '/api/loadalluniversitiesdropdown/?AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);
    }

    loadmarksdropdown(): Observable<any> {

        return this.http.get(this.BaseApiurl + '/api/loadmarksdropdown/?AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);
    }
    loadgradesdropdown(): Observable<any> {

        return this.http.get(this.BaseApiurl + '/api/loadgradesdropdown/?AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);
    }

    loadgradestypedropdown(grades_id): Observable<any> {
        return this.http.get(this.BaseApiurl + '/api/loadgradestypedropdown?grades_id=' + grades_id + '&AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);
    }
    loadcoursetypedropdown(): Observable<any> {

        return this.http.get(this.BaseApiurl + '/api/loadcoursetypedropdown/?AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);
    }
    coursedata(): Observable<any> {


        var id = 1;

        return this.http.get(this.BaseApiurl + '/api/coursedata/?id=' + id + '&AuthToken=' + localStorage.getItem('AuthToken')
        )
            .map(this.extractData)
            .catch(this.handleError);
    }
    /* eduspecifications(e) {
         

     var eduspecid=e;
  
     return this.http.get(this.BaseApiurl+'/api/eduspecifications/?eduspecid='+eduspecid)
         .map(this.extractData)
         .catch(this.handleError);
     }*/

    eduspecifications(courseid): Observable<any> {


        return this.http.get(this.BaseApiurl + '/api/eduspecifications/?courseid=' + courseid + '&AuthToken=' + localStorage.getItem('AuthToken')
        )
            .map(this.extractData)
            .catch(this.handleError);
    }
    coursedata1(): Observable<any> {


        var id = 2;

        return this.http.get(this.BaseApiurl + '/api/coursedata/?id=' + id + '&AuthToken=' + localStorage.getItem('AuthToken')
        )
            .map(this.extractData)
            .catch(this.handleError);
    }
    coursedata2(): Observable<any> {


        var id = 3;

        return this.http.get(this.BaseApiurl + '/api/coursedata/?id=' + id + '&AuthToken=' + localStorage.getItem('AuthToken')
        )
            .map(this.extractData)
            .catch(this.handleError);
    }
    coursedata3(): Observable<any> {


        var id = 4;

        return this.http.get(this.BaseApiurl + '/api/coursedata/?id=' + id + '&AuthToken=' + localStorage.getItem('AuthToken')
        )
            .map(this.extractData)
            .catch(this.handleError);
    }
    coursedata4() : Observable<any>{


        var id = 5;

        return this.http.get(this.BaseApiurl + '/api/coursedata/?id=' + id + '&AuthToken=' + localStorage.getItem('AuthToken')
        )
            .map(this.extractData)
            .catch(this.handleError);
    }



    emailCheck(email): Observable<any> {

        return this.http.get(this.BaseApiurl + '/api/email/?email=' + email + '&AuthToken=' + localStorage.getItem('AuthToken')
        )
            .map(this.extractData)
            .catch(this.handleError);
    }

    userNameChecking(userNameCheck) : Observable<any>{

        return this.http.get(this.BaseApiurl + '/api/userNameCheck/?username=' + userNameCheck + '&AuthToken=' + localStorage.getItem('AuthToken')
        )
            .map(this.extractData)
            .catch(this.handleError);
    }



    update(user: User) : Observable<any>{
        return this.http.put('/api/users/' + user.id, user + '&AuthToken=' + localStorage.getItem('AuthToken')
        );
    }

    delete(id: number) : Observable<any>{
        return this.http.delete('/api/users/' + id + '&AuthToken=' + localStorage.getItem('AuthToken')
        );
    }


    SaveProfile(profile_image): Observable<any> {
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        var username1 = localStorage.getItem('loginusername')
        var username = simpleCrypt.decode("my-key", username1);
        return this.http.post(this.BaseApiurl + '/profile/profileimage/?AuthToken=' + localStorage.getItem('AuthToken'), { profile_image: profile_image, loginuserid: eval(loginuserid), username: username })
    }
    getImage() {
        var loginuserid1 = localStorage.getItem('loginuserid')
        var simpleCrypt = new SimpleCrypt();
        var loginuserid = simpleCrypt.decode("my-key", loginuserid1);
        return this.http.get(this.BaseApiurl + '/profile/profile_image/?loginuserid=' + eval(loginuserid) + '&AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);
    }
    // applicantprofile(user_registration_id) {



    //     return this.http.get(this.BaseApiurl + '/profile/getprofiledetails/?id=' + user_registration_id + '&AuthToken=' + localStorage.getItem('AuthToken'))
    //         .map(this.extractData)
    //         .catch(this.handleError);

    // }
    applicantprofile(user_registration_id): Observable<any> {
        return this.http.get(this.BaseApiurl + '/profile/getprofiledetails/?loginuserid=' + user_registration_id + '&AuthToken=' + localStorage.getItem('AuthToken'))
            .map(this.extractData)
            .catch(this.handleError);

    }
    getsalarylakh(): Observable<any> {

        return this.http.get(this.BaseApiurl + '/profile/getsalarylakh/?AuthToken=' + localStorage.getItem('AuthToken')
        )
            .map(this.extractData)
            .catch(this.handleError);
    }
    getsalaryth() : Observable<any>{

        return this.http.get(this.BaseApiurl + '/profile/getsalarythousand/?AuthToken=' + localStorage.getItem('AuthToken')
        )
            .map(this.extractData)
            .catch(this.handleError);
    }
    getallcourses(): Observable<any> {
        return this.http.get(this.BaseApiurl + '/profile/allcourses/?AuthToken=' + localStorage.getItem('AuthToken')
        )
            .map(this.extractData)
            .catch(this.handleError);
    }
    // sendotp() {

    //     const headers = new Headers();
    //     headers.append("Access-Control-Allow-Origin", "http://localhost:4200")
    //     const options = new RequestOptions({ headers: headers });
    //     return this.http.post('http://bulk.onlineindiasms.com/api/mt/SendSMS?user=Unicsol&password=123456&senderid=UNICSL&channel=Trans&DCS=8&flashsms=0&number=918897097704&text=test%20message&route=15 ', options)
    //         .map(this.extractData)
    //         .catch(this.handleError);
    // }

    uploadresume(filedata): Observable<any> {

        const postData = {
            client_id: filedata.filename,
            contenttype: filedata.contenttype,

        }
        return this.http.post(this.BaseApiurl + '/profile/vedioresume/?AuthToken=' + localStorage.getItem('AuthToken'), postData)
            .map(this.extractData)
            .catch(this.handleError);

    }
    getpresignedurl(filedata): Observable<any> {

        const postData = {
            filename: filedata.filename,
            contenttype: filedata.contenttype,
            data: filedata.data,
        }
        return this.http.post(this.BaseApiurl + '/profile/getpresignedurl/?AuthToken=' + localStorage.getItem('AuthToken'), postData)
            .map(this.extractData)
            .catch(this.handleError);
    }
   

    private extractData(res: Response) {

        let body = res.json();
        this.dataArray = body || [];


        return body || [];
    }

    //handleError
    private handleError(error: any) {

        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg);
        return Observable.throw(errMsg);
    }

}
