import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'
import {environment} from '../../environments/environment'


@Injectable()
export class GetdropdownsService {

    BaseApiurl=environment.Apiurl;
    constructor(private http: HttpClient) { }
    login(username: string, password: string): Observable<any> {
        
        return this.http.post<any>(this.BaseApiurl+'/api/authenticate/', { username: username, password: password })
            .map(user => {
                // login successful if there's a jwt token in the response
                if (user) {
                    
                    var serverdata=JSON.parse(user.data);
                    var metadata=serverdata[0];
                  
                   localStorage.setItem('loginuserid',metadata.user_registration_id);
                   localStorage.setItem('loginusername',metadata.username);
                   localStorage.setItem('password',metadata.password);
                }
               
                return user;
            });
    }

}