import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginModule } from './login/login.module';
import { JobsRoutingModule } from './jobs/jobs-routing.module';
import { JobsModule } from './jobs/jobs.module';
import { ProfileModule } from './profile/profile.module';
import { HomeModule } from './home/home.module';
import { CharityModule } from './charity/charity.module';
import { EventsModule } from './events/events.module';
import { AdviceModule } from './advice/advice.module';
import { LoginpageComponent } from './login/loginpage/loginpage.component';
import { RegistrationComponent } from './login/registration/registration.component';
import { MainContentComponent } from './login/main-content/main-content.component';
import { MainPageComponent } from './login/main-page/main-page.component';
import { PostsComponent } from './login/posts/posts.component';
import { TodayWalkinsComponent } from './jobs/today-walkins/today-walkins.component';
import { SnapShotComponent } from './profile/snap-shot/snap-shot.component';
import { AchievementsComponent } from './profile/achievements/achievements.component';
import { ProfileDashboardComponent } from './profile/profile-dashboard/profile-dashboard.component';
import { JobProfileLayoutComponent } from './profile/job-profile-layout/job-profile-layout.component';
import { AppliedJobsComponent } from './profile/applied-jobs/applied-jobs.component';
import { skip } from 'rxjs-compat/operator/skip';
import { SkillsComponent } from './profile/skills/skills.component';
import { EducationComponent } from './profile/education/education.component';
import { MoreDetailsComponent } from './profile/more-details/more-details.component';
import { IntrestedEventsComponent } from './profile/intrested-events/intrested-events.component';
import { MyEventsComponent } from './profile/my-events/my-events.component';
import { MyJobPostsComponent } from './profile/my-job-posts/my-job-posts.component';
import { VideoResumeComponent } from './profile/video-resume/video-resume.component';
import { MyAdvicesComponent } from './profile/my-advices/my-advices.component';
import { ProjectsComponent } from './profile/projects/projects.component';
import { MyGalleryComponent } from './profile/my-gallery/my-gallery.component';
import { CertificationsComponent } from './profile/certifications/certifications.component';
import { MyCharityComponent } from './profile/my-charity/my-charity.component';
import { CompanyDetailsComponent } from './profile/company-details/company-details.component';
import { EmployerDesignationComponent } from './profile/employer-designation/employer-designation.component';

const routes: Routes = [
  {
    path: '', redirectTo: 'login', pathMatch: 'full'

  },
  { path: 'login', component: LoginpageComponent },
  { path: 'registration', component: RegistrationComponent },
 


  {
    path: '',
    component: MainContentComponent,
    children: [
      {
        path: 'home', component: MainPageComponent,
        // canActivate: [AuthGuard],
        children: [
          
          { path: 'posts', component: PostsComponent },
          { path: 'walkin', component: TodayWalkinsComponent },
          // { path: 'news', component: ENewsFeedComponent, canActivate: [AuthGuardRole] },
       


        ]

      },
      // { path: 'about', component: AboutComponent },    
      // { path: 'cookies', component: CookiesComponent },
      // { path: 'termsandconditions', component: TermsAndConditionsComponent },

    ]
  }
, {
    path: '',
    component: JobProfileLayoutComponent,
    // canActivate: [AuthGuard],
    children: [
     
      { path:'profile-dashboard', component: ProfileDashboardComponent },
      { path: 'achievements', component: AchievementsComponent },  
      { path: 'profile-dashboard', component: ProfileDashboardComponent },
      // { path: 'profile-details', component: ProfileDetailsComponent },
      { path: 'profile/:id', component: SnapShotComponent },
      { path: 'employment-details', component: EmployerDesignationComponent },
      { path: 'skill', component: SkillsComponent },
      { path: 'education', component: EducationComponent },
      { path: 'profile-moredetails', component: MoreDetailsComponent },
      { path: 'interestedevents', component: IntrestedEventsComponent },
      { path: 'appliedjobs', component: AppliedJobsComponent },
      { path: 'events', component: MyEventsComponent },
      { path: 'myposts', component: MyJobPostsComponent },
      { path: 'videoresume', component: VideoResumeComponent },
      { path: 'myevents', component: MyEventsComponent },
      { path: 'myadvices', component: MyAdvicesComponent },
      // { path: 'myfriends', component: MyFriendsComponent },
      { path: 'jobsapplied', component: AppliedJobsComponent },
      { path: 'evetssapplied', component: IntrestedEventsComponent },
      { path: 'projects', component: ProjectsComponent },
      { path: 'achievements', component: AchievementsComponent },
      { path: 'gallery', component: MyGalleryComponent },
      { path: 'certifications', component: CertificationsComponent },
      { path: 'charity', component: MyCharityComponent },
      { path: 'companydetails', component: CompanyDetailsComponent,  },

    ]
  },

  // { path: 'passwordreset/:id', component: ForgotpasswordComponent },
  // { path: '404', component: Filenotfound404Component },
  { path: '**', redirectTo: '/404' }
];



@NgModule({
  imports: [RouterModule.forRoot(routes),LoginModule,JobsModule,HomeModule,CharityModule,EventsModule,AdviceModule,ProfileModule],
  exports: [RouterModule,LoginModule,JobsModule,ProfileModule]
})
export class AppRoutingModule { }
