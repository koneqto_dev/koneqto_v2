import { Component, OnInit, ViewChild } from '@angular/core';
import { UserService } from '../../services/index';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../../environments/environment';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
const URL = '/api/';

@Component({
  selector: 'app-video-resume',
  templateUrl: './video-resume.component.html',
  styleUrls: ['./video-resume.component.css']
})
export class VideoResumeComponent implements OnInit {

   // public uploader:FileUploader = new FileUploader({url: URL});
   public hasBaseDropZoneOver: boolean = false;
   public hasAnotherDropZoneOver: boolean = false;
   videoresume: any
   videos: any = {}
   private url:any = '';
   public videoPlayer: any;
   modalRef: BsModalRef;
 
  
 
   public fileOverBase(e: any): void {
     this.hasBaseDropZoneOver = e;
   }
 
   public fileOverAnother(e: any): void {
     this.hasAnotherDropZoneOver = e;
   }
   constructor(public userservice: UserService,private spinnerService: Ng4LoadingSpinnerService, private tostersuc: ToastrService, private modalService: BsModalService) {
 
   }
   Apiurl = environment.Apiurl
   refreshData:any
   interval:any
   ngOnInit() {
     this.loadvideodetails()
 
   }
   id: any;
   video: any;
   size: any;
   refresh:any
 
 
 
 
   videofile:any
   videoSize: any;
   errVideoSize: boolean
   VideoType: string
   errVideoType: boolean
   profilevideoresume:any = {
     type: '',
     vid_data: ''
   }
   checkVideo(event:any) {
     this.VideoType = event.target.files[0].type
     this.videoSize = event.target.files[0].size
     this.profilevideoresume.type = this.VideoType.replace("video/", ".")
     this.videoname = false
 
     if (event.target.files.length) {
 
       if (this.VideoType == "video/mp4" || this.VideoType == "video/3gpp" || this.VideoType == "video/avi" || this.VideoType == "video/quicktime" ||this.VideoType == "video/3gpp2" 
       ||this.VideoType =="video/x-msvideo" ||this.VideoType =="video/x-flv"||this.VideoType =="video/x-m4v") {
 
        
 
         this.errVideoType = false;
 
         if (eval(this.videoSize) / 1048576 < 500) {
          
           this.errVideoSize = false;
         }
         else {
          
           this.errVideoSize = true;
         }
 
         var reader:any = new FileReader();
         reader.readAsDataURL(event.target.files[0]); // read file as data url
         reader.onload = (ev: any) => { // called once readAsDataURL is completed
           this.profilevideoresume.vid_data = ev.target.result;
           this.profilevideoresume.vid_data = this.profilevideoresume.vid_data.replace("data:" + this.VideoType + ";base64,", "");
         }
       }
 
       else {
        
         this.errVideoType = true;
         //this.eventVideo = "";
         //this.eventVideoType = "";
       }
     }
   }
 
   videoname: boolean
   post() {
 
     // this.size=video.srcElement.files[0].size;
     if (!this.video) {
       this.videoname = true
     }
     if (this.video) {
       let video:any = {
         id: this.id,
         video: this.profilevideoresume
       }
 
 
 
       if (video.id == "" || video.id == null || video.id == undefined) {
         if (this.errVideoSize == false && this.errVideoType == false) {
           this.spinnerService.show();
           this.userservice.Addvideoresume(video).subscribe(
             data => {
 
 
               this.tostersuc.success('Video added successfully');
               //this.refreshData();
               this.loadvideodetails()
               this.spinnerService.hide();
               this.video = ''
               // this.videofile=''
               this.VideoType = '';
               this.videoSize = false;
               this.videoname = false
 
 
             },
             error => {
 
             });
         }
 
       }
     }
 
   }
   vid: string = "{{Apiurl}}/videoresume";
   videoUrl:any = []
   loadvideodetails() {
 
     this.spinnerService.show();
     this.userservice.Getvideoresume().subscribe(
       data => {
 
 
         this.videoresume = data;
         this.spinnerService.hide();
 
 
 
       },
       error => {
 
       });
   }
 
   videoresumeid: any
   delete(videoid: any, template:any) {
 
     this.videoresumeid = videoid
 
     this.modalRef = this.modalService.show(
       template
     );
 
   }
 
   confirmation(res_confirm: boolean) {
     if (res_confirm) {
 
       this.userservice.DeleteVideoResume(this.videoresumeid).subscribe(
         data => {
 
           this.tostersuc.success('Video Resume deleted  Successfully');
           this.loadvideodetails()
           // this.videoresume=[]
           this.modalRef.hide()
 
 
         },
         error => {
 
         });
 
     }
     else {
       this.modalRef.hide()
     }
 
   }
 
 
 }
 
