import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/index';
import { ToastrService } from 'ngx-toastr';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-certifications',
  templateUrl: './certifications.component.html',
  styleUrls: ['./certifications.component.css']
})
export class CertificationsComponent implements OnInit {

  certification: any = [];
  certifications:any
  loadcertification: any;
  Yearofpass: any=[];
  id: any;
  modalRef: BsModalRef;
  constructor(private userservice: UserService,private modalService: BsModalService, private tostersuc: ToastrService,private spinnerService: Ng4LoadingSpinnerService,) { }


  editorConfig = {
    "editable": true,
    "spellcheck": false,
    "height": "auto",
    "minHeight": "0",
    "width": "auto",
    "minWidth": "0",
    "translate": "yes",
    "enableToolbar": true,
    "showToolbar": true,
    "placeholder": "",
    // "imageEndPoint": "",
    "toolbar": [
      ["bold", "italic", "underline", "unorderedList"]
      //  "strikeThrough", "superscript", "subscript"],
      // ["fontName", "fontSize", "color"],
      // ["justifyLeft", "justifyCenter", "justifyRight", "justifyFull", "indent", "outdent"],
      // ["cut", "copy", "delete", "removeFormat", "undo", "redo"],
      // ["paragraph", "blockquote", "removeBlockquote", "horizontalLine", "orderedList", "unorderedList"],
      // ["link", "unlink", "image", "video"]
    ]
  }

  ngOnInit() {
    this.loadcertifications()
    this.userservice.loadyeardropdown()
      .subscribe(
        data => {
         
          this.Yearofpass = data;
        

        },
        error => {
          //this.alertService.error(error);

        });
  }
  errmsg:boolean
  errName:boolean
  errYear:boolean
  savecertifications(certification:any) {
    
   if(!certification.certification_name){
     this.errName=true
   }
   if(!certification.year){
    this.errYear=true
  }
    if(certification.certification_name && certification.year){
      this.spinnerService.show();
    this.userservice.Addcertifications(certification)
      .subscribe(
        data => {
          this.errName=false
          this.errYear=false
          this.certification=[]
         // certification.reset()
          this.tostersuc.success('Certification details added Successfully');
          this.loadcertifications();
          this.spinnerService.hide();
          // alert("certification Details updated successfully");
          //this.loadcertifications()
        },
        error => {
          //this.alertService.error(error);
        });
        this.errmsg=false;
      
      }
     

  }
  editbyid(c:any){
   
  
      this.certification.certification_name = c.certification_name;
      this.certification.certification_body = c.certification_body;
      this.certification.year = c.year;
      this.certification.id=c.id
  }

  clearForm() {

    this.certification.certification_name ='';
    this.certification.certification_body = '';
    this.certification.year = '';
    this.certification.id='';
   
  }
deleteeventid:any
deletebyid(id:any,template:any) {
 
  this.deleteeventid=id
  
  this.modalRef = this.modalService.show(
    template
  );

}
confirmation(res_confirm:boolean)


{
  if(res_confirm)
  {
    this.userservice.deletecertifications(this.deleteeventid).subscribe(
      data => {
       
        this.tostersuc.success('Certification details deleted  successfully');
        this.modalRef.hide()
        // this.certifications = [];
      this.loadcertifications();
      this.spinnerService.hide();
      },
      error => {
       
      });

  }
  else{
    this.modalRef.hide()
  }
 
}



validate(event: any) {
 
  
  /  alert(event.keyCode)  /
    const pattern = /^[A-Za-z0-9' ']+$/;
    
  
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
    if(event.keyCode>=49 && event.keyCode <=57 && this.certification.certification_name.trim().length==1)
    {
      event.preventDefault();  
   
    }
    

  }




loadcertifications(){
  this.spinnerService.show();
 this.userservice.loadcertifications().subscribe(
   data => {
   
    this.certifications =data
    this.spinnerService.hide();

    error => {
     
    }
   });

  }


}

