import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyCharityComponent } from './my-charity.component';

describe('MyCharityComponent', () => {
  let component: MyCharityComponent;
  let fixture: ComponentFixture<MyCharityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyCharityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyCharityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
