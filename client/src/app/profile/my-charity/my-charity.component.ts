import { Component, OnInit, TemplateRef } from '@angular/core';
import { JobpostService } from '../../services';
import { NgxGalleryOptions, NgxGalleryAnimation } from 'ngx-gallery';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-my-charity',
  templateUrl: './my-charity.component.html',
  styleUrls: ['./my-charity.component.css']
})
export class MyCharityComponent implements OnInit {

  charities :any= []
  allcharityposts: any;
  modalRef: BsModalRef;
  galleryOptions: NgxGalleryOptions[];
  postdonate: any = {};
  display: boolean = false;
  allcharitypostsview = [];
  charity: any = []
  constructor(private jobpostService: JobpostService, private modalService: BsModalService, private ToastrService: ToastrService, ) { }
  charity_name:any;

  Apiurl = environment.Apiurl
  table = -1

  editorConfig = {
    "editable": true,
    "spellcheck": false,
    "height": "auto",
    "minHeight": "0",
    "width": "auto",
    "minWidth": "0",
    "translate": "yes",
    "enableToolbar": true,
    "showToolbar": true,
    "placeholder": "",
    // "imageEndPoint": "",
    "toolbar": [
      ["bold", "italic", "underline", "unorderedList"]
      //  "strikeThrough", "superscript", "subscript"],
      // ["fontName", "fontSize", "color"],
      // ["justifyLeft", "justifyCenter", "justifyRight", "justifyFull", "indent", "outdent"],
      // ["cut", "copy", "delete", "removeFormat", "undo", "redo"],
      // ["paragraph", "blockquote", "removeBlockquote", "horizontalLine", "orderedList", "unorderedList"],
      // ["link", "unlink", "image", "video"]
    ]
  }













  moredetails(charity_id:any, i:any) {
    if (this.table == -1) {

      this.table = charity_id;
      var buttons = document.getElementsByClassName('showall');
      for (var a = 0; a < buttons.length; a++) {
        var button = buttons[a];
        button.innerHTML = "<div class=" + "'fas fa-info-circle'" + ">" + "</div>" + " " + "View More"

      }
      document.getElementById(i).innerHTML = "<div class=" + "'fas fa-times-circle'" + ">" + "</div>" + " " + "Hide Details"


    }
    else if (this.table != charity_id) {


      this.table = charity_id;
      var buttons = document.getElementsByClassName('showall');
      for (var a = 0; a < buttons.length; a++) {
        var button = buttons[a];
        button.innerHTML = "<div class=" + "'fas fa-info-circle'" + ">" + "</div>" + " " + "View More"

      }
      document.getElementById(i).innerHTML = "<div class=" + "'fas fa-times-circle'" + ">" + "</div>" + " " + "Hide Details"



    }
    else {

      this.table = -1
      var buttons = document.getElementsByClassName('showall');
      for (var a = 0; a < buttons.length; a++) {
        var button = buttons[a];
        button.innerHTML = "<div class=" + "'fas fa-info-circle'" + ">" + "</div>" + " " + "View More"

      }

    }

    
    this.jobpostService.getallcharitypostsview(charity_id).subscribe(data => {

      this.allcharitypostsview = data;

this.imagesArr1=[]
      // if(this.allcharitypostsview!=null){
      //   for(var j=0;j<this.allcharitypostsview.length;j++){
      //    this.picsArr=this.allcharitypostsview[j].supporting_image;
      //    if(this.picsArr!=null){
      //       for(var i=0;i<this.picsArr.length-1;i++){
      //         this.picsstring[j]=this.picsArr.split(' ')
      //       }
      //       }
      //       }
            
      //     }
 
 if(this.allcharitypostsview!=null){
       for(var j=0;j<this.allcharitypostsview.length;j++){
           // let imagesArr:any=[];
           let imagesArr:any=[];
          this.picsArr=this.allcharitypostsview[j].supporting_image;
         
          // if(this.picsArr!=null){
          //    for(var i=0;i<this.picsArr.length-1;i++){
             
             
          //      this.picsstring[j]=this.picsArr.split(' ')
                 
            
          //    }
          //     }
           
 
              if(this.picsArr.length>0){
             for(var k=0;k<this.picsArr.length;k++){
              
        
             this.Imagejson = 
                  {
                    
                      small: this.Apiurl+'/charity/'+this.picsArr[k],
                      medium: this.Apiurl+'/charity/'+this.picsArr[k],
                      big: this.Apiurl+'/charity/'+this.picsArr[k]
                     //  small: './assets/charity/images/'+'archana-1541653994106-0.png',
                     //  medium: './assets/charity/images/'+'archana-1541653994106-0.png',
                     //  big: './assets/charity/images/'+'archana-1541653994106-0.png'
                  }
                 
                  imagesArr.push(this.Imagejson);
             }
           
 
           //}
             this.imagesArr1.push(imagesArr)
            }else{
              this.imagesArr1.push(null)
            }
          
             
           }
           }
           this.galleryOptions = [
             {
                 width: '500px',
                 height: '400px',
                 thumbnailsColumns: 4,
                 imageAnimation: NgxGalleryAnimation.Slide
             },
             // max-width 800
             {
                 breakpoint: 800,
                 width: '100%',
                 height: '600px',
                 imagePercent: 80,
                 thumbnailsPercent: 20,
                 thumbnailsMargin: 20,
                 thumbnailMargin: 20
             },
             // max-width 400
             {
                 breakpoint: 400,
                 preview: false
             }
         ];
  
 
 





    },
      error => {
      });

  }

  moredetais1() {
    this.display = false;
  }

  openModalDonate(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(
      template,
      Object.assign({}, { class: 'gray modal-lg' })
    );
  }

  closeModal(template: TemplateRef<any>) {
    this.modalRef.hide();
  }


  savedonaters(postdonate:any) {

    this.jobpostService.savedonaters(postdonate).subscribe(data => {

      this.ToastrService.success("Donate Successfully", "Donation")
    })
  }


  //loadcharity images
  images: any
  pics: string;
  picsArr: any;
  picsstring: any = [];
  Charity: any = [];
  Imagejson: any = {};
  imagesArr1: any = [];

  loadcharitydetails() {

    this.jobpostService.getcharitydetails().subscribe(
      data => {

        this.Charity = data;

      
   
  
      },
      error => {

      });
  }


  dropdownSettings:any= {}
  selectedItems :any= ['']
  noResult:any;

  edit(template: TemplateRef<any>, charity:any) {
    
    if (this.charity.charitytime) {
      this.charity.charity_help_start_date = this.charity.charitytime[0];
      this.charity.charity_help_end_date = this.charity.charitytime[1];
    }
    
    this.charity.firstname = charity.firstname
    this.charity.charity_name = charity.charity_name
    this.charity.charity_mobile_no = charity.charity_mobile_no
    this.charity.charity_help_start_date = charity.charity_help_start_date
    this.charity.charity_help_end_date = charity.charity_help_end_date
    this.charity.charity_address = charity.charity_address
    this.charity.volunteers_name = charity.volunteers_name
    this.charity.volunteers_mobile = charity.volunteers_mobile
    this.charity.bankname = charity.bankname
    this.charity.account_name = charity.account_name
    this.charity.account_number = charity.account_number
    this.charity.account_type = charity.account_type
    this.charity.ifsc_code = charity.ifsc_code
    this.charity.charity_description = charity.charity_description
    this.charity.charity_id = charity.charity_id


    this.modalRef = this.modalService.show(
      template,
      Object.assign({}, { class: 'gray modal-lg' }));
  };
  // ImageCompressService
  // processedImage
  // checkImage
  // checkedImage
  // onFileSelected(fileInput: any) {

  // }

  charityid:any
  editjob(charity:any) {

    
    this.jobpostService.editcharity(charity).subscribe(data => {
      
      this.charities = data;
      this.loadmycharityposts()
      
      this.modalRef.hide();
      //  this.closeModal(edittemplate1)
      this.ToastrService.success('charity details edited successfully');;
      //  this.mypost()
    })
    error => {

    }

  }


  // charity_id
  // Deletecharity(charityid: any, template) {

  //   this.charityid = charityid

  //   this.modalRef = this.modalService.show(
  //     template
  //   );

  // }

  // confirmation(res_confirm: boolean) {
  //   if (res_confirm) {
  //     this.jobpostService.Deletecharity(this.charity_id).subscribe(
  //       data => {

  //         this.ToastrService.success("Deletejob", "JOB Deleted SUCCESSFULLY")
  //         this.modalRef.hide()
  //         // this.mypost()

  //       },
  //       error => {

  //       });

  //   }
  //   else {
  //     this.modalRef.hide()
  //   }

  // }
  // selected = [];

  // charity1: any = {}
  // rej_status
  // rejectConfirm(charity, rejectconfirm) {
  //   if (this.selected.length && charity.status != "rejected") {
  //     this.charity = charity
  //     this.rej_status = status

  //     this.modalRef = this.modalService.show(
  //       rejectconfirm
  //     );
  //   }
  //   else if (this.selected.length && charity.status == "rejected") {
  //     this.ToastrService.warning("This is a rejected")
  //   }
  //   else {
  //     this.ToastrService.error("No User Selelcted")
  //   }


  // }
  // reject(res) {
  //   if (res) {
  //     this.changeStatus(this.charity, this.rej_status)
  //   }
  //   else {
  //     this.modalRef.hide()
  //   }
  // }

  loadmycharityposts() {
    this.jobpostService.getmycharityposts().subscribe(data => {


      this.allcharityposts = data;
      this.loadcharitydetails();


    },
      error => {
      });
  }

  ngOnInit() {
    this.loadcharitydetails();
    this.loadmycharityposts()

    this.jobpostService.charityRefresh.subscribe(data => {
      this.allcharityposts = data
    })


  }

}

