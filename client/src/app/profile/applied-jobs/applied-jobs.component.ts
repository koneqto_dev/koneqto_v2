import { Component, OnInit, TemplateRef } from '@angular/core';
import { AuthenticationService, UserService, JobpostService } from '../../services/index';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs/Rx';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { SimpleCrypt } from "ngx-simple-crypt";
import { ratingService } from '../../services/index';
import { ActivatedRoute } from '@angular/router';
import { environment } from '../../../environments/environment';
// import { id } from '@swimlane/ngx-datatable/release/utils';

@Component({
  selector: 'app-applied-jobs',
  templateUrl: './applied-jobs.component.html',
  styleUrls: ['./applied-jobs.component.css']
})
export class AppliedJobsComponent implements OnInit {
  appliedjobs: any = {}
  status: any = [];

  apply: any;
  modalRef: BsModalRef;
  sub:any;
  userIdred: number;
  jobidred: number;
  constructor(private route: ActivatedRoute, private jobpostService: JobpostService, private ToastrService: ToastrService, private userservice: UserService, private spinnerService: Ng4LoadingSpinnerService, private modalService: BsModalService, private ratingService: ratingService) { }
  Apiurl = environment.Apiurl
  table1 = -1


  getjobapplicantstatus(jobid, registration_id) {


    if (this.table1 == -1) {
      this.table1 = jobid;
      this.table = -1
      this.status = []
      this.jobpostService.getjobapplicantstatus(jobid, registration_id)
        .subscribe(
          data => {
            
            if (data.length != 0) {
              this.status = data
            }
            else {
              this.ToastrService.error("No Ratings given yet for this post");

            }



          });
    }
    else if (this.table1 != jobid) {
      this.table1 = jobid
      this.status = []
      this.jobpostService.getjobapplicantstatus(jobid, registration_id)
        .subscribe(
          data => {
            if (data.length != 0) {
              this.status = data
            }
            else {
              this.ToastrService.error("No Ratings given yet for this post");

            }

          


          });

    }
    else {
      this.table1 = -1
      this.status = []
    }

  }
  posting_id:any
  userrating:any
  imageUrl: string = "{{Apiurl}}/jobposts/images"
  videoUrl: string = "{{Apiurl}}/jobposts/videos"
  imagePath = []
  videoPath = []
  getappliedjobs(jobid) {

    this.jobpostService.getappliedjobs(jobid)
      .subscribe(
        data => {
          this.spinnerService.hide();
          this.apply = data;
         
          for (var i = 0; i < this.apply.length; i++) {
            this.imagePath.push(this.imageUrl + '/' + this.apply[i].venuephoto);

          }
          for (var i = 0; i < this.apply.length; i++) {
            this.videoPath.push(this.videoUrl + '/' + this.apply[i].venuevideo);

          }


        },

        error => {



        });

  }
  ngOnInit() {

    this.sub = this.route

      .queryParams
      .subscribe(params => {
        // Defaults to 0 if no query param provided.
        this.userIdred = +params['uid'] || 0;
        this.jobidred = +params['pid'] || 0;

        this.getappliedjobs(this.jobidred)

      });

    this.spinnerService.show();


  }

  table = -1

  viewMore(jobid, i) {

    this.table = -1


    if (this.table == -1) {

      this.table = jobid;
      var buttons = document.getElementsByClassName('showall');
      for (var a = 0; a < buttons.length; a++) {
        var button = buttons[a];
        button.innerHTML = "Show Details"

      }
      document.getElementById(i).innerHTML = "Hide Details"


    }
    else if (this.table != jobid) {


      this.table = jobid;
      var buttons = document.getElementsByClassName('showall');
      for (var a = 0; a < buttons.length; a++) {
        var button = buttons[a];
        button.innerHTML = "Show Details"

      }
      document.getElementById(i).innerHTML = "Hide Details"



    }
    else {

      this.table = -1
      var buttons = document.getElementsByClassName('showall');
      for (var a = 0; a < buttons.length; a++) {
        var button = buttons[a];
        button.innerHTML = "Show Details"

      }

    }

  }


  max: number = 10;
  rate: number = 7;
  isReadonly: boolean = false;
  x: number = 5;
  y: number = 2;
  z: number = 8;
  overall_exp:any
  jobid:any
  posted_user_id:any
  ratings = {
    infrastructure: 0,
    hospitality: 0,
    timeing: 0,
    arrangement: 0,
    overall_exp: '',
    id:0

  }
  ratingsComp = {
    infrastructureComp: 0,
    hospitalityComp: 0,
    timeingComp: 0,
    arrangementComp: 0,
    overall_expComp: '',
    id:0

  }
  pre_ratting = []
  id:any
  rateRecruiter(jobid, posted_user_id, template, status,id) {
    debugger
    this.jobid = jobid
    this.posted_user_id = posted_user_id;
    this.overall_exp = '';
    var simpleCrypt = new SimpleCrypt();

    var loginuserid = localStorage.getItem('loginuserid')
    loginuserid = simpleCrypt.decode("my-key", loginuserid)
    loginuserid = eval(loginuserid)
    var job = { jobid: jobid, loginuserid: loginuserid}
    this.id=id
    this.ratingService.loadallreviews(job).subscribe(data => {
      this.pre_ratting = data
  
      // if(data[0] && data[0].rated_for=='recruiter' ){
      this.pre_ratting = data

        if (this.pre_ratting.length < 1) {
          debugger
          this.ratings.infrastructure = 0;
          this.ratings.hospitality = 0;
          this.ratings.timeing = 0;
          this.ratings.arrangement = 0;
          this.ratings.overall_exp = ''
          this.modalRef = this.modalService.show(
            template
          );
        }
        if(this.pre_ratting){
     
       debugger
        this.ratings.infrastructure = data[0].infrastructure
        this.ratings.hospitality = data[0].hostpitality
        this.ratings.timeing = data[0].waiting_time
        this.ratings.arrangement = data[0].arrangement
        this.ratings.overall_exp = data[0].overall_exp
        this.ratings.id = data[0].id
        this.infracomp=data[0].infrastructurecomp
        this.hostcomp=data[0].hostpitalitycomp
        this.timecomp=data[0].waiting_timecomp
        this.arrangecomp=data[0].arrangementcomp
        this.overall_expcomp=data[0].overall_expcomp
        this.c_created_date=data[0].com_created_date
        this.modalRef = this.modalService.show(
          template
        );
     
}
    })

  
  }
  pre_rattingcomp = []
  rateCompany(jobid, posted_user_id, template, status,id) {
    this.jobid = jobid
    this.posted_user_id = posted_user_id;
    this.overall_exp = '';
    var simpleCrypt = new SimpleCrypt();

    var loginuserid = localStorage.getItem('loginuserid')
    loginuserid = simpleCrypt.decode("my-key", loginuserid)
    loginuserid = eval(loginuserid)
    var job = { jobid: jobid, loginuserid: loginuserid }

    this.ratingService.loadallreviews(job).subscribe(data => {
      this.pre_rattingcomp = data
   
      if (this.pre_rattingcomp.length < 1) {
        this.ratingsComp.infrastructureComp = 0;
        this.ratingsComp.hospitalityComp = 0;
        this.ratingsComp.timeingComp = 0;
        this.ratingsComp.arrangementComp = 0;
        this.ratingsComp.overall_expComp = ''
        this.modalRef = this.modalService.show(
          template
        );
      }
// applylog(this.pre_rattingcomp,"this.pre_rattingcomp")
if(this.pre_rattingcomp){
// for(var i=0;i<this.pre_rattingcomp.length;i++){
// if(data.rated_for=='company'){
      
     
        // if(data[i].rated_for=='comapny'){
          debugger
          this.ratingsComp.infrastructureComp = data[0].infrastructurecomp
          this.ratingsComp.hospitalityComp = data[0].hostpitalitycomp
          this.ratingsComp.timeingComp = data[0].waiting_timecomp
          this.ratingsComp.arrangementComp = data[0].arrangementcomp
          this.ratingsComp.overall_expComp = data[0].overall_expcomp
          this.ratingsComp.id = data[0].id
          this.infrarec=data[0].infrastructure
          this.hostrec=data[0].hostpitality
          this.timerec=data[0].waiting_time
          this.arrangerec=data[0].arrangement
          this.overall_exprec=data[0].overall_exp
          this.r_created_date=data[0].rec_created_date
          this.modalRef = this.modalService.show(
            template
          );
        // }
      // }
  }
    })


  }

  userId = localStorage.getItem('loginuserid')
  infracomp:any
  hostcomp:any
  timecomp:any
  arrangecomp:any
  overall_expcomp:any
  infrarec:any
   hostrec:any
    timerec:any
     arrangerec:any
      overall_exprec:any
      r_created_date:any
      c_created_date:any
  recruiterRating(infrarec, hostrec, timerec, arrangerec, overall_exprec,id) {

    // alert(overall_exp)
    debugger
    if (infrarec && hostrec && timerec && arrangerec && overall_exprec) {
      var simpleCrypt = new SimpleCrypt();
      var p_registration_id = eval(simpleCrypt.decode("my-key", this.userId));
      var rating_for='recruiter'
      this.r_created_date=new Date();
      this.ratingService.recruiterRating(infrarec, hostrec, timerec, arrangerec, overall_exprec,this.infracomp,this.hostcomp,this.timecomp,this.arrangecomp,this.overall_expcomp,
         this.jobid, this.posted_user_id, p_registration_id,rating_for,this.ratings.id,this.r_created_date,this.c_created_date,rating_for).subscribe(data => {
        this.ToastrService.success('Review submited successfully!', 'Review submited!');
        this.modalRef.hide()

      })
    }
  }
  companyRating(infracomp, hostcomp, timecomp, arrangecomp, overall_expcomp) {
  
    debugger
    // alert(overall_exp)
    if (infracomp && hostcomp && timecomp && arrangecomp && overall_expcomp) {
      var simpleCrypt = new SimpleCrypt();
      var p_registration_id = eval(simpleCrypt.decode("my-key", this.userId));
      var rating_for='comapny'
      this.c_created_date=new Date()
      this.ratingService.recruiterRating(this.infrarec, this.hostrec, this.timerec, this.arrangerec, this.overall_exprec,infracomp, hostcomp, timecomp, arrangecomp, 
        overall_expcomp, this.jobid, this.posted_user_id, p_registration_id,rating_for,this.ratingsComp.id,this.r_created_date,this.c_created_date,rating_for).subscribe(data => {
        this.ToastrService.success('Review submited successfully!', 'Review submited!');
        this.modalRef.hide()

      })
    }
  }
}

