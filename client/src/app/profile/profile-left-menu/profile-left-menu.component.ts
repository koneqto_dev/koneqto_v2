import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserService } from '../../services/user.service'
//import { timingSafeEqual } from 'crypto';
import { FileUploader } from 'ng2-file-upload';
import { environment } from '../../../environments/environment';
import { SimpleCrypt } from "ngx-simple-crypt";
import { ToastrService } from 'ngx-toastr';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-profile-left-menu',
  templateUrl: './profile-left-menu.component.html',
  styleUrls: ['./profile-left-menu.component.css']
})
export class ProfileLeftMenuComponent implements OnInit {
  fnarealist: any;
  fnarea: any;
  countries: any;
  states: any;
  public img:any = '';
  private url:any= '';
  profileImage: any;
  profile_img:any
  RoleID: string
  isrecruter: boolean = false;

  imgsrc:any;
  user:any;
  skill: any = []
  selectedFile:any = null;
  userId:any

  imageChangedEvent: any = '';
  croppedImage: any = ''
  image_valid:any
  result: any = {};

  public uploader: FileUploader = new FileUploader({ url: environment.Apiurl + '/profile/profileimageuploads/?AuthToken=' + localStorage.getItem('AuthToken') });

  constructor(private modalService: BsModalService, private http: HttpClient, private spinnerService: Ng4LoadingSpinnerService,private userservice: UserService, private ToastrService: ToastrService) { }
  Apiurl = environment.Apiurl
  cur_des: any = [];
  pref_loc1: any = [];
  pref_loc2: any = [];
  languagesdisplay: any = [];
  pskills: any = []
  a: any = []

  skillsarraydisplay: any = []
  skilldetails: any = []
  presentdesignation: any = [];
  previousdesignation: any = [];
  gradsarray: any = []
  graduniversity: any = []
  pgarray: any = []
  pguniversity: any = [];
  phdarray: any = []
  phduniversity: any = [];
  proskillsarray: any = []
  proskillsarraydisplay: any = []
  ApiUrl = environment.Apiurl;
  reimage2: any;
  hidetemp: boolean = true;

  //Resume template Open function
  openresume(template: TemplateRef<any>) {
    this.hidetemp = false;
    this.skillsarraydisplay = []
    this.skilldetails = []
    this.spinnerService.show();
    //this.modalRef = this.modalService.show(template);
    this.userservice.getprofiledetails(this.userId).subscribe(data => {

      this.result = data[0];
      this.spinnerService.hide();
      // this.reimage2 = "https://c2.staticflickr.com/2/1574/25734996011_637430f5d8_c.jpg";
      // this.reimage2 = "http://35.200.188.66:3000/events/images/201811710.jpeg";


      //current_designation
      if (this.result.current_designation) {
        this.cur_des=[]
        this.cur_des.push(this.result.current_designation.split(':')[1])
      }

      //pref_location
      if (this.result.preferred_location) {
        //var display = []
        this.pref_loc1 = [];
        for (let p = 0; p < this.result.preferred_location.length; p++) {
          this.pref_loc1.push(this.result.preferred_location[p].split(':'))

        }
        //this.pref_loc1.push(display)
      }

      if (this.pref_loc1) {
        this.pref_loc2 = []
        for (let p1 = 0; p1 < this.pref_loc1.length; p1++) {
          if (this.pref_loc1[p1 + 1]) {
            this.pref_loc2.push(this.pref_loc1[p1][1])
          }
          else {
            this.pref_loc2.push(this.pref_loc1[p1][1] + '.')
          }
          //this.pref_loc2.push(this.pref_loc2)
        }

      }

      //language_known

      if (this.result.language_known) {
        this.languagesdisplay = []
        // for (let i = 0; i < this.result[0].language_known.length; i++) {

        //   this.languagesdisplay.push(JSON.parse(this.userProfile[0].language_known[i].language_name))

        // }
        for (let i = 0; i < this.result.language_known.length; i++) {
          this.languagesdisplay.push(JSON.parse(this.result.language_known[i].language_name))
        }
      }

      // for()
      //university_or_institute
      if (this.result.grad_details) {

        for (let j = 0; j < this.result.grad_details.length; j++) {
          var splitarray: any = []
          if (this.result.grad_details[j].university_or_institute) {
            for (var i = 0; i < this.result.grad_details[j].university_or_institute.length; i++) {
              splitarray.push(this.result.grad_details[j].university_or_institute[i].split(':'))

            }
          }
          this.gradsarray.push(splitarray)


        }
        for (let i = 0; i < this.gradsarray.length; i++) {
          var display:any = []
          let j:any = 0
          while (j < this.gradsarray[i].length) {

            let sep:any = ''
            if (this.gradsarray[i][j + 1]) {
              sep = ','
            }
            display.push(this.gradsarray[i][j][1] + sep)
            j++

          }
          this.graduniversity.push(display)


        }
        // for (let i = 0; i < this.result.project_details.length; i++) {

        //   this.skill.push(JSON.parse(this.result.project_details[i].skills))

        // }
      }

      if (this.result.pg_details) {
        for (let j = 0; j < this.result.pg_details.length; j++) {
          var splitarray: any = []
          if (this.result.pg_details[j].university_or_institute) {
            for (var i = 0; i < this.result.pg_details[j].university_or_institute.length; i++) {
              splitarray.push(this.result.pg_details[j].university_or_institute[i].split(':'))

            }
          }
          this.pgarray.push(splitarray)

        }
        for (let i = 0; i < this.pgarray.length; i++) {
          var display:any = []
          let j:any = 0
          while (j < this.pgarray[i].length) {

            let sep:any = ''
            if (this.pgarray[i][j + 1]) {
              sep = ','
            }
            display.push(this.pgarray[i][j][1] + sep)
            j++

          }
          this.pguniversity.push(display)

        }
      }
      if (this.result.phd_details) {
        for (let j = 0; j < this.result.phd_details.length; j++) {
          var splitarray: any = []
          if (this.result.phd_details[j].university_or_institute) {
            for (var i = 0; i < this.result.phd_details[j].university_or_institute.length; i++) {
              splitarray.push(this.result.phd_details[j].university_or_institute[i].split(':'))

            }
          }
          this.phdarray.push(splitarray)

        }
        for (let i = 0; i < this.phdarray.length; i++) {
          var display:any = []
          let j:any = 0
          while (j < this.phdarray[i].length) {

            let sep:any = ''
            if (this.phdarray[i][j + 1]) {
              sep = ','
            }
            display.push(this.phdarray[i][j][1] + sep)
            j++

          }
          this.phduniversity.push(display)


        }
      }

      //key_skills
      if (this.result.skill_details) {
        for (let j = 0; j < this.result.skill_details.length; j++) {
          this.skilldetails.push(this.result.skill_details[j][0].split(':'))
        }
      }
      let count:any = 0
      // for(let i=0;i<this.skilldetails.length;i++){
      while (count < this.skilldetails.length) {
        let sep:any = '.'
        if (this.skilldetails[count + 1]) {
          sep = ', '
        }
        // display.push(this.skilldetails[i][1]+sep)         
        this.skillsarraydisplay.push(this.skilldetails[count][1] + sep)
        // sep=''
        count++

      }

      //presentdesignation
      if (this.result.presentemp_details != null) {
        this.presentdesignation=[]
        for (let i = 0; i < this.result.presentemp_details.length; i++) {
          this.presentdesignation.push(this.result.presentemp_details[i].designation.split(':')[1])
        }
      }

      //previousdesignation
      if (this.result.previousemp_details != null) {
        this.previousdesignation=[]
        for (let i = 0; i < this.result.previousemp_details.length; i++) {
          this.previousdesignation.push(this.result.previousemp_details[i].designation.split(':')[1])
        }
      }

      if (this.result.project_details) {

        for (let j = 0; j < this.result.project_details.length; j++) {
          var splitarray: any = []
          if (this.result.project_details[j].skills) {
            for (var i = 0; i < this.result.project_details[j].skills.length; i++) {
              splitarray.push(this.result.project_details[j].skills[i].split(':'))

            }
          }
          this.proskillsarray.push(splitarray)


        }
        for (let i = 0; i < this.proskillsarray.length; i++) {
          var display:any = []
          let j:any = 0
          while (j < this.proskillsarray[i].length) {

            let sep:any = ''
            if (this.proskillsarray[i][j + 1]) {
              sep = ','
            }
            display.push(this.proskillsarray[i][j][1] + sep)
            j++

          }
          this.proskillsarraydisplay.push(display)


        }
        // for (let i = 0; i < this.result.project_details.length; i++) {

        //   this.skill.push(JSON.parse(this.result.project_details[i].skills))

        // }
      }
    }, error => {

      //this.alertService.error(error);
    })

    this.modalRef = this.modalService.show(
      template,
      Object.assign({
        backdrop: 'static',
        keyboard: false
      }, { class: 'gray modal-lg' })
    );
  }

  //Resume template close function
  closeResumeTemplate(template2: TemplateRef<any>) {
    this.hidetemp = true;
    this.modalRef.hide();
  }

  //   onFileSelected(event: any) {
  //     
  //     this.imageChangedEvent = event;
  //     if (event.target.files && event.target.files[0]) {

  //       var reader = new FileReader();
  //       reader.readAsDataURL(event.target.files[0]); // read file as data url
  //       reader.onload = (ev: any) => { // called once readAsDataURL is completed
  //         this.url = ev.target.result;
  // 
  //         this.profileImage = reader.result;

  //         //this.imgsrc=this.profileImage;

  //       }
  //     }
  //   }

  imageType: string
  errImageType: boolean
  profileimage:any = {
    type: '',
    img_data: ''
  }
  check(file:any) {

    this.imageType = file.target.files[0].type

    this.profileimage.type = this.imageType.replace("image/", ".")

    if (file.target.files.length) {


      if (this.imageType == "image/jpeg" || this.imageType == "image/jpg" || this.imageType == "image/png" || this.imageType == "image/gif") {
        var reader:any = new FileReader();
        reader.readAsDataURL(file.target.files[0]); // read file as data url
        reader.onload = (ev: any) => { // called once readAsDataURL is completed
          this.profileimage.img_data = ev.target.result;

          this.profileimage.img_data = this.profileimage.img_data.replace("data:" + this.imageType + ";base64,", "");

          this.errImageType = false


        }


      }
      else {
        this.errImageType = true;
  
        this.profileimage = { type: '', img_data: '' }
        this.image = ''
        // this.ToastrService.error("Inavalid Image Format");
  
      }

    }


  
  }

  image: any
  onUpload(template:any) {



    if (this.profileimage.img_data != '' && !this.errImageType) {
      this.userservice.SaveProfile(this.profileimage).subscribe(data => {
        this.getimage()
        this.ToastrService.success("Profile image saved Successfully");
   
      }


      )

      // this.imgsrc = this.profileImage

      this.closeModal()
    }
    else {
      this.image_valid = true
    }
  }




  modalRef: BsModalRef;

  closeModal() {
    this.croppedImage = ''
    this.profileImage = ''
    this.modalRef.hide();

  }
  openModal(template: TemplateRef<any>) {
    
    this.profileImage = ''
    this.croppedImage = ''
    this.imageChangedEvent = null
    this.modalRef = this.modalService.show(template);
    this.image_valid = false;
    this.croppedImage = ''
    this.errImageType=false
  }
  getimage() {


    this.userservice.getImage().subscribe(data => {
      // this.profile_img = JSON.parse(JSON.stringify(data[0]))
      this.imgsrc = data[0].image

    })
  }

  // simpleCrypt = new SimpleCrypt();
  ngOnInit() {
    var userId:any = localStorage.getItem('loginuserid')
    var simpleCrypt:any = new SimpleCrypt();
    this.userId = simpleCrypt.decode("my-key", userId);

    this.userservice.getImage().subscribe(data => {

      // this.profile_img = JSON.parse(JSON.stringify(data[0]))
      this.imgsrc = data[0].image
    })
    // var userid1 = localStorage.getItem('loginuserid')
    // var userid = this.simpleCrypt.decode("my-key", userid1);
    //var userId = userid;

    var rollId1:any = localStorage.getItem('RoleID')
    var roleID:any = simpleCrypt.decode("my-key", rollId1);

    this.RoleID = eval(roleID);
    if (this.RoleID == "5") {
      this.isrecruter = true;
    }


  }

  imageCropped(image: string) {
    this.croppedImage = image;
  }
  imageLoaded() {
    // show cropper
  }
  loadImageFailed() {
    // show message
  }

  //To download resume 
  getBase64Image(imgg:any) {
    var canvas:any = document.createElement("canvas");
    // var ctx = canvas.getContext("2d");
    //ctx.drawImage(img, 0, 0);
    var dataURL:any = canvas.toDataURL("image/png");

    return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");//data:image/png;base64,
  }
  confirmation(res_confirm: boolean) {
    if (res_confirm) {
      this.userservice.Deleteprofileimage(this.profileimage).subscribe(
        // data => {
        data => {
          this.ToastrService.success('Profile image deleted  successfully');
          this.modalRef.hide()
          this.getimage()
          // this.loadAllSkills();

        },
        error => {

        });

    }
    else {
      this.modalRef.hide()
    }

  }

}
