import { Component, OnInit } from '@angular/core';
import {IMyDpOptions} from 'mydatepicker';
import {  AuthenticationService ,UserService} from '../../services/index';
import { AppComponent } from '../../app.component';
import {Observable} from 'rxjs/Rx';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import {SimpleCrypt} from "ngx-simple-crypt";


@Component({
  selector: 'app-snap-shot',
  templateUrl: './snap-shot.component.html',
  styleUrls: ['./snap-shot.component.css']
})
export class SnapShotComponent implements OnInit {
  public myDatePickerOptions: IMyDpOptions = {
    // other options...
    dateFormat: 'dd.mm.yyyy',
};
// Initialized to specific date (09.10.2018).
public model: any = { date: { year: 2018, month: 10, day: 9 } };
userProfile: any ;
  constructor(private  userService:UserService,
    private  AppComponent:AppComponent,
    private router: Router,
    private route: ActivatedRoute,
    private toastr1: ToastrService,private userservice:UserService
) 
  { }
  options = [1, 2, 3];
  optionSelected: any;
  imageChangedEvent: any = '';
  croppedImage: any = '';
  fnarealist
  rolelist:Observable<any[]>;
  locationlist:Observable<any[]>;
  selectedfnarea:any;
 
  cropped="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==";
  imageCropp() {
      
      this.croppedImage = this.cropped;
  }
  fileChangeEvent(event: any): void {
      
      this.imageChangedEvent = event;
  }
  imageCropped(image: string) {
      
      this.croppedImage = image;
  }
  imageLoaded() {
      
      // show cropper
  }
  loadImageFailed() {
      
      // show message
  }
  
 
  saveprofile(){
    
    var newuserProfile=  this.userProfile;
    var imgdata=this.croppedImage;
  
    this.userService.saveprofile(newuserProfile)
    .subscribe(
        data => {
          
          this.toastr1.success('Saved sucessfully!', 'Profile!');
         },
        error => {
            });


  }
  onOptionSelected(event){

  }



  
  ngOnInit() {    

//this.imageCropp();
      
    /*this.userService.getfunctinalareadata()
    .subscribe(
        data => {
          
          this.fnarealist=data;
          this.toastr1.success('Hello world!', 'Toastr fun!');
           },
        error => {
           });
        this.userService.getlocationadata()
        .subscribe(
            data => {
              
              this.locationlist=data;
              this.toastr1.success('Hello world!', 'Toastr fun!');
           },
            error => {
               });*/

            
        this.getprofile();

   }

getprofile(){
   var userId=localStorage.getItem('loginuserid')    
   var simpleCrypt = new SimpleCrypt();  
   var loginuserid = simpleCrypt.decode("my-key",userId);
   
   this.userService.getprofiledetails(loginuserid)
   .subscribe(
       data => {

         
         if(data.length==0){
            this.userProfile={};
        
         }
         else{
         
             this.userProfile=data[1];
         }
       

        
       },
       error => {
           
       });
   

    }





   onChange(fn_area_id:any){
       
    this.userService.getroledata(fn_area_id)
    .subscribe(
        data => {
          
          this.rolelist=data;
          this.toastr1.success('Hello world!', 'Toastr fun!');
          },
        error => {
             });

   }
}

