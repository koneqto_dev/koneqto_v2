import { Component, OnInit } from '@angular/core';
import {  AuthenticationService ,UserService} from '../../services/index';
import { TabsetComponent } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { User,educationaldetails,userProfile} from '../../_models/index';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-education',
  templateUrl: './education.component.html',
  styleUrls: ['./education.component.css']
})
export class EducationComponent implements OnInit {
 
  userProfile:userProfile;
  educationaldetails: any = [];
 educationaldetails1: any = [];
  educationaldetails2: any = [];
  educationaldetails3: any = [];
  educationaldetails4: any = [];
  modalRef: BsModalRef;
  master_id:any
  constructor(private userservice:UserService,private modalService: BsModalService,private tostersuc: ToastrService,private spinnerService: Ng4LoadingSpinnerService) { }

      Gspec:any=[];
      PGspec:any=[];
      PHDspec:any=[];
      gradetypes:any=[];
      Yearofpass:any=[];
      Boards:any=[];
      studies:any=[];
      Marks:any=[];
      grades:any=[];
      coursetype:any=[];
      universities:any=[];
      interboards:any=[];
      GCourses:any=[];
      PGCourses:any=[];
      PHDCourses:any=[];
      vehicles:any=[];
      wantedData:any=[];
      education_id:any;
      private disabled = false;
  optionSelected: any;
  e:any;
  public show:boolean = false;
 public posteduname:any = 'post';
 
  divide(){
    
    var wantedData:any = this.vehicles.filter(function(i) {
    return i.mass === 2;
    });
    this.wantedData=wantedData;

    }
    posteduNew(){
      this.show = !this.posteduNew;
  
      if(this.show)  
        this.posteduname = "Hide";
      else
        this.posteduname = "Show";
    }
  
  ngOnInit() {
    this.spinnerService.show();
    this.loadsscdetails();
    this.loadinterdetails();
    this.loadgraduationdetails();
    this.loadpgdetails();
    this.loadphddetails();
    // this.loaduniversities();
    
    this.spinnerService.show();

        this.userservice.coursedata()
        .subscribe(
          data => { this.spinnerService.hide();
         
            this.Boards=data;

             
          },
          error => {
              //this.alertService.error(error);
            
          });
          this.spinnerService.show();

          this.userservice.coursedata1()
        .subscribe(
          data => {
         
            this.interboards=data;
           // alert("coursedata loaded sucessfully");
             
          },
          error => {
              //this.alertService.error(error);
            
          });
          this.spinnerService.show();

          this.userservice.coursedata2()
        .subscribe(
          data => {
         
            this.GCourses=data;
            //alert("coursedata2 loaded sucessfully");
             
          },
          error => {
              //this.alertService.error(error);
            
          });
          this.spinnerService.show();

          this.userservice.coursedata3()
          .subscribe(
            data => {
           
              this.PGCourses=data;
             // alert("coursedata33 loaded sucessfully");
               
            },
            error => {
                //this.alertService.error(error);
              
            });
            this.spinnerService.show();

            this.userservice.coursedata4()
          .subscribe(
            data => {
                        this.PHDCourses=data;
             // alert("coursedata4 loaded sucessfully");
               
            },
            error => {
                //this.alertService.error(error);
              
            });
            this.spinnerService.show();

            this.userservice.loadyeardropdown()
            .subscribe(
                data => {
               
                  this.Yearofpass=data;
                 // alert("loadyeardropdown loaded sucessfully");
                   
                },
                error => {
                    //this.alertService.error(error);
                  
                }); 
                // this.spinnerService.show();

                // this.userservice.loaduniversitiesdropdown()
                // .subscribe(
                //     data => {
                   
                //       this.universities=data
                //     //this.grades=Array.of(this.grades);
                //      // alert("loadstudiesdropdown loaded sucessfully");
                       
                //     },
                //     error => {
                //         //this.alertService.error(error);
                      
                //     });
                    this.spinnerService.show();

                this.userservice.loadstudiesdropdown()
                .subscribe(
                    data => {
                   
                      this.studies=data;
    //alert("loadstudiesdropdown loaded sucessfully");
                       
                    },
                    error => {
                        //this.alertService.error(error);
                      
                    });
                    this.spinnerService.show();

                    this.userservice.loadmarksdropdown()
                    .subscribe(
                        data => {
                       
                         this.Marks=data
                        
                          //this.Marks=Array.of(this.Marks);
                         // alert("loadstudiesdropdown loaded sucessfully");
                           
                        },
                        error => {
                            //this.alertService.error(error);
                          
                        });
                        this.spinnerService.show();

                        this.userservice.loadgradesdropdown()
                    .subscribe(
                        data => {
                       
                          this.grades=data
                        //this.grades=Array.of(this.grades);
                         // alert("loadstudiesdropdown loaded sucessfully");
                           
                        },
                        error => {
                            //this.alertService.error(error);
                          
                        });

                       
                       
                        this.spinnerService.show();

                        this.userservice.loadcoursetypedropdown()
                    .subscribe(
                        data => {
                       
                        
                          this.coursetype=data
                         // this.coursetype=Array.of(this.coursetype);
                         // alert("loadstudiesdropdown loaded sucessfully");
                           
                        },
                        error => {
                            //this.alertService.error(error);
                          
                        });
                        this.spinnerService.show();

    
            this.userservice.Retriveedudetails()
            .subscribe(
                data => {
               
                 if(data.length>0)
                 {
                  for (let entry of data) {
                 
                   if(entry.education_id==1)
                   {
                    this.educationaldetails=entry
                   } 
                   if(entry.education_id==2)
                   {
                    this.educationaldetails1=entry
                   } 
                   if(entry.education_id==3)
                   {
                    if(entry.qualification!="")
                    {
                      this.Oncoursechange(entry.qualification)
                      //Specialization
                    }
                    this.educationaldetails2=entry
                   } 
                   if(entry.education_id==4)
                   {
                    if(entry.qualification!="")
                    {
                      this.OnPGcoursechange(entry.qualification)
                      //Specialization
                    }
                    this.educationaldetails3=entry
                   }
                   if(entry.education_id==5)
                   {
                    if(entry.qualification!="")
                    {
                      this.PHDCourseChange(entry.qualification)
                      //Specialization
                    }
                    this.educationaldetails4=entry
                   }
                }
        
                 }
                  
                  
                 // alert("Retriveedudetails  sucessfully");
                   
                },
                error => {
                    //this.alertService.error(error);
                  
                });
        
          
  }
  OnGradechange(e:any){
if(e==4)
{
  this.disabled = true;
}
else{
  this.disabled = false;
}
  }
  Oncoursechange(e:any){
    this.educationaldetails2.specialization=''

  this.userservice.eduspecifications(e)
  .subscribe(
    data => {
      
      this.Gspec=data;
     // alert("Oncoursechange loaded sucessfully");
       
    },
    error => {
        //this.alertService.error(error);
      
    });
  }
  OnPGcoursechange(e:any){
    this.educationaldetails3.specialization=''
    this.userservice.eduspecifications(e)
    .subscribe(
      data => {
        
        this.PGspec=data;
        //alert("OnPGcoursechange loaded sucessfully");
         
      },
      error => {
          //this.alertService.error(error);
        
      });
    
   
      }
      PHDCourseChange(e:any){
        this.educationaldetails4.specialization=''
        this.userservice.eduspecifications(e)
        .subscribe(
          data => {
            
            this.PHDspec=data;
           // alert("PHDCourseChange loaded sucessfully");
             
          },
          error => {
              //this.alertService.error(error);
            
          });
          }
          onSelectgrad(grades_id:any) {
             this.educationaldetails2.marks=undefined
            this.userservice.loadgradestypedropdown(grades_id)
            .subscribe(
              data => {
      
                this.gradetypes = data;
            
              },
              error => {
                //this.alertService.error(error);            
              });
            } 
            onSelectpg(grades_id:any) {
              this.educationaldetails3.marks=undefined
              this.userservice.loadgradestypedropdown(grades_id)
              .subscribe(
                data => {
        
                  this.gradetypes = data;
              
                },
                error => {
                  //this.alertService.error(error);            
                });
              } 

              onSelectphd(grades_id:any) {
                this.educationaldetails4.marks=undefined
                this.userservice.loadgradestypedropdown(grades_id)
                .subscribe(
                  data => {
          
                    this.gradetypes = data;
                
                  },
                  error => {
                    //this.alertService.error(error);            
                  });
                } 
errmsg:boolean


univdropdownList:any=[];
university:any
loaduniversities(event:any){
  this.univdropdownList=[]
  if(event.target.value){
    this.university=event.target.value
  }else{
     this.university=''
  }
  this.userservice.loaduniversitiesdropdown(this.university)
  .subscribe(
      data => {
        for (var a = 0; a < data.length; a++) {
          var univ:any = { "id": 2, "itemName": "Singapore" };
          univ.id = a;
          univ.itemName = data[a].university;
          this.univdropdownList.push(univ);
        }
     
        this.universities=data
        // this.spinnerService.show();
      //this.grades=Array.of(this.grades);
       // alert("loadstudiesdropdown loaded sucessfully");
         
      },
      error => {
          //this.alertService.error(error);
        
      });

      this.selectedItems = []

      this.dropdownSettings = {
        singleSelection: true,
        text: "Select ",
        enableCheckAll: false,
        enableSearchFilter: true,
        classes: "myclass custom-class",
        maxHeight: "150",
        // lazyLoading:true,
        showCheckbox:false
      };
      this.dropdownSettings1 = {
        singleSelection: true,
        text: "Select ",
        enableCheckAll: false,
        enableSearchFilter: true,
        classes: "myclass custom-class",
        maxHeight: "150",
        showCheckbox: false
      }
    }
    dropdownSettings:any= {}
    dropdownSettings1:any = {}
    selectedItems:any = []
    desgdropdownList:any = []



  //SSC Details
  saveeducationaldetails(){
  
  var educationdetail:any = this.educationaldetails;

  educationdetail.education_type="1";
  if(educationdetail.education_id == "" || educationdetail.education_id == null || educationdetail.education_id == undefined ){
  if(educationdetail.qualification && educationdetail.year){
    this.spinnerService.show();
    this.userservice.AddEducationdetails(educationdetail)
  .subscribe(
      data => {
       
       
      //  
        this.tostersuc.success('SSC details added successfully');
       // alert("SSC Details updated successfully");
        //this.educationaldetails = {};
        this.loadsscdetails()
        this.spinnerService.hide();
         
      },
      error => {
          //this.alertService.error(error);
        
      });
      this.errmsg=false;
    }
    else{
      this.errmsg=true;
     // alert("enter valid data")
    }
  }
    else{
      if(educationdetail.qualification && educationdetail.year){
        this.spinnerService.show();
      this.userservice.Editsscdetails(educationdetail).subscribe(
        data => {
     
          this.tostersuc.success('SSC details edited successfully');
       
          //this.langsData = {};
          this.loadsscdetails();
          this.spinnerService.hide();
        },
        error => {
        
        });
      //  this.educationaldetails={};
      this.errmsg=false;
    }
    else{
      this.errmsg=true;
 
    }
      
      }
      }
      editssc(ssc:any){

        //if(row.lang != null|| undefined){
        
          this.educationaldetails.qualification=ssc.qualification;
          this.educationaldetails.year=ssc.year;
          this.educationaldetails.medium=ssc.medium;
          this.educationaldetails.marks=ssc.marks;
          this.educationaldetails.education_id=ssc.education_id;
          this.educationaldetails.education_type=ssc.education_type;

      }

delssc:any
deletessc(ssc:any,template:any) {
  
  this.delssc=ssc
  
  this.modalRef = this.modalService.show(
    template
  );

}
confirmation(res_confirm:boolean)


{
  if(res_confirm)
  {
    this.userservice.DeletesscById(this.delssc.education_id).subscribe(
      data => {
        
        this.tostersuc.success('SSC details deleted  successfully');
       
        this.modalRef.hide()
        this.educationaldetails = [];
        this.loadsscdetails();
      },
      error => {
     
      });

  }
  else{
    this.modalRef.hide()
  }
 
}


loadsscdetails(){
  this.spinnerService.show();
  this.userservice.Getsscdetails().subscribe(
    data => {

   
      this.educationaldetails = data;
      this.spinnerService.hide();
    },
    error => {
  
    });    
}




//Inter/Dipolma Details
saveeducationaldetailsInter(){
 // 
 
//  this.spinnerService.show();
var educationdetail:any = this.educationaldetails1;

educationdetail.education_type="2";
if(educationdetail.education_id == "" || educationdetail.education_id == null || educationdetail.education_id == undefined ){
  if(educationdetail.qualification && educationdetail.year){
    this.spinnerService.show();
  this.userservice.AddEducationdetails(educationdetail)
.subscribe(
    data => {
   
      this.tostersuc.success('Secondary education details added successfully');
      //alert("Inter/Diploma updated successfully");
      this.loadinterdetails();
      this.spinnerService.hide();
    },
    error => {
        //this.alertService.error(error);
      
    });
    this.errmsg=false;
  }
  else{
    this.errmsg=true;
    //alert("Enter valid data")
  }
}
  else{
    if(educationdetail.qualification && educationdetail.year){
      this.spinnerService.show();
    this.userservice.Editinterdetails(educationdetail).subscribe(
      data => {
           
        this.tostersuc.success('Secondary education details edited successfully');
       // alert("successfully edit inter details")
        //this.langsData = {};
        this.loadinterdetails();
        this.spinnerService.hide();
      },
      error => {
   
      });
     // this.educationaldetails1={};
     this.errmsg=false;
  }
  else{
    this.errmsg=true;
   
  }
    }
  
}
editinter(inter:any){

  //if(row.lang != null|| undefined){
  
    this.educationaldetails1.qualification=inter.qualification;
    this.educationaldetails1.year=inter.year;
   
    this.educationaldetails1.medium=inter.medium;
    
    this.educationaldetails1.marks=inter.marks;
    this.educationaldetails1.education_id=inter.education_id;
    this.educationaldetails1.education_type=inter.education_type;

}

delinter:any
deleteinter(inter:any,template:any) {
  
  this.delinter=inter
  
  this.modalRef = this.modalService.show(
    template
  );

}
confirmations(res_confirm:boolean)


{
  if(res_confirm)
  {
    this.userservice.DeleteinterById(this.delinter.education_id).subscribe(
      data => {
        
        this.tostersuc.success('Secondary education details deleted  successfully');
       
        this.modalRef.hide()
        this.educationaldetails1 = [];
        this.loadinterdetails();
      },
      error => {
 
      });

  }
  else{
    this.modalRef.hide()
  }
 
}

loadinterdetails(){
  
  this.spinnerService.show();
  this.userservice.Getinterdetails().subscribe(
    data => {
 
      this.educationaldetails1 = data;
      this.spinnerService.hide();
    },
    error => {

    });    
}


g:any=[]

present_name:boolean=false
  previous_name:boolean=false
  other_name:boolean=false
  grad:any = { id: '', name: '' }
 
  university_or_institute:any=[]





  gtestObject:any=[]
//Graduation details
saveeducationaldetailsG(){
  // 
  this.g=[]
  this.gtestObject=[]
  // this.spinnerService.show();
 var educationdetail:any = this.educationaldetails2;
 
 educationdetail.education_type="3";
 
 
 if(this.educationaldetails2.university_or_institute){
//  for (var a = 0; a < this.educationaldetails2.university_or_institute.length; a++) {
//    
//    this.grad.id = this.educationaldetails2.university_or_institute[a].id
//    this.grad.name = this.educationaldetails2.university_or_institute[a].itemName
//    this.g.push(this.grad);
//   this.grad={ id: '', name: '' }
  
//  }


    

  for (var a = 0; a < this.educationaldetails2.university_or_institute.length; a++) {
    
    this.gtestObject.push(this.educationaldetails2.university_or_institute[a].id+':'+ this.educationaldetails2.university_or_institute[a].itemName)

    
}

}
 
 
//  this.educationaldetails2.university_or_institute = JSON.stringify(this.g);
this.educationaldetails2.university_or_institute =this.gtestObject;
 if(educationdetail.education_id == "" || educationdetail.education_id == null || educationdetail.education_id == undefined ){
   if(educationdetail.qualification && educationdetail.specialization && educationdetail.year && educationdetail.medium){
    this.spinnerService.show();
   this.userservice.AddEducationdetails(educationdetail)
 .subscribe(
     data => {
    
       this.tostersuc.success('Graduation details added successfully');
      // alert("Graduation details updated successfully");
      
        this.loadgraduationdetails();
        this.spinnerService.hide();
     },
     error => { this.spinnerService.hide();
         //this.alertService.error(error);
       
     });
     
     this.errmsg=false
 }
 else{
   this.errmsg=true
   //alert("Enter valid data")
 }
 }
 else{
   if(educationdetail.qualification && educationdetail.specialization && educationdetail.year && educationdetail.medium){
    this.spinnerService.show();
   this.userservice.Editgraduationdetails(educationdetail).subscribe(
     data => {
       
       this.tostersuc.success('Graduation details edited successfully');
      
       this.loadgraduationdetails();
       this.spinnerService.hide();
     },
     error => {
     
     });
   
    this.errmsg=false
 }
 else{
   this.errmsg=true
 }
     
 }
 }
 
 
 
 
 
 
 
 univ:any=[]
 grad1:any=[]
 gsplitarray1:any=[]
 guniversity:any=[]
 editgraduation(graduation:any){
  this.Oncoursechange(graduation.qualification);
  if(graduation.grading_system){
  this.onSelectgrad(graduation.grading_system)
  }
  window.scrollTo(0, 0)
     this.univ=[]
     this.gsplitarray1=[]
     this.guniversity=[]
     this.educationaldetails2.qualification=graduation.qualification;
     this.educationaldetails2.specialization=graduation.specialization;
   
     this.educationaldetails2.year=graduation.year;
     this.educationaldetails2.grading_system=graduation.grading_system;
     this.educationaldetails2.marks=graduation.marks;
     this.educationaldetails2.medium=graduation.medium;
     this.educationaldetails2.natureof_job=graduation.natureof_job;
     this.educationaldetails2.education_id=graduation.education_id;
     this.educationaldetails2.education_type=graduation.education_type;
 
  
    
    var universityNames:any={id:'',itemName:''}
 

    for(var i=0;i<graduation.university_or_institute.length;i++){
      this.gsplitarray1.push(graduation.university_or_institute[i].split(':'))
     
    }
  
  for (let index = 0; index < this.gsplitarray1.length; index++) {
   
    universityNames.id=this.gsplitarray1[index][0]
    universityNames.itemName=this.gsplitarray1[index][1]
   this.guniversity.push(universityNames)
   universityNames={id:'',itemName:''}
  
  }
  this.educationaldetails2.university_or_institute = this.guniversity;
   }
 
 
 
 
delgraduation:any
deletegraduation(graduation:any,template:any) {
  
  this.delgraduation=graduation
  
  this.modalRef = this.modalService.show(
    template
  );

}
delgraduationconfirmation(res_confirm:boolean)


{
  if(res_confirm)
  {
    this.userservice.DeletegraduationById(this.delgraduation.education_id).subscribe(
      data => {
        
        this.tostersuc.success('Graduation details deleted  successfully');
        this.modalRef.hide()
        this.educationaldetails2 = [];
       this.loadgraduationdetails();
        },
      error => {

      });

  }
  else{
    this.modalRef.hide()
  }
 
}

university1:any=[]
gsplitarray:any=[]
guniversityarray:any=[]
loadgraduationdetails(){
  this.spinnerService.show();
  this. gsplitarray=[]
  this.guniversityarray=[]
  this.userservice.Getgraduationdetails().subscribe(
    data => {

      
      this.educationaldetails2 = data;

   

      for(let j=0;j<this.educationaldetails2.length;j++){
        
        if(this.educationaldetails2[j].university_or_institute[0]){
          
             this.gsplitarray.push(this.educationaldetails2[j].university_or_institute[0].split(':'))
           
          }
          if(this.gsplitarray[j]){
           this.guniversityarray.push(this.gsplitarray[j][1])
          }
          
         }
    
      this.spinnerService.hide();
    },
    error => {
  
    });    
}

g1:any=[]
pgtestObject:any=[]
//PG Details
saveeducationaldetailsPG(){
  //
  
  this.pgtestObject=[]
  // this.spinnerService.show();
var educationdetail:any = this.educationaldetails3;
educationdetail.education_type="4";
if(this.educationaldetails3.university_or_institute){
// for (var a = 0; a < this.educationaldetails3.university_or_institute.length; a++) {
//   this.grad.id = this.educationaldetails3.university_or_institute[a].id
//   this.grad.name = this.educationaldetails3.university_or_institute[a].itemName
//   this.g1.push(this.grad);
//  this.grad={ id: '', name: '' }
 
// }
for (var a = 0; a < this.educationaldetails3.university_or_institute.length; a++) {
 
  this.pgtestObject.push(this.educationaldetails3.university_or_institute[a].id+':'+ this.educationaldetails3.university_or_institute[a].itemName)

  
}
}

this.educationaldetails3.university_or_institute = this.pgtestObject;


if(educationdetail.education_id == "" || educationdetail.education_id == null || educationdetail.education_id == undefined ){
  if(educationdetail.qualification && educationdetail.specialization && educationdetail.year && educationdetail.medium){
    this.spinnerService.show();
  this.userservice.AddEducationdetails(educationdetail)
.subscribe(
    data => {
      this.spinnerService.hide();
      this.tostersuc.success('Post Graduation details added successfully');
      this.loadpgdetails()
      this.spinnerService.hide();
    },
    error => {
      
    });
this.errmsg=false
  }
  else{
    this.errmsg=true;
  }
}
  else{
    if(educationdetail.qualification && educationdetail.specialization && educationdetail.year && educationdetail.medium){
      this.spinnerService.show();
    this.userservice.Editpgdetails(educationdetail).subscribe(
      data => {
          
        this.tostersuc.success('Post Graduation details edited successfully');
     
        this.loadpgdetails();
      },
      error => {
  
      });
     this.errmsg=false
    }
    else{
      this.errmsg=true;

    }
  }
}

pgsplitarray1:any=[]
 pguniversity:any=[]
editpg(pg:any){
  this.OnPGcoursechange(pg.qualification);
  if(pg.grading_system){
  this.onSelectpg(pg.grading_system)
  }
  window.scrollTo(0, 0)
    this.univ=[]
    this.pgsplitarray1=[]
this.pguniversity=[]
    this.educationaldetails3.qualification=pg.qualification;
    this.educationaldetails3.specialization=pg.specialization;
   
    this.educationaldetails3.year=pg.year;
    this.educationaldetails3.grading_system=pg.grading_system;
    this.educationaldetails3.marks=pg.marks;
    this.educationaldetails3.medium=pg.medium;
    this.educationaldetails3.natureof_job=pg.natureof_job;
    this.educationaldetails3.education_id=pg.education_id;
    this.educationaldetails3.education_type=pg.education_type;

    // var university2=JSON.parse(pg.university_or_institute)
    // var gradnames={id:'',itemName:''}
    
    // for (let index = 0; index < this.university2.length; index++) {
    //  gradnames.id=university2[index].id
    //  gradnames.itemName=university2[index].name
    //  this.univ.push(gradnames)
    //   gradnames={id:'',itemName:''}
    //   this.educationaldetails3.university_or_institute = this.univ;
    // }
    var universityNames:any={id:'',itemName:''}
 

    for(var i=0;i<pg.university_or_institute.length;i++){
      this.pgsplitarray1.push(pg.university_or_institute[i].split(':'))
     
    }
  
  for (let index = 0; index < this.gsplitarray1.length; index++) {
   
    universityNames.id=this.pgsplitarray1[index][0]
    universityNames.itemName=this.pgsplitarray1[index][1]
   this.pguniversity.push(universityNames)
   universityNames={id:'',itemName:''}
  
  }
   
  this.educationaldetails3.university_or_institute=this.pguniversity;


}

delpg:any
deletepg(pg:any,template:any) {
  
  this.delpg=pg
  
  this.modalRef = this.modalService.show(
    template
  );

}
delpgconfirmation(res_confirm:boolean)


{
  if(res_confirm)
  {
    this.userservice.DeletepgById(this.delpg.education_id).subscribe(
      data => {
        
        this.tostersuc.success('Post Graduation details deleted  successfully');
        this.modalRef.hide()
        this.educationaldetails3 = [];
        this.loadpgdetails();
        },
      error => {
 
      });

  }
  else{
    this.modalRef.hide()
  }
 
}



pgsplitarray:any=[]
pguniversityarray:any=[]
loadpgdetails(){
  this.spinnerService.show();
  
  this.pgsplitarray=[]
  this.pguniversityarray=[]
  this.userservice.Getpgdetails().subscribe(
    data => {
    
  
      this.educationaldetails3 = data;


      for(let j=0;j<this.educationaldetails3.length;j++){
        
        if(this.educationaldetails3[j].university_or_institute){
          
             this.pgsplitarray.push(this.educationaldetails3[j].university_or_institute[0].split(':'))
           
          }
          if(this.pgsplitarray[j]){
           this.pguniversityarray.push(this.pgsplitarray[j][1])
          }
          
         }
         this.spinnerService.hide();

    },
    error => {
 
    });    
}
item
onItemSelect(item: any) {

  this.item = item

}
OnItemDeSelect(item: any) {
  this.item = item


}

g2:any=[]
phdtestObject:any=[]
//Phd Details

saveeducationaldetailsPHD(){
 
  // this.spinnerService.show();
  
  this.g2=[]
  this.phdtestObject=[]
var educationdetail:any = this.educationaldetails4;
educationdetail.education_type="5";
if(this.educationaldetails4.university_or_institute){
// for (var a = 0; a < this.educationaldetails4.university_or_institute.length; a++) {
//   this.grad.id = this.educationaldetails4.university_or_institute[a].id
//   this.grad.name = this.educationaldetails4.university_or_institute[a].itemName
//   this.g2.push(this.grad);
//  this.grad={ id: '', name: '' }
 
// }
for (var a = 0; a < this.educationaldetails4.university_or_institute.length; a++) {
 
  this.phdtestObject.push(this.educationaldetails4.university_or_institute[a].id+':'+ this.educationaldetails4.university_or_institute[a].itemName)

  
}
}

this.educationaldetails4.university_or_institute = this.phdtestObject;



if(educationdetail.education_id == "" || educationdetail.education_id == null || educationdetail.education_id == undefined ){
  if(educationdetail.qualification && educationdetail.specialization && educationdetail.year && educationdetail.medium){
    this.spinnerService.show();
  this.userservice.AddEducationdetails(educationdetail)
.subscribe(
    data => {
     
   
      this.tostersuc.success('Ph.D details added successfully');
    
      this.loadphddetails()
      this.spinnerService.hide();
    },
    error => {
      
      
    });
    this.errmsg=false;
  }
  else{
    this.errmsg=true;
  }
}
  else{
    if(educationdetail.qualification && educationdetail.specialization && educationdetail.year && educationdetail.medium){
      this.spinnerService.show();
    this.userservice.Editphddetails(educationdetail).subscribe(
      data => {   
      
        this.tostersuc.success('Ph.D details edited successfully');

        this.loadphddetails();
        this.spinnerService.hide();
      },
      error => {
     
      });
      
    this.errmsg=false;
    }
    else{
      this.errmsg=true;
    }
    
  }
}
phdsplitarray1:any=[]
 phduniversity:any=[]
editphd(phd:any){
  this.PHDCourseChange(phd.qualification)
  if(phd.grading_system){
    this.onSelectpg(phd.grading_system)
    }
  window.scrollTo(0, 0)
  this.spinnerService.show();
  this.univ=[]
  this.phdsplitarray1=[]
this.phduniversity=[]
    this.educationaldetails4.qualification=phd.qualification;
    this.educationaldetails4.specialization=phd.specialization;
  
    this.educationaldetails4.year=phd.year;
    this.educationaldetails4.grading_system=phd.grading_system;
    this.educationaldetails4.marks=phd.marks;
    this.educationaldetails4.medium=phd.medium;
    this.educationaldetails4.natureof_job=phd.natureof_job;
    this.educationaldetails4.education_id=phd.education_id;
    this.educationaldetails4.education_type=phd.education_type;

    // var university3=JSON.parse(phd.university_or_institute)
    // var gradnames={id:'',itemName:''}
    
    // for (let index = 0; index < this.university3.length; index++) {
    //  gradnames.id=university3[index].id
    //  gradnames.itemName=university3[index].name
    //  this.univ.push(gradnames)
    //   gradnames={id:'',itemName:''}
    //   this.educationaldetails4.university_or_institute = this.univ;
    // }
   
    var universityNames:any={id:'',itemName:''}
 

    for(var i=0;i<phd.university_or_institute.length;i++){
      this.phdsplitarray1.push(phd.university_or_institute[i].split(':'))
     
    }
  
  for (let index = 0; index < this.phdsplitarray1.length; index++) {
   
    universityNames.id=this.phdsplitarray1[index][0]
    universityNames.itemName=this.phdsplitarray1[index][1]
   this.phduniversity.push(universityNames)
   universityNames={id:'',itemName:''}
  
  }
   
  this.educationaldetails4.university_or_institute=this.phduniversity;
   



    this.spinnerService.hide();
}
delphd:any
deletephd(phd:any,template:any) {
  
  this.delphd=phd
  
  this.modalRef = this.modalService.show(
    template
  );

}
delphdconfirmation(res_confirm:boolean)


{
  if(res_confirm)
  {
    this.userservice.DeletephdById(this.delphd.education_id).subscribe(
      data => {
        
        this.tostersuc.success('Ph.D details deleted  successfully');
        this.modalRef.hide()
        this.educationaldetails4 = [];
        this.loadphddetails();
        },
      error => {

      });

  }
  else{
    this.modalRef.hide()
  }
 
}




university3:any=[]
phdsplitarray:any=[]
phduniversityarray:any=[]
loadphddetails(){
  this.spinnerService.show();
  this.phdsplitarray=[]
 this.phduniversityarray=[]
  this.userservice.Getphddetails().subscribe(
    data => {
    
   
      this.educationaldetails4 = data;


      for(let j=0;j<this.educationaldetails4.length;j++){
        
        if(this.educationaldetails4[j].university_or_institute){
          
             this.phdsplitarray.push(this.educationaldetails4[j].university_or_institute[0].split(':'))
           
          }
          if(this.phdsplitarray[j]){
           this.phduniversityarray.push(this.phdsplitarray[j][1])
           
          }
          
         }
         this.spinnerService.hide();

    },
    error => {
 
    });    
}

clearForm1() {

  this.educationaldetails.qualification='';
          this.educationaldetails.year='';
          this.educationaldetails.medium='';
          this.educationaldetails.marks='';
          this.educationaldetails.education_id='';
          this.educationaldetails.education_type='';
          
}
clearForm2() {
          this.educationaldetails1.qualification='';
          this.educationaldetails1.year='';
          this.educationaldetails1.medium='';
          this.educationaldetails1.marks='';
          this.educationaldetails1.education_id='';
          this.educationaldetails1.education_type='';
}
clearForm3() {
          this.educationaldetails2.qualification='';
          this.educationaldetails2.specialization='';
          this.educationaldetails2.year='';
          this.educationaldetails2.grading_system='';
          this.educationaldetails2.marks='';
          this.educationaldetails2.medium='';
          this.educationaldetails2.natureof_job='';
          this.educationaldetails2.education_id='';
          this.educationaldetails2.education_type='';
         
}
clearForm4() {
  this.educationaldetails3.qualification='';
  this.educationaldetails3.specialization='';
          this.educationaldetails3.year='';
          this.educationaldetails3.grading_system='';
          this.educationaldetails3.marks='';
          this.educationaldetails3.medium='';
          this.educationaldetails3.natureof_job='';
          this.educationaldetails3.education_id='';
          this.educationaldetails3.education_type='';
         
}
clearForm5() {
          this.educationaldetails4.year='';
          this.educationaldetails4.grading_system='';
          this.educationaldetails4.marks='';
          this.educationaldetails4.medium='';
          this.educationaldetails4.natureof_job='';
          this.educationaldetails4.education_id='';
          this.educationaldetails4.education_type='';
          this.educationaldetails4.qualification='';
          this.educationaldetails4.specialization='';
         
 
}




onOptionSelected(event:any){
 
 }
 

}
