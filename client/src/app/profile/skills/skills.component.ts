import { Component, OnInit } from '@angular/core';
import { UserService, JobpostService } from '../../services/index';
import { ToastrService } from 'ngx-toastr';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.css']
})
export class SkillsComponent implements OnInit {

  skill: any = {};
  skillSet: any = [];
  skills: any[] = [];

  skill_name: any;
  public show: boolean = false;
  public postskillname: any = 'post';
  model: any = {};
  years: any = [];
  months: any = [];
  modalRef: BsModalRef;
  skillsdisplay:any = []
  skilldata1:any = []
  keyskills:any = []
  constructor(public userService: UserService, private modalService: BsModalService, private tostersuc: ToastrService, private spinnerService: Ng4LoadingSpinnerService, private jobpostService: JobpostService) { }
  getToday(): string {
    return new Date().toISOString().split('T')[0]
  }
  ngOnInit() {
    this.spinnerService.show();
    this.loadAllSkills();
    // this.getskills();
    this.loadexpyears();
    this.loadexpmonths()
  }
  item
  onItemSelect(item: any) {

    this.item = item

  }
  OnItemDeSelect(item: any) {
    this.item = item


  }


  postskillNew() {
    this.show = !this.postskillNew;

    if (this.show)
      this.postskillname = "Hide";
    else
      this.postskillname = "Show";
  }
  loadexpyears() {
    this.userService.loadexpyears().subscribe(
      data => {
        this.spinnerService.hide();

        this.years = data;
      },
      error => {

      });
  }
  keyeventname(name: any) {
    const enamepattern = /^[0-9a-zA-Z' '.!?,@&+=()%#$<>//*:;'"{}_-|]/;
    let inputChar = String.fromCharCode(name.charCode);
    if (name.keyCode != 8 && !enamepattern.test(inputChar)) {
      name.preventDefault();
    }
  }

  loadexpmonths() {
    this.userService.loadexpmonths().subscribe(
      data => {
        this.spinnerService.hide();

        this.months = data;
      },
      error => {

      });
  }

  event1
  getskillnames(event:any) {
    
    this.dropdownList = []
    if (event.target.value) {
      
      this.event1 = event.target.value
    } else {
      
      this.event1 = '';
    } 
    this.userService.getskillnames(this.event1).subscribe(data => {
      
      this.skills = data;
      for (var a = 0; a < data.length; a++) {
        var skillnames:any = { "id": 2, "itemName": "Singapore" };
        skillnames.id = data[a].skill_id;
        skillnames.itemName = data[a].skill_name;
        this.dropdownList.push(skillnames);
        
      }
    });
    this.selectedItems = []
  }


  dropdownSettings:any = {
    singleSelection: true,
    text: "Select ",
    enableCheckAll: false,
    enableSearchFilter: true,
    classes: "myclass custom-class",
    maxHeight: "150",
    lazyLoading: true,
    showCheckbox: false
  };
  dropdownSettings2:any = {
    singleSelection: true,
    text: "Select Skill",
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    enableSearchFilter: true,
    classes: "myclass custom-class",
    showCheckbox: false
  };
  // loadskillnames(){
  //   this.userService.Getskillnames().subscribe(
  //     data => { this.spinnerService.hide();


  //         this.skills=data;
  //         for (var a = 0; a < data.length; a++) {
  //           var skillnames = { "id": 2, "itemName": "Singapore" };
  //           skillnames.id = a;
  //           skillnames.itemName = data[a].skill_name;
  //           this.dropdownList.push(skillnames);
  //         }
  //     },
  //     error => {

  //     });
  //     this.selectedItems = []

  //     this.dropdownSettings = {
  //       singleSelection: true,
  //       text: "Select ",
  //       enableCheckAll: false,
  //       enableSearchFilter: true,
  //       classes: "myclass custom-class",
  //       maxHeight: "150",
  //       lazyLoading:true,
  //       showCheckbox:false
  //     };
  //   }
  // dropdownSettings = {}
  selectedItems:any = []
  dropdownList:any = [];
  skillsarray: any = []
  skillsarraydisplay: any = []
  splitarray: any = []
  loadAllSkills() {
    this.splitarray = []
    this.skillsarray = []

    this.userService.Getallskills().subscribe(
      data => {
        this.spinnerService.hide();

        this.skillSet = data;


        for (let j = 0; j < this.skillSet.length; j++) {

          if (this.skillSet[j].skill_name) {

            this.splitarray.push(this.skillSet[j].skill_name[0].split(':'))


          }
          if (this.splitarray[j]) {
            this.skillsarray.push(this.splitarray[j][1])

          }

        }


        //  for(let i=0;i<this.skillsarray.length;i++){
        //    var display=[]
        //    let j=0
        //    while(j<this.skillsarray[i].length){

        //    let sep=''
        //    if(this.skillsarray[i][j+1]){
        //      sep=','
        //    }
        //    display.push(this.skillsarray[i][j][1]+sep)
        //    j++

        //    }
        //    this.skillsarraydisplay.push(display)


        //  } 

      },
      error => {

      });
  }

  keyeventdate(date: any) {
    const pattern = /^(((0[1-9]|[12][0-9]|30)[-/]?(0[13-9]|1[012])|31[-/]?(0[13578]|1[02])|(0[1-9]|1[0-9]|2[0-8])[-/]?02)[-/]?[0-9]{4}|29[-/]?02[-/]?([0-9]{2}(([2468][048]|[02468][48])|[13579][26])|([13579][26]|[02468][048]|0[0-9]|1[0-6])00))$/;
    let inputChar = String.fromCharCode(date.charCode)
    if (date.keyCode != 8 && !pattern.test(inputChar)) {
      date.preventDefault();
    }
  }

  clearForm() {

    this.skill.key_skills1 = '';
    this.skill.version = '';
    this.skill.last_used = '';
    this.skill.experienceyears = '';
    this.skill.experiencemonths = '';
    this.skill.id = '';

  }
  skillversion: boolean = false
  skillsdata:any = { id: '', name: '' }
  s:any = []
  testObject: any = []
  skillid: any = []
  errmsg:boolean
  add() {

    this.testObject = []
    this.skillid = []
    //  alert(JSON.stringify(this.skill.key_skills1))
    //   alert(JSON.stringify(this.skill.key_skills1[0].id))
    if (this.skill.key_skills1) {
      this.errmsg = false;
      for (var i = 0; i < this.skillSet.length; i++) {
        this.skillid.push(JSON.stringify(eval(this.skillSet[i].skill_name[0].split(':')[0])))

        if ((JSON.stringify(this.skill.key_skills1[0].id)) == (this.skillid[i])) {
          this.tostersuc.error('This skill added already');
          this.skill = {}
          this.errmsg = false;
        }
      }
      if (this.skill.key_skills1) {


        for (var a = 0; a < this.skill.key_skills1.length; a++) {

          this.testObject.push(this.skill.key_skills1[a].id + ':' + this.skill.key_skills1[a].itemName)

        }
      }




      let obj: any
      if (this.testObject.length > 0) {
        obj = {
          skill_name: this.testObject,
          version: this.skill.version,
          last_used: this.skill.last_used,
          exp_in_years: this.skill.experienceyears,
          exp_in_months: this.skill.experiencemonths,
          skill_id: this.skill.id
        }
      }

      if (this.skill.version) {
        obj = {
          skill_name: this.testObject,
          version: this.skill.version.trim(),
          last_used: this.skill.last_used,
          exp_in_years: this.skill.experienceyears,
          exp_in_months: this.skill.experiencemonths,
          skill_id: this.skill.id
        }

        if (!obj.version) {
          this.skillversion = true;
          //this.tostersuc.error('Aadhar no is not valid',"hello");
        }
      }
      if (this.skill.id == "" || this.skill.id == null || this.skill.id == undefined) {
        if (this.testObject.length > 0) {
          this.spinnerService.show();
          this.userService.Addskillsdetails(obj).subscribe(
            data => {

              this.loadAllSkills();
              this.spinnerService.hide();
              // this.skill ={};
              this.tostersuc.success('Skills added Successfully');
              this.skillversion = false

            },
            error => {

            });
          this.skill = {};
          this.errmsg = false;
        }
        else {
          this.errmsg = true;
        }
      }
      else {
        if (obj.skill_name) {
          this.spinnerService.show();
          this.userService.Editskillsdetails(obj).subscribe(
            data => {

              this.tostersuc.success('Skills edited Successfully');
              this.spinnerService.hide();
              //alert("successfully edit skills")
              //this.langsData = {};
              this.skillversion = false
              this.loadAllSkills();
            },
            error => {

            });
          this.skill = {};
          this.errmsg = false;
        }
        else {
          this.errmsg = true;
        }
      }
    } else {
      this.errmsg = true;
    }

  }
  splitarray1: any = []
  edit(row) {

    this.splitarray1 = []
    this.keyskills = []

    this.skill.version = row.version;
    this.skill.last_used = row.last_used;
    this.skill.experienceyears = row.exp_in_years;
    this.skill.experiencemonths = row.exp_in_months;
    this.skill.id = row.skill_id

    var skillNames:any = { id: '', itemName: '' }


    for (var i = 0; i < row.skill_name.length; i++) {
      this.splitarray1.push(row.skill_name[i].split(':'))

    }

    for (let index = 0; index < this.splitarray1.length; index++) {

      skillNames.id = this.splitarray1[index][0]
      skillNames.itemName = this.splitarray1[index][1]
      this.keyskills.push(skillNames)
      skillNames = { id: '', itemName: '' }

    }
    this.skill.key_skills1 = this.keyskills;



  }

  deleteskill:any
  delete(row:any, template:any) {
    this.deleteskill = row

    this.modalRef = this.modalService.show(
      template
    );

  }
  confirmation(res_confirm: boolean) {
    if (res_confirm) {
      this.userService.DeleteskillsById(this.deleteskill).subscribe(
        // data => {
        data => {
          this.tostersuc.success('Skills deleted  Successfully');
          this.modalRef.hide()

          this.loadAllSkills();

        },
        error => {

        });

    }
    else {
      this.modalRef.hide()
    }

  }





}

