import { Component, OnInit } from '@angular/core';
import { UserService, JobpostService,LoginAuthService } from '../../services/index';
import { ToastrService } from 'ngx-toastr';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { SimpleCrypt } from "ngx-simple-crypt";
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-more-details',
  templateUrl: './more-details.component.html',
  styleUrls: ['./more-details.component.css']
})
export class MoreDetailsComponent implements OnInit {

  options1:any = [1, 2, 3];
  optionSelected1: any;
  Languag: any = {};
  langsData: any = [];
  languages: any = [];
  maritalstatus: any = [];
  Gender: any = [];
  jobtype: any = [];
  salinlakhs: any = [];
  salinthousands: any = [];
  emptype: any = [];
  dshift: any = [];
  bloodgroup: any = []
  physicalstatus: any = []
  expyears: any = []
  expmonths: any = []
  status: any = []
  languagesdisplay:any = []
  languagedata1:any = []
  modalRef: BsModalRef;
  locality: any = [];
  
  fnarealists: any = []
  // dropdownSettings={}
  today = new Date()
  date = this.today.getFullYear() - 18 + "/" + this.today.getMonth() + "/" + this.today.getDate()

  dateArr: any = this.date.split("/")
  max1 = new Date(this.dateArr[0], this.dateArr[1], this.dateArr[2])
  min1 = new Date(1900, 1, 1);

  public show: boolean = false;
  public postlanguagename: any = 'post';
  preferred_location:any= [];

  constructor(private jobpostService: JobpostService, private LoginAuthService: LoginAuthService, private userService: UserService, private modalService: BsModalService, private tostersuc: ToastrService, private spinnerService: Ng4LoadingSpinnerService) { }
  model: any = [];
  model1: any = {}

  options: any = [{ "Language": "Aaron 2Moore", "Fluency": "Heath44@hotmail.com", "Reading": "Regional Configuration Producer", "Writing": "Writing", "Speaking": "Speaking" }]
  city_id: any
  dropdownSettings:any = {
    singleSelection: true,
    text: "Select ",
    enableCheckAll: false,
    enableSearchFilter: true,
    classes: "myclass custom-class",
    maxHeight: "150",
     showCheckbox: false
  };
  dropdownSettings1:any = {
    singleSelection: false,
    text: "Select ",
    enableCheckAll: false,
    enableSearchFilter: true,
    classes: "myclass custom-class",
    maxHeight: "150",
    // lazyLoading: true,
    showCheckbox: true,
    limitSelection:5,
   
    
  };
  splitarray1: any = []
  locations:any = []
  splitarray2: any = []
  localities:any = []
  model2:any = {
    preferred_location: [],
    locality: []

  }
  ngOnInit() {
    this.spinnerService.show();

    this.loadLanguages();
    this.loadlanguagenames();
    this.loadmaritalstatus();
    this.loadgender();
    this.loadjobtype();
    this.loadsalinlakhs();
    this.loadsalinthousands();
    this.loademptype();
    this.loaddesiredshift();
    this.loadbloodgroup();
    this.loadphysicalstatus()
    this.loadexpyears()
    this.loadexpmonths()
    this.loadfresher_or_exp()
    this.loadfunctionalarea()
    // this.loadcities(event)
    //this.loadlocalities()

    this.localities = []
    this.splitarray2 = []
    this.locations = []
    this.splitarray1 = []

    var simpleCrypt:any = new SimpleCrypt();
    var userId:any = localStorage.getItem('loginuserid')
    var loginuserid:any = simpleCrypt.decode("my-key", userId);

    this.userService.getprofiledetails(loginuserid)
      .subscribe(data => {
       
        this.spinnerService.hide();
        this.model = data;
      console.log(this.model,"model")
      // this.locations = []
      // this.splitarray1 = []
       
        //pref_loc

        if (this.model[0].preferred_location) {
        
          var locationNames:any = { id: '', itemName: '' }
          
          for (var i = 0; i < this.model[0].preferred_location.length; i++) {
            this.splitarray1.push(this.model[0].preferred_location[i].split(':'))

          }
          for (let index = 0; index < this.splitarray1.length; index++) {


            locationNames.id = this.splitarray1[index][0]
            locationNames.itemName = this.splitarray1[index][1]
            this.locations.push(locationNames)
            locationNames = { id: '', itemName: '' }

          }

          this.model2.preferred_location = this.locations
          this.model.preferred_location = this.model2.preferred_location


        }

        //locality
        if (this.model[0].locality) {
          var localityNames:any = { id: '', itemName: '' }


          for (var i = 0; i < this.model[0].locality.length; i++) {
            this.splitarray2.push(this.model[0].locality[i].split(':'))

          }

          for (let index = 0; index < this.splitarray2.length; index++) {

            localityNames.id = this.splitarray2[index][0]
            localityNames.itemName = this.splitarray2[index][1]
            this.localities.push(localityNames)
            localityNames = { id: '', itemName: '' }

          }
          this.model2.locality = this.localities
          this.model.locality = this.model2.locality

        }
        //city_id
        this.city_id = this.model[0].current_location
        this.jobpostService.getLocalitybyid1(this.city_id).subscribe(
          data => {



            this.locality = data;

            for (var a = 0; a < data.length; a++) {
              var localitynames:any = { "id": 2, "itemName": "Singapore" };
              localitynames.id =  data[a].loc_id;
              localitynames.itemName = data[a].area;
              this.dropdownList1.push(localitynames);
            }
          })

      },
        error => {

        });

  }
  dropdownList1:any = [];
  item:any
  loadareas2(event:any) {
    
    this.dropdownList1=[]
    this.city_id = this.model[0].current_location
    // this.city_id = event.target.value;
    this.jobpostService.getLocalitybyid2(this.city_id, event.target.value)
      .subscribe(
        data => {
          // this.localities = []
          for (var a = 0; a < data.length; a++) {
            var c = { "id": 2, "itemName": "Singapore" };
            c.id = data[a].loc_id;
            c.itemName = data[a].area;
            this.dropdownList1.push(c);
          }
        },
        error => {
        });
  }
  onItemSelect(item: any) {

    this.item = item

  }
  OnItemDeSelect(item: any) {
    this.item = item


  }

  loadmaritalstatus() {
    this.userService.loadmaritalstatus()
      .subscribe(
        data => {

          this.maritalstatus = data;

        },
        error => {
          //this.alertService.error(error);

        });

  }

  loadgender() {
    this.userService.loadgender()
      .subscribe(
        data => {

          this.Gender = data;

        },
        error => {
          //this.alertService.error(error);

        });

  }

  loadjobtype() {
    this.userService.loadjobtype()
      .subscribe(
        data => {

          this.jobtype = data;

        },
        error => {
          //this.alertService.error(error);

        });

  }

  loadsalinlakhs() {
    this.userService.loadsalinlakhs()
      .subscribe(
        data => {

          this.salinlakhs = data;

        },
        error => {
          //this.alertService.error(error);

        });

  }

  loadsalinthousands() {
    this.userService.loadsalinthousands()
      .subscribe(
        data => {

          this.salinthousands = data;

        },
        error => {
          //this.alertService.error(error);

        });

  }
  loademptype() {
    this.userService.loademptype()
      .subscribe(
        data => {

          this.emptype = data;

        },
        error => {
          //this.alertService.error(error);

        });

  }
  loaddesiredshift() {
    this.userService.loaddesiredshift()
      .subscribe(
        data => {

          this.dshift = data;

        },
        error => {
          //this.alertService.error(error);

        });

  }
  postlanguageNew() {
    this.show = !this.postlanguageNew;

    if (this.show)
      this.postlanguagename = "Hide";
    else
      this.postlanguagename = "Show";
  }

  citydropdownList:any = [];
  event1:any
  loadcities(event:any) {
    this.citydropdownList = []
    
    if (event.target.value) {
      
      this.event1 = event.target.value;
    } else {
      
      this.event1 = '';
    }
    this.userService.loadcities(this.event1)
      .subscribe(
        data => {
        
          this.citydropdownList = []
          
          for (var a = 0; a < data.length; a++) {
            var citynames:any = { "id": 2, "itemName": "Singapore" };
            citynames.id = data[a].city_id;
            citynames.itemName = data[a].city_name+','+data[a].state_name+','+data[a].country_name;
            this.citydropdownList.push(citynames);
          } 
        });
  }



  loadlanguagenames() {
    this.userService.Getlanguagenames().subscribe(
      data => {

        this.languages = data;
        for (var a = 0; a < data.length; a++) {
          var languagenames = { "id": 2, "itemName": "Singapore" };
          languagenames.id = a;
          languagenames.itemName = data[a].language_name;
          this.dropdownList.push(languagenames);
        }

      },
      error => {

      });



  }

  selectedItems:any = []
  dropdownList:any = [];



  loadLanguages() {
    this.languagesdisplay = []
    this.userService.GetlanguagesdetailsById().subscribe(
      data => {


        this.langsData = data;

        for (let i = 0; i < this.langsData.length; i++) {

          this.languagesdisplay.push(JSON.parse(this.langsData[i].language_name))


        }


      },
      error => {

      });
  }
  loadbloodgroup() {
    this.userService.loadbloodgroups().subscribe(
      data => {
        ;

        this.bloodgroup = data;
      },
      error => {

      });
  }
  loadphysicalstatus() {
    this.userService.loadphysicalstatus().subscribe(
      data => {
        ;

        this.physicalstatus = data;
      },
      error => {

      });
  }
  loadexpyears() {
    this.userService.loadexpyears().subscribe(
      data => {
        ;

        this.expyears = data;
      },
      error => {

      });
  }
  loadexpmonths() {
    this.userService.loadexpmonths().subscribe(
      data => {
        ;

        this.expmonths = data;
      },
      error => {

      });
  }
  loadfresher_or_exp() {
    this.userService.loadfresher_or_exp().subscribe(
      data => {
        ;

        this.status = data;
      },
      error => {

      });
  }


  salary: boolean = false
  aadhar: boolean = false
  pannumber: boolean = false
  passport: boolean = false
  contact: boolean = false
  pref_loc: any = []
  locality1: any = []
  cityList:any=[]
  savedetails(model:any) {
    this.pref_loc = []
    this.locality1 = []


    for (var a = 0; a < this.model2.preferred_location.length; a++) {
      var citynames:any = { "id": 2, "itemName": "Singapore" };
      citynames.id = this.model2.preferred_location[a].id;
      citynames.itemName=this.model2.preferred_location[a].itemName.split(',')[0]
      // citynames.itemName = data[a].city_name+','+data[a].state_name+','+data[a].country_name;
      this.cityList.push(citynames);

    } 
    model.preferred_location = this.cityList
    
    model.locality = this.model2.locality
    if (model.expectedsalary_min != 0 && model.expectedsalary_max != 0 && model.permanent_address) {
      if (eval(model.expectedsalary_min) >= eval(model.expectedsalary_max)) {
        //this.message='';
        //this.tostersuc.error('Max Salary is less than min salary',"hello");
        this.salary = true


      }



      else if ((model.aadhar_no && model.aadhar_no.length <= 11)) {
        // this.tostersuc.error('Aadhar no is not valid',"hello");
        this.aadhar = true
      }

      else if ((model.pan_no && model.pan_no.length <= 9)) {
        //this.tostersuc.error('Pan no is not valid',"hello");
        this.pannumber = true
      }
      else if ((model.passport_no && model.passport_no.length <= 8)) {
        //this.tostersuc.error('Passport no is not valid',"hello");
        this.passport = true
      }
      else if ((model.alternate_contact_number && model.alternate_contact_number.length <= 9)) {
        //this.tostersuc.error('Alternate contact no is not valid',"hello");
        this.contact = true
      }

      else {


        if (model.preferred_location) {
          for (var a = 0; a < model.preferred_location.length; a++) {

            this.pref_loc.push(model.preferred_location[a].id + ':' + model.preferred_location[a].itemName)

          }
        }
        if (model.locality) {
          for (var b = 0; b < model.locality.length; b++) {

            this.locality1.push(model.locality[b].id + ':' + model.locality[b].itemName)

          }
        }

        model.preferred_location = this.pref_loc;
        model.locality = this.locality1;

        if (eval(model.expectedsalary_min) <= eval(model.expectedsalary_max)) {
          this.salary = false
          this.aadhar = false
          this.pannumber = false
          this.passport = false
          this.contact = false
          this.userService.editprofiledetails(model).subscribe(data => {


            this.tostersuc.success('Profile details updated Successfully');

            //this.message='';
            this.cityList=[]


          })
        }
      }

    }

  }

  errmsg1:any = 1
  errmsg2:any = 1
  errmsg:boolean
  language_name:any
  languagesdata:any = { id: '', name: '' }
  l:any = []
  add() {

    this.l = []
    this.errmsg1 = 1;
    this.errmsg2 = 1;
    if (!this.model1.language_name) {
      this.errmsg2 = -1;
    }
    if (!this.model1.is_speak || !this.model1.is_read || !this.model1.is_write) {
      this.errmsg1 = -1;
    }

    if (this.model1.is_speak || this.model1.is_read || this.model1.is_write && this.model1.language_name) {

      for (var a = 0; a < this.model1.language_name.length; a++) {
        this.languagesdata.id = this.model1.language_name[a].id
        this.languagesdata.name = this.model1.language_name[a].itemName
        this.l.push(this.languagesdata);
        this.languagesdata = { id: '', name: '' }

      }
    }
    this.language_name = JSON.stringify(this.l);
    let obj:any

    // if (this.model1.is_speak || this.model1.is_read || this.model1.is_write) {
      if(!this.model1.is_speak){
        
        this.model1.is_speak=false
      }
      if(!this.model1.is_read){
        
        this.model1.is_read=false
      }
      if(!this.model1.is_write){
        
        this.model1.is_write=false
      }
    obj = {
      language_name: this.language_name,
      is_speak: this.model1.is_speak,
      is_read: this.model1.is_read,
      is_write: this.model1.is_write,
      language_id: this.model1.id
    }
    // }

    // else{
    //   this.errmsg=1;
    //     }
    // this.langsData.push(obj);
    if (obj.language_id == "" || obj.language_id == null || obj.language_id == undefined) {
    for (var i = 0; i < this.langsData.length; i++) {
      
      if (obj.language_name && obj.is_speak || obj.is_read || obj.is_write) {
        if (((JSON.parse(obj.language_name)[0].id)) == (JSON.parse(this.langsData[i].language_name)[0].id)) {
          this.tostersuc.error('This language details already added');

          obj = {}
          this.model1 = {};
          this.errmsg1 = 1;
          this.errmsg2 = 1;

        }
      }
    }
  
    
      if (obj.language_name && obj.is_speak || obj.is_read || obj.is_write) {
        this.userService.Addlanguagesdetails(obj).subscribe(
          data => {

            this.tostersuc.success('Language details Successfully added');
            // this.langsData = {};
            // obj={}
            this.loadLanguages();
          },
          error => {

          });

        // this.errmsg=1;
        this.model1 = {};
        // this.errmsg=''
        this.errmsg1 = 1;
        this.errmsg2 = 1;
        }
    } else {
      if (obj.language_name && obj.is_speak || obj.is_read || obj.is_write) {
        this.userService.Editlanguagesdetails(obj).subscribe(
          data => {

            this.tostersuc.success('Language details edited Successfully');
            //this.langsData = {};
            this.loadLanguages();
          },
          error => {

          });
        this.model1 = {};
        // this.errmsg = false;
        this.errmsg1 = 1;
        this.errmsg2 = 1;
      }

    }

  }

  numberchek(event: any) {
    const pattern = /^[0-9]+$/;
  
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }


  keyeventname(name: any) {
    const enamepattern = /^[A-Za-z' '0-9]+$/;
    let inputChar = String.fromCharCode(name.charCode);
    if (name.keyCode != 8 && !enamepattern.test(inputChar)) {
      name.preventDefault();
    }
  }
  panno(event: any) {

    //alert(this.t)

    // const pattern =/^[A-Z0-9.,/ $@()]+$/;
    const pattern = /[A-Za-z0-9]/;




    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  loadfunctionalarea() {
    this.LoginAuthService.loadfunctinalarea()
      .subscribe(
        data => {


          this.fnarealists = data;

        },
        error => {
        });
  }
  editdetails(model:any) {


    if (model.length > 0) {


      this.model.resume_headline = model[0].resume_headline
      this.model.hometown = model[0].hometown
      this.model.passport_no = model[0].passport_no
      this.model.aadhar_no = model[0].aadhar_no
      this.model.pan_no = model[0].pan_no
      this.model.marital_status = model[0].marital_status
      this.model.permanent_address = model[0].permanent_address
      this.model.pin_code = model[0].pin_code
      this.model.prefered_jobtype = model[0].prefered_jobtype
      this.model.currentsalary = model[0].currentsalary


      this.model.expectedsalary_min = model[0].expectedsalary_min
      this.model.expectedsalary_max = model[0].expectedsalary_max
      this.model.employment_type = model[0].employment_type
      this.model.desired_shift = model[0].desired_shift
      this.model.role = model[0].role
      this.model.date_of_birth = model[0].date_of_birth

    }
  }

  datecheck(event:any) {


    const pattern = /^(((0[1-9]|[12][0-9]|30)[-/]?(0[13-9]|1[012])|31[-/]?(0[13578]|1[02])|(0[1-9]|1[0-9]|2[0-8])[-/]?02)[-/]?[0-9]{4}|29[-/]?02[-/]?([0-9]{2}(([2468][048]|[02468][48])|[13579][26])|([13579][26]|[02468][048]|0[0-9]|1[0-6])00))$/;
    let inputChar = String.fromCharCode(event.charCode)
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }
  /* ================================================= */
  currentsal(event: any) {

    const pattern = /^[0-9]+$/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
    if (event.keyCode == 32 && this.model.currentsalary.trim('').length == 0) {
      event.preventDefault();
    }
  }

  Hometownevent(name: any) {
    const enamepattern = /^[A-Za-z' ']+$/;
    let inputChar = String.fromCharCode(name.charCode);
    if (name.keyCode != 8 && !enamepattern.test(inputChar)) {
      name.preventDefault();
    }
  }

  expminsal(event: any) {

    const pattern = /^[0-9]+$/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
    if (event.keyCode == 32 && this.model.currentmodel.expectedsalary_min.trim('').length == 0) {
      event.preventDefault();
    }
  }
  titleCheck(event:any) {
    //

    const pattern = /^[A-Za-z' ']+$/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
    if (event.keyCode == 32 && this.model.hometown.trim('').length == 0) {
      event.preventDefault();
    }
  }

  expmaxsal(event: any) {

    const pattern = /^[0-9]+$/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
    if (event.keyCode == 32 && this.model.expectedsalary_max.trim('').length == 0) {
      event.preventDefault();
    }
  }



  edit(row:any) {
    this.languagedata1 = []
    //if(row.lang != null|| undefined){

    this.model1.is_speak = row.is_speak;
    this.model1.is_read = row.is_read;
    this.model1.is_write = row.is_write,
      this.model1.id = row.language_id
    var languages:any = JSON.parse(row.language_name)
    var languagenames:any = { id: '', itemName: '' }

    for (let index = 0; index < languages.length; index++) {

      languagenames.id = languages[index].id
      languagenames.itemName = languages[index].name
      this.languagedata1.push(languagenames)
      languagenames = { id: '', itemName: '' }

      this.model1.language_name = this.languagedata1;
    }


  }


  deletelang:any
  delete(row:any, template:any) {

    this.deletelang = row

    this.modalRef = this.modalService.show(
      template
    );

  }
  confirmation(res_confirm: boolean) {
    if (res_confirm) {
      this.userService.DeletelanguagesdetailsById(this.deletelang.language_id).subscribe(
        data => {

          this.tostersuc.success('Language details deleted Successfully');

          this.modalRef.hide()
          //  this.langsData = [];
          this.loadLanguages();

        },
        error => {

        });

    }
    else {
      this.modalRef.hide()
    }

  }


}
