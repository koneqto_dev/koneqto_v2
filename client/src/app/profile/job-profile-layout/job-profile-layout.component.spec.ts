import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobProfileLayoutComponent } from './job-profile-layout.component';

describe('JobProfileLayoutComponent', () => {
  let component: JobProfileLayoutComponent;
  let fixture: ComponentFixture<JobProfileLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobProfileLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobProfileLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
