import { Component, OnInit, TemplateRef } from '@angular/core';
import { AuthenticationService, UserService, JobpostService } from '../../services/index';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs/Rx';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { environment } from '../../../environments/environment';


@Component({
  selector: 'app-my-advices',
  templateUrl: './my-advices.component.html',
  styleUrls: ['./my-advices.component.css']
})
export class MyAdvicesComponent implements OnInit {

  modalRef: BsModalRef;


  alladvice : any= []
  adviceoptions: any;
  jobposting: any = [];
  pagesize: any = 0;
  pagelength: any = 10;
  psize : any= 0;
  plength : any= this.pagelength;
  modalScrollDistance: any = 2;
  modalScrollDistanceUp: any = 3;
  modalScrollThrottle : any= 50;
  modalText: any;
  modalBody = [this.modalText];
  selector = null;

  // editexpadvice: any = {};
  constructor(private jobpostService: JobpostService, private userService: UserService, private spinnerService: Ng4LoadingSpinnerService, private modalService: BsModalService,private toasterservice: ToastrService, ) { }
  fnarealist2: Observable<any[]>;
  adviclist2: Observable<any[]>;
  advicemessage: any
  functionalarea:any
  adviceoption:any
  expertadvice:any={}
  Apiurl = environment.Apiurl
  openmodaladvice(editadvice: TemplateRef<any>,ad: any) {
    //this.modalRef = this.modalService.show(template);
    
    this.expertadvice.advicemessage = ad.advice_message
    this.expertadvice.functionalarea=ad.functional_area
    this.expertadvice.adviceoption=ad.advice_options
    this.expertadvice.id=ad.id;

    //this.editexpadvice = {};
    // var eventtype = JSON.parse(ad.event_type)
    // var eventtypenames = { id: '', itemName: '' }
    // for (let index = 0; index < eventtype.length; index++) {
    //   
    //   eventtypenames.id = eventtype[index].id
    //   eventtypenames.itemName = eventtype[index].name
    //   this.ad.push(eventtypenames)
    //   eventtypenames = { id: '', itemName: '' }
    //   
    //   this.postevent.eventtype = this.eventtype1;
    // }
    this.modalRef = this.modalService.show(editadvice,
      Object.assign({}, { class: 'gray modal-lg' })

    );

    // this.adviceOptions();
   
  }
  editorConfig = {
    "editable": true,
    "spellcheck": false,
    "height": "auto",
    "minHeight": "0",
    "width": "auto",
    "minWidth": "0",
    "translate": "yes",
    "enableToolbar": true,
    "showToolbar": true,
    "placeholder": "",
    // "imageEndPoint": "",
    "toolbar": [
      ["bold", "italic", "underline", "unorderedList"]
      //  "strikeThrough", "superscript", "subscript"],
      // ["fontName", "fontSize", "color"],
      // ["justifyLeft", "justifyCenter", "justifyRight", "justifyFull", "indent", "outdent"],
      // ["cut", "copy", "delete", "removeFormat", "undo", "redo"],
      // ["paragraph", "blockquote", "removeBlockquote", "horizontalLine", "orderedList", "unorderedList"],
      // ["link", "unlink", "image", "video"]
    ]
  }



  // edit expert advice
  edit(editexpadvice:any) {

  }
  expadvice:any
  expadviceall:any={}
  advice_msg:boolean=false
  postadvice2(expertadvice:any) {

    if (expertadvice.adviceoption && expertadvice.functionalarea && expertadvice.advicemessage) {
      this.expadviceall = {
        advice_options: expertadvice.adviceoption,
        functional_area: expertadvice.functionalarea,
        advice_message: expertadvice.advicemessage.trim(),
        id:expertadvice.id
      }
      if (!this.expadviceall.advice_message) {
        this.advice_msg = true;
        // this.ToastrService.error('Aadhar no is not valid', "hello");
      }
      if (this.expadviceall.advice_message) {
        this.jobpostService.updateAdvice(this.expadviceall).subscribe(
          (data) => {
            //
            // this.ToastrService.success('Advice sucess full!', 'Expert Advice!');
            //resetting fields
            // this.expadvice.adviceoption = '';
            // this.expadvice.functionalarea = '';
            // this.expadvice.advicemessage = '';
            // this.router.navigate(["home/advice"])
      
            
           

            this.modalRef.hide();
            this.toasterservice.success('Advice Updated SuccessFully!', 'Advice Updated!');
            this.loadadvice()

          })
      }
    }
    else {
      error => {
           };
    }

  }

  closeModalAdvicePosting(template2: TemplateRef<any>) {
    this.modalRef.hide();

  }

  // delete expert advice
  id:any
  deleteadvice(id:any,template) {
   // this.jobpostService.deleteexpertAdviceonly(id).subscribe(
      //data => {
        this.id=id
        this.modalRef = this.modalService.show(
          template
        );
      

        this.loadadvice()
    



  }
confirmation(res_confirm: boolean) {
  if (res_confirm) {
    this.jobpostService.deleteexpertAdviceonly(this.id).subscribe(
      data => {

        this.toasterservice.success('Advice Deleted SuccessFully!', 'Advice DELETED!');
        this.modalRef.hide()

        this.loadadvice()

      },
      error => {

      });

  }
  else {
    this.modalRef.hide()
  }

/////////////////////////////
}


  loadadvice() {
    
    this.jobpostService.getexpertAdviceonly().subscribe(
      data => {
        this.alladvice = data
        // for (let index = 0; index < this.alladvice.length; index++) {
         
        //   this.alladvice[index].advice_message=this.alladvice[index].advice_message.split(" ")
        // }
       
        // debugger
     }
    );

  }


  adviceOptions() {
    this.userService.getadviceoptionsdata().subscribe(data => {
      
      this.adviclist2 = data;
      });

  }



  ngOnInit() {

    this.loadadvice();
    this.adviceOptions();
    
    this.userService.getadviceoptionsdata().subscribe(data => {
      
      this.adviclist2 = data;
    });

    this.jobpostService.getfunctinalareadata().subscribe(data => {
      
      this.fnarealist2 = data;
     });


  }



}
