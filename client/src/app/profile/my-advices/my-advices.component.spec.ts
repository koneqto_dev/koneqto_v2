import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyAdvicesComponent } from './my-advices.component';

describe('MyAdvicesComponent', () => {
  let component: MyAdvicesComponent;
  let fixture: ComponentFixture<MyAdvicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyAdvicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyAdvicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
