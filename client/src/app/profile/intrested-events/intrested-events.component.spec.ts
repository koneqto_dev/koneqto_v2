import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntrestedEventsComponent } from './intrested-events.component';

describe('IntrestedEventsComponent', () => {
  let component: IntrestedEventsComponent;
  let fixture: ComponentFixture<IntrestedEventsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntrestedEventsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntrestedEventsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
