import { Component, OnInit, TemplateRef } from '@angular/core';import { AuthenticationService, UserService, JobpostService } from '../../services/index';
import { ToastrService } from 'ngx-toastr';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { SimpleCrypt } from "ngx-simple-crypt";
import { environment } from '../../../environments/environment';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
  selector: 'app-intrested-events',
  templateUrl: './intrested-events.component.html',
  styleUrls: ['./intrested-events.component.css']
})
export class IntrestedEventsComponent implements OnInit {

  appliedeventslist: any = []
  reg_id: any;
  modalRefPost: BsModalRef;
  modalRef: BsModalRef;
  
  user_id: any;
  fnarealist:any;
  simpleCrypt = new SimpleCrypt();
  userId = localStorage.getItem('loginuserid')
  loginuserid = this.simpleCrypt.decode("my-key", this.userId);
  //user_reg_id=new SimpleCrypt().decode("my-key",localStorage.getItem('loginuserid'))

  //ael: any;
  constructor(private modalService: BsModalService,private jobpostService: JobpostService, private toastrService: ToastrService, private userService: UserService, private spinnerService: Ng4LoadingSpinnerService) {
    this.user_id = eval(this.loginuserid);
  }

  Apiurl = environment.Apiurl
  //Getting interestedinevents
  event_loc: any = [];
  event_typ: any = [];
  event_local: any = [];
  event_typ_array:any=[];
  event_typ1:any=[];
  event_loc_array:any=[];
  event_local_array:any=[]
  getappliedevents() {
    
    this.jobpostService.getappliedevents()
      .subscribe(
        data => {
          this.event_loc = [];
          this.event_typ = [];
          this.event_local = [];
          this.event_typ_array=[];
          this.event_typ1=[];
          this.event_loc_array=[];
          this.event_local_array=[]
          this.appliedeventslist = data;
          // for (let i = 0; i < this.appliedeventslist.length; i++) {
          //   this.event_typ.push(JSON.parse(this.appliedeventslist[i].event_type))
         
          // }
          for (let i: any = 0; i < this.appliedeventslist.length; i++) {
            //this.event_typ.push(JSON.parse(this.mysavedeventslist[i].event_type))
            this.event_typ_array.push(this.appliedeventslist[i].event_type[0].split(':'))
            this.event_typ1.push(this.event_typ_array[i][1])
            
          }

          // for (let j = 0; j < this.appliedeventslist.length; j++) {
          //   this.event_loc.push(JSON.parse(this.appliedeventslist[j].event_location))
           
          // }
          for (let j : any = 0; j < this.appliedeventslist.length; j++) {
            //this.event_loc.push(JSON.parse(this.mysavedeventslist[j].event_location))
            this.event_loc_array.push(this.appliedeventslist[j].event_location[0].split(':'))
            this.event_loc.push(this.event_loc_array[j][1])
            
          }

          // for (let k = 0; k < this.appliedeventslist.length; k++) {
          //   this.event_local.push(JSON.parse(this.appliedeventslist[k].event_locality))
          
          // }
          for (let k : any = 0; k < this.appliedeventslist.length; k++) {
  
            if (this.appliedeventslist[k].event_locality) {
              this.event_local_array.push(this.appliedeventslist[k].event_locality[0].split(':'))
              this.event_local.push(this.event_local_array[k][1])
              
            }
          }
        },
        error => {
         
        });
  }

  //uninterested in event click
  uninterestdata: any = {}
  uninterestedinevent(allsavedeventslist:any) {
   
    this.jobpostService.uninterestedinevent(allsavedeventslist)
      .subscribe(
        data => {
         
          this.uninterestdata = data;
       
          this.getappliedevents();
         
          this.toastrService.info('You are Uninterested in this Event!', 'Uninterested!', { "closeButton": true });
        },
        error => {
         
        });
  }


  ngOnInit() {
    this.getappliedevents();
  }

  
}
