import { Component, OnInit } from '@angular/core';
import {UserService} from '../../services/index';
import { SimpleCrypt } from "ngx-simple-crypt";


//prudhvi
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-company-details',
  templateUrl: './company-details.component.html',
  styleUrls: ['./company-details.component.css']
})
export class CompanyDetailsComponent implements OnInit {

  
  org:any={}
 fnarealis:any;
 companydetails:any=[];
  establishedyear: any;
  registartiondetails:any=[];
  constructor(private userservice:UserService,private tostersuc: ToastrService) { }
 
  saveorg(org: any){
   debugger
    
    this.userservice.orgdetails(org).subscribe(
      data => {
         } )
         this.tostersuc.success('Company details updated Successfully');

  
  }
  numbercheck(event: any) {


    const pattern = /^[0-9]/; 
    let inputChar = String.fromCharCode(event.charCode)
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();


    }
  }

  noOfEmployeeMsg: boolean;
  c(number_ofemployees: string) {
    if (number_ofemployees.split('')[0] == '0') {
      this.noOfEmployeeMsg = true;
    }
    else {
      this.noOfEmployeeMsg = false;
    }
  }



  
  address(event: any) {

    //alert(this.t)

    // const pattern =/^[A-Z0-9.,/ $@()]+$/;
    const pattern = /[A-Za-z0-9]/;




    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }
  ngOnInit(){
    var simpleCrypt = new SimpleCrypt();
    var userId = localStorage.getItem('loginuserid')
    var loginuserid = simpleCrypt.decode("my-key", userId);

    this.userservice.getregistartiondetails(loginuserid)
    .subscribe(
      data => {
        
        this.registartiondetails = data;

    
        
        
     
      },
      error => {
      });

    this.userservice.getfunctinalareadata()
    .subscribe(
      data => {
        
        this.fnarealis = data;
     
      },
      error => {
      });
    this.userservice.displaycompanydetails()
   .subscribe(
     data => {
       
       this.companydetails = data;
      //  console.log(this.companydetails,"this.companydetails")
    
       this.org.current_company=this.registartiondetails[0].company_name;
       if(this.companydetails.length>0){
       this.org.overview_company=this.companydetails[0].overview_company;
       this.org.established_year=this.companydetails[0].established_year;
       this.org.industry_type=this.companydetails[0].industry_type;
       this.org.type_company=this.companydetails[0].type_company;
       this.org.ceo_directors_cfo=this.companydetails[0].ceo_directors_cfo;
       this.org.number_ofemployees=this.companydetails[0].number_ofemployees;
       this.org.financial_info=this.companydetails[0].financial_info;
       this.org.office_address=this.companydetails[0].office_address;
       this.org.locations_otherplaces=this.companydetails[0].locations_otherplaces;
       this.org.cmmi_level=this.companydetails[0].cmmi_level;
       this.org.prestigious_projects=this.companydetails[0].prestigious_projects;
       this.org.awards_recognitions=this.companydetails[0].awards_recognitions;
       this.org.cetifications=this.companydetails[0].cetifications;
       this.org.client=this.companydetails[0].client;
       this.org.any_participations_globally=this.companydetails[0].any_participations_globally;
       }
     
     },
     error => {
     });
}
}