import { Component, OnInit } from '@angular/core';
import { FileUploadModule, FileUploader} from "ng2-file-upload";
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery';
import { UserService} from '../../services/index';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../../environments/environment';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { SimpleCrypt } from "ngx-simple-crypt";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-my-gallery',
  templateUrl: './my-gallery.component.html',
  styleUrls: ['./my-gallery.component.css']
})
export class MyGalleryComponent implements OnInit {

  galleryOptions: NgxGalleryOptions[];
  // galleryActions:NgxGalleryAction[]
    modalRef: BsModalRef;
    isrecruter: boolean = false;
    RoleID
  constructor(public userservice: UserService,private spinnerService: Ng4LoadingSpinnerService,private modalService: BsModalService,private tostersuc: ToastrService) { }
  simpleCrypt = new SimpleCrypt();
  Apiurl = environment.Apiurl
  public uploader:FileUploader = new FileUploader({url: environment.Apiurl + '/profile/galleryuploads/?AuthToken=' + localStorage.getItem('AuthToken'), allowedMimeType: [ 'image/jpeg','image/png','image/jpg']});
  ngOnInit() {
    var rollId1 = localStorage.getItem('RoleID')
    var roleID = this.simpleCrypt.decode("my-key", rollId1);
    this.RoleID = eval(roleID);
    if (this.RoleID == 5) {
      this.isrecruter = true;
    }
    this.loadGallery()
  }

  errmsg:boolean
  name:any;
  description:any
  image:any
  id:any
  imageType: string
  errImageType: boolean
  check(file:any) {
    this.imageType = file.target.files[0].type
    if (this.imageType == "image/jpeg" || this.imageType == "image/bmp" || this.imageType == "image/png" || this.imageType == "image/gif") {
      this.errImageType = false;
      this.imageType = ""
      this.uploader.uploadAll()
    }
    else {
      this.errImageType = true;
      this.imageType = ""

    }
  }

  imageChangedEvent:any
  galleryimages:any = []
  imageurl:any
  images_array: any = []
  achidata_preview:any=[]
  filestype: any=[];
  jobimage: { type: string; img_data: string; };
  achidata: any=[];
  onFileSelected(event: any) {
    if(event.target.files.length<=6)
    {
 
    for(var i=0;i<event.target.files.length;i++){
      this.filestype.push(event.target.files[i].type.split("/")[1])
      }
    for(var i=0;i<event.target.files.length;i++){
      this.jobimage = {
        type: '',
        img_data: ''
      }
  var imageType:any = event.target.files[i].type
 this.jobimage.type=imageType.replace("image/",".")

    // if (event.target.files.length) {
      if(imageType=="image/jpeg" || imageType=="image/jpg" || imageType=="image/png" || imageType=="image/gif" || imageType=="image/bmp")
      {
        var reader:any = new FileReader();
        reader.readAsDataURL(event.target.files[i]); // read file as data url
        reader.onload = (ev: any) => { // called once readAsDataURL is completed
          this.jobimage.img_data = ev.target.result;
          this.achidata_preview.push(this.jobimage.img_data)
       this.jobimage.img_data =  this.jobimage.img_data.replace("data:image/jpeg;base64,", "");
         this.jobimage.img_data =  this.jobimage.img_data.replace("data:image/png;base64,", "");
         this.jobimage.img_data =  this.jobimage.img_data.replace("data:image/bmp;base64,", "");
         this.jobimage.img_data =  this.jobimage.img_data.replace("data:image/jpg;base64,", "");
         this.jobimage.img_data =  this.jobimage.img_data.replace("data:image/gif;base64,", "");

        
         this.achidata.push(this.jobimage.img_data)
         
        this.errImageType=false
        }}
      else{
        // this.tostersuc.error('Please select images only');
        this.errImageType=true
      }}
      }
      else{
        this.tostersuc.error('You can only select maximum of 6 images ');
        this.image=''
      }
      // }
    }
  removeimage(i:number)
  {
   
   this.achidata.splice(i,1)
   this.achidata_preview.splice(i,1)
  }
  savegallery(){
    
  
  
  
    let gallery:any={
    name:this.name,
    image:this.achidata,
    id:this.id,
    filetype:this.filestype
    }
   
   if(gallery.id ==""|| gallery.id == null || gallery.id == undefined){
     
   if(gallery.name && gallery.image.length>0 ){
    this.spinnerService.show();
    this.userservice.Addgallery(gallery).subscribe(
      data => {
        this.achidata=[]
        this.name=''
        this.filestype=[]
        this.image=''
        this.loadGallery()
        this.spinnerService.hide();
        this.tostersuc.success(gallery.name+' folder added successfully');
        this.achidata_preview=[]
        this.uploader.queue=[]
        //this.processedImage=''
      
      },
      error => {
  
      });
      this.errmsg=false;
  }
  else{
  this.errmsg=true
  }
  }
  
  }

  deletegallery:any
  delete(row:any,template:any) {
    
    this.deletegallery=row
    
    this.modalRef = this.modalService.show(
      template
    );
  
  }
  confirmation(res_confirm:boolean)
  
  
  {
    if(res_confirm)
    {
      this.userservice.Deletegallery(this.deletegallery.id,this.deletegallery.image).subscribe(
        data => {
          
          this.tostersuc.success(this.deletegallery.name+' folder images deleted  successfully');
          this.modalRef.hide()
          this.loadGallery()
      
  
        },
        error => {
       
        });
  
    }
    else{
      this.modalRef.hide()
    }
   
  }

  remove(b:number){
    for(var c=0;c<b; c++){
      this.achidata.splice(c,b)
      this.achidata_preview.splice(c,b)

    }
  }
  
Gallery:any;
picsArr:any;
picsstring:any=[];
Imagejson:any={};
imagesArr1:any=[];
galleryAction:any

  loadGallery(){
    this.spinnerService.show();
    this.imagesArr1=[]
   this.userservice.GetGallery().subscribe(
     data => {
       this.Gallery=data;
       this.spinnerService.hide();
   
    for(var j=0;j<this.Gallery.length;j++){
     // let imagesArr:any=[];
     let imagesArr:any=[];
    this.picsArr=this.Gallery[j].image;

      //  for(var i=0;i<this.picsArr.length-1;i++){
       
       
      //    this.picsstring[j]=this.picsArr.split(' ')
           
      //   }
if(this.picsArr.length>0){
       for(var k=0;k<this.picsArr.length;k++){
        
       this.Imagejson = 
            {
              // small :'./assets/achievements/file-1535346857656.jpg',
                small: this.Apiurl+'/mygallery/'+this.picsArr[k],
                medium: this.Apiurl+'/mygallery/'+this.picsArr[k],
                big: this.Apiurl+'/mygallery/'+this.picsArr[k]
            }
           
            imagesArr.push(this.Imagejson);
       }
      }
      
       this.imagesArr1.push(imagesArr)
 

     }
     this.galleryOptions = [
      {
          width: '500px',
          height: '400px',
          thumbnailsColumns: 4,
          imageAnimation: NgxGalleryAnimation.Slide,
          closeIcon:'fa fa-times-circle',
          previewCloseOnClick:true
      },
      // max-width 800
      {
          breakpoint: 800,
          width: '100%',
          height: '600px',
          imagePercent: 80,
          thumbnailsPercent: 20,
          thumbnailsMargin: 20,
          thumbnailMargin: 20,
          
      },
      // max-width 400
      {
          breakpoint: 400,
          preview: false
      }
  ];
  
     },
     error => {

     });    
 }

}

