import { Component, OnInit, TemplateRef, ViewChild,ElementRef } from '@angular/core';
import { UserService, JobpostService, LoginAuthService } from '../../services/index';
import { ToastrService } from 'ngx-toastr';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { interview } from '../../_models/user'
import { BsDatepickerDirective } from 'ngx-bootstrap/datepicker';
import { ratingService } from '../../services/index';
import { ImageCompressService, ResizeOptions } from 'ng2-image-compress';
import * as jspdf from 'jspdf';
import html2canvas from 'html2canvas';
import { FileUploadModule, FileUploader } from "ng2-file-upload";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { environment } from '../../../environments/environment';
//import { Date } from 'core-js';
import { SimpleCrypt } from "ngx-simple-crypt";
// import { integer } from 'aws-sdk/clients/cloudfront';
//import { timingSafeEqual } from 'crypto';
import { ActivatedRoute } from '@angular/router';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery';


@Component({
  selector: 'app-my-job-posts',
  templateUrl: './my-job-posts.component.html',
  styleUrls: ['./my-job-posts.component.css']
})
export class MyJobPostsComponent implements OnInit {
  loginusername: any = localStorage.getItem('loginusername')
  jobposting: any = []
  applied: any = {}
  interview: any
  interviewmode: any = [];
  interviewtype: any = [];
  max: any = 10;
  rate: number = 7;
  isReadonly: boolean = false;
  x: number = 5;
  y: number = 2;
  z: number = 8;
  isCollapsed : any= false;
  temp: any = [];
  jobpost: any = []
  today = new Date()
  dateMin: any = this.today.getFullYear() + "/" + this.today.getMonth() + "/" + (this.today.getDate())

  dateArr: any = this.dateMin.split("/")
  min = new Date(this.dateArr[0], this.dateArr[1], this.dateArr[2])
  mintime : any= new Date()
  timeMin: any = this.mintime.getTime();

  pagesize : any= 0;
  pagelength: any = 10;
  psize : any= 0;
  plength: any = this.pagelength;


  constructor(private jobpostingservice: JobpostService, private userService: UserService,
    private tostservice: ToastrService, private modalService: BsModalService, private ratingservice: ratingService, private imgCompressService: ImageCompressService, private spinnerService: Ng4LoadingSpinnerService, private route: ActivatedRoute,private LoginAuthService: LoginAuthService) {
    this.interview = new interview()

  }

  Apiurl : any= environment.Apiurl
  skills: any
  states: any
  fnarealist: any
  year: any
  month: any
  jobtype: any
  profile_img: any
  imgsrc: any
  courses: any
  salaryl: any
  salaryt: any
  images: any = []
  notice: any
  keyskills: any
  startDate: any
  phystatus: any
  dropdownList: any = []
  skillDisplayName: any = []
  skill : any= []
  skillopt : any= []
  educational_details: any = []
  sub: any
  userIdred: any
  jobidred: any
  public iuploader: FileUploader = new FileUploader({ url: environment.Apiurl + '/jobposting/ijobposts/?AuthToken=' + localStorage.getItem('AuthToken') });
  public vuploader: FileUploader = new FileUploader({ url: environment.Apiurl + '/jobposting/vjobposts/?AuthToken=' + localStorage.getItem('AuthToken') });
event1:any
getskills2(event: any){
    this.dropdownList=[]
    // this.dropdownList1=[]
    if(event.target.value){
      this. event1=event.target.value
    }else{
       this.event1=''
    }
 
    this.jobpostingservice.getSkills2(this.functionalid, event.target.value).subscribe(data => {

      this.dropdownList = []
      for (var a = 0; a < data.length; a++) {

        var skilldata : any= { "id": 2, "itemName": "Singapore" };
        skilldata.id = data[a].skill_id;
        skilldata.itemName = data[a].skill_name;
        this.dropdownList.push(skilldata);
      }

    });
  }
count:any
picsArr: any;
ApiUrl: any = environment.Apiurl
Imagejson:any={}
imagesArr1: any=[]
galleryOptions: NgxGalleryOptions[];
@ViewChild('content') content: ElementRef
confirm: BsModalRef
// confirmtemplate=confirm

confirmtemplate: TemplateRef<any>
@ViewChild('confirmtemplate') ctemplate: ElementRef;
handleLoad(template) {
  this.ctemplate
  this.modalRef = this.modalService.show(
    this.ctemplate,
    Object.assign({
      backdrop: 'static',
      keyboard: false
    }, { class: 'gray modal-md' })
  );
}
loginuserid1:any
  ngOnInit() {

    // this.userService.getprofiledetails(this.loginuserid1).subscribe(data => {

    //   this.result = data[0];
    //   this.skillsarraydisplay = []

    //   this.userService.getlogincount(this.loginuserid1).subscribe(data1 => {

    //     this.count = data1;

    //     if (this.count[0].lastlogin_time % 10 == 0) {

    //       if (this.result.role == 3) {
    //         if (this.result.skill_details == null || this.result.educational_details == null) {
    //           this.handleLoad(this.ctemplate)
    //         }
    //       }
    //       else {
    //         if (this.result.presentemp_details == null || this.result.skill_details == null || this.result.educational_details == null) {
    //           this.handleLoad(this.ctemplate)
    //         }
    //       }
    //     }

    //   })



    //   if (this.result.presentemp_details != null) {
    //     for (let i = 0; i < this.result.presentemp_details.length; i++) {
    //       this.presentdesignation.push(this.result.presentemp_details[i].designation.split(':')[1])

    //     }
    //   }

    //   if (this.result.previousemp_details != null) {
    //     for (let i = 0; i < this.result.previousemp_details.length; i++) {
    //       this.previousdesignation.push(this.result.previousemp_details[i].designation.split(':')[1])

    //     }
    //   }
    //   if (this.result.project_details) {

    //     for (let j = 0; j < this.result.project_details.length; j++) {
    //       var splitarray: any = []
    //       if (this.result.project_details[j].skills) {
    //         for (var i = 0; i < this.result.project_details[j].skills.length; i++) {
    //           splitarray.push(this.result.project_details[j].skills[i].split(':'))
    //         }
    //       }
    //       this.proskillsarray.push(splitarray)
    //     }
    //     for (let i = 0; i < this.proskillsarray.length; i++) {
    //       var display = []
    //       let j = 0
    //       while (j < this.proskillsarray[i].length) {

    //         let sep = ''
    //         if (this.proskillsarray[i][j + 1]) {
    //           sep = ','
    //         }
    //         display.push(this.proskillsarray[i][j][1] + sep)
    //         j++

    //       }
    //       this.proskillsarraydisplay.push(display)

    //     }
    //     // for (let i = 0; i < this.result.project_details.length; i++) {

    //     //   this.skill.push(JSON.parse(this.result.project_details[i].skills))

    //     // }
    //   }

    //   if (this.result.skill_details) {
    //     for (let j = 0; j < this.result.skill_details.length; j++) {
    //       this.skilldetails.push(this.result.skill_details[j][0].split(':'))
    //     }
    //   }
    //   let count = 0
    //   // for(let i=0;i<this.skilldetails.length;i++){
    //   while (count < this.skilldetails.length) {


    //     let sep = ''
    //     if (this.skilldetails[count + 1]) {
    //       sep = ','
    //     }
    //     // display.push(this.skilldetails[i][1]+sep)         
    //     this.skillsarraydisplay.push(this.skilldetails[count][1] + sep)
    //     // sep=''
    //     count++
    //   }
    //   //-------------------------------------------------------------------------------------------------------------------

    //   if (this.result.achievement_details != null) {
    //     for (var j = 0; j < this.result.achievement_details.length; j++) {
    //       this.picsArr = this.result.achievement_details[j].image;
    //       // if (this.picsArr != null) {
    //       //   for (var i = 0; i < this.picsArr.length - 1; i++) {
    //       //     this.picsstring[j] = this.picsArr.split(' ')

    //       //   }
    //       // }

    //     }

    //   }

    //   if (this.result.achievement_details != null) {
    //     for (var j = 0; j < this.result.achievement_details.length; j++) {
    //       // let imagesArr:any=[];
    //       let imagesArr: any = [];
    //       this.picsArr = this.result.achievement_details[j].image;

    //       // if (this.picsArr != null) {
    //       //   for (var i = 0; i < this.picsArr.length - 1; i++) {


    //       //     this.picsstring[j] = this.picsArr.split(' ')

    //       //   }

    //       // }

    //       if (this.picsArr[j] != null) {
    //         for (var k = 0; k < this.picsArr[j].length; k++) {


    //           this.Imagejson =
    //             {
    //               small: this.ApiUrl + '/achievements/' + this.picsArr[k],
    //               medium: this.ApiUrl + '/achievements/' + this.picsArr[k],
    //               big: this.ApiUrl + '/achievements/' + this.picsArr[k]
    //             }

    //           imagesArr.push(this.Imagejson);
    //         }
    //       }
    //       //}
    //       this.imagesArr1.push(imagesArr)
    //     }
    //   }
    //   this.galleryOptions = [
    //     {
    //       width: '400px',
    //       height: '300px',
    //       thumbnailsColumns: 4,
    //       imageAnimation: NgxGalleryAnimation.Slide
    //     },
    //     // max-width 800
    //     {
    //       breakpoint: 800,
    //       width: '100%',
    //       height: '600px',
    //       imagePercent: 80,
    //       thumbnailsPercent: 20,
    //       thumbnailsMargin: 20,
    //       thumbnailMargin: 20
    //     },
    //     // max-width 400
    //     {
    //       breakpoint: 400,
    //       preview: false
    //     }
    //   ];
    //   if (this.result.language_known) {
    //     for (let i = 0; i < this.result.language_known.length; i++) {
    //       this.languagesdisplay.push(JSON.parse(this.result.language_known[i].language_name))
    //     }
    //   }
    //   //university_or_institute
    //   //   if(this.result.grad_details){
    //   //   for (let i = 0; i < this.result.grad_details.length; i++) {
    //   //     this.graduniversity.push(JSON.parse(this.result.grad_details[i].university_or_institute)[0].name)
    //   //   }
    //   // }
    //   if (this.result.grad_details) {

    //     for (let j = 0; j < this.result.grad_details.length; j++) {
    //       var splitarray: any = []
    //       if (this.result.grad_details[j].university_or_institute) {
    //         for (var i = 0; i < this.result.grad_details[j].university_or_institute.length; i++) {
    //           splitarray.push(this.result.grad_details[j].university_or_institute[i].split(':'))

    //         }
    //       }
    //       this.gradsarray.push(splitarray)

    //     }
    //     for (let i = 0; i < this.gradsarray.length; i++) {
    //       var display = []
    //       let j = 0
    //       while (j < this.gradsarray[i].length) {

    //         let sep = ''
    //         if (this.gradsarray[i][j + 1]) {
    //           sep = ','
    //         }
    //         display.push(this.gradsarray[i][j][1] + sep)
    //         j++

    //       }
    //       this.graduniversity.push(display)

    //     }
    //     // for (let i = 0; i < this.result.project_details.length; i++) {

    //     //   this.skill.push(JSON.parse(this.result.project_details[i].skills))

    //     // }
    //   }
    //   if (this.result.pg_details) {
    //     for (let j = 0; j < this.result.pg_details.length; j++) {
    //       var splitarray: any = []
    //       if (this.result.pg_details[j].university_or_institute) {
    //         for (var i = 0; i < this.result.pg_details[j].university_or_institute.length; i++) {
    //           splitarray.push(this.result.pg_details[j].university_or_institute[i].split(':'))

    //         }
    //       }
    //       this.pgarray.push(splitarray)

    //     }
    //     for (let i = 0; i < this.pgarray.length; i++) {
    //       var display = []
    //       let j = 0
    //       while (j < this.pgarray[i].length) {

    //         let sep = ''
    //         if (this.pgarray[i][j + 1]) {
    //           sep = ','
    //         }
    //         display.push(this.pgarray[i][j][1] + sep)
    //         j++

    //       }
    //       this.pguniversity.push(display)

    //     }
    //   }
    //   if (this.result.phd_details) {
    //     for (let j = 0; j < this.result.phd_details.length; j++) {
    //       var splitarray: any = []
    //       if (this.result.phd_details[j].university_or_institute) {
    //         for (var i = 0; i < this.result.phd_details[j].university_or_institute.length; i++) {
    //           splitarray.push(this.result.phd_details[j].university_or_institute[i].split(':'))

    //         }
    //       }
    //       this.phdarray.push(splitarray)

    //     }
    //     for (let i = 0; i < this.phdarray.length; i++) {
    //       var display = []
    //       let j = 0
    //       while (j < this.phdarray[i].length) {

    //         let sep = ''
    //         if (this.phdarray[i][j + 1]) {
    //           sep = ','
    //         }
    //         display.push(this.phdarray[i][j][1] + sep)
    //         j++

    //       }
    //       this.phduniversity.push(display)

    //     }
    //   }

    //   if (this.result.current_designation) {

    //     // let objson = JSON.parse(this.result.current_designation)

    //     // for (let i = 0; i < objson.length; i++) {
    //       this.cur_des.push(this.result.current_designation.split(':')[1])
    //     // }
    //   }
    // })


    this.sub = this.route

      .queryParams
      .subscribe(params => {
        // Defaults to 0 if no query param provided.
        this.userIdred = +params['uid'] || 0;
        this.jobidred = +params['pid'] || 0;
        
        this.mypost(this.userIdred, this.jobidred,this.name,null,null)
      });
    
      this.jobpostingservice.jobRefresh.subscribe(
        data=>
        {
        
          this.jobposting=data
          
        }
      );

    
    this.spinnerService.show();
    // var userId = localStorage.getItem('loginuserid')
    // var simpleCrypt = new SimpleCrypt();
    // var loginuserid = simpleCrypt.decode("my-key", userId);


    this.userService.getfunctinalareadata()
      .subscribe(
        data => {
          // this.spinnerService.hide();

          this.fnarealist = data;

        },
        error => {
        });
    this.spinnerService.show();
    // var skillDisplayName=[]

    var skillJson : any= []

    var skilldata: any


    this.jobpostingservice.getexpyear()
      .subscribe(
        data => {

          this.year = data;
        },
        error => {


        });

    this.userService.getinterviewmode()
      .subscribe(
        data => {

          this.interviewmode = data;

        },
        error => {
        });

    this.userService.getinterviewtype()
      .subscribe(
        data => {

          this.interviewtype = data;


        },
        error => {
        });





    this.jobpostingservice.natureofjob()
      .subscribe(
        data => {

          this.jobtype = data;

        },
        error => {
        });


    // this.jobpostingservice.getcity(1)
    //   .subscribe(
    //     data => {

    //       this.states = data;


    //     },
    //     error => {


    //     });

    // this.jobpostingservice.Getskillnames().subscribe(data => {
    //   if (data != null)
    //     for (var a = 0; a < data.length; a++) {
    //       var skilldata = { "id": 2, "itemName": "Singapore" };
    //       skilldata.id = a;
    //       skilldata.itemName = data[a].skill_name;
    //       this.dropdownList.push(skilldata);

    //     }
    //   //this.keyskills=data
    // })
    //getting notice period
    this.jobpostingservice.loadnoticeperiod().subscribe(data => {
      this.notice = data
    })
    //getting physical status
    this.userService.loadphysicalstatus().subscribe(data => {

      this.phystatus = data;

    })





  }
  initialiseState(): any {
    throw new Error("Method not implemented.");
  }



  loginuserid : any= localStorage.getItem('loginuserid')
  skillsarray: any = []
  skillsarraydisplay: any = []
  optarray: any = []
  optarraydisplay: any = []
  eduarray: any = []
  eduarraydisplay: any = []
  locality: any = []
  localityNames: any = []
  district: any = []
  districtNames: any = []
name:any;
  Search(search:any){
this.name=search;
this.mypost(0,0,search,this.psize, this.plength)
  }
  mypost(userIdred: any, jobidred: any,search:any,psize: any, plength: any) {
    debugger
    var simpleCrypt : any= new SimpleCrypt();
    var loginuserid : any= simpleCrypt.decode("my-key", this.loginuserid);
    this.optarray = []
    this.optarraydisplay = []

    this.jobpostingservice.getmyposts(loginuserid, jobidred,search,psize,plength).subscribe(
      data => {
        debugger
        this.spinnerService.hide();
        this.images = []
        
        this.jobposting = data;
        // var skillDisplayName=[]
        // var skillJson=[]
        // var skill=[]
        for (let j = 0; j < this.jobposting.length; j++) {
          var splitarray: any = []
          if (this.jobposting[j].skill) {
            for (var i = 0; i < this.jobposting[j].skill.length; i++) {
              splitarray.push(this.jobposting[j].skill[i].split(':'))

            }
          }
          this.skillsarray.push(splitarray)


        }
        for (let i = 0; i < this.skillsarray.length; i++) {
          var display: any = []
          let j = 0
          while (j < this.skillsarray[i].length) {

            let sep = ''
            if (this.skillsarray[i][j + 1]) {
              sep = ','
            }
            display.push(this.skillsarray[i][j][1] + sep)
            j++

          }
          this.skillsarraydisplay.push(display)

        }

        // for (let i = 0; i < this.jobposting.length; i++) {
        //   this.skill.push(JSON.parse(this.jobposting[i].skill))
        // }

        for (let j = 0; j < this.jobposting.length; j++) {
          var splitarray: any = []
          if (this.jobposting[j].optional_skills) {
            for (var i = 0; i < this.jobposting[j].optional_skills.length; i++) {
              splitarray.push(this.jobposting[j].optional_skills[i].split(':'))

            }
          }
          this.optarray.push(splitarray)


        }
        for (let i = 0; i < this.optarray.length; i++) {
          var display: any = []
          let j = 0
          while (j < this.optarray[i].length) {

            let sep = ''
            if (this.optarray[i][j + 1]) {
              sep = ','
            }
            display.push(this.optarray[i][j][1] + sep)
            j++

          }
          this.optarraydisplay.push(display)

        }
        // for (let j = 0; j < this.jobposting.length; j++) {
        //   this.skillopt.push(JSON.parse(this.jobposting[j].optional_skills))
        // }

        for (let j = 0; j < this.jobposting.length; j++) {
          var splitarray: any = []
          if (this.jobposting[j].optional_skills) {
            for (var i = 0; i < this.jobposting[j].optional_skills.length; i++) {
              splitarray.push(this.jobposting[j].optional_skills[i].split(':'))

            }
          }
          this.optarray.push(splitarray)

        }
        for (let i = 0; i < this.optarray.length; i++) {
          var display : any= []
          let j = 0
          while (j < this.optarray[i].length) {

            let sep = ''
            if (this.optarray[i][j + 1]) {
              sep = ','
            }
            display.push(this.optarray[i][j][1] + sep)
            j++

          }
          this.optarraydisplay.push(display)

        }




        for (var i = 0; i < this.jobposting.length; i++) {
          this.imagePath.push(this.imageUrl + '/' + this.jobposting[i].venuephoto);

        }
        for (var i = 0; i < this.jobposting.length; i++) {
          this.videoPath.push(this.videoUrl + '/' + this.jobposting[i].venuevideo);

        }
        // for (let k = 0; k < this.jobposting.length; k++) {
        //   this.educational_details.push(JSON.parse(this.jobposting[k].education_details))


        // }
        for (let j = 0; j < this.jobposting.length; j++) {
          var splitarray: any = []
          if (this.jobposting[j].education_details) {
            for (var i = 0; i < this.jobposting[j].education_details.length; i++) {
              splitarray.push(this.jobposting[j].education_details[i].split(':'))

            }
          }
          this.eduarray.push(splitarray)


        }
        for (let i = 0; i < this.eduarray.length; i++) {
          var display : any= []
          let j = 0
          while (j < this.eduarray[i].length) {

            let sep = ''
            if (this.eduarray[i][j + 1]) {
              sep = ','
            }
            display.push(this.eduarray[i][j][1] + sep)
            j++

          }
          this.eduarraydisplay.push(display)


        }
        for (var i = 0; i < this.jobposting.length; i++) {
          if (this.jobposting[i].district) {
            this.district.push(this.jobposting[i].district[0].split(':'))
            this.districtNames.push(this.district[i][1])
             }
        }
        for (var i = 0; i < this.jobposting.length; i++) {
          if (this.jobposting[i].locality) {
            this.locality.push(this.jobposting[i].locality[0].split(':'))
            this.localityNames.push(this.locality[i][1])
             }
        }

      },
      error => {

      });
  }

  deletejobid: any
  check_app: any
  jobimage: any
  jobvideo: any
  Deletejob(jobid: any,image,video, template) {
    this.jobpostingservice.checkApplicants(jobid, 'post').subscribe(data => {
      this.check_app = eval(data[0].count)

      if (this.check_app == 0) {
        this.deletejobid = jobid
        this.jobimage=image
        this.jobvideo=video

        this.modalRef = this.modalService.show(
          template
        );
      }
      else {
        this.tostservice.error("Someone applied for this post", "Not able to Delete")
      }

    })

  }

  // titleCheck(event) {
  //   //
  //   const pattern = /[a-zA-Z]/;
  //   let inputChar = String.fromCharCode(event.charCode)
  //   if (event.keyCode != 8 && !pattern.test(inputChar)) {
  //     event.preventDefault();
  //   }
  // }
  titleCheck(event) {
    const pattern: any = /^[A-Za-z' ']+$/;
    let inputChar : any= String.fromCharCode(event.charCode)
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
    if (event.keyCode == 32 && this.interview.interviewer.trim('').length == 0) {
      event.preventDefault();
    }
  }

  confirmation(res_confirm: boolean) {
    if (res_confirm) {
      this.jobpostingservice.Deletejob(this.deletejobid,this.jobimage,this.jobvideo).subscribe(
        data => {

          this.tostservice.success("Job Deleted Successfully")
          this.modalRef.hide()
          this.mypost(0, 0,this.name,null,null)

        },
        error => {

        });

    }
    else {
      this.modalRef.hide()
    }

  }


  numbercheck(event) {

    const pattern = /^[0-9]$/;
    let inputChar = String.fromCharCode(event.charCode)
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }
  maxsalarycheck_res: any
  maxsalary: any
  minsalary: any
  maxsalarycheck() {
    this.maxsalary = (this.jobpost.maxsalary);

    this.minsalary = (this.jobpost.minsalary);

    this.maxsalary = +this.maxsalary;
    this.minsalary = +this.minsalary;
    // console.log( this.maxsalary)
    // console.log( this.minsalary) 
    if (this.maxsalary < this.minsalary) {
      this.maxsalarycheck_res = 1

    }
    else {
      this.maxsalarycheck_res = -1


    }
    if(this.maxsalary == this.minsalary)
{
  this.maxsalarycheck_res = 0

}
  }
  // maxsalcheck_res
  // maxsalcheck() {
    
  //   if (eval(this.jobpost.maxsalary) < eval(this.jobpost.minsalary)) {
  //     this.maxsalcheck_res = 1

  //   }
  //   else {
  //     this.maxsalcheck_res = -1


  //   }
  // }



  getjobappliedusers(jobid, type) {
    this.jobpostingservice.getjobappliedusers(jobid, type).subscribe(
      data => {

      },
      error => {

      });

  }
  rows: any
  table : any= -1

  showTable(job: any, jobid: any, type: any) {


    // show btn with id btnId in DOM
    if (this.table == -1) {
      this.table = jobid
    }
    else if (this.table != jobid) {
      this.table = jobid
    }
    else {
      this.table = -1
    }
    this.tables = -1
    this.scheduleshow = -1
    this.showProgressed = -1

    this.shortlistCandidate = []
    this.selected = []

    this.schedule_user = []
    this.getApplied(jobid)
    /////apply component


  }

  getApplied(jobid: any) {
    this.spinnerService.show();
    this.jobpostingservice.getApplied(jobid).subscribe(
      data => {

        this.spinnerService.hide();
        this.temp = [...data];
        this.rows = JSON.parse(JSON.stringify(data));
      },
      error => {

      });

  }

  getShortlist(jobid) {
    this.spinnerService.show();
    this.jobpostingservice.shortlistedList(jobid).subscribe(data => {

      this.spinnerService.hide();
      this.temp = [...data];

      this.rows = JSON.parse(JSON.stringify(data));

    },
      error => {

      });
  }
  shortlisted: any

  tables : any= -1

  showShortlist(jobid : any) {
    this.spinnerService.show();
    this.selected = [];
    this.schedule_user = []
    if (this.tables == -1) {
      this.tables = jobid
    }
    else if (this.tables != jobid) {
      this.tables = jobid
      this.selected = []
    }
    else {
      this.tables = -1
      this.selected = []
    }

    this.table = -1
    this.scheduleshow = -1
    this.scheduleshow = -1
    this.showProgressed = -1
    this.getShortlist(jobid)
    this.spinnerService.hide();

  }
  scheduleshow: any = -1
  showSchedule(jobid : any) {
    this.spinnerService.show();
    this.selected = [];
    this.schedule_user = []
    if (this.scheduleshow == -1) {
      this.scheduleshow = jobid
    }
    else if (this.scheduleshow != jobid) {
      this.scheduleshow = jobid
      this.selected = []
    }
    else {
      this.scheduleshow = -1
      this.selected = []
    }
    this.table = -1
    this.tables = -1
    this.showProgressed = -1
    this.scheduled(jobid)
    this.spinnerService.hide();
  }
  scheduled(jobid) {
    this.spinnerService.show();
    this.jobpostingservice.scheduledList(jobid).subscribe(data => {
      this.spinnerService.hide();

      this.temp = [...data];

      this.rows = JSON.parse(JSON.stringify(data));


    },
      error => {

      });
  }
  showProgressed : any= -1
  progressed(jobid) {
    this.spinnerService.show();
    this.selected = [];
    this.schedule_user = []
    if (this.showProgressed == -1) {
      this.showProgressed = jobid
    }
    else if (this.showProgressed != jobid) {
      this.showProgressed = jobid
      this.selected = []

    }
    else {
      this.showProgressed = -1
      this.selected = []
    }
    this.table = -1
    this.tables = -1
    this.scheduleshow = -1

    this.progressedList(jobid)
    this.spinnerService.hide();

  }
  progressedList(jobid) {
    this.spinnerService.show();
    this.jobpostingservice.progressed(jobid).subscribe(data => {

      this.spinnerService.hide();

      this.temp = [...data];

      this.rows = JSON.parse(JSON.stringify(data));


    },
      error => {

      });
  }





  selected: any = [];

  onSelect({ selected }) {

this.selected=[]
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);

  }



  add() {
    this.selected.push(this.rows[1], this.rows[3]);
  }

  update() {
    this.selected = [this.rows[1], this.rows[3]];
  }

  remove() {
    this.selected = [];
  }
  shortlistCandidate: any
  element: any = []
  email_id : any= []
  first_name : any= []
  mobile_no: any=[]
  shortlist(jobid, job_title,mobile_no) {


    this.spinnerService.show();

    this.shortlistCandidate = this.selected

    for (let index = 0; index < this.selected.length; index++) {
      this.element[index] = this.selected[index].user_registration_id;
this.mobile_no[index]=this.selected[index].mobile_no;
this.first_name[index]=this.selected[index].first_name
    }
    if (this.shortlistCandidate.length) {
      this.jobpostingservice.shorlisted(jobid, this.element, this.email_id, this.first_name, job_title,this.mobile_no).subscribe(data => {
    this.spinnerService.hide();
       
        
        this.selected = []
        this.getApplied(jobid)
        this.tostservice.success("Shorlisted  Successfully")


      },
        error => {

        });
    }
    else {
      this.tostservice.error('No selections')
    }

  }

  date: Date = new Date();
  settings : any= {
    bigBanner: true,
    timePicker: false,
    format: 'dd-MM-yyyy',
    defaultOpen: true
  }

  updateFilter(event) {


    const val : any= event.target.value.toLowerCase();

    // filter our data
    const temprow = this.temp.filter(function (temp) {

      return temp = temp.first_name.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.rows = temprow;
    // update the rows


    // Whenever the filter changes, always go back to the first page

  }

  selectedCount: number = 0;
  selectedMessage: string | boolean

  modeChange(event) {

  }

  schedulejobid: any
  schedulejobtitle: any
  modalRef: BsModalRef;
  schedule_user : any= []
 
  schedule(template: TemplateRef<any>, job) {
    this.mobile_no=[]
    for (let index = 0; index < this.selected.length; index++) {
      this.schedule_user[index] = this.selected[index].user_registration_id;
      this.mobile_no[index]= this.selected[index].mobile_no;
      this.first_name[index]=this.selected[index].first_name
      this.interview = {}
    }
    this.schedulejobid = job.jobid
    this.schedulejobtitle = job.job_title
    if (this.schedule_user.length) {
      this.modalRef = this.modalService.show(template,
        Object.assign({
          backdrop: 'static',
          keyboard: false
        }, { class: 'gray modal-lg' })
      );
    }
    else {
      this.tostservice.error("No User Selelcted")
    }
  }
  job: any = {}
  rej_status: any
status:any
  rejectConfirm(job, status, rejectconfirm) {
    
    if (this.selected.length && job.status != "rejected") {
      this.job = job
      this.rej_status = status

      this.modalRef = this.modalService.show(
        rejectconfirm
      );
    }
    else if (this.selected.length && job.status == "rejected") {
      this.tostservice.warning("This is a rejected")
    }
    else {
      this.tostservice.error("No User Selelcted")
    }


  }
  reject(res) {
    if (res) {
      this.changeStatus(this.job, this.rej_status)
    }
    else {
      this.modalRef.hide()
    }
  }
  changeStatus(job, status) {
    this.mobile_no=[]
    this.first_name=[]
    for (let index = 0; index < this.selected.length; index++) {
      this.schedule_user[index] = this.selected[index].user_registration_id;
      this.mobile_no[index]=this.selected[index].mobile_no
      this.first_name[index]=this.selected[index].first_name
    }
    this.schedulejobid = job.jobid
    this.schedulejobtitle = job.job_title
    if (this.schedule_user.length) {

      this.jobpostingservice.changeStatus(this.schedule_user, this.schedulejobid, this.schedulejobtitle, status,this.mobile_no,this.first_name).subscribe(data => {



        this.selected = [];
        this.schedule_user = []

        this.progressedList(this.schedulejobid)

        this.modalRef.hide()
      },
        error => {


        });
    }
    else {
      this.tostservice.error("No User Selelcted")
    }
  }

  //schedulepost
  formatdate: any;
  formatscheduledate(date) {

    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear(),
      hour = '' + d.getHours(),
      minute = '' + d.getMinutes();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    if (hour.length < 2) hour = '0' + hour;
    if (minute.length < 2) minute = '0' + minute;
    this.formatdate = [year, month, day].join('-') + 'T' + hour + ':' + minute + ''; 
    return this.formatdate;
  }

  schedulepost(interview) {
    
    interview.scheduledate1 = this.formatscheduledate(interview.scheduledate);
    interview.scheduletime1 = this.formatscheduledate(interview.scheduletime);
    
    this.jobpostingservice.schedulepost(interview, this.schedule_user, this.schedulejobid, this.schedulejobtitle,this.mobile_no,this.first_name).subscribe(data => {

      this.selected = [];
      this.schedule_user = []
      
      interview={}
      
      this.tostservice.success('Scheduling jobpost sucess full!', 'SCHEDULED!');
      //resetting fields
      // this.interview.scheduledate = '';
      // this.interview.scheduletime = '';
      // this.interview.interviewer = '';
      // this.interview.location = '';
      // this.interview.mode = '';
      // this.interview.type = '';
      // this.interview.notifyme = '';
      this.getShortlist(this.schedulejobid)
      this.modalRef.hide();
     
    })

    error => {

     };
    
  }


  nextRoundPost(interview) {

    interview.scheduledate2 = this.formatscheduledate(interview.schedule_date);
    interview.scheduletime2 = this.formatscheduledate(interview.schedule_time);
    this.jobpostingservice.nextRoundPost(interview, this.schedule_user, this.schedulejobid, this.schedulejobtitle,this.mobile_no,this.first_name).subscribe(data => {



      this.selected = [];
      this.schedule_user = []

      this.scheduled(this.schedulejobid)

      this.modalRef.hide()
    },
      error => {

      });
  }
  nextRound(nextRoundTemplate, job) {
    
    this.mobile_no=[]
    this.first_name=[]
    for (let index = 0; index < this.selected.length; index++) {
      this.schedule_user[index] = this.selected[index].user_registration_id;
      this.mobile_no[index]=this.selected[index].mobile_no;
      this.first_name[index]=this.selected[index].first_name;
    }
    this.schedulejobid = job.jobid
    this.schedulejobtitle = job.job_title
    if (this.schedule_user.length) {
      this.modalRef = this.modalService.show(nextRoundTemplate,
        Object.assign({
          backdrop: 'static',
          keyboard: false
        }, { class: 'gray modal-lg' })
      );
    }
    else {
      this.tostservice.error("No User Selelcted")
    }

  }

  public captureScreen() {
    var data : any= document.getElementById('contentToConvert');
    html2canvas(data).then(canvas => {
      // Few necessary setting options  
      var imgWidth : any= 208;
      var pageHeight: any = 295;
      var imgHeight : any= canvas.height * imgWidth / canvas.width;
      var heightLeft : any= imgHeight;

      const contentDataURL = canvas.toDataURL('image/png')
      let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF  
      var position = 0;
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
      pdf.save('Resume.pdf'); // Generated PDF   
    });
  }
  result

  cur_des: any = [];
  languagesdisplay: any = [];
  pskills: any = []
  a: any = []


  keyskillsarraydisplay: any = []
  skilldetails: any = []
  presentdesignation: any = [];
  previousdesignation: any = [];
  gradsarray: any = []
  graduniversity: any = []
  pgarray: any = []
  pguniversity: any = [];
  phdarray: any = []
  phduniversity: any = [];
  proskillsarray: any = []
  proskillsarraydisplay: any = []
  skillsarraydisplay1:any=[]

  openresume(profileoverview, user_registration_id:any) {
    this.spinnerService.show();
    this.skilldetails=[];
     this.cur_des=[];
    this.skillsarraydisplay=[];
    this.previousdesignation=[];
    this.presentdesignation=[];
    this.pskills = []
    this.a = []
    this.languagesdisplay = [];
    this.cur_des = [];
    this.previousdesignation = [];
    this.presentdesignation = [];
    this.graduniversity = [];
    this.pguniversity= [];
    this.phduniversity = [];
    this. proskillsarray=[]
    this.proskillsarraydisplay=[]
    this.skillsarraydisplay=[]
    this.skilldetails=[]
    this.pgarray=[]
    this.gradsarray=[]
    this.phdarray=[]
    this.skilldetails = [];
    this.keyskillsarraydisplay = [];
    this.skillsarraydisplay1=[];
    this.userService.getprofiledetails1(eval(user_registration_id)).subscribe(data => {

      this.result = data[0];
      this.spinnerService.hide();

      if (this.result.presentemp_details != null) {
        for (let i = 0; i < this.result.presentemp_details.length; i++) {
          this.presentdesignation.push(this.result.presentemp_details[i].designation.split(':')[1])

        }
      }

      if (this.result.previousemp_details != null) {
        for (let i = 0; i < this.result.previousemp_details.length; i++) {
          this.previousdesignation.push(this.result.previousemp_details[i].designation.split(':')[1])

        }
      }
      if (this.result.project_details) {

        for (let j = 0; j < this.result.project_details.length; j++) {
          var splitarray: any = []
          if (this.result.project_details[j].skills) {
            for (var i = 0; i < this.result.project_details[j].skills.length; i++) {
              splitarray.push(this.result.project_details[j].skills[i].split(':'))
            }
          }
          this.proskillsarray.push(splitarray)
        }
        for (let i = 0; i < this.proskillsarray.length; i++) {
          var display : any= []
          let j = 0
          while (j < this.proskillsarray[i].length) {

            let sep = ''
            if (this.proskillsarray[i][j + 1]) {
              sep = ','
            }
            display.push(this.proskillsarray[i][j][1] + sep)
            j++

          }
          this.proskillsarraydisplay.push(display)

        }
        // for (let i = 0; i < this.result.project_details.length; i++) {

        //   this.skill.push(JSON.parse(this.result.project_details[i].skills))

        // }
      }

      if (this.result.skill_details) {
        for (let j = 0; j < this.result.skill_details.length; j++) {
          this.skilldetails.push(this.result.skill_details[j][0].split(':'))
        }
      }
      let count = 0
      // for(let i=0;i<this.skilldetails.length;i++){
      while (count < this.skilldetails.length) {


        let sep = ''
        if (this.skilldetails[count + 1]) {
          sep = ','
        }
        // display.push(this.skilldetails[i][1]+sep)         
        this.skillsarraydisplay1.push(this.skilldetails[count][1] + sep)
        // sep=''
        count++
      }
      // console.log(this.skillsarraydisplay,"skillsarraydisplay")
      //-------------------------------------------------------------------------------------------------------------------

      if (this.result.achievement_details != null) {
        for (var j = 0; j < this.result.achievement_details.length; j++) {
          this.picsArr = this.result.achievement_details[j].image;
          // if (this.picsArr != null) {
          //   for (var i = 0; i < this.picsArr.length - 1; i++) {
          //     this.picsstring[j] = this.picsArr.split(' ')

          //   }
          // }

        }

      }

      if (this.result.achievement_details != null) {
        for (var j = 0; j < this.result.achievement_details.length; j++) {
          // let imagesArr:any=[];
          let imagesArr: any = [];
          this.picsArr = this.result.achievement_details[j].image;

          // if (this.picsArr != null) {
          //   for (var i = 0; i < this.picsArr.length - 1; i++) {


          //     this.picsstring[j] = this.picsArr.split(' ')

          //   }

          // }

          if (this.picsArr) {
            for (var k = 0; k < this.picsArr.length; k++) {


              this.Imagejson =
                {
                  small: this.ApiUrl + '/achievements/' + this.picsArr[k],
                  medium: this.ApiUrl + '/achievements/' + this.picsArr[k],
                  big: this.ApiUrl + '/achievements/' + this.picsArr[k]
                }

              imagesArr.push(this.Imagejson);
            }
          
          //}
          this.imagesArr1.push(imagesArr)
        }else{
          this.imagesArr1.push(null)
        }
      }
      }
      this.galleryOptions = [
        {
          width: '400px',
          height: '300px',
          thumbnailsColumns: 4,
          imageAnimation: NgxGalleryAnimation.Slide
        },
        // max-width 800
        {
          breakpoint: 800,
          width: '100%',
          height: '600px',
          imagePercent: 80,
          thumbnailsPercent: 20,
          thumbnailsMargin: 20,
          thumbnailMargin: 20
        },
        // max-width 400
        {
          breakpoint: 400,
          preview: false
        }
      ];
      if (this.result.language_known) {
        for (let i = 0; i < this.result.language_known.length; i++) {
          this.languagesdisplay.push(JSON.parse(this.result.language_known[i].language_name))
        }
      }
      //university_or_institute
      //   if(this.result.grad_details){
      //   for (let i = 0; i < this.result.grad_details.length; i++) {
      //     this.graduniversity.push(JSON.parse(this.result.grad_details[i].university_or_institute)[0].name)
      //   }
      // }
      if (this.result.grad_details) {

        for (let j = 0; j < this.result.grad_details.length; j++) {
          var splitarray: any = []
          if (this.result.grad_details[j].university_or_institute) {
            for (var i = 0; i < this.result.grad_details[j].university_or_institute.length; i++) {
              splitarray.push(this.result.grad_details[j].university_or_institute[i].split(':'))

            }
          }
          this.gradsarray.push(splitarray)

        }
        for (let i = 0; i < this.gradsarray.length; i++) {
          var display: any = []
          let j = 0
          while (j < this.gradsarray[i].length) {

            let sep = ''
            if (this.gradsarray[i][j + 1]) {
              sep = ','
            }
            display.push(this.gradsarray[i][j][1] + sep)
            j++

          }
          this.graduniversity.push(display)

        }
        // for (let i = 0; i < this.result.project_details.length; i++) {

        //   this.skill.push(JSON.parse(this.result.project_details[i].skills))

        // }
      }
      if (this.result.pg_details) {
        for (let j = 0; j < this.result.pg_details.length; j++) {
          var splitarray: any = []
          if (this.result.pg_details[j].university_or_institute) {
            for (var i = 0; i < this.result.pg_details[j].university_or_institute.length; i++) {
              splitarray.push(this.result.pg_details[j].university_or_institute[i].split(':'))

            }
          }
          this.pgarray.push(splitarray)

        }
        for (let i = 0; i < this.pgarray.length; i++) {
          var display : any= []
          let j = 0
          while (j < this.pgarray[i].length) {

            let sep = ''
            if (this.pgarray[i][j + 1]) {
              sep = ','
            }
            display.push(this.pgarray[i][j][1] + sep)
            j++

          }
          this.pguniversity.push(display)

        }
      }
      if (this.result.phd_details) {
        for (let j = 0; j < this.result.phd_details.length; j++) {
          var splitarray: any = []
          if (this.result.phd_details[j].university_or_institute) {
            for (var i = 0; i < this.result.phd_details[j].university_or_institute.length; i++) {
              splitarray.push(this.result.phd_details[j].university_or_institute[i].split(':'))

            }
          }
          this.phdarray.push(splitarray)

        }
        for (let i = 0; i < this.phdarray.length; i++) {
          var display : any= []
          let j = 0
          while (j < this.phdarray[i].length) {

            let sep = ''
            if (this.phdarray[i][j + 1]) {
              sep = ','
            }
            display.push(this.phdarray[i][j][1] + sep)
            j++

          }
          this.phduniversity.push(display)

        }
      }

      if (this.result.current_designation) {

          this.cur_des.push(this.result.current_designation.split(':')[1])
        // }
      }
    })
   
    this.modalRef = this.modalService.show(
      profileoverview,
      Object.assign({}, { class: 'gray modal-lg' })
    );
  }

  dropdownSettings: any = {}
  selectedItems : any= ['']
  noResult: any
  splitarray1: any = []
  optskills: any = []
  functionalid: any
  edit(template: TemplateRef<any>, job) {

this.functionalid=job.functional_area;
this.jobpostingservice.getSkills1(this.functionalid).subscribe(data => {


  for (var a = 0; a < data.length; a++) {
    var skilldata: any = { "id": 2, "itemName": "Singapore" };
    skilldata.id = data[a].skill_id;
    skilldata.itemName = data[a].skill_name;
    this.dropdownList.push(skilldata);
  }
});
    this.jobpostingservice.checkApplicants(job.jobid, 'post').subscribe(data => {
      this.check_app = eval(data[0].count)

      if (this.check_app == 0) {
        this.optskills = []
        this.splitarray1 = []

        this.jobpost.maxsalary = job.maxsalary
        this.jobpost.contactperson = job.contact_person
        this.jobpost.notice_period = job.notice_period
        this.jobpost.minsalary = job.minsalary

        this.jobpost.gender = job.gender_preference
        this.jobpost.physicalstatus = job.physical_status


        /////////////////////////////////
        for (var i = 0; i < job.optional_skills.length; i++) {
          this.splitarray1.push(job.optional_skills[i].split(':'))

        }


        var skillOptNames: any = { id: '', itemName: '' }
        var optskills : any= []
        for (let index = 0; index < this.splitarray1.length; index++) {
          // for(let i=0;i<this.splitarray1[index].length;i++){
          // if(this.splitarray1[index][i+1]){
          skillOptNames.id = this.splitarray1[index][0]
          skillOptNames.itemName = this.splitarray1[index][1]
          this.optskills.push(skillOptNames)
          skillOptNames = { id: '', itemName: '' }
          // }
          // }
        }

        this.jobpost.optional_skills = this.optskills
        //////////////////////////////


        this.jobpost.tilldate = new Date()
        this.jobpost.tilldate.setDate( job.tilldate.split('T')[0].split('-')[2],job.tilldate.split('T')[0].split('-')[1], job.tilldate.split('T')[0].split('-')[0])
        // job.tilldate.split('T')[0].split('-')[0]/+job.tilldate.split('T')[0].split('-')[1]/+job.tilldate.split('T')[0].split('-')[2]-1
        // alert(job.tilldate.split('T')[0].split('-')[0]+job.tilldate.split('T')[0].split('-')[1]+job.tilldate.split('T')[0].split('-')[2]-1)

        this.jobid = job.jobid
        this.selectedItems = []

        this.dropdownSettings = {
          singleSelection: false,
          text: "Select Skills",
          enableCheckAll: false,
          enableSearchFilter: true,
          classes: "myclass custom-class",
          maxHeight: "150"

        };
        this.modalRef = this.modalService.show(
          template,
          Object.assign({
            backdrop: 'static',
            keyboard: false
          }, { class: 'gray modal-lg' }));
      }
      else {
        this.tostservice.error('"Someone applied for this post", "Not able to Edit"')
      }
    });


  };
  ImageCompressService: any
  processedImage: any
  checkImage: any
  checkedImage: any
  onFileSelected(fileInput: any) {

  }
  jobid: any
  imageUrl: string = "{{Apiurl}}/jobposts/images"
  videoUrl: string = "{{Apiurl}}/jobposts/videos"
  imagePath : any= []
  videoPath : any= []
  o: any = []
  testObjectopt: any = []
  skillsOptObj : any= { id: '', name: '' }
  editjob(jobpost) {
    
    
    if (eval(this.jobpost.maxsalary) > eval(this.jobpost.minsalary)) {
      
      for (var a = 0; a < jobpost.optional_skills.length; a++) {
        this.testObjectopt.push(this.jobpost.optional_skills[a].id + ':' + this.jobpost.optional_skills[a].itemName)
        // this.skillsOptObj.id = jobpost.optional_skills[a].id
        // this.skillsOptObj.name = jobpost.optional_skills[a].itemName
        // this.o.push(this.skillsOptObj);
        // this.skillsOptObj = { id: '', name: '' }

      }

      jobpost.optional_skills = this.testObjectopt;


      jobpost.photo = this.processedImage;

      
      if ((eval(this.jobpost.maxsalary) > eval(this.jobpost.minsalary))) {
        this.spinnerService.show();
        this.jobpostingservice.editjobpost(jobpost, this.jobid).subscribe(data => {
          

          this.jobposting = data;
          this.jobposting.optional_skills = ' '
          this.maxsalarycheck_res = -1
          this.testObjectopt = []
          this.modalRef.hide();
          this.tostservice.success('sucessfull!', 'Jobposting edit!');;
          this.mypost(0, 0,this.name,null,null)
        })
        error => {

        }

      }
    }
  }

  test() {
    this.psize;
    this.plength = this.plength + this.plength;
    
    this.mypost(this.userIdred, this.jobidred,this.name,this.psize, this.plength)

    
  }

  //@HostListener("window:scroll", ["$event"])
  onWindowScroll() {
    var d: any = document.documentElement;
    var offset : any= d.scrollTop + window.innerHeight;
    var height: any = d.offsetHeight;
    if (offset === height) {
      this.spinnerService.show()
      this.test();
      setTimeout(() => this.spinnerService.hide(), 6000)
    }
  }

  // maxsalcheck_res = -1
  // maxsalcheck() {

  //   if (eval(this.jobpost.maxsalary) < eval(this.jobpost.minsalary)) {
  //     this.maxsalcheck_res = 1

  //   }
  //   else {
  //     this.maxsalcheck_res = -1


  //   }
  // }

  item: any
  onItemSelect(item: any) {

    this.item = item


  }
  OnItemDeSelect(item: any) {
    this.item = item

  }

  closeModal(template) {


    this.modalRef.hide()

  }
  datecheck(event) {

    const pattern = /^(((0[1-9]|[12][0-9]|30)[-/]?(0[13-9]|1[012])|31[-/]?(0[13578]|1[02])|(0[1-9]|1[0-9]|2[0-8])[-/]?02)[-/]?[0-9]{4}|29[-/]?02[-/]?([0-9]{2}(([2468][048]|[02468][48])|[13579][26])|([13579][26]|[02468][048]|0[0-9]|1[0-6])00))$/;
    let inputChar = String.fromCharCode(event.charCode)
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }
  // id:any
  ratings = {
    tech_rating: 0,
    info_rating: 0,
    behaviour_rating: 0,
    etiquette_rating: 0,
    communication_rating: 0,
    expectations:'',
    overall_exp:''

  }
  userrating:any
  
  rateCandidate(rattingTemplate, job) {
    var id
    this.expectation='';
    this.overallexp='';
    this.ratings = {
      tech_rating: 0,
      info_rating: 0,
      behaviour_rating: 0,
      etiquette_rating: 0,
      communication_rating: 0,
      expectations:'',
      overall_exp:''
  
    }
    this.schedule_user=[]

    debugger
    
    this.schedule_user.length=0;
    if(this.selected.length<=1){
    for (let index = 0; index < this.selected.length; index++) {
      this.schedule_user[index] = this.selected[index].user_registration_id;

    }
  
    for(var i=0;i<this.schedule_user.length;i++){
      id=this.schedule_user[i]
    }
    this.ratingservice.getuserrating(job.jobid,id).subscribe(data => {
    this.userrating=data

   
  
    this.jobid = job.jobid
    this.schedulejobtitle = job.job_title
    if (this.schedule_user.length>0 ) {
      if(this.userrating.length!=0){
        debugger
      this.ratings.behaviour_rating=data[0].behaviour
      this.ratings.tech_rating=data[0].subject_skills
      this.ratings.info_rating=data[0].info_provided
      this.ratings.etiquette_rating=data[0].interview_ettiquete
      this.ratings.communication_rating=data[0].communication_skills
      this.ratings.expectations=data[0].org_expectation
      this.ratings.overall_exp=data[0].overall_description
      }else{
      this.ratings.behaviour_rating=0
      this.ratings.tech_rating=0
      this.ratings.info_rating=0
      this.ratings.etiquette_rating=0
      this.ratings.communication_rating=0
      this.ratings.expectations=''
      this.ratings.overall_exp=''
      }
      this.modalRef = this.modalService.show(rattingTemplate);
      Object.assign({
        backdrop: 'static',
        keyboard: false
      }, { class: 'gray modal-lg' })
    }
    else {
      this.tostservice.error("No User Selected ")
    }
  })
  }

else{
    this.tostservice.error("Multiple users selected")
  }

  }
  tech_rating: any
  expectation: any
  overallexp: any
  rating: any = {}
  saverating(tech_rating, info_rating, behaviour_rating, etiquette_rating, communication_rating, expectation, overallexp, rattingTemplate) {
    debugger
    // tech_rating, info_rating, behaviour_rating, etiquette_rating, communication_rating, rattngTemplate 

    if (tech_rating && info_rating && behaviour_rating && etiquette_rating && communication_rating && expectation && overallexp && expectation
    ) {

      this.rating = {
        tech_rating: tech_rating,
        info_rating: info_rating,
        behaviour_rating: behaviour_rating,
        etiquette_rating: etiquette_rating,
        communication_rating: communication_rating,
        expectations: expectation,
        overallexp: overallexp,
        user: this.schedule_user,
        jobid: this.jobid

      }

      this.spinnerService.show();
      this.ratingservice.saveuserrating(this.rating).subscribe(data => {
        this.spinnerService.hide();

        this.tostservice.success("Rated SUCCESSFULL", "Rating")
        this.selected = []
        this.userrating=[]
        this.closeModal(rattingTemplate)
        this.schedule_user.length=0;
      })
    }

  }






  tablemore = -1


  viewMore(jobid, i) {


    if (this.tablemore == -1) {

      this.tablemore = jobid;
      var buttons: any = document.getElementsByClassName('showall');
      for (var a = 0; a < buttons.length; a++) {
        var button: any = buttons[a];
        button.innerHTML = "<div class=" + "'fas fa-info-circle'" + ">" + "</div>" + " " + "Show Details"

      }
      document.getElementById(i).innerHTML = "<div class=" + "'fas fa-times-circle'" + ">" + "</div>" + " " + "Hide Details"


    }
    else if (this.tablemore != jobid) {


      this.tablemore = jobid;
      var buttons : any= document.getElementsByClassName('showall');
      for (var a = 0; a < buttons.length; a++) {
        var button : any= buttons[a];
        button.innerHTML = "<div class=" + "'fas fa-info-circle'" + ">" + "</div>" + " " + "Show Details"

      }
      document.getElementById(i).innerHTML = "<div class=" + "'fas fa-times-circle'" + ">" + "</div>" + " " + "Hide Details"



    }
    else {

      this.tablemore = -1
      var buttons : any= document.getElementsByClassName('showall');
      for (var a = 0; a < buttons.length; a++) {
        var button: any = buttons[a];
        button.innerHTML = "<div class=" + "'fas fa-info-circle'" + ">" + "</div>" + " " + "Show Details"

      }

    }

  }
  first_question: any
  second_question: any
  third_question: any
  fourth_question: any
  fifth_question: any

  app_answer: any = {}
  viewAnswer(answers, user_registration_id, job) {

    this.first_question = job.first_question
    this.second_question = job.second_question
    this.third_question = job.third_question
    this.fourth_question = job.fourth_question
    this.fifth_question = job.fifth_question

    this.jobpostingservice.getanswer(user_registration_id, job.jobid).subscribe(data => {
      this.app_answer = data


      if (this.app_answer[0].first_answer != null || this.app_answer[0].second_answer != null || this.app_answer[0].third_answer != null || this.app_answer[0].fourth_answer != null || this.app_answer[0].fifth_answer != null) {
        this.modalRef = this.modalService.show(answers);

        Object.assign({}, { class: 'gray modal-lg' })
      }
      else {
        this.tostservice.error("No User Answers")
      }
    })
  }
  videoResume : any= []
  video: string
  getVideoResume(videoresume, user_registration_id) {
    this.videoResume = []
    this.video = ''
    this.jobpostingservice.getVideoResume(user_registration_id).subscribe(data => {
      this.videoResume = data


      if (this.videoResume.length) {
        this.video = data[0].video
        this.modalRef = this.modalService.show(videoresume);
        Object.assign({}, { class: 'gray modal-lg' })
      }
      else {
        this.tostservice.error("No Video Resume Available")
      }
    })

  }
}

