import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/index';
import { ToastrService } from 'ngx-toastr';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BsModalService } from 'ngx-bootstrap/modal';


@Component({
  selector: 'app-employer-designation',
  templateUrl: './employer-designation.component.html',
  styleUrls: ['./employer-designation.component.css']
})
export class EmployerDesignationComponent implements OnInit {

  Empdetails1: any = [];
  Empdetails2: any = [];
 previousemp: any = [];
  Empdetails3: any = [];
  otheremp: any = []
  emptype: any = [];
  presentemp: any = [];
  notice: any = [];
  designations: any = [];

  desg1:any=[];
  desg2:any=[];
  designation:any=[]
  designation1:any=[]
  designation2:any=[]

  present:boolean
  previous:boolean
  other:boolean;

  desgname:any=[];
  
  modalRef: BsModalRef;
  a:boolean
  public show: boolean = false;
  public postempname: any = 'post';

  desgdrop: any;
  

  constructor(public userService: UserService,private modalService: BsModalService, private tostersuc: ToastrService, private spinnerService: Ng4LoadingSpinnerService) { }


  editorConfig = {
    "editable": true,
    "spellcheck": false,
    "height": "auto",
    "minHeight": "0",
    "width": "auto",
    "minWidth": "0",
    "translate": "yes",
    "enableToolbar": true,
    "showToolbar": true,
    "placeholder": "",
    // "imageEndPoint": "",
    "toolbar": [
      ["bold", "italic", "underline", "unorderedList"]
      //  "strikeThrough", "superscript", "subscript"],
      // ["fontName", "fontSize", "color"],
      // ["justifyLeft", "justifyCenter", "justifyRight", "justifyFull", "indent", "outdent"],
      // ["cut", "copy", "delete", "removeFormat", "undo", "redo"],
      // ["paragraph", "blockquote", "removeBlockquote", "horizontalLine", "orderedList", "unorderedList"],
      // ["link", "unlink", "image", "video"]
    ]
  }



  loadnoticeperioddetails() {
    this.userService.loadnoticeperiod().subscribe(
      data => {
        this.spinnerService.hide();
      
        this.notice = data;
       
      },
      error => {
       
      });
  }
  postempNew() {
    this.show = !this.postempNew;

    if (this.show)
      this.postempname = "Hide";
    else
      this.postempname = "Show";
  }
  present_company(event:any) {
    this.present = false
    this.previous = true
    this.other = true
    this.a = true
  }
  previous_company(event:any) {
    this.present = true
    this.previous = false
    this.other = true
    this.a = true


  }
  other_company(event:any) {
    this.present = true
    this.previous = true
    this.other = false
    this.a = true

  }

  getToday(): string {
    return new Date().toISOString().split('T')[0]
  }
  ngOnInit() {
    this.spinnerService.show();
    this.a = false
    this.loadpresentdetails();
    this.loadpreviousdetails();
    this.loadotherdetails();
    this.loadnoticeperioddetails();
    this.loaddesignations();
    this.loademptype();

  }
  loademptype() {
    this.userService.loademptype()
      .subscribe(
        data => {
          this.spinnerService.hide();
         
          this.emptype = data;

        },
        error => {
     
        });

  }
 loaddesignations() {
    this.userService.loaddesignations().subscribe(
      data => {
        for (var a = 0; a < data.length; a++) {
          var desg:any = { "id": 2, "itemName": "Singapore" };
          desg.id = a;
          desg.itemName = data[a].designation_name;
          this.desgdropdownList.push(desg);
        }

        this.spinnerService.hide();
        
      });

    
      this.selectedItems = []

      this.dropdownSettings = {
        singleSelection: true,
        text: "Select ",
        enableCheckAll: false,
        enableSearchFilter: true,
        classes: "myclass custom-class",
        maxHeight: "150",
        
        showCheckbox:false
      };
    }
    dropdownSettings:any = {}
    selectedItems:any = []
    desgdropdownList:any = [];
  errmsg:boolean
  emparray:any = []
  emppresarray:any=[]
  loadpresentdetails() {
    // this.designation1=[]
    this.spinnerService.show();
    this.emparray = []
    this.emppresarray=[]
    this.userService.Getpresentdetails().subscribe(
      data => {

  
        
    
        this.presentemp = data;

        // for (let i = 0; i < this.presentemp.length; i++) {
        //   this.designation1.push(JSON.parse(this.presentemp[i].designation))
         
        // }
        
        for (let j = 0; j < this.presentemp.length; j++) {

          if (this.presentemp[j].designation) {

            this.emparray.push(this.presentemp[j].designation.split(':')[1])
     


          }
          // if (this.emparray[j]) {
            
          //   this.emppresarray.push(this.emparray[j][1])
           
          // }
        
        }
        this.spinnerService.hide();
       
      },
      error => {
       
      });
  }
  emp2array:any = []
  empprevarray:any=[]
  loadpreviousdetails() {
    this.spinnerService.show();
    this.emp2array=[]
    this.empprevarray=[]
    this.userService.Getpreviousdetails().subscribe(
      data => {
        this.spinnerService.hide();
     
     
        this.previousemp = data;
       
        for (let j = 0; j < this.previousemp.length; j++) {
         
                    if (this.previousemp[j].designation) {
        
                      this.emp2array.push(this.previousemp[j].designation.split(':'))
                     
          
          
                    }
                    if (this.emp2array[j]) {
                      
                      this.empprevarray.push(this.emp2array[j][1])
                     
                    }
                   
                  }
        // for (let i = 0; i < this.previousemp.length; i++) {
        //   this.designation2.push(JSON.parse(this.previousemp[i].designation))
         
        // }
      
      },
      error => {
 
      });
  }

  loadotherdetails() {
    this.spinnerService.show();
    this.userService.Getotherdetails().subscribe(
      data => {
        this.spinnerService.hide();
    
        this.otheremp = data;
      },
      error => {
      
      });
  }

  keyeventname(name: any) {
    const enamepattern = /[a-zA-Z' '0-9]/;
    let inputChar = String.fromCharCode(name.charCode);
    if (name.keyCode != 8 && !enamepattern.test(inputChar)) {
      name.preventDefault();
    }
  }

  keyeventdate(date: any) {
    const pattern = /^(((0[1-9]|[12][0-9]|30)[-/]?(0[13-9]|1[012])|31[-/]?(0[13578]|1[02])|(0[1-9]|1[0-9]|2[0-8])[-/]?02)[-/]?[0-9]{4}|29[-/]?02[-/]?([0-9]{2}(([2468][048]|[02468][48])|[13579][26])|([13579][26]|[02468][048]|0[0-9]|1[0-6])00))$/;
    let inputChar = String.fromCharCode(date.charCode)
    if (date.keyCode != 8 && !pattern.test(inputChar)) {
      date.preventDefault();
    }
  }
  present_name:boolean=false
  previous_name:boolean=false
  other_name:boolean=false
 
  design:any = { id: '', name: '' }
  design1:any = { id: '', name: '' }
  d:any = ''
  d1:any=''

  clearForm1() {

    this.Empdetails1.organization_name='';
    this.Empdetails1.designation='';
    this.Empdetails1.from_date='';
    this.Empdetails1.description='';
    this.Empdetails1.notice_period='';
     this.Empdetails1.employment='';
   this.Empdetails1.employment_type='';
   this.Empdetails1.id='';
  
   

   }
   clearForm2(){
    this.Empdetails2.organization_name='';
    this.Empdetails2.designation='';
    this.Empdetails2.from_date='';
     this.Empdetails2.to_date='';
    this.Empdetails2.description='';
     this.Empdetails2.employment='';
     this.Empdetails2.employment_type='';
    this.Empdetails2.id='';
   }
   clearForm3(){
    this.Empdetails3.employment_type='';
    this.Empdetails3.other_emp='';
    this.Empdetails3.employment='';
    this.Empdetails3.id='';
   }
   formatdate: any;
   formateventdate(date:any) {
     
     var d:any = new Date(date),
       month = '' + (d.getMonth() + 1),
       day = '' + d.getDate(),
       year = d.getFullYear(),
       hour = '' + d.getHours(),
       minute = '' + d.getMinutes();
     if (month.length < 2) month = '0' + month;
     if (day.length < 2) day = '0' + day;
     if (hour.length < 2) hour = '0' + hour;
     if (minute.length < 2) minute = '0' + minute;
     this.formatdate = [year, month, day].join('-') + 'T' + hour + ':' + minute + ''; 
     return this.formatdate;
   }

  
  saveempdetails1() {

  
 
    this.d=''
    this.Empdetails1.employment = "present";
    // for (var a = 0; a < this.Empdetails1.designation.length; a++) {
    //   this.design.id = this.Empdetails1.designation[a].id
    //   this.design.name = this.Empdetails1.designation[a].itemName
    //   this.d.push(this.design);
    //  this.design={ id: '', name: '' }
     
    // }
  

    for (var a = 0; a < this.Empdetails1.designation.length; a++) {

      this.d=this.Empdetails1.designation[a].id + ':' + this.Empdetails1.designation[a].itemName

    }
  // }


    this.Empdetails1.designation = this.d;
    if(this.Empdetails1.from_date){
     
      this.Empdetails1.from_date = this.formateventdate(this.Empdetails1.from_date)
    }
    
    let empdetails1:any = {
     
      organization_name: this.Empdetails1.organization_name.trim(),
      designation: this.Empdetails1.designation,
      from_date: this.Empdetails1.from_date,
      description: this.Empdetails1.description,
      notice_period: this.Empdetails1.notice_period,
      employment: this.Empdetails1.employment,
      employment_type: this.Empdetails1.employment_type,
      id: this.Empdetails1.id


    }
    if(!empdetails1.organization_name){
      this.present_name=true;
     
    }
  

    
    if (empdetails1.id == "" || empdetails1.id == null || empdetails1.id == undefined) {
    
      if (empdetails1.organization_name && empdetails1.designation && empdetails1.notice_period ) {
        this.spinnerService.show();
        this.userService.Addempdetails1(empdetails1).subscribe(
          data => {
             
            this.tostersuc.success('Present employment details added successfully');
           
            
            this.loadpresentdetails()
            this.spinnerService.hide();
          },
          error => {
          });
      
        this.Empdetails1 = [];
        this.errmsg = false;

      }
      else {
        this.errmsg = true;

      }
    }

    else {
      if (empdetails1.organization_name && empdetails1.designation && empdetails1.notice_period) {
        this.spinnerService.show();
        this.userService.Editpresentemp(empdetails1).subscribe(
          data => {
       
            this.tostersuc.success('Present employment details edited successfully');


            this.loadpresentdetails();
            this.spinnerService.hide();
          },
          error => {
           
          });
        this.errmsg = false;
        this.Empdetails1 = [];
        this.errmsg = false;
      }
      else {
        this.errmsg = true;
    
      }

    }
  
  }
 splitarray1:any=[]
editpresent(row:any) {

  this.splitarray1=[]
 this.desg1=[]

  this.Empdetails1.organization_name = row.organization_name;
 if(row.from_date){
  
  this.Empdetails1.from_dates = this.formateventdate(row.from_date);
  this.Empdetails1.from_date=new Date()
this.Empdetails1.from_date.setDate( this.Empdetails1.from_dates.split('T')[0].split('-')[2],this.Empdetails1.from_dates.split('T')[0].split('-')[1], this.Empdetails1.from_dates.split('T')[0].split('-')[0])
 }
this.Empdetails1.description = row.description;
  this.Empdetails1.notice_period = row.notice_period;
  this.Empdetails1.employment = row.employment;
  this.Empdetails1.employment_type = row.employment_type;
 
  this.Empdetails1.id = row.id;
  // var designation1=JSON.parse(row.designation)
  var designationnames1:any={id:'',itemName:''}

  // for (let index = 0; index < this.designation1.length; index++) {
  //  designationnames1.id=designation1[index].id
  //  designationnames1.itemName=designation1[index].name
  //  this.desg1.push(designationnames1)
  //  designationnames1={id:'',itemName:''}
   
   for (var i = 0; i < row.designation.length; i++) {
    this.splitarray1.push(row.designation.split(':'))

  }

  for (let index = 0; index < this.splitarray1.length; index++) {

    designationnames1.id = this.splitarray1[index][0]
    designationnames1.itemName = this.splitarray1[index][1]
    this.desg1.push(designationnames1)
    designationnames1 = { id: '', itemName: '' }

  }
  
   this.Empdetails1.designation = this.desg1;
   
  }
 
  
 



item:any
onItemSelect(item: any) {

  this.item = item

}
OnItemDeSelect(item: any) {
  this.item = item


}

  saveempdetails2() {
    
    this.d1=''
    if(this.Empdetails2.date){
      this.Empdetails2.from_date =this.formateventdate(this.Empdetails2.date[0]);
      this.Empdetails2.to_date = this.formateventdate(this.Empdetails2.date[1]);
    }

    this.Empdetails2.employment = "previous";

    // for (var b = 0; b < this.Empdetails2.designation.length; b++) {
    //   this.design1.id = this.Empdetails2.designation[b].id
    //   this.design1.name = this.Empdetails2.designation[b].itemName
    //   this.d1.push(this.design1);
    //  this.design1={ id: '', name: '' }
     
    // }

    for (var a = 0; a < this.Empdetails2.designation.length; a++) {

      this.d1=this.Empdetails2.designation[a].id + ':' + this.Empdetails2.designation[a].itemName

    }
  


    this.Empdetails2.designation = this.d1;

    let empdetails2:any = {
      organization_name: this.Empdetails2.organization_name.trim(),
      designation: this.Empdetails2.designation,
      from_date: this.Empdetails2.from_date,
      to_date: this.Empdetails2.to_date,
      description: this.Empdetails2.description,
      employment: this.Empdetails2.employment,
      employment_type: this.Empdetails2.employment_type,
      id: this.Empdetails2.id

    }
    if(!empdetails2.organization_name){
      this.previous_name=true;
      //this.tostersuc.error('Aadhar no is not valid',"hello");
    }
    if (empdetails2.id == "" || empdetails2.id == null || empdetails2.id == undefined) {
      if (empdetails2.organization_name && empdetails2.designation) {
        this.spinnerService.show();
        this.userService.Addempdetails2(empdetails2).subscribe(
          data => {
           
            this.tostersuc.success('Previous employment details added successfully');
            
          
            this.loadpreviousdetails()
            this.spinnerService.hide();
          },
          error => {
            
          });
   
        this.Empdetails2 = [];
        this.errmsg = false;
      }
      else {
        this.errmsg = true;
       
      }
    }

    else {
      if (empdetails2.organization_name && empdetails2.designation) {
        this.spinnerService.show();
        this.userService.Editpreviousemp(empdetails2).subscribe(
          data => {
          
            this.tostersuc.success('Previous employment details edited successfully');
           
          
            this.loadpreviousdetails();
            this.spinnerService.hide();
          },
          error => {
        
          });
        this.Empdetails2 = [];
        this.errmsg = false;
      }
      else {
        this.errmsg = true;
     
      }

    }
  }
 splitarray2:any=[]
  editprevious(row:any) {
   
   
    this.desg2=[]
    this.splitarray2=[]

   
    this.Empdetails2.organization_name = row.organization_name;
//     if(row.from_date){
//       
//      this.Empdetails2.from_dates = this.formateventdate(row.from_date);
//      this.Empdetails2.date=new Date()
//    this.Empdetails2.date.setDate( this.Empdetails2.from_dates.split('T')[0].split('-')[2],this.Empdetails2.from_dates.split('T')[0].split('-')[1], this.Empdetails2.from_dates.split('T')[0].split('-')[0])
//     }
//     if(row.to_date){
//    this.Empdetails2.to_dates = this.formateventdate(row.to_date);
//    this.Empdetails2.date=new Date()
//  this.Empdetails2.date.setDate( this.Empdetails2.to_dates.split('T')[0].split('-')[2],this.Empdetails2.to_dates.split('T')[0].split('-')[1], this.Empdetails2.to_dates.split('T')[0].split('-')[0])
  
//   }
    // this.Empdetails2.date = row.from_date;
    // this.Empdetails2.date = row.to_date;
    this.Empdetails2.description = row.description;
    this.Empdetails2.employment = row.employment;
    this.Empdetails2.employment_type = row.employment_type;
   

    var designationnames2:any={id:'',itemName:''}

  // for (let index = 0; index < this.designation1.length; index++) {
  //  designationnames1.id=designation1[index].id
  //  designationnames1.itemName=designation1[index].name
  //  this.desg1.push(designationnames1)
  //  designationnames1={id:'',itemName:''}
   
   for (var i = 0; i < row.designation.length; i++) {
    this.splitarray2.push(row.designation.split(':'))

  }

  for (let index = 0; index < this.splitarray2.length; index++) {

    designationnames2.id = this.splitarray2[index][0]
    designationnames2.itemName = this.splitarray2[index][1]
    this.desg2.push(designationnames2)
    designationnames2 = { id: '', itemName: '' }

  }
  
   this.Empdetails2.designation = this.desg2;
 

    this.Empdetails2.id = row.id;
   


  }
  saveempdetails3() {
    this.Empdetails3.employment = "other";
    if(this.Empdetails3.other_emp){
    let empdetails3:any = {
      other_emp: this.Empdetails3.other_emp.trim(),
      employment: this.Empdetails3.employment,
      employment_type: this.Empdetails3.employment_type,
      id: this.Empdetails3.id
    }
  
    if(!empdetails3.other_emp){
      this.other_name=true;
      
    }
    if (empdetails3.id == "" || empdetails3.id == null || empdetails3.id == undefined) {
      if (empdetails3.other_emp) {
        this.spinnerService.show();
        this.userService.Addempdetails3(empdetails3).subscribe(
          data => {
     
            this.tostersuc.success('Other employment details added successfully');
         
         
            this.loadotherdetails()
            this.spinnerService.hide();
          },
          error => {
    
          });
     
        this.Empdetails3 = [];
        this.errmsg = false;
      }
      else {
        this.errmsg = true;

        
      }
    }
    else {
      if (empdetails3.other_emp) {
        this.spinnerService.show();
        this.userService.Editotheremp(empdetails3).subscribe(
          data => {
          
            this.tostersuc.success('Other employment details edited successfully');
           

            this.loadotherdetails();
            this.spinnerService.hide();
          },
          error => {
          
          });

        this.Empdetails3 = [];
        this.errmsg = false;
      }
      else {
        this.errmsg = true;

      
      }
    }
    }
  }
  editother(row:any) {
   

    this.Empdetails3.other_emp = row.other_emp;
    this.Empdetails3.employment = row.employment;
    this.Empdetails3.employment_type = row.employment_type;
    this.Empdetails3.id = row.id;


  }

  
  delpresent:any
  deletepresent(row:any,template:any) {
   
    this.delpresent=row
    
    this.modalRef = this.modalService.show(
      template
    );
  
  }
  confirmation(res_confirm:boolean)
  
  
  {
    if(res_confirm)
    {
      this.userService.DeletepresentempById(this.delpresent).subscribe(
        data => {
         
          this.tostersuc.success('Present employment details deleted  successfully');
          this.modalRef.hide()
          this.presentemp = [];
     this.loadpresentdetails();
  
        },
        error => {
      
        });
  
    }
    else{
      this.modalRef.hide()
    }
   
  }
  


  delprevious:any
  deleteprevious(row:any,template:any) {
   
    this.delprevious=row
    
    this.modalRef = this.modalService.show(
      template
    );
  
  }
  confirmations(res_confirm:boolean)
  
  
  {
    if(res_confirm)
    {
      this.userService.DeletepreviousempById(this.delprevious.id).subscribe(
        data => {
         
          this.tostersuc.success('Previous employment details deleted  successfully');
          this.modalRef.hide()
          this.Empdetails2 = [];
          this.loadpreviousdetails();
  
        },
        error => {

        });
  
    }
    else{
      this.modalRef.hide()
    }
   
  }
  
 
  delother:any
  deleteother(row:any,template:any) {
   
    this.delother=row
    
    this.modalRef = this.modalService.show(
      template
    );
  
  }
  confirm(res_confirm:boolean)
  
  
  {
    if(res_confirm)
    {
      this.userService.DeleteotherempById(this.delother.id).subscribe(
        data => {
         
          this.tostersuc.success('other employment details deleted  successfully');
          this.modalRef.hide()
           
           this.loadotherdetails();
        },
        error => {
   
        });
  
    }
    else{
      this.modalRef.hide()
    }
   
  }
  

}
