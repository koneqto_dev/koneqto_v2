import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployerDesignationComponent } from './employer-designation.component';

describe('EmployerDesignationComponent', () => {
  let component: EmployerDesignationComponent;
  let fixture: ComponentFixture<EmployerDesignationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployerDesignationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployerDesignationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
