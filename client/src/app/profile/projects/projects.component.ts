import { Component, OnInit } from '@angular/core';
import { UserService,JobpostService } from '../../services/index';
import { ToastrService } from 'ngx-toastr';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit {

  project: any = {};
  pro: any = {};
  projectdetails: any={};
  allProjects: any = [];
  skill:any=[]
  keyskills:any=[]
  modalRef: BsModalRef;
  public listItems: Array<{ text: string, value: number }>;
  public value:any={}
  constructor(public userService: UserService,public jobpostingservice: JobpostService, private tostersuc: ToastrService,private modalService: BsModalService,private spinnerService: Ng4LoadingSpinnerService) { }

  skilldata:any;
  dropdownList1:any=[]
  dropdownSettings2:any = {
    singleSelection: false,
      text: "Select skills",
      enableCheckAll: false,
      enableSearchFilter: true,
      classes: "myclass custom-class",
      maxHeight: "150"
  };  
  
  editorConfig = {
    "editable": true,
    "spellcheck": false,
    "height": "auto",
    "minHeight": "0",
    "width": "auto",
    "minWidth": "0",
    "translate": "yes",
    "enableToolbar": true,
    "showToolbar": true,
    "placeholder": "",
    // "imageEndPoint": "",
    "toolbar": [
      ["bold", "italic", "underline", "unorderedList"]
      //  "strikeThrough", "superscript", "subscript"],
      // ["fontName", "fontSize", "color"],
      // ["justifyLeft", "justifyCenter", "justifyRight", "justifyFull", "indent", "outdent"],
      // ["cut", "copy", "delete", "removeFormat", "undo", "redo"],
      // ["paragraph", "blockquote", "removeBlockquote", "horizontalLine", "orderedList", "unorderedList"],
      // ["link", "unlink", "image", "video"]
    ]
  }

  event1:any
  getskillnames(event:any){
  
    this.dropdownList1=[]
    if(event.target.value){
     
      this.event1=event.target.value
    }else{
     
       this.event1=''
    }
    this.userService.getskillnames(this.event1).subscribe(data => {


      for (var a = 0; a < data.length; a++) {
        var skilldata:any = { "id": 2, "itemName": "Singapore" };
        skilldata.id = data[a].skill_id;
        skilldata.itemName = data[a].skill_name;
        this.dropdownList1.push(skilldata);
      }
      });
  }

  ngOnInit() {
    this.spinnerService.show();
    this.loadAllProjects();
    var event:any=''
    this.userService.getskillnames(event).subscribe(data => {


      for (var a = 0; a < data.length; a++) {
        var skilldata:any = { "id": 2, "itemName": "Singapore" };
        skilldata.id = data[a].skill_id;
        skilldata.itemName = data[a].skill_name;
        this.dropdownList1.push(skilldata);
      }
      });
  
    // this.userService.Getskillnames().subscribe(data => {
     
     
    //   for (var a = 0; a < data.length; a++) {
    //     var skilldata = { "id": 2, "itemName": "Singapore" };
    //     skilldata.id = a;
    //     skilldata.itemName = data[a].skill_name;
    //     this.dropdownList.push(skilldata);
    //   }
    // });
  
   
    this.selectedItems = []

  
  }
  // dropdownSettings = {}
  selectedItems:any = []
  dropdownList:any = [];
  keyskills1:any=[]
  // splitarray:any=[]
  splitarray2:any=[]
  displayskill:any=[]
  display2:any=[]
  loadAllProjects(){
    //this.spinnerService.show();
   this.splitarray2=[]
    this.display2=[]
    this.userService.Getallprojects().subscribe(
      data => {
  

        this.allProjects = data;
       
     for(let j=0;j<this.allProjects.length;j++){
     var splitarray:any=[]
     if(this.allProjects[j].skills){
        for(var i=0;i<this.allProjects[j].skills.length;i++){
          splitarray.push(this.allProjects[j].skills[i].split(':'))
            }
        this.splitarray2.push(splitarray)
          } 
      }
      for(let i=0;i<this.splitarray2.length;i++){
        
        var display:any=[]
        let j:any=0
        while(j<this.splitarray2[i].length){
       
        let sep:string=''
        if(this.splitarray2[i][j+1]){
          sep=','
        }
        display.push(this.splitarray2[i][j][1]+sep)
        j++

        }
        this.display2.push(display)
     
        }        
        this.spinnerService.hide();
      },
      error => {
   
      });  
    }
splitarray1:any=[]
  edit(row:any) {
    window.scrollTo(0, 0)
if(!this.project.skills){
  this.splitarray1=[]
  this.keyskills=[]
    //if(row.lang != null|| undefined){
    this.project.name = row.name;
    this.project.team = row.team;
    this.project.description = row.description;
    this.project.duration = row.duration;
    // var skill=JSON.parse(row.skills)
    var skillNames:any={id:'',itemName:''}
   
      for(var i=0;i<row.skills.length;i++){
        this.splitarray1.push(row.skills[i].split(':'))
        }
    
    for (let index = 0; index < this.splitarray1.length; index++) {
   
      
     skillNames.id=this.splitarray1[index][0]
     skillNames.itemName=this.splitarray1[index][1]
     this.keyskills.push(skillNames)
      skillNames={id:'',itemName:''}
  
  }
   this.project.skills=this.keyskills
  
    this.project.id = row.id;
  
   
  }
  }
  deleteproject:any
  delete(projectid:any,template:any) {
    
    this.deleteproject=projectid

    this.modalRef = this.modalService.show(
      template
    );

  }
  confirmation(res_confirm:boolean)


  {
    if(res_confirm)
    {
      this.userService.DeleteProjectById(this.deleteproject).subscribe(
        data => {
          
          this.tostersuc.success('projects deleted  Successfully');
          this.modalRef.hide()
          this.loadAllProjects()
  
        },
        error => {
        
        });
 
    }
    else{
      this.modalRef.hide()
    }
   
  }

  clearForm() {

    this.project.name = '';
    this.project.team = '';
    this.project.description = '';
    this.project.duration = '';
    this.project.skills='';
  
    this.project.id = '';
   
  }
  keyeventname(name: any) {
    const enamepattern = /^[A-Za-z' '0-9]+$/;
    let inputChar = String.fromCharCode(name.charCode);
    if (name.keyCode != 8 && !enamepattern.test(inputChar)) {
      name.preventDefault();
    }
  }

  keycheck(event:any) {


    const pattern = /^[0-9]/;
    let inputChar = String.fromCharCode(event.charCode)
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    
      
    }
  
  }
  fundMsg:boolean
  a(fund:string)
  {
    if(fund.split('')[0]=='0')
    {
      this.fundMsg=true
    }
    else{
      this.fundMsg=false
    }
  }



  keyPress(event: any) {
    
    
    const pattern = /^[0-9]\d*/;


    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  } 
  // keycheck(event) {


  //   const pattern = /^[0-9]/;
  //   let inputChar = String.fromCharCode(event.charCode)
  //   if (event.keyCode != 8 && !pattern.test(inputChar)) {
  //     event.preventDefault();
    
      
  //   }
  
  // }
  // fundMsg:boolean
  // a(fund:string)
  // {
  //   if(fund.split('')[0]=='0')
  //   {
  //     this.fundMsg=true
  //   }
  //   else{
  //     this.fundMsg=false
  //   }
  // }
  
  
errmsg:boolean
errName:any
errDuration:any
errSkillName:any
errDes:any
proj_skill:any
s:any = []
skillsObj:any = { id: '', name: '' }
proj_name:boolean=false;
proj_desc:boolean=false;
proj_dur:boolean=false;
proj_team:boolean=false;
testObject:any=[]
saveproject() {
  // this.s = [];
  if(!this.project.name){
    this.proj_name=true;
    ;}
    else{
      this.proj_name=false;
    }

  if(!this.project.description){
    this.proj_desc=true;
   }
   else{
    this.proj_desc=false;
  }


  if(!this.project.duration){
    this.proj_dur=true;
   }
   else{
    this.proj_dur=false;
  }


  if(!this.project.skills){
    this.proj_skill=true;
   }
   else{
    this.proj_skill=false;
  }
  if(!this.project.team){
    this.proj_team=true;
   }
   else{
    this.proj_team=false;
  }


  if(this.project.skills==''){
    this.proj_skill=true;
   }

  // if(!this.project.description){
  //   this.proj_desc=true;
  //   //this.tostersuc.error('Aadhar no is not valid',"hello");project.skills
  // }
  this.testObject=[]


if(this.project.skills){
for (var a = 0; a < this.project.skills.length; a++) {
  
  this.testObject.push(this.project.skills[a].id+':'+ this.project.skills[a].itemName)

  }
}
  // this.project.skills = JSON.stringify(this.s);
  // this.project.skills=this.testObject

  if(this.project.name && this.project.description && this.project.duration && this.project.skills && this.project.skills!='' && this.project.team){
  let project:any = {
    
    id: this.project.id,
    name: this.project.name.trim(),
    team: this.project.team,
    description: this.project.description.trim(),
    duration: this.project.duration.trim(),
    skills: this.testObject

  }




  if (project.id == "" || project.id == null || project.id == undefined) {
    if (project.name && project.description && project.duration && project.skills && project.skills!='' && this.project.team && project.team !=0) {
      this.userService.Addprojectdetails(project).subscribe(
        data => {
          this.project = {};
          this.proj_name=false;
          this.proj_desc=false;
          this.proj_dur=false;
          this.proj_skill=false;
          this.testObject=[]
          this.tostersuc.success('project details added successfully');
         this.loadAllProjects();
       
        },
        error => {
       
          
        });
        this.errmsg=false;
      }
      else{
        this.errmsg=true;
      }
    }
     else {
      this.testObject=[]
      if (project.name && project.description && project.duration && project.skills!=''&& project.skills && this.project.team && project.team !=0){
       
     this.userService.Editprojectdetails(project).subscribe(
       data => {
        this.project = {};
        this.proj_name=false;
          this.proj_desc=false;
          this.proj_dur=false;
          this.proj_skill=false;
          this.tostersuc.success('projects edited successfully');
          this.proj_name=false;
          this.proj_desc=false;
          this.proj_dur=false;
          this.proj_skill=false;
         
          this.loadAllProjects();
       
       
      
       },
       error => {
       
       });
      
       this.errmsg=false;
      }
      else{
        this.errmsg=true;
      }
    
  }
  
}

  }
  item:any
  OnItemSelect(item: any) {

    this.item = item

  }
  OnItemDeSelect(item: any) {
    this.item = item


  }
  
}

