import { Component, OnInit, TemplateRef, ViewChild,ElementRef  } from '@angular/core';
import { AuthenticationService, UserService, JobpostService } from '../../services/index';
import { ToastrService } from 'ngx-toastr';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ImageCompressService, ResizeOptions } from 'ng2-image-compress';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { environment } from '../../../environments/environment';
import { ActivatedRoute } from '@angular/router';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery';

@Component({
  selector: 'app-my-events',
  templateUrl: './my-events.component.html',
  styleUrls: ['./my-events.component.css']
})
export class MyEventsComponent implements OnInit {

  savedeventslist: any = {}
  fnarealist: any = [];
  mysavedeventslist: any = []
  myeventapplieduserslist: any = {}
  modalRef: BsModalRef;
  //msel1: any;
  msel: any;
  msel2: any;

  event_id: any;
  rows: any;
  columns: any;
  datatable: any = -1;
  regid: any;
  result: any;
  temp: any;
  min: any = new Date();
  eventname: any;
  eventfunctionalarea: any;
  eventtype: any;
  eventlocation: any;
  eventtime: any;
  eventdescription: any;
  eventaddress: any;
  eventimage: any;
  citydropdown:any = []
  eventypedropdown:any = []
  dropdownSettings:any = {}
  selectedItems:any = []
  dropdownList:any = [];
  eventtype1:any = [];
  eventlocation1:any = [];
  eventlocality1:any = [];

  constructor(private jobpostService: JobpostService, private route: ActivatedRoute, private toasterservice: ToastrService, private modalService: BsModalService, private imgCompressService: ImageCompressService, private userService: UserService, private spinnerService: Ng4LoadingSpinnerService) { }

  ApiUrl= environment.Apiurl
  openmodal(imageenlarge: TemplateRef<any>) {
    //this.modalRef = this.modalService.show(template);
    this.modalRef = this.modalService.show(
      imageenlarge,
      Object.assign({}, { class: 'gray modal-md' })
    );
  }
  closeModal(template: TemplateRef<any>) {
    this.modalRef.hide();

  }

  editorConfig = {
    "editable": true,
    "spellcheck": false,
    "height": "auto",
    "minHeight": "0",
    "width": "auto",
    "minWidth": "0",
    "translate": "yes",
    "enableToolbar": true,
    "showToolbar": true,
    "placeholder": "",
    // "imageEndPoint": "",
    "toolbar": [
      ["bold", "italic", "underline", "unorderedList"]
      //  "strikeThrough", "superscript", "subscript"],
      // ["fontName", "fontSize", "color"],
      // ["justifyLeft", "justifyCenter", "justifyRight", "justifyFull", "indent", "outdent"],
      // ["cut", "copy", "delete", "removeFormat", "undo", "redo"],
      // ["paragraph", "blockquote", "removeBlockquote", "horizontalLine", "orderedList", "unorderedList"],
      // ["link", "unlink", "image", "video"]
    ]
  }



  //Getting my events
  event_loc: any = [];
  event_loc1: any = [];
  event_loc_array: any = [];

  event_typ: any = [];
  event_typ1: any = [];
  event_typ_array: any = [];

  event_local: any = [];
  event_local1: any = [];
  event_local_array: any = [];
  event_local_names: any = []
  event_location_names: any = []

  loadmysavedevents() {
    this.spinnerService.show();
    this.jobpostService.mysavedevents()
      .subscribe(
        data => {
          this.event_typ = []
          this.event_typ1 = []
          this.event_typ_array = []
          this.event_loc = []
          this.event_loc1 = []
          this.event_loc_array = []
          this.event_local = []
          this.event_local1 = []
          this.event_local_array = []
          this.event_local_names = []
          this.event_location_names = []
          this.spinnerService.hide();

          this.mysavedeventslist = data;
     
          //event_typ
          for (let i = 0; i < this.mysavedeventslist.length; i++) {
            //this.event_typ.push(JSON.parse(this.mysavedeventslist[i].event_type))
            this.event_typ_array.push(this.mysavedeventslist[i].event_type[0].split(':'))
            
          }

          var event_typ_Names = { id: '', itemName: '' }

          for (let index = 0; index < this.event_typ_array.length; index++) {

            event_typ_Names.id = this.event_typ_array[index][0]
            event_typ_Names.itemName = this.event_typ_array[index][1]
            this.event_typ1.push(event_typ_Names)
            event_typ_Names = { id: '', itemName: '' }
            
          }

          this.event_typ = this.event_typ1;


          //event_loc
          for (let j = 0; j < this.mysavedeventslist.length; j++) {
            //this.event_loc.push(JSON.parse(this.mysavedeventslist[j].event_location))
            this.event_loc_array.push(this.mysavedeventslist[j].event_location[0].split(':'))
            this.event_location_names.push(this.event_loc_array[j][1])
            
          }

          var event_loc_Names = { id: '', itemName: '' }

          for (let index = 0; index < this.event_loc_array.length; index++) {

            event_loc_Names.id = this.event_loc_array[index][0]
            event_loc_Names.itemName = this.event_loc_array[index][1]
            this.event_loc1.push(event_loc_Names)
            event_loc_Names = { id: '', itemName: '' }
            
          }

          this.event_loc = this.event_loc1;

          //event_local
          for (let k = 0; k < this.mysavedeventslist.length; k++) {

            if (this.mysavedeventslist[k].event_locality) {
              this.event_local_array.push(this.mysavedeventslist[k].event_locality[0].split(':'))
              this.event_local_names.push(this.event_local_array[k][1])
              
            }

            else {
              this.event_local_array.push(null);
            }

          }

          var event_local_Names = { id: '', itemName: '' }

          for (let index = 0; index < this.event_local_array.length; index++) {
            if (this.event_local_array[index]) {
              event_local_Names.id = this.event_local_array[index][0]
              event_local_Names.itemName = this.event_local_array[index][1]
              this.event_local1.push(event_local_Names)
              event_local_Names = { id: '', itemName: '' }
              
            }
            else {
              this.event_local1.push(null)
            }
          }

          this.event_local = this.event_local1;
  
        },
        error => {
      
        });
  }

  //Deleting my event
  deleteeventid:any
  check_app:any
  deleteeventimage:any
  eventvideo:any
  deletemyevent(eventid:any,eventimage:any,eventvideo:any, template) {
    this.deleteeventimage=eventimage
    this.jobpostService.checkApplicants(eventid, 'event').subscribe(data => {
      this.check_app = eval(data[0].count)

      if (this.check_app == 0) {

        this.deleteeventid = eventid
        this.eventvideo=eventvideo

        this.modalRef = this.modalService.show(
          template
        );
      }
      else {
        this.toasterservice.error("Someone Interested in this Event", "Unable to delete.")
      }
    });
  }
  confirmation(res_confirm: boolean) {
    if (res_confirm) {
      this.jobpostService.deletemyevent(this.deleteeventid,this.deleteeventimage,this.eventvideo).subscribe(
        data => {

          this.toasterservice.success('Event Closed SuccessFully!', 'EVENT DELETED!');
          this.modalRef.hide()

          this.loadmysavedevents()

        },
        error => {

        });

    }
    else {
      this.modalRef.hide()
    }

  }

  cur_des: any = [];
  languagesdisplay: any = [];
  pskills: any = []
  a: any = []


  keyskillsarraydisplay: any = []
  skilldetails: any = []
  presentdesignation: any = [];
  previousdesignation: any = [];
  gradsarray: any = []
  graduniversity: any = []
  pgarray: any = []
  pguniversity: any = [];
  phdarray: any = []
  phduniversity: any = [];
  proskillsarray: any = []
  proskillsarraydisplay: any = []
  skillsarraydisplay: any = []
  openresume(resume:any, user_registration_id:any) {

      this.skilldetails = [];
      this.cur_des = [];
      this.skillsarraydisplay = [];
      this.previousdesignation = [];
      this.presentdesignation = [];
      this.keyskillsarraydisplay = []
      this.skilldetails = []
      this.presentdesignation = [];
      this.previousdesignation = [];
      this.gradsarray = []
      this.graduniversity = []
      this.pgarray = []
      this.pguniversity = [];
      this.phdarray = []
      this.phduniversity = [];
      this.proskillsarray = []
      this.proskillsarraydisplay = []
      this.skillsarraydisplay = []
    this.userService.getprofiledetails(user_registration_id).subscribe(data => {

      this.result = data[0];
      this.skillsarraydisplay = []

      // this.userService.getlogincount(user_registration_id).subscribe(data1 => {

      //   this.count = data1;

      //   if (this.count[0].lastlogin_time % 10 == 0) {

      //     if (this.result.role == 3) {
      //       if (this.result.skill_details == null || this.result.educational_details == null) {
      //         this.handleLoad(this.ctemplate)
      //       }
      //     }
      //     else {
      //       if (this.result.presentemp_details == null || this.result.skill_details == null || this.result.educational_details == null) {
      //         this.handleLoad(this.ctemplate)
      //       }
      //     }
      //   }

      // })



      if (this.result.presentemp_details != null) {
        for (let i = 0; i < this.result.presentemp_details.length; i++) {
          this.presentdesignation.push(this.result.presentemp_details[i].designation.split(':')[1])

        }
      }

      if (this.result.previousemp_details != null) {
        for (let i = 0; i < this.result.previousemp_details.length; i++) {
          this.previousdesignation.push(this.result.previousemp_details[i].designation.split(':')[1])

        }
      }
      if (this.result.project_details) {

        for (let j = 0; j < this.result.project_details.length; j++) {
          var splitarray: any = []
          if (this.result.project_details[j].skills) {
            for (var i = 0; i < this.result.project_details[j].skills.length; i++) {
              splitarray.push(this.result.project_details[j].skills[i].split(':'))
            }
          }
          this.proskillsarray.push(splitarray)
        }
        for (let i = 0; i < this.proskillsarray.length; i++) {
          var display = []
          let j = 0
          while (j < this.proskillsarray[i].length) {

            let sep = ''
            if (this.proskillsarray[i][j + 1]) {
              sep = ','
            }
            display.push(this.proskillsarray[i][j][1] + sep)
            j++

          }
          this.proskillsarraydisplay.push(display)

        }
        // for (let i = 0; i < this.result.project_details.length; i++) {

        //   this.skill.push(JSON.parse(this.result.project_details[i].skills))

        // }
      }

      if (this.result.skill_details) {
        for (let j = 0; j < this.result.skill_details.length; j++) {
          this.skilldetails.push(this.result.skill_details[j][0].split(':'))
        }
      }
      let count = 0
      // for(let i=0;i<this.skilldetails.length;i++){
      while (count < this.skilldetails.length) {


        let sep = ''
        if (this.skilldetails[count + 1]) {
          sep = ','
        }
        // display.push(this.skilldetails[i][1]+sep)         
        this.skillsarraydisplay.push(this.skilldetails[count][1] + sep)
        // sep=''
        count++
      }
      //-------------------------------------------------------------------------------------------------------------------

      if (this.result.achievement_details != null) {
        for (var j = 0; j < this.result.achievement_details.length; j++) {
          this.picsArr = this.result.achievement_details[j].image;
          // if (this.picsArr != null) {
          //   for (var i = 0; i < this.picsArr.length - 1; i++) {
          //     this.picsstring[j] = this.picsArr.split(' ')

          //   }
          // }

        }

      }

      if (this.result.achievement_details != null) {
        for (var j = 0; j < this.result.achievement_details.length; j++) {
          // let imagesArr:any=[];
          let imagesArr: any = [];
          this.picsArr = this.result.achievement_details[j].image;

          // if (this.picsArr != null) {
          //   for (var i = 0; i < this.picsArr.length - 1; i++) {


          //     this.picsstring[j] = this.picsArr.split(' ')

          //   }

          // }

          if (this.picsArr.length>0) {
            for (var k = 0; k < this.picsArr.length; k++) {


              this.Imagejson =
                {
                  small: this.ApiUrl + '/achievements/' + this.picsArr[k],
                  medium: this.ApiUrl + '/achievements/' + this.picsArr[k],
                  big: this.ApiUrl + '/achievements/' + this.picsArr[k]
                }

              imagesArr.push(this.Imagejson);
            }
          
          //}
          this.imagesArr1.push(imagesArr)
          }else{
            this.imagesArr1.push(null)
          }
        }

      }
      this.galleryOptions = [
        {
          width: '400px',
          height: '300px',
          thumbnailsColumns: 4,
          imageAnimation: NgxGalleryAnimation.Slide
        },
        // max-width 800
        {
          breakpoint: 800,
          width: '100%',
          height: '600px',
          imagePercent: 80,
          thumbnailsPercent: 20,
          thumbnailsMargin: 20,
          thumbnailMargin: 20
        },
        // max-width 400
        {
          breakpoint: 400,
          preview: false
        }
      ];
      if (this.result.language_known) {
        for (let i = 0; i < this.result.language_known.length; i++) {
          this.languagesdisplay.push(JSON.parse(this.result.language_known[i].language_name))
        }
      }
      //university_or_institute
      //   if(this.result.grad_details){
      //   for (let i = 0; i < this.result.grad_details.length; i++) {
      //     this.graduniversity.push(JSON.parse(this.result.grad_details[i].university_or_institute)[0].name)
      //   }
      // }
      if (this.result.grad_details) {

        for (let j = 0; j < this.result.grad_details.length; j++) {
          var splitarray: any = []
          if (this.result.grad_details[j].university_or_institute) {
            for (var i = 0; i < this.result.grad_details[j].university_or_institute.length; i++) {
              splitarray.push(this.result.grad_details[j].university_or_institute[i].split(':'))

            }
          }
          this.gradsarray.push(splitarray)

        }
        for (let i = 0; i < this.gradsarray.length; i++) {
          var display = []
          let j = 0
          while (j < this.gradsarray[i].length) {

            let sep = ''
            if (this.gradsarray[i][j + 1]) {
              sep = ','
            }
            display.push(this.gradsarray[i][j][1] + sep)
            j++

          }
          this.graduniversity.push(display)

        }
        // for (let i = 0; i < this.result.project_details.length; i++) {

        //   this.skill.push(JSON.parse(this.result.project_details[i].skills))

        // }
      }
      if (this.result.pg_details) {
        for (let j = 0; j < this.result.pg_details.length; j++) {
          var splitarray: any = []
          if (this.result.pg_details[j].university_or_institute) {
            for (var i = 0; i < this.result.pg_details[j].university_or_institute.length; i++) {
              splitarray.push(this.result.pg_details[j].university_or_institute[i].split(':'))

            }
          }
          this.pgarray.push(splitarray)

        }
        for (let i = 0; i < this.pgarray.length; i++) {
          var display = []
          let j = 0
          while (j < this.pgarray[i].length) {

            let sep = ''
            if (this.pgarray[i][j + 1]) {
              sep = ','
            }
            display.push(this.pgarray[i][j][1] + sep)
            j++

          }
          this.pguniversity.push(display)

        }
      }
      if (this.result.phd_details) {
        for (let j = 0; j < this.result.phd_details.length; j++) {
          var splitarray: any = []
          if (this.result.phd_details[j].university_or_institute) {
            for (var i = 0; i < this.result.phd_details[j].university_or_institute.length; i++) {
              splitarray.push(this.result.phd_details[j].university_or_institute[i].split(':'))

            }
          }
          this.phdarray.push(splitarray)

        }
        for (let i = 0; i < this.phdarray.length; i++) {
          var display = []
          let j = 0
          while (j < this.phdarray[i].length) {

            let sep = ''
            if (this.phdarray[i][j + 1]) {
              sep = ','
            }
            display.push(this.phdarray[i][j][1] + sep)
            j++

          }
          this.phduniversity.push(display)

        }
      }

      if (this.result.current_designation) {

        // let objson = JSON.parse(this.result.current_designation)

        // for (let i = 0; i < objson.length; i++) {
          this.cur_des.push(this.result.current_designation.split(':')[1])
        // }
      }
    })

    // this.userService.applicantprofile(user_registration_id).subscribe(data => {
    //   this.skilldetails = [];
    //   this.cur_des = [];
    //   this.skillsarraydisplay = [];
    //   this.previousdesignation = [];
    //   this.presentdesignation = [];
    //   this.keyskillsarraydisplay = []
    //   this.skilldetails = []
    //   this.presentdesignation = [];
    //   this.previousdesignation = [];
    //   this.gradsarray = []
    //   this.graduniversity = []
    //   this.pgarray = []
    //   this.pguniversity = [];
    //   this.phdarray = []
    //   this.phduniversity = [];
    //   this.proskillsarray = []
    //   this.proskillsarraydisplay = []
    //   this.skillsarraydisplay = []
      
    //   this.result = data[0]

    //   //current_designation
    //   if (this.result.current_designation) {
    //     // for (let i = 0; i < this.result.current_designation.length; i++) {
    //       this.cur_des.push(this.result.current_designation.split(':')[1])
    //     // }
    //   }

    //   //language_known
    //   if (this.result.language_known) {
    //     for (let i = 0; i < this.result.language_known.length; i++) {
    //       this.languagesdisplay.push(JSON.parse(this.result.language_known[i].language_name))
    //     }
    //   }

    //   //university_or_institute
    //   if (this.result.grad_details) {

    //     for (let j = 0; j < this.result.grad_details.length; j++) {
    //       var splitarray: any = []
    //       if (this.result.grad_details[j].university_or_institute) {
    //         for (var i = 0; i < this.result.grad_details[j].university_or_institute.length; i++) {
    //           splitarray.push(this.result.grad_details[j].university_or_institute[i].split(':'))
  
    //         }
    //       }
    //       this.gradsarray.push(splitarray)

    //     }
    //     for (let i = 0; i < this.gradsarray.length; i++) {
    //       var display = []
    //       let j = 0
    //       while (j < this.gradsarray[i].length) {

    //         let sep = ''
    //         if (this.gradsarray[i][j + 1]) {
    //           sep = ','
    //         }
    //         display.push(this.gradsarray[i][j][1] + sep)
    //         j++

    //       }
    //       this.graduniversity.push(display)

    //     }

    //   }

    //   if (this.result.pg_details) {
    //     for (let j = 0; j < this.result.pg_details.length; j++) {
    //       var splitarray: any = []
    //       if (this.result.pg_details[j].university_or_institute) {
    //         for (var i = 0; i < this.result.pg_details[j].university_or_institute.length; i++) {
    //           splitarray.push(this.result.pg_details[j].university_or_institute[i].split(':'))
             
    //         }
    //       }
    //       this.pgarray.push(splitarray)
        

    //     }
    //     for (let i = 0; i < this.pgarray.length; i++) {
    //       var display = []
    //       let j = 0
    //       while (j < this.pgarray[i].length) {

    //         let sep = ''
    //         if (this.pgarray[i][j + 1]) {
    //           sep = ','
    //         }
    //         display.push(this.pgarray[i][j][1] + sep)
    //         j++

    //       }
    //       this.pguniversity.push(display)

    //     }
    //   }
    //   if (this.result.phd_details) {
    //     for (let j = 0; j < this.result.phd_details.length; j++) {
    //       var splitarray: any = []
    //       if (this.result.phd_details[j].university_or_institute) {
    //         for (var i = 0; i < this.result.phd_details[j].university_or_institute.length; i++) {
    //           splitarray.push(this.result.phd_details[j].university_or_institute[i].split(':'))
    //         }
    //       }
    //       this.phdarray.push(splitarray)
    //     }
    //     for (let i = 0; i < this.phdarray.length; i++) {
    //       var display = []
    //       let j = 0
    //       while (j < this.phdarray[i].length) {

    //         let sep = ''
    //         if (this.phdarray[i][j + 1]) {
    //           sep = ','
    //         }
    //         display.push(this.phdarray[i][j][1] + sep)
    //         j++

    //       }
    //       this.phduniversity.push(display)

    //     }
    //   }



    //   //key_skills

    //   for (let j = 0; j < this.result.skill_details.length; j++) {
    //     this.skilldetails.push(this.result.skill_details[j][0].split(':'))

    //   }
    //   let count = 0
    //   while (count < this.skilldetails.length) {
    //     let sep = ''
    //     if (this.skilldetails[count + 1]) {
    //       sep = ','
    //     }
    //     // display.push(this.skilldetails[i][1]+sep)         
    //     this.skillsarraydisplay.push(this.skilldetails[count][1] + sep)
    //     // sep=''
    //     count++
    //   }

    //   //presentdesignation
    //   if (this.result.presentemp_details != null) {
    //     for (let i = 0; i < this.result.presentemp_details.length; i++) {
    //       this.presentdesignation.push(this.result.presentemp_details[i].designation.split(':')[1])
    //     }
    //   }

    //   //previousdesignation
    //   if (this.result.previousemp_details != null) {
    //     for (let i = 0; i < this.result.previousemp_details.length; i++) {
    //       this.previousdesignation.push(this.result.previousemp_details[i].designation.split(':')[1])
    //     }
    //   }

    //   if (this.result.project_details) {
        
    //     for (let j = 0; j < this.result.project_details.length; j++) {
    //       var splitarray: any = []
    //       if (this.result.project_details[j].skills) {
    //         for (var i = 0; i < this.result.project_details[j].skills.length; i++) {
    //           splitarray.push(this.result.project_details[j].skills[i].split(':'))
    //         }
    //       }
    //       this.proskillsarray.push(splitarray)

    //     }
    //     for (let i = 0; i < this.proskillsarray.length; i++) {
    //       var display = []
    //       let j = 0
    //       while (j < this.proskillsarray[i].length) {

    //         let sep = ''
    //         if (this.proskillsarray[i][j + 1]) {
    //           sep = ','
    //         }
    //         display.push(this.proskillsarray[i][j][1] + sep)
    //         j++

    //       }
    //       this.proskillsarraydisplay.push(display)

    //     }

    //   }

    // })

    this.modalRef = this.modalService.show(
      resume,
      Object.assign({}, { class: 'gray modal-lg' })

    );
    //  this. skillsarraydisplay='';
  }

  //To show applied users for my event
  showApplicants(eventid: any) {
    this.spinnerService.show();
    if (this.datatable == -1) {
      this.datatable = eventid;
    }
    else {
      this.datatable = -1;
    }
    this.event_id = eventid;

    this.jobpostService.getmyeventappliedusers(this.event_id)
      .subscribe(
        data1 => {
          this.myeventapplieduserslist = data1;
          this.temp = [...data1];
          this.rows = this.myeventapplieduserslist;
          this.spinnerService.hide();
        },
        error => {
       
        });
  }

  //To filter by search
  updateFilter(event:any) {

    const val:any = event.target.value.toLowerCase();

    // filter our data
    const temprow :any= this.temp.filter(function (temp) {
      return temp.username.toLowerCase().indexOf(val) !== -1 || !val;
    });
    // update the rows
    this.rows = temprow;
    // Whenever the filter changes, always go back to the first page
    //this.rows.offset = 0;
  }

  //closing fn for interested user resume
  closeModalResume(template2: TemplateRef<any>) {
    this.modalRef.hide();
  }


  //Opening pop up when editing an event
  postevent: any = {}
  openModalEventEditing(template1: TemplateRef<any>, msel) {
    this.jobpostService.checkApplicants(msel.event_id, 'event').subscribe(data => {
      this.check_app = eval(data[0].count)

      if (this.check_app == 0) {
        this.selectedItems = []
        this.dropdownSettings = {
          singleSelection: true,
          text: "Select ",
          enableCheckAll: false,
          enableSearchFilter: true,
          classes: "myclass custom-class",
          maxHeight: "150",
          showCheckbox: false
        }


        this.postevent.eventid = msel.event_id;
        this.postevent.eventname = msel.event_name;
        // this.postevent.eventfunctionalarea = msel.functional_area;
        //this.postevent.eventtype
        // this.eventtype1 = []
        // var eventtype = [];
        // eventtype.push(msel.event_type[0].split(':'))
        // var eventtypenames = { id: '', itemName: '' }
        // 
        // eventtypenames.id = eventtype[0][0]
        // eventtypenames.itemName = eventtype[0][1]
        // this.eventtype1.push(eventtypenames)
        // eventtypenames = { id: '', itemName: '' }
        // 
        // this.postevent.eventtype = this.eventtype1;


        // //this.postevent.eventlocation
        // this.eventlocation1 = []
        // var eventlocation = [];
        // eventlocation.push(msel.event_location[0].split(':'))
        // var eventlocationnames = { id: '', itemName: '' }
        // eventlocationnames.id = eventlocation[0][0]
        // eventlocationnames.itemName = eventlocation[0][1]
        // this.eventlocation1.push(eventlocationnames)
        // eventlocationnames = { id: '', itemName: '' }
        // 
        // this.postevent.eventlocation = this.eventlocation1;


        // //this.postevent.eventlocality
        // this.eventlocality1 = []
        // var eventlocality = [];
        // if (msel.event_locality) {
        //   eventlocality.push(msel.event_locality[0].split(':'))
        //   var eventlocalitynames = { id: '', itemName: '' }
        //   eventlocalitynames.id = eventlocality[0][0]
        //   eventlocalitynames.itemName = eventlocality[0][1]
        //   this.eventlocality1.push(eventlocalitynames)
        // }
        // eventlocalitynames = { id: '', itemName: '' }
        // 
        // this.postevent.eventlocality = this.eventlocality1;
        this.postevent.eventtime = msel.event_start_date + ' ' + '~' + ' ' + msel.event_end_date;
        this.postevent.eventdescription = msel.event_description;
        this.postevent.eventaddress = msel.event_address;

        this.modalRef = this.modalService.show(
          template1,
          Object.assign({}, { class: 'gray modal-lg' })
        );
      }
      else {
        this.toasterservice.error('"Someone Interested for this Event", "Not able to Edit"')
      }
    });

  }


  //Event posting image
  checkImage
  //checkedImage
  onFileSelected(fileInput: any) {
    //this.image_valid=true;
    //
    this.checkImage = fileInput.target.files;
    //this.checkedImage = this.checkImage.replace("data:image/jpeg;base64,", "");

  }

  //Editing events validations
  keyeventname(name: any) {
    const enamepattern = /^[A-Za-z' ']+$/;
    let inputChar = String.fromCharCode(name.charCode);
    if (name.keyCode != 8 && !enamepattern.test(inputChar)) {
      name.preventDefault();
    }
  }

  keyeventtype(type: any) {
    const etypepattern = /^[A-Za-z' ']+$/;
    let inputChar = String.fromCharCode(type.charCode);
    if (type.keyCode != 8 && !etypepattern.test(inputChar)) {
      type.preventDefault();
    }
  }

  keyeventloc(loc: any) {
    const elocpattern = /^[A-Za-z' ']+$/;
    let inputChar = String.fromCharCode(loc.charCode);
    if (loc.keyCode != 8 && !elocpattern.test(inputChar)) {
      loc.preventDefault();
    }
  }

  keyeventdate(date: any) {
    const pattern = /^(((0[1-9]|[12][0-9]|30)[-/]?(0[13-9]|1[012])|31[-/]?(0[13578]|1[02])|(0[1-9]|1[0-9]|2[0-8])[-/]?02)[-/]?[0-9]{4}|29[-/]?02[-/]?([0-9]{2}(([2468][048]|[02468][48])|[13579][26])|([13579][26]|[02468][048]|0[0-9]|1[0-6])00))$/;
    let inputChar = String.fromCharCode(date.charCode)
    if (date.keyCode != 8 && !pattern.test(inputChar)) {
      date.preventDefault();
    }
  }

  formatdate: any;
  formateventdate(date:any) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear(),
      hour = '' + d.getHours(),
      minute = '' + d.getMinutes();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    if (hour.length < 2) hour = '0' + hour;
    if (minute.length < 2) minute = '0' + minute;
    this.formatdate = [year, month, day].join('-') + 'T' + hour + ':' + minute + '';
    return this.formatdate;
  }

  //Editing an event function
  posteventall: any;
  eventstartdate:any
  eventenddate:any
  eventtypes: any;
  cities: any;

  event_name: boolean = false;
  desc_name: boolean = false;
  addr_name: boolean = false
  postevent_res: boolean;
  typeObj: any = []
  cityObj: any = []
  locObj: any = []
  editmyevent(postevent: any) {
    
    this.spinnerService.show();
    if (this.postevent.eventname && this.postevent.eventtime && this.postevent.eventdescription && this.postevent.eventaddress && postevent) {

      postevent.eventstartdate = this.formateventdate(postevent.eventtime[0]);
      postevent.eventenddate = this.formateventdate(postevent.eventtime[1]);
      postevent.eventname = postevent.eventname.trim();
      postevent.eventdescription = postevent.eventdescription.trim();
      postevent.eventaddress = postevent.eventaddress.trim();
      postevent.eventid = this.postevent.eventid;

      

      this.jobpostService.saveevent(postevent)
        .subscribe(
          data => {
            this.modalRef.hide();
            this.toasterservice.success('Event updated successfully!', 'EVENT Updated!');
            this.loadmysavedevents();
          })
    }
  }



  //closing for editing an event pop up 
  closeModalEventEditing(template1: TemplateRef<any>) {
    this.modalRef.hide();
  }

  city_id:any
  localities:any = []
  getLocality(event:any) {
    this.localities = []
    this.city_id = event.id
    this.jobpostService.getLocalitybyid1(this.city_id)
      .subscribe(
        data => {
          for (var a = 0; a < data.length; a++) {
            var c = { "id": 2, "itemName": "Singapore" };
            c.id = data[a].id;
            // c.id=a;
            c.itemName = data[a].area;
            this.localities.push(c);
          }
        },
        error => {
        });
  }
  loginuserid:any
  count:any
picsArr: any;

Imagejson:any={}
imagesArr1:any=[]
galleryOptions: NgxGalleryOptions[];
@ViewChild('content') content: ElementRef
confirm: BsModalRef
// confirmtemplate=confirm

confirmtemplate: TemplateRef<any>
@ViewChild('confirmtemplate') ctemplate: ElementRef;
handleLoad(template) {
  this.ctemplate
  this.modalRef = this.modalService.show(
    this.ctemplate,
    Object.assign({
      backdrop: 'static',
      keyboard: false
    }, { class: 'gray modal-md' })
  );
}
  ngOnInit() {

    //Getting functional area data 
    

    this.userService.getfunctinalareadata()
      .subscribe(
        data => {
          this.fnarealist = data;
        },
        error => {
       });

    //loadcityinevents
   /*  this.jobpostService.loadcityinevents1().subscribe(data => {
      this.cities = data;
      for (var a = 0; a < data.length; a++) {
        var c = { "id": 2, "itemName": "Singapore" };
        c.id = data[a].city_id;
        c.itemName = data[a].city_name;
        this.citydropdown.push(c);
      }
    }) */

    //loadeventtypes
    this.jobpostService.loadeventtypes().subscribe(data => {
      this.eventtypes = data;
      for (var a = 0; a < data.length; a++) {
        var c = { "id": 2, "itemName": "Singapore" };
        c.id = data[a].event_type_id;
        c.itemName = data[a].event_type;
        this.eventypedropdown.push(c);
      }
    })




    /*  //loadcityinevents
     this.jobpostService.loadcityinevents().subscribe(data => {
       this.cities = data;
 
     })
     //loadeventtypes
     this.jobpostService.loadeventtypes().subscribe(data => {
       this.eventtypes = data;
 
     }) */

    //Getting my events
    this.loadmysavedevents();
  }

}
