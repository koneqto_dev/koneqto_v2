import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileRoutingModule } from './profile-routing.module';
import { SnapShotComponent } from './snap-shot/snap-shot.component';
import { SkillsComponent } from './skills/skills.component';
import { EducationComponent } from './education/education.component';
import { EmployerDesignationComponent } from './employer-designation/employer-designation.component';
import { ProjectsComponent } from './projects/projects.component';
import { MoreDetailsComponent } from './more-details/more-details.component';
import { VideoResumeComponent } from './video-resume/video-resume.component';
import { SampleResumeComponent } from './sample-resume/sample-resume.component';
import { AppliedJobsComponent } from './applied-jobs/applied-jobs.component';
import { MyJobPostsComponent } from './my-job-posts/my-job-posts.component';
import { MyEventsComponent } from './my-events/my-events.component';
import { MyAdvicesComponent } from './my-advices/my-advices.component';
import { MyCharityComponent } from './my-charity/my-charity.component';
import { IntrestedEventsComponent } from './intrested-events/intrested-events.component';
import { AchievementsComponent } from './achievements/achievements.component';
import { CertificationsComponent } from './certifications/certifications.component';
import { MyGalleryComponent } from './my-gallery/my-gallery.component';
import { CompanyDetailsComponent } from './company-details/company-details.component';
import { FormsModule } from '@angular/forms';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { TabsModule } from 'ngx-bootstrap';
import { NgxEditorModule } from 'ngx-editor';
import { AvatarModule } from 'ngx-avatar';
import { RatingModule } from 'ngx-bootstrap/rating';
import {} from 'ngx-datatable';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxGalleryModule } from 'ngx-gallery';
import { ProfileLeftMenuComponent } from './profile-left-menu/profile-left-menu.component'
import { PDFExportModule } from '@progress/kendo-angular-pdf-export';
import { ProfileDashboardComponent } from './profile-dashboard/profile-dashboard.component';
import { JobProfileLayoutComponent } from './job-profile-layout/job-profile-layout.component';

@NgModule({
  declarations: [SnapShotComponent, SkillsComponent, EducationComponent, EmployerDesignationComponent, ProjectsComponent, MoreDetailsComponent, VideoResumeComponent, SampleResumeComponent, AppliedJobsComponent, MyJobPostsComponent, MyEventsComponent, MyAdvicesComponent, MyCharityComponent, IntrestedEventsComponent, AchievementsComponent, CertificationsComponent, MyGalleryComponent, CompanyDetailsComponent, ProfileLeftMenuComponent, ProfileDashboardComponent, JobProfileLayoutComponent,],
  imports: [
    CommonModule,
    ProfileRoutingModule,
    FormsModule,
    AngularMultiSelectModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    Ng4LoadingSpinnerModule.forRoot(),
    TabsModule,
    NgxEditorModule,
    AvatarModule,
    RatingModule,
    NgxDatatableModule,
    NgxGalleryModule,
    PDFExportModule,

    



  ]
})
export class ProfileModule { }
