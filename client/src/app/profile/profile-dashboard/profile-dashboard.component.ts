import { Component, OnInit } from '@angular/core';
import { SimpleCrypt } from "ngx-simple-crypt";

@Component({
  selector: 'app-profile-dashboard',
  templateUrl: './profile-dashboard.component.html',
  styleUrls: ['./profile-dashboard.component.css']
})
export class ProfileDashboardComponent implements OnInit {
  isrecruter: boolean = false;
  RoleID
  constructor() { }

  simpleCrypt = new SimpleCrypt();
  ngOnInit() {
    var rollId1 = localStorage.getItem('RoleID')
    var roleID = this.simpleCrypt.decode("my-key", rollId1);
    this.RoleID = eval(roleID);
    if (this.RoleID == 5) {
      this.isrecruter = true;
    }

  }

}
