import { Component, OnInit } from '@angular/core';
import { UserService} from '../../services';
import { ToastrService } from 'ngx-toastr';
import { ImageCompressService, ResizeOptions } from 'ng2-image-compress';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery';
import { environment } from '../../../environments/environment';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
//import { FileUploader } from 'ng2-file-upload';
import { FileUploadModule, FileUploader} from "ng2-file-upload";
// import { VgControlsModule } from 'videogular2/controls';
const URL = 'https://evening-anchorage-3159.herokuapp.com/api/';
import {SimpleCrypt} from "ngx-simple-crypt";

@Component({
  selector: 'app-achievements',
  templateUrl: './achievements.component.html',
  styleUrls: ['./achievements.component.css']
})
export class AchievementsComponent implements OnInit {
  galleryOptions: NgxGalleryOptions[];
  // galleryImages: NgxGalleryImage[][];
  public img:any = '';
  private url:any= '';
  profileImage:any; 
  profile_img:any
 
  imgsrc:any ;
  user:any;
  achievement:any={}
  Achievements:any=[];



  selectedFile = null;


 
croppedImage: any =''
image_valid:any
processedImage:any
loginuserid1=localStorage.getItem('loginuserid')
 simpleCrypt = new SimpleCrypt();       
 loginuseriddecoded = this.simpleCrypt.decode("my-key",this.loginuserid1);
loginuserid=eval(this.loginuseriddecoded)
//this.loginuserid=localStorage.getItem('loginuserid')
modalRef: BsModalRef;

public uploader:FileUploader = new FileUploader({url: environment.Apiurl + '/profile/uploads/?AuthToken=' + localStorage.getItem('AuthToken'), allowedMimeType: [ 'image/jpeg','image/png','image/jpg']});
filestype: any=[];
jobimage: { type: string; img_data: string; };
achidata: any=[];
 
constructor(public userservice: UserService,private tostersuc: ToastrService,private spinnerService: Ng4LoadingSpinnerService,private modalService: BsModalService,private imgCompressService: ImageCompressService) {

   
   }


   Apiurl = environment.Apiurl
  images:any
  pics:string;
  picsArr:any;
  picsstring:any=[];

  Imagejson:any={};
  imagesArr1:any=[];
  editorConfig = {
    "editable": true,
    "spellcheck": false,
    "height": "auto",
    "minHeight": "0",
    "width": "auto",
    "minWidth": "0",
    "translate": "yes",
    "enableToolbar": true,
    "showToolbar": true,
    "placeholder": "",
    // "imageEndPoint": "",
    "toolbar": [
      ["bold", "italic", "underline", "unorderedList"]
      //  "strikeThrough", "superscript", "subscript"],
      // ["fontName", "fontSize", "color"],
      // ["justifyLeft", "justifyCenter", "justifyRight", "justifyFull", "indent", "outdent"],
      // ["cut", "copy", "delete", "removeFormat", "undo", "redo"],
      // ["paragraph", "blockquote", "removeBlockquote", "horizontalLine", "orderedList", "unorderedList"],
      // ["link", "unlink", "image", "video"]
    ]
  }


   loadachievementdetails(){
    this.spinnerService.show();
    this.imagesArr1=[]

    this.userservice.Getachievementdetails().subscribe(
      data => {
        this.Achievements=data;
        this.spinnerService.hide();
       
        // for(var j=0;j<this.Achievements.length;j++){
        //   this.picsArr=this.Achievements[j].image;
        //      for(var i=0;i<this.picsArr.length-1;i++){
        //        this.picsstring[j]=this.picsArr.split(' ')
              
   
        //      }
   
        //    }


           for(var j=0;j<this.Achievements.length;j++){
            // let imagesArr:any=[];
            let imagesArr:any=[];
           this.picsArr=this.Achievements[j].image;
         
              // for(var i=0;i<this.picsArr.length-1;i++){
              
              
                // this.picsstring[j]=this.picsArr.split(' ')
                  
             
    
              //  }
            if(this.picsArr.length>0){
              for(var k=0;k<this.picsArr.length;k++){
               
     
              this.Imagejson = 
                   {
                     
                       small: this.Apiurl+'/achievements/'+this.picsArr[k],
                       medium: this.Apiurl+'/achievements/'+this.picsArr[k],
                       big:this.Apiurl+'/achievements/'+this.picsArr[k]
                   }
                  
                   imagesArr.push(this.Imagejson);
              }
      
      
              this.imagesArr1.push(imagesArr)
           
            }else{
              
              this.imagesArr1.push(null)
            }
    
            }
            this.galleryOptions = [
              {
                  width: '600px',
                  height: '400px',
                  thumbnailsColumns: 4,
                  imageAnimation: NgxGalleryAnimation.Slide,  
                  previewCloseOnClick:true
              },
              // max-width 800
              {
                  breakpoint: 800,
                  width: '100%',
                  height: '600px',
                  imagePercent: 80,
                  thumbnailsPercent: 20,
                  thumbnailsMargin: 20,
                  thumbnailMargin: 20
              },
              // max-width 400
              {
                  breakpoint: 400,
                  preview: false
              }
          ];
   
       
      },
      error => {
      
      });    
  }

  array:any = [];


  imageType: string
  errImageType: boolean 

  check(file:any) {
    this.imageType = file.target.files[0].type
    if (this.imageType == "image/jpeg" || this.imageType == "image/bmp" || this.imageType == "image/png" || this.imageType == "image/gif") {
      this.errImageType = false;
      this.imageType = ""
      this.uploader.uploadAll()
    }
    else {
      this.errImageType = true;
      this.imageType = ""

    }
  }


//image

imageChangedEvent:any
achievementimages:any = []
imageurl:any
images_array: any = []
img_obj:any={
  img_data:"",
  type:""
}
achidata_preview:any=[]
onFileSelected(event: any) {
  if(event.target.files.length<=6)
  {

 
  
  for(var i=0;i<event.target.files.length;i++){
    this.filestype.push(event.target.files[i].type.split("/")[1])
  }
  for(var i=0;i<event.target.files.length;i++){
    this.jobimage = {
      type: '',
      img_data: ''
    }
var imageType:any = event.target.files[i].type

  this.jobimage.type=imageType.replace("image/",".")

  // if (event.target.files.length) {
    if(imageType=="image/jpeg" || imageType=="image/jpg" || imageType=="image/png" || imageType=="image/gif" || imageType=="image/bmp")
    {
      var reader:any = new FileReader();
      reader.readAsDataURL(event.target.files[i]); // read file as data url
      reader.onload = (ev: any) => { // called once readAsDataURL is completed
        this.jobimage.img_data = ev.target.result;
        this.achidata_preview.push(this.jobimage.img_data)
       this.jobimage.img_data =  this.jobimage.img_data.replace("data:image/jpeg;base64,", "");
       this.jobimage.img_data =  this.jobimage.img_data.replace("data:image/png;base64,", "");
       this.jobimage.img_data =  this.jobimage.img_data.replace("data:image/bmp;base64,", "");
       this.jobimage.img_data =  this.jobimage.img_data.replace("data:image/jpg;base64,", "");
       this.jobimage.img_data =  this.jobimage.img_data.replace("data:image/gif;base64,", "");

      
       this.achidata.push(this.jobimage.img_data)
       
       this.errImageType=false
      }}
      else{
        // this.tostersuc.error('Please select images only');
        this.errImageType=true
      }
    }
    }
    else{
      this.tostersuc.error('You can select maximum of 6 images only');
      this.image=''
    }
    // }
  }
removeimage(i:number)
{
 
 this.achidata.splice(i,1)
 this.achidata_preview.splice(i,1)

}


remove(b:number){
  for(var c=0;c<b; c++){
    this.achidata.splice(c,b)
    this.achidata_preview.splice(c,b)

  }
}
// upload() {
//   for (let index = 0; index < this.images_array.length; index++) {
//     this.images_array[index] = this.images_array[index].replace("data:image/jpeg;base64,", "");

//   }

//   this.userservice.uploadimage(this.images_array).subscribe(data => {
//     this.images_array=[]

//   })
// }

errmsg:boolean
name:any;
description:any
image:any
id:any
achv_name:boolean=false;
// achv_desc:boolean=false
saveachievements(){



if(this.name){
let achievement:any={
  name:this.name.trim(),
  description:this.description,
 image:this.achidata,
  id:this.id,
  filetype:this.filestype
  }

if(!achievement.name){
  this.achv_name=true;
  //this.tostersuc.error('Aadhar no is not valid',"hello");
}

// if(!achievement.description){
//   this.achv_desc=true;
//   //this.tostersuc.error('Aadhar no is not valid',"hello");
// }

if(achievement.id ==""|| achievement.id == null || achievement.id == undefined){
if(achievement.name  && achievement.description ){
this.spinnerService.show();
this.userservice.Addachievementdetails(achievement).subscribe(
  data => {
    this.images_array=[]
    // this.uploader.queue=[]
   
    this.name=''
    this.description=''
     this.image=''
    this.achievement ={};
    this.achidata_preview=[]
    this.filestype=[]
    this.achidata=[]
    this.loadachievementdetails()
    this.spinnerService.hide();
   
    this.tostersuc.success('Achievement details added successfully');
  
  
  },
  error => {
   
  });
  this.errmsg=false;
  this.achievement ={};

}

else{
this.errmsg=true
}
}

else{
if(achievement.name){
  this.spinnerService.show();
  this.userservice.Editachievementdetails(achievement).subscribe(
    data => {
     

      this.spinnerService.hide();

      this.tostersuc.success('Achievement details updated successfully');
      this.loadachievementdetails()
      this.name=''
      this.description=''
      this.image=''
      this.achievement ={};
       
    
    },
    error => {
  
    });
    this.errmsg=false;
    this.achievement ={};
}
else{
this.errmsg=true
}
}
}
}

edit(row:any){



  this.name = row.name;
  this.description = row.description;
  this.image = row.images;
  this.id = row.id;
  

}
deleteeventid:any
delete(row:any,template:any) {

this.deleteeventid=row

this.modalRef = this.modalService.show(
  template
);

}
confirmation(res_confirm:boolean)


{
if(res_confirm)
{
  this.userservice.Deleteachievementdetails(this.deleteeventid.id,this.deleteeventid.image).subscribe(
    data => {
     
      this.tostersuc.success('Achievement details deleted successfully');
      this.modalRef.hide()
      this.achievement ={};
     this.loadachievementdetails()
    },
    error => {
    
    });

}
else{
  this.modalRef.hide()
}

}
  ngOnInit() {
    this.loadachievementdetails()
  }
  imageCropped(image: string) {
      this.croppedImage = image;
  }
  imageLoaded() {
      // show cropper
  }
  loadImageFailed() {
      // show message
  }
}
