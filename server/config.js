var log4js = require('log4js');
//configure('./filename');

var connectionString = process.env.DATABASE_URL || 'pg://postgres:root@localhost:5432/koneqto2';

// var connectionString = process.env.DATABASE_URL || 'pg://postgres:pass4unic@35.200.250.128:5432/koneqto';

//var connectionString = process.env.DATABASE_URL ||  'pg://postgres:root@localhost:5432/13122018';



var pg = require("pg");
var port = process.env.LISTEN_PORT || 5000;
var client = new pg.Client(connectionString);
var email = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');



client.connect(function (error) {
  if (error) {
    ccderrorlogger.error("client Problem with Postgresql" + error);
  }
  else {
    uiinfologger.debug("client Connected with Database");
  }
});




/////////////////////////////////////////////////////////////////
var nodemailer = email.createTransport(smtpTransport({
  service: 'gmail',
  host: 'smtp.gmail.com',
  auth: {
    user: 'mailto:koneqto-noreply@koneqto.com',
    pass: 'koneqto&07'
  },
  // authMethod:'NTLM',
  // secure:false,
  tls: { rejectUnauthorized: false }
  // debug:true
}));
/////////////////////////////////////////////////////////////////
// var nodemailer = email.createTransport({
//   service: 'Gmail',
//   auth: {
//     user: 'koneqto-noreply@koneqto.com',
//     pass: 'koneqto&07',
//     tls: { rejectUnauthorized: false }
//   }
// });
////////////////////////////////////////////////////////////////

//log file
/* var logger = log4js.getLogger();
logger.level = 'debug';
logger.debug("Some debug messages");
var today_Date = new Date();
logger_file_name = today_Date.getFullYear() + "-" + today_Date.getMonth() + "-" + today_Date.getDate() + " log file" + '.txt';
logger_file_name_path = './public/log_files/' + logger_file_name;
log4js.configure({
  appenders:
  {
    file:
    {
      type: "file",
      filename: logger_file_name_path
    }
  },
  categories: { file: { appenders: ['file'], level: 'DEBUG' } },
  categories: { default: { appenders: ['file'], level: 'DEBUG' } }
}); */

//creating logger file
var today_Date = new Date();
logger_file_name = today_Date.getFullYear() + "-" + today_Date.getMonth() + "-" + today_Date.getDate() + " log file" + '.txt';
logger_file_name_path = './public/log_files/' + logger_file_name;
log4js.configure({
  appenders: [
    { type: 'console' },
    { type: 'file', filename: logger_file_name_path }
  ]
});

module.exports.nodemailer = nodemailer;
module.exports.connectionString = connectionString;
module.exports.client = client;
module.exports.port = port;



var uiinfologger = log4js.getLogger("ui-info-logger");
var uierrorlogger = log4js.getLogger("ui-error-logger");
var ccdinfologger = log4js.getLogger("ccd-info-logger");
var ccderrorlogger = log4js.getLogger("ccd-error-logger");
module.exports.uiinfologger = uiinfologger;
module.exports.uierrorlogger = uierrorlogger;
module.exports.ccdinfologger = ccdinfologger;
module.exports.ccderrorlogger = ccderrorlogger;
