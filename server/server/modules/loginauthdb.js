var express = require('express');
var path = require('path');
var router = express.Router();
var Q = require('q');
var conString = require(path.join(__dirname, '../', '../', 'config')).connectionString;
var uiinfologger = require(path.join(__dirname, '../', '../', 'config')).uiinfologger;
var uierrorlogger = require(path.join(__dirname, '../', '../', 'config')).ccderrorlogger;
var client = require(path.join(__dirname, '../', '../', 'config')).client;




//Login authentication
exports.authenticate = function (username, password) {
    uiinfologger.debug("Entering into login method..", username, password);
    var deferred = Q.defer();


    var query = "select * from unic_sol.fn_user_validation(lower($1),$2)";
    client.query(query, [username, password], function (err, rows) {
        if (err) {
            uierrorlogger.error("login  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {

            //   console.log("authenticate server rows" + rows);
            //   console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}
//Employee Registration
exports.empregistration = function (userfname, usermobile, useremail, useruname, userpwd, roleid, gender, company_websie,name, hr_designation, country, state, city, functional_area) {
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_registration($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18)";
    uiinfologger.debug("Entering into registrion  method.." + userfname, '', 0, usermobile, useremail, useruname, userpwd, roleid, gender, company_websie,name, hr_designation,
        country, state, city, functional_area);

    client.query(query, [name, null, null, usermobile, useremail, useruname, userpwd, 5, country, state, city, functional_area, null, gender, null, userfname, company_websie, 
        hr_designation  ], function (err, rows) {

        if (err) {

            uierrorlogger.error("employeeregistration  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            //  console.log("employeeregistration Sucessful"+JSON.parse(JSON.stringify(rows.rows))[0].message)
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}
//User Registration
exports.newregistration = function (userfname, userlname, dob, usermobile, useremail, useruname, userpwd, roleid, country, state, city, functional_area, id, gender, currentworkingstatus) {
    uiinfologger.debug("Entering into newregistration method.." + userfname, userlname, dob, usermobile, useremail, useruname, userpwd, roleid, country, state, city,
     functional_area, id, gender,  currentworkingstatus);
    var deferred = Q.defer();

    

    var query = "select * from unic_sol.fn_registration($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18)";
    uiinfologger.debug("Entering into registrion  method.." + userfname, userlname, dob, usermobile, useremail, useruname, userpwd, roleid, country, state, city, functional_area, id, gender, currentworkingstatus);
    client.query(query, [userfname, userlname, dob, usermobile, useremail, useruname, userpwd, 3, country, state, city, functional_area, id, gender, currentworkingstatus, null, null,null], function (err, rows) {


        if (err) {

            uierrorlogger.error("newregistration  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            // console.log("Registration  Sucessful"+JSON.parse(JSON.stringify(rows.rows))[0].message)
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}

//saveIpbyid
exports.saveIpbyid = function (UserIp,loginuserid) {
    uiinfologger.debug("Entering into saveIpbyid>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> method.." + UserIp,loginuserid);
    var deferred = Q.defer();
   // INSERT INTO table_name (column1, column2, column3, ...)VALUES (value1, value2, value3, ...);

    var query = "select * from unic_sol.fn_save_user_ip($1,$2)";
    client.query(query, [UserIp,loginuserid], function (err, rows) {
        if (err) {
            uierrorlogger.error("saveIpbyid Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("saveIpbyid successfully >>>>>>!"+  JSON.stringify(rows.rows))
            console.log("saveIpbyid successfully!");
            deferred.resolve(rows);
        }
    })
    return deferred.promise;

}
//verifyemailbyid
exports.verifyemailbyid = function (id) {
    uiinfologger.debug("Entering into verifyemailbyid>>>> method.." + id);
    var deferred = Q.defer();
   // 
    var query = "select * from unic_sol.fn_isactive_update($1)";
    client.query(query, [id], function (err, rows) {
        if (err) {
            uierrorlogger.error("verifyemailbyid Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("verifyemailbyid successfully >>>>>>!"+  JSON.parse(JSON.stringify(rows.rows))[0].message)
            console.log("verifyemailbyid successfully!");
            deferred.resolve(rows);
        }
    })
    return deferred.promise;

}
//checksocialid
exports.checksocialid = function (socialid, email) {
    
    uiinfologger.debug("Entering into loginauthdb checksocialid method.." + socialid, email);
    var deferred = Q.defer();


    var query = "select * from unic_sol.lu_user_registration where socialid=$1 or email_id =$2 ";
    client.query(query, [socialid, email], function (err, rows) {
        if (err) {
            uierrorlogger.error("getsocialuser  Problem with Postgresql.." + err);
            deferred.reject(err);
            
        }
        else {

            // console.log("getgmail data ***** server rows" + rows);
            // console.log("55555555555555555555555555555555555555555" + JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}
//checkmailinappuser
exports.checkemailinappuser = function (email) {
    var deferred = Q.defer();
    try {
        uiinfologger.info("Entering into checkemailinappuser ..");
        uiinfologger.debug(" checkemailinappuser user input Data.." + email);

        var query = "select * from  unic_sol.fn_reg_email($1)";
        client.query(query, [parameter(email)], function (err, rows) {
            if (err) {

                uierrorlogger.error("Error in checkemailinappuser Problem with DB.." + err);
                deferred.reject(err);
            }
            else {

                uiinfologger.info("Leaving from checkemailinappuser  .." + JSON.stringify(rows.rows));
                deferred.resolve(rows);
            }
        });
    }
    catch (error) {
        uierrorlogger.error("Error catch expection  in checkemailinappuser ..." + error);
        deferred.reject(error);
    }
    return deferred.promise;
}
//checkmobile
exports.checkmobile = function (mobile) {
    uiinfologger.debug("Entering into checkmobile method.." + mobile);
    var deferred = Q.defer();
    var query = "select count(*) from unic_sol.lu_user_registration where mobile_no=$1";
    client.query(query, [mobile], function (err, rows) {
        if (err) {
            uierrorlogger.error("checkmobile  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            //        console.log("checkmobile sucess in Database.js"+rows);
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}
//checkMail
exports.checkMail = function (email) {
    uiinfologger.debug("Entering into checkMail method.." + email);
    var deferred = Q.defer();
    var query = "select count(*) from unic_sol.lu_user_registration where email_id=$1";
    client.query(query, [email], function (err, rows) {
        if (err) {
            uierrorlogger.error("checkMail  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            //        console.log("checkMail sucess in Database.js"+rows);
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}
//userNameCheck
exports.userNameChek = function (username) {
    uiinfologger.debug("Entering into userNameCheck method.." + username);
    var deferred = Q.defer();
    var query = "select count(*) from unic_sol.lu_user_registration where LOWER (username)=LOWER ($1)";
    client.query(query, [username], function (err, rows) {
        if (err) {
            uierrorlogger.error("userNameCheck  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            //    console.log("userNameCheck sucess in Database.js"+rows);
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}
//loadcountrydropdown
exports.loadcountrydropdown = function () {
    uiinfologger.debug("Entering into loadcountrydropdown method..");
    var deferred = Q.defer();


    var query = "select * from unic_sol.lu_country order by country_name ASC ";
    client.query(query, function (err, rows) {
        if (err) {
            uierrorlogger.error("login  Problem with Postgresql..loadcountrydropdown Database" + err);
            deferred.reject(err);
        }
        else {

            //   console.log("server rows loadcountrydropdown Database" + rows);
            //   console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}
//loadstatedropdown
exports.loadstatesdropdown = function (country_id) {
    uiinfologger.debug("Entering into loadstatesdropdown method.." + country_id);
    var deferred = Q.defer();


    var query = "select state_id,state_name,country_id from unic_sol.lu_state where country_id=$1 order by state_name ASC ";
    client.query(query, [country_id], function (err, rows) {
        if (err) {
            uierrorlogger.error("login  Problem with Postgresql..loadcstatedropdown Database" + err);
            deferred.reject(err);
        }
        else {

            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}
//loadcitydropdown
exports.loadcitydropdown = function (state_id) {
    uiinfologger.debug("Entering into loadcitydropdown method.." + state_id);
    var deferred = Q.defer();


    var query = "select city_id,city_name,state_id from unic_sol.lu_city where state_id=$1 ORDER BY city_name ASC ";
    client.query(query, [state_id], function (err, rows) {
        if (err) {
            uierrorlogger.error("login  Problem with Postgresql..loadcitydropdown Database" + err);
            deferred.reject(err);
        }
        else {

            //        console.log("server rows loadcitydropdown Database" + rows);
            //         console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}
//loadgenderdropdown
exports.loadgender = function () {
    uiinfologger.debug("Entering into loadgender method..");
    var deferred = Q.defer();


    var query = "select * from unic_sol.lu_gender";
    client.query(query, function (err, rows) {
        if (err) {
            uierrorlogger.error("login  Problem with Postgresql..loadgender Database" + err);
            deferred.reject(err);
        }
        else {

            //     console.log("server rows loadgender Database" + rows);
            //     console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}
//loadfuunctionalareadropdown
exports.loadfunctinalareadata = function () {
    uiinfologger.debug("Entering into getfunctinalareadata method..");
    var deferred = Q.defer();


    var query = "select * from unic_sol.lu_functional_area";
    client.query(query, [], function (err, rows) {
        if (err) {
            uierrorlogger.error("getfunctinalareadata  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {

            // console.log("getfunctinalareadataofile server rows" + rows);
            // console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}
//getphonecode
exports.getPhonecode = function (country_id) {
    uiinfologger.debug("enter into get getphonecode method.." + country_id);
    var deferred = Q.defer();

    var query = "select * from unic_sol.lu_country where country_id=$1";

    client.query(query, [country_id], function (err, rows) {
        if (err) {
            uierrorlogger.error("getphonecode  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {

            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}
//parameter function
function parameter(param) {
    if (param == undefined) {
        return "";
    }
    else if (param == null || param == "") {
        return param;
    }
    else {
        return param.trim();
    }
}
//SavenewPasswordbyid
exports.SavenewPasswordbyid = function (pass, id) {
    var deferred = Q.defer();
    try {
        uiinfologger.info("Entering into SavenewPassword..");
        uiinfologger.debug("SavenewPassword input data...." + pass, id);
        var query = "update  unic_sol.lu_user_registration set password=$1 where user_registration_id=$2";
        client.query(query, [pass, id], function (err, rows) {
            if (err) {
                uierrorlogger.error("SavenewPassword  Problem with Postgresql.." + err);
                deferred.reject(error);
            }
            else {
                uiinfologger.info("Leaving From SavenewPassword..");
                deferred.resolve(rows);
            }
        });
    }
    catch (error) {
        uierrorlogger.error("Error catch expection newpassworddb SavenewPassword..." + error);
        deferred.reject(error);
    }
    return deferred.promise;
}


//update socket io details
module.exports.updateUserSocketId = (username, scoketId, status, cb) => {
    client.query(`Update unic_sol.lu_user_registration SET "status"=${status},"scoketId"=${scoketId} WHERE "username"=${username}`, cb)
}

module.exports.getFriendsList = (username, cb) => {
    client.query(`SELECT * FROM unic_sol.lu_user_registration WHERE "username"!=${username}`, cb)
}

module.exports.updateOnlineStatus = (userId, status, cb) => {
    console.log("userId update status....", userId)
    client.query(`UPDATE unic_sol.lu_user_registration SET "status"=${status} WHERE "username"='${userId}'`, cb)
}