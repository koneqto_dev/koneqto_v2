var express = require('express');
var path = require('path');
var router = express.Router();
var Q = require('q');
var conString = require(path.join(__dirname, '../', '../', 'config')).connectionString;
var uiinfologger = require(path.join(__dirname, '../', '../', 'config')).uiinfologger;
var uierrorlogger = require(path.join(__dirname, '../', '../', 'config')).ccderrorlogger;
var client = require(path.join(__dirname, '../', '../', 'config')).client;




// exports.sociallogin = function (first_name, last_name, apikey, email, photourl) {

//     uiinfologger.debug("Entering into socaillogin method..", apikey, email);
//     var deferred = Q.defer();


//     var query = "select * from unic_sol.fn_social_registration($1,$2,$3,$4,$5,$6)";
//     client.query(query, [first_name + "" + last_name, apikey, email, apikey, first_name, last_name], function (err, rows) {
//         if (err) {

//             uierrorlogger.error("login  Problem with Postgresql.." + err);
//             deferred.reject(err);
//         }
//         else {

//             // console.log("authenticate socaillogin server rows" + rows);
//             // console.log(JSON.stringify(rows.rows));
//             deferred.resolve(rows);
//         }

//     })
//     return deferred.promise;

// }
exports.Changeemail = function (newemail, loginuserid) {

    uiinfologger.debug("Entering into change email method.." + { newemail, loginuserid }
    );
    var deferred = Q.defer();
    var query =
        "update unic_sol.lu_user_registration set  email_id=$1  where user_registration_id=$2";
    client.query(query, [newemail, loginuserid], function (err, rows) {
        if (err) {
            uierrorlogger.error("change  newemail  Problem with Postgresql.." + err);
            deferred.reject(err);
        } else {
            console.log("saved  newemail successfully!");
            deferred.resolve(rows);
        }
    });
    return deferred.promise;
}
// exports.ChangeMobile = function (newmobile, loginuserid) {
//     uiinfologger.debug(
//         "Entering into change Mobile method.." + { newmobile, loginuserid }
//     );
//     var deferred = Q.defer();
//     var query =
//         "select * from unic_sol.fn_update_mobile($1,$2)";
//     client.query(query, [newmobile, loginuserid], function (err, rows) {
//         if (err) {
//             uierrorlogger.error("change newmobile  Problem with Postgresql.." + err);
//             deferred.reject(err);
//         } else {
//             console.log("saved newmobilel successfully!");
//             deferred.resolve(rows);
//         }
//     });
//     return deferred.promise;
// };
exports.checksocialid = function (socialid,email_id) {
    
    uiinfologger.debug("Entering into db checksocialid method.."+socialid,email_id);
    var deferred = Q.defer();


    var query = "select * from unic_sol.lu_user_registration where socialid=$1 or email_id=$2 ";
    client.query(query, [socialid,email_id], function (err, rows) {
        if (err) {
            uierrorlogger.error("getsocialuser  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {

            //  console.log("getprofile server rows" + rows);
            //console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}
exports.getphonecode = function (loginuserid) {
    
    uiinfologger.debug("Entering into getphonecode method.." + loginuserid);
    var deferred = Q.defer();


    var query = "select phonecode from unic_sol.fn_get_user_profile($1)";
    client.query(query, [loginuserid], function (err, rows) {
        if (err) {
            uierrorlogger.error("getprofile  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {

            //  console.log("getprofile server rows" + rows);
            //console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}
// exports.empregistration = function (userfname, usermobile, useremail, useruname, userpwd, roleid, gender, company_websie, hr_designation, country, state, city,functional_area) {
//     var deferred = Q.defer();
//     var query = "select * from unic_sol.fn_registration($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18)";
//     uiinfologger.debug("Entering into registrion  method.." + userfname, '', 0, usermobile, useremail, useruname, userpwd, roleid, gender, company_websie, hr_designation,
//         country, state, city,functional_area);

//     client.query(query, [null, null, null, usermobile, useremail, useruname, userpwd, 5, country, state, city, functional_area,null, gender, null, userfname, company_websie, hr_designation], function (err, rows) {

//         if (err) {

//             uierrorlogger.error("employeeregistration  Problem with Postgresql.." + err);
//             deferred.reject(err);
//         }
//         else {
//             //  console.log("employeeregistration Sucessful"+JSON.parse(JSON.stringify(rows.rows))[0].message)
//             deferred.resolve(rows);
//         }

//     })
//     return deferred.promise;

// }
// exports.newregistration = function (userfname, userlname, dob, usermobile, useremail, useruname, userpwd, roleid, country, state, city, functional_area,id, gender, currentworkingstatus) {
//     uiinfologger.debug("Entering into newregistration method.." + userfname, userlname, dob, usermobile, useremail, useruname, userpwd, roleid, country, state, city, functional_area, id,gender, currentworkingstatus);
//     var deferred = Q.defer();

//     

//     var query = "select * from unic_sol.fn_registration($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18)";
//     uiinfologger.debug("Entering into registrion  method.." + userfname, userlname, dob, usermobile, useremail, useruname, userpwd, roleid, country, state, city, functional_area,id, gender, currentworkingstatus);
//     client.query(query, [userfname, userlname, dob, usermobile, useremail, useruname, userpwd, 3, country, state, city, functional_area,id, gender, currentworkingstatus, null, null, null], function (err, rows) {


//         if (err) {

//             uierrorlogger.error("newregistration  Problem with Postgresql.." + err);
//             deferred.reject(err);
//         }
//         else {
//             // console.log("Registration Sucessful"+JSON.parse(JSON.stringify(rows.rows))[0].message)
//             deferred.resolve(rows);
//         }

//     })
//     return deferred.promise;

// }
// exports.checkemailinappuser = function (email) {
//     var deferred = Q.defer();
//     try {
//         uiinfologger.info("Entering into checkemailinappuser ..");
//         uiinfologger.debug(" checkemailinappuser user input Data.." + email);
//         var query = "select first_name ||' '|| last_name as name,username,user_registration_id from unic_sol.lu_user_registration  where trim(lower(email_id))=lower($1)";
//         client.query(query, [parameter(email)], function (err, rows) {
//             if (err) {

//                 uierrorlogger.error("Error in checkemailinappuser Problem with DB.." + err);
//                 deferred.reject(err);
//             }
//             else {

//                 uiinfologger.info("Leaving from checkemailinappuser  .." + JSON.stringify(rows.rows));
//                 deferred.resolve(rows);
//             }
//         });
//     }
//     catch (error) {
//         uierrorlogger.error("Error catch expection  in checkemailinappuser ..." + error);
//         deferred.reject(error);
//     }
//     return deferred.promise;
// }
exports.SavenewPassword = function (newpassword, loginuserid) {
    var deferred = Q.defer();
    try {
        uiinfologger.info("Entering into SavenewPassword..");
        uiinfologger.debug("SavenewPassword input data...." + newpassword, loginuserid);
        var query = "update  unic_sol.lu_user_registration set password=$1 where user_registration_id=$2";
        client.query(query, [parameter(newpassword), 1], function (err, rows) {
            if (err) {
                uierrorlogger.error("SavenewPassword  Problem with Postgresql.." + err);
                deferred.reject(error);
            }
            else {
                uiinfologger.info("Leaving From SavenewPassword..");
                deferred.resolve(rows);
            }
        });
    }
    catch (error) {
        uierrorlogger.error("Error catch expection newpassworddb SavenewPassword..." + error);
        deferred.reject(error);
    }
    return deferred.promise;
}
exports.ProfileSanpshot = function (loginuserid, Resume_Headline, Current_Designation, Current_Company, Current_Location, Preferred_Location, Functional_Area, Role, Industry, Date_of_Birth, Gender, Total_Experience, Annual_Salary, Highest_Degree, Phone, Email, Permanent_Address, Hometown, Pin_Code, Marital_Status, Key_Skills) {
    uiinfologger.debug("Entering into login method.." + Current_Designation, Current_Company, Current_Location, Preferred_Location, Functional_Area, Role, Industry, Date_of_Birth, Gender, Total_Experience, Annual_Salary, Highest_Degree, Phone, Email, Permanent_Address, Hometown, Pin_Code, Marital_Status, Resume_Headline, Key_Skills);
    var deferred = Q.defer();

    var query = "select * from unic_sol.fn_save_profile($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20)";
    client.query(query, [loginuserid, Resume_Headline, Current_Designation, Current_Company, Current_Location, Preferred_Location,
        1, 1, Date_of_Birth, Gender, Total_Experience, Annual_Salary, Highest_Degree, Phone, Email,
        Permanent_Address, Hometown, Pin_Code, Marital_Status, Key_Skills], function (err, rows) {
            if (err) {
                uierrorlogger.error("login  Problem with Postgresql.." + err);
                deferred.reject(err);
            }
            else {
                //   console.log("rrrrrrr"+rows.rows);
                deferred.resolve(rows);
            }
        })
    return deferred.promise;

}
exports.retrivedudetails = function (userid) {
    //  uiinfologger.debug("Entering into retrivedudetails db method..",userid);
    var deferred = Q.defer();


    var query = "select * from unic_sol.lu_user_education where user_registration_id=$1";
    client.query(query, [userid], function (err, rows) {
        if (err) {
            uierrorlogger.error("login  Problem with Postgresql..Retrivedudetails Database" + err);
            deferred.reject(err);
        }
        else {

            //    console.log("server rows Retrivedudetails Database" + rows);
            //    console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}
exports.coursedata = function (id) {
    uiinfologger.debug("Entering into coursedata database method.." + id);
    var deferred = Q.defer();


    var query = "select * from unic_sol.lu_courses where id=$1 order by coursename asc";
    client.query(query, [id], function (err, rows) {
        if (err) {
            uierrorlogger.error("coursedata  Problem with Postgresql..coursedata Database" + err);
            deferred.reject(err);
        }
        else {

            //    console.log("server rows coursedata Database" + rows);
            //    console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}
exports.eduspecifications = function (courseid) {
    uiinfologger.debug("Entering into eduspecifications database method.." + courseid);
    var deferred = Q.defer();


    var query = "select * from unic_sol.lu_eduspecifications where courseid=$1 order by specialization asc";
    client.query(query, [courseid], function (err, rows) {
        if (err) {
            uierrorlogger.error("login  Problem with Postgresql..eduspecifications Database" + err);
            deferred.reject(err);
        }
        else {

            //     console.log("server rows eduspecifications Database" + rows);
            //     console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}
exports.loadyeardropdown = function () {
    uiinfologger.debug("Entering into loadyeardropdown method..");
    var deferred = Q.defer();


    var query = "select id,year from unic_sol.lu_year";
    client.query(query, function (err, rows) {
        if (err) {
            uierrorlogger.error("login  Problem with Postgresql..loadyeardropdown Database" + err);
            deferred.reject(err);
        }
        else {

            // console.log("server rows loadyeardropdown Database" + rows);
            //  console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}
exports.loadstudiesdropdown = function () {
    uiinfologger.debug("Entering into loadstudiesdropdown method..");
    var deferred = Q.defer();


    var query = "select * from unic_sol.lu_medium order by medium_name asc";
    client.query(query, function (err, rows) {
        if (err) {
            uierrorlogger.error("login  Problem with Postgresql..loadstudiesdropdown Database" + err);
            deferred.reject(err);
        }
        else {

            //   console.log("server rows loadstudiesdropdown Database" + rows);
            //   console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}
exports.loadmarksdropdown = function () {
    uiinfologger.debug("Entering into loadmarksdropdown method..");
    var deferred = Q.defer();


    var query = "select * from unic_sol.lu_marks";
    client.query(query, function (err, rows) {
        if (err) {
            uierrorlogger.error("login  Problem with Postgresql..loadmarksdropdown Database" + err);
            deferred.reject(err);
        }
        else {

            //    console.log("server rows loadmarksdropdown Database" + rows);
            //    console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}
exports.loadgradesdropdown = function () {
    uiinfologger.debug("Entering into loadgradesdropdown method..");
    var deferred = Q.defer();


    var query = "select * from unic_sol.lu_grades";
    client.query(query, function (err, rows) {
        if (err) {
            uierrorlogger.error("login  Problem with Postgresql..loadgradesdropdown Database" + err);
            deferred.reject(err);
        }
        else {

            //console.log("server rows loadgradesdropdown Database" + rows);
            //   console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}
exports.loadgradestypedropdown= function(grades_id){
    uiinfologger.debug("Entering into loadgradetype dropdown method..");
    var deferred = Q.defer();
    var query = " select * from unic_sol.lu_grade_types where grade_id=$1";
    client.query(query,[grades_id],function(err,rows){
        if(err){
            uierrorlogger.error("getting problem on loadgradetype dropdown method.."+err);
            deferred.reject(err);
        }
        else{
            console.log("get loadgradetype dropdown successfully..!");
            deferred.resolve(rows);
        }
    })
    return deferred.promise;
}
exports.loaduniversitiesdropdown = function (university) {
    uiinfologger.debug("Entering into loaduniversitiesdropdown method db..",university);
    var deferred = Q.defer();

    var query = "select * from unic_sol.fn_university_search($1)";
    client.query(query,[university], function (err, rows) {
        if (err) {
            uierrorlogger.error("login  Problem with Postgresql..loaduniversitiesdropdown Database" + err);
            deferred.reject(err);
        }
        else {

            //console.log("server rows loadgradesdropdown Database" + rows);
            //   console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}
// get all university dropdown for mobile api
exports.loadalluniversitiesdropdown = function () {
    uiinfologger.debug("Entering into loadalluniversitiesdropdown method db..");
    var deferred = Q.defer();

    var query = "select * from unic_sol.lu_universities";
    client.query(query,[], function (err, rows) {
        if (err) {
            uierrorlogger.error("login  Problem with Postgresql..loadalluniversitiesdropdown Database" + err);
            deferred.reject(err);
        }
        else {

            //console.log("server rows loadgradesdropdown Database" + rows);
            //   console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}

exports.loadcoursetypedropdown = function () {
    uiinfologger.debug("Entering into loadcoursetypedropdown method..");
    var deferred = Q.defer();


    var query = "select * from unic_sol.lu_coursetype";
    client.query(query, function (err, rows) {
        if (err) {
            uierrorlogger.error("login  Problem with Postgresql..loadcoursetypedropdown Database" + err);
            deferred.reject(err);
        }
        else {

            //  console.log("server rows loadcoursetypedropdown Database" + rows);
            //  console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}
exports.AddEducationdetails = function (user_registration_id, qualification, year, education_type, university_or_institute, natureof_job, marks, specialization, grading_system, medium) {
    uiinfologger.debug("Entering into AddEducationdetails method.." + user_registration_id, qualification, year, education_type, university_or_institute, natureof_job, marks, specialization, grading_system, medium);
    var deferred = Q.defer();

    var query = "select * from unic_sol.fn_save_education($1,$2,$3,$4,$5,$6,$7,$8,$9,$10)";
    client.query(query, [user_registration_id, qualification, year, education_type,university_or_institute, natureof_job, marks,  specialization, grading_system, medium], function (err, rows) {
        if (err) {
            uierrorlogger.error("AddEducationdetails  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("AddEducationdetails sucess in Database.js");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}
exports.getsscdetails = function (loginuserid, education_type) {
    uiinfologger.debug("Entering into getpresentemp>>>> method.." + loginuserid, education_type);
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_get_educationaldetails($1,$2)";
    client.query(query, [loginuserid, education_type], function (err, rows) {
        if (err) {
            uierrorlogger.error("getsscdetails  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("get sscdetails successfully!");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}
exports.getinterdetails = function (loginuserid, education_type) {
    uiinfologger.debug("Entering into getinterdetails>>>> method.." + loginuserid, education_type);
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_get_educationaldetails($1,$2)";
    client.query(query, [loginuserid, education_type], function (err, rows) {
        if (err) {
            uierrorlogger.error("getinterdetails  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("get interdetails successfully!");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}
exports.getgraduationdetails = function (loginuserid, education_type) {
    uiinfologger.debug("Entering into getgraduationdetails>>>> method.." + loginuserid, education_type);
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_get_educationaldetails($1,$2)";
    client.query(query, [loginuserid, education_type], function (err, rows) {
        if (err) {
            uierrorlogger.error("getgraduationdetails  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("get graduation details successfully!");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}
exports.getpgdetails = function (loginuserid, education_type) {
    uiinfologger.debug("Entering into getpgdetails>>>> method.." + loginuserid, education_type);
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_get_educationaldetails($1,$2)";
    client.query(query, [loginuserid, education_type], function (err, rows) {
        if (err) {
            uierrorlogger.error("getpgdetails  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("get pg details successfully!");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}
exports.getphddetails = function (loginuserid, education_type) {
    uiinfologger.debug("Entering into getphddetails>>>> method.." + loginuserid, education_type);
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_get_educationaldetails($1,$2)";
    client.query(query, [loginuserid, education_type], function (err, rows) {
        if (err) {
            uierrorlogger.error("getphddetails  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("get phd details successfully!");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}
exports.editsscdetails = function (loginuserid, education_id, qualification, year, medium, marks, education_type) {
    uiinfologger.debug("Entering into editsscdetails>>>> method.." + loginuserid, education_id, qualification, year, medium, marks, education_type);
    var deferred = Q.defer();

    var query = "update unic_sol.lu_user_education set qualification=$3,year=$4,medium=$5,marks=$6,education_type=$7 where user_registration_id=$1 and education_id=$2";
    client.query(query, [loginuserid, education_id, qualification, year, medium, marks, education_type], function (err, rows) {
        if (err) {
            uierrorlogger.error("editsscdetails  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("edit ssc details successfully!");
            deferred.resolve(rows);
        }
    })
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_save_education($1,$2,$3,$4,$5,$6,$7,$8)";
    return deferred.promise;

}
exports.editinterdetails = function (loginuserid, education_id, qualification, year, medium, marks, education_type) {
    uiinfologger.debug("Entering into editinterdetails>>>> method.." + loginuserid, education_id, qualification, year, medium, marks, education_type);
    var deferred = Q.defer();
    var query = "update unic_sol.lu_user_education set qualification=$3,year=$4,medium=$5,marks=$6,education_type=$7 where user_registration_id=$1 and education_id=$2";
    client.query(query, [loginuserid, education_id, qualification, year, medium, marks, education_type], function (err, rows) {
        if (err) {
            uierrorlogger.error("editinterdetails  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("edit inter details successfully!");
            deferred.resolve(rows);
        }
    })
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_save_education($1,$2,$3,$4,$5,$6,$7,$8)";
    return deferred.promise;

}
exports.editgraduationdetails = function (user_registration_id, education_id, university_or_institute, year, natureof_job, qualification, specialization, grading_system, marks, medium, education_type) {
    uiinfologger.debug("Entering into editgraduationdetails>>>> method.." + user_registration_id, education_id, university_or_institute, year, natureof_job, qualification, specialization, grading_system, marks, medium, education_type);
    var deferred = Q.defer();
    var query = "update unic_sol.lu_user_education set university_or_institute=$3,year=$4,natureof_job=$5,qualification=$6,specialization=$7,grading_system=$8,marks=$9,medium=$10,education_type=$11  where user_registration_id=$1 and education_id=$2";
    client.query(query, [user_registration_id, education_id, university_or_institute, year, natureof_job, qualification, specialization, grading_system, marks, medium, education_type], function (err, rows) {
        if (err) {
            uierrorlogger.error("editgraduationdetails  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("edit graduation details successfully!");
            deferred.resolve(rows);
        }
    })
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_save_education($1,$2,$3,$4,$5,$6,$7,$8)";
    return deferred.promise;

}
exports.editpgdetails = function (user_registration_id, education_id, university_or_institute, year, natureof_job, qualification, specialization, grading_system, marks, medium, education_type) {
    uiinfologger.debug("Entering into editpgdetails>>>> method.." + user_registration_id, education_id, university_or_institute, year, natureof_job, qualification, specialization, grading_system, marks, medium, education_type);
    var deferred = Q.defer();
    var query = "update unic_sol.lu_user_education set university_or_institute=$3,year=$4,natureof_job=$5,qualification=$6,specialization=$7,grading_system=$8,marks=$9,medium=$10,education_type=$11  where user_registration_id=$1 and education_id=$2";
    client.query(query, [user_registration_id, education_id, university_or_institute, year, natureof_job, qualification, specialization, grading_system, marks, medium, education_type], function (err, rows) {
        if (err) {
            uierrorlogger.error("editpgdetails  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("edit pg details successfully!");
            deferred.resolve(rows);
        }
    })
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_save_education($1,$2,$3,$4,$5,$6,$7,$8)";
    return deferred.promise;

}
exports.editphddetails = function (user_registration_id, education_id, university_or_institute, year, natureof_job, qualification, specialization, grading_system, marks, medium, education_type) {
    uiinfologger.debug("Entering into editphddetails>>>> method.." + user_registration_id, education_id, university_or_institute, year, natureof_job, qualification, specialization, grading_system, marks, medium, education_type);
    var deferred = Q.defer();
    var query = "update unic_sol.lu_user_education set university_or_institute=$3,year=$4,natureof_job=$5,qualification=$6,specialization=$7,grading_system=$8,marks=$9,medium=$10,education_type=$11  where user_registration_id=$1 and education_id=$2";
    client.query(query, [user_registration_id, education_id, university_or_institute, year, natureof_job, qualification, specialization, grading_system, marks, medium, education_type], function (err, rows) {
        if (err) {
            uierrorlogger.error("editphddetails  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("edit phd details successfully!");
            deferred.resolve(rows);
        }
    })
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_save_education($1,$2,$3,$4,$5,$6,$7,$8)";
    return deferred.promise;

}
exports.deletesscbyid = function (loginuserid, education_id) {
    uiinfologger.debug("Entering into deletesscbyid>>>> method.." + loginuserid, education_id);
    var deferred = Q.defer();
    var query = "delete from unic_sol.lu_user_education where education_id=$1 and user_registration_id=$2";
    client.query(query, [education_id, loginuserid], function (err, rows) {
        if (err) {
            uierrorlogger.error("deletesscbyid  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("deleted successfully!");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}
exports.deleteinterbyid = function (loginuserid, education_id) {
    uiinfologger.debug("Entering into deleteinterbyid>>>> method.." + loginuserid, education_id);
    var deferred = Q.defer();
    var query = "delete from unic_sol.lu_user_education where education_id=$1 and user_registration_id=$2";
    client.query(query, [education_id, loginuserid], function (err, rows) {
        if (err) {
            uierrorlogger.error("deleteinterbyid  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("deleted successfully!");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}
exports.deletegraduationbyid = function (loginuserid, education_id) {
    uiinfologger.debug("Entering into deletegraduationbyid>>>> method.." + loginuserid, education_id);
    var deferred = Q.defer();
    var query = "delete from unic_sol.lu_user_education where education_id=$1 and user_registration_id=$2";
    client.query(query, [education_id, loginuserid], function (err, rows) {
        if (err) {
            uierrorlogger.error("deletegraduationbyid  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("deleted successfully!");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}
exports.deletepgbyid = function (loginuserid, education_id) {
    uiinfologger.debug("Entering into deletepgbyid>>>> method.." + loginuserid, education_id);
    var deferred = Q.defer();
    var query = "delete from unic_sol.lu_user_education where education_id=$1 and user_registration_id=$2";
    client.query(query, [education_id, loginuserid], function (err, rows) {
        if (err) {
            uierrorlogger.error("deletepgbyid  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("deleted successfully!");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}
exports.deletephdbyid = function (loginuserid, education_id) {
    uiinfologger.debug("Entering into deletephdbyid>>>> method.." + loginuserid, education_id);
    var deferred = Q.defer();
    var query = "delete from unic_sol.lu_user_education where education_id=$1 and user_registration_id=$2";
    client.query(query, [education_id, loginuserid], function (err, rows) {
        if (err) {
            uierrorlogger.error("deletephdbyid  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("deleted successfully!");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}
exports.searchjob = function (p_skills, p_location, p_experience, p_max_salary) {
    uiinfologger.debug("Entering into searchjob method.." + p_skills, p_location, p_experience, p_max_salary);
    var deferred = Q.defer();

    var query = "select * from  unic_sol.fn_job_search($1,$2,$3,$4)";
    client.query(query, [p_skills, p_location, p_experience, p_max_salary], function (err, rows) {
        if (err) {
            uierrorlogger.error("searchjob  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("searchjob sucess in Database.js");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}
function parameter(param) {
    if (param == undefined) {
        return "";
    }
    else if (param == null || param == "") {
        return param;
    }
    else {
        return param.trim();
    }
}
exports.checkMail = function (email) {
    uiinfologger.debug("Entering into checkMail method.." + email);
    var deferred = Q.defer();
    var query = "select count(*) from unic_sol.lu_user_registration where email_id=$1";
    client.query(query, [email], function (err, rows) {
        if (err) {
            uierrorlogger.error("checkMail  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            //        console.log("checkMail sucess in Database.js"+rows);
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}
exports.invitemail = function (loginuserid, name, mail, mobile) {
    uiinfologger.debug("Entering into invitemail >>>> method.." +
        loginuserid, name, mail, mobile);
    var deferred = Q.defer();
    var query = "insert into unic_sol.t_txn_invite_friends (user_registration_id,name,mail,mobile) values ($1,$2,$3,$4)";
    client.query(query, [loginuserid, name, mail, mobile], function (err, rows) {
        if (err) {
            uierrorlogger.error("invitemail Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("saved successfully!");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}
//checkmobile
exports.checkmobile = function (mobile) {
    uiinfologger.debug("Entering into checkmobile method.." + mobile);
    var deferred = Q.defer();
    var query = "select count(*) from unic_sol.lu_user_profile where phone=$1";
    client.query(query, [mobile], function (err, rows) {
        if (err) {
            uierrorlogger.error("checkmobile  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            //        console.log("checkmobile sucess in Database.js"+rows);
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}