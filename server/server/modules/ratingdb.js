
var express = require('express');
var path = require('path');
var router = express.Router();
var Q = require('q');
var conString = require(path.join(__dirname, '../', '../', 'config')).connectionString;
var uiinfologger = require(path.join(__dirname, '../', '../', 'config')).uiinfologger;
var uierrorlogger = require(path.join(__dirname, '../', '../', 'config')).ccderrorlogger;
var client= require(path.join(__dirname, '../', '../', 'config')).client;


exports.loadcompanylist = function (p_registration_id) {
    uiinfologger.debug("Entering into ratingdb==> loadcompanylist database method.."+p_registration_id);
    var deferred = Q.defer();

    
    var query = "select * from unic_sol.fn_suggested_friends($1)";
    client.query(query,[p_registration_id], function (err, rows) {
        if (err) {
            uierrorlogger.error("ratingdb==>loadcompanylist  Problem with Postgresql.Database" + err);
            deferred.reject(err);
        }
        else {
           
         //   console.log("server rows ratingdb==>loadcompanylist Database" + rows);
         //   console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}

exports.loadcandidatelist = function (p_registration_id) {
    uiinfologger.debug("Entering into ratingdb==> loadcandidatelist database method.."+p_registration_id);
    var deferred = Q.defer();

    
    var query = "select * from unic_sol.fn_suggested_friends($1)";
    client.query(query,[p_registration_id], function (err, rows) {
        if (err) {
            uierrorlogger.error("ratingdb==>loadcandidatelist  Problem with Postgresql.Database" + err);
            deferred.reject(err);
        }
        else {
           
         //   console.log("server rows ratingdb==>loadcandidatelist Database" + rows);
        //    console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}


exports.saverating = function (user_registration_id,
    company_registration_id,
    aspect1,
    aspect2,
    aspect3,
    aspect4, 
    aspect5,
    description) {
    uiinfologger.debug("Entering into ratingdb==>saveratingdb method.."+user_registration_id,
    company_registration_id,
    aspect1,
    aspect2,
    aspect3,
    aspect4, 
    aspect5,
    description);
    var deferred = Q.defer();

    
    var query = "select * from unic_sol.fn_save_event($1,$2,$3,$4,$5,$6,$7)";
    client.query(query, [p_registration_id,p_event_name,p_event_type,p_envet_desription,p_functional_area,p_event_time,location], function (err, rows) {
        if (err) {
            uierrorlogger.error("ratingdb==>saveratingdb  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
           
         //   console.log("ratingdb==>saveratingdb server rows" + rows);
//console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}



exports.saveuserrating = function (tech_rating,
    info_rating,
    behaviour_rating,
    etiquette_rating,
    communication_rating,
    expectations,
    overall_exp,
    user,
    jobid,
    loginuserid) {
    uiinfologger.debug("Entering into ratingdb==>saveratingdb method.." + 
        tech_rating,
        info_rating,
        behaviour_rating,
        etiquette_rating,
        communication_rating,
        expectations,
        overall_exp,
        user,
        jobid,
        loginuserid);

    for (let index = 0; index < user.length; index++) {

        var deferred = Q.defer();

        var query = "select * from unic_sol.fn_save_user_rating($1,$2,$3,$4,$5,$6,$7,$8,$9,$10)";
        client.query(query, [user[index], loginuserid,jobid, tech_rating,info_rating, behaviour_rating, etiquette_rating, communication_rating, expectations, overall_exp], function (err, rows) {
            if (err) {
                uierrorlogger.error("ratingdb==>saveratingdb  Problem with Postgresql.." + err);
                deferred.reject(err);
            }
            else {

                console.log("ratingdb==>saveratingdb server rows" + rows);
                console.log(JSON.stringify(rows.rows));
                deferred.resolve(rows);
            }

        })  
      }

        return deferred.promise;

}


exports.recruiterrating = function ( 
    id,   loginuserid, recruiter_id,
    infrastructurerec,
    hostpitalityrec,
    waiting_timerec,
    arrangementrec,
    overall_exprec,
    r_created_date,
    created_by,
    rating_for,
    jobid,
    infrastructurecomp,
    hostpitalitycomp,
    waiting_timecomp,
    arrangementcomp,
    overall_expcomp,
    c_created_date,
    p_rated_for_comp) {
    uiinfologger.debug("Entering into ratingdb==>saveratingdb method.." + 
    id,   loginuserid, recruiter_id,
    infrastructurerec,
    hostpitalityrec,
    waiting_timerec,
    arrangementrec,
    overall_exprec,
    r_created_date,
    created_by,
    rating_for,
    jobid,
    infrastructurecomp,
    hostpitalitycomp,
    waiting_timecomp,
    arrangementcomp,
    overall_expcomp,
    c_created_date,
    p_rated_for_comp);

  
        var deferred = Q.defer();

        var query = "select * from unic_sol.fn_save_rating2($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19)";
        client.query(query, [id,loginuserid,recruiter_id,infrastructurerec,hostpitalityrec, waiting_timerec, arrangementrec, overall_exprec,r_created_date,
            created_by,rating_for,jobid,infrastructurecomp, hostpitalitycomp, waiting_timecomp, arrangementcomp, overall_expcomp,c_created_date,p_rated_for_comp], function (err, rows) {
            if (err) {
                uierrorlogger.error("ratingdb==>saveratingdb  Problem with Postgresql.." + err);
                deferred.reject(err);
            }
            else {

                console.log("ratingdb==>saveratingdb server rows" + rows);
                console.log(JSON.stringify(rows.rows));
                deferred.resolve(rows);
            }

        })  
     

        return deferred.promise;

}

exports.loadallreviews = function ( jobid,loginuserid) {
    uiinfologger.debug("Entering into ratingdb==>loadallreviews method.." + jobid,loginuserid);

  
  
        var deferred = Q.defer();

        var query = "select * from unic_sol.fn_get_rating($1,$2)";
        client.query(query, [jobid,loginuserid], function (err, rows) {
            if (err) {
                uierrorlogger.error("ratingdb==>loadallreviews  Problem with Postgresql.." + err);
                deferred.reject(err);
            }
            else {

                console.log("ratingdb==>loadallreviews server rows");
               
                deferred.resolve(rows);
            }

        })  
     

        return deferred.promise;

}

exports.loadalluserreviews = function ( posting_id,loginuserid) {
    uiinfologger.debug("Entering into ratingdb==>loadalluserreviews method.." + posting_id,loginuserid);
        var deferred = Q.defer();
        var query = "SELECT * FROM unic_sol.t_txn_user_rating where posting_id=$1 and user_registration_id=$2";
        client.query(query, [posting_id,loginuserid], function (err, rows) {
            if (err) {
                uierrorlogger.error("ratingdb==>loadalluserreviews  Problem with Postgresql.." + err);
                deferred.reject(err);
            }
            else {
                console.log("ratingdb==>loadalluserreviews server rows");               
                deferred.resolve(rows);
            }
        })       
        return deferred.promise;

}




