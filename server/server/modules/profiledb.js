var express = require('express');
var path = require('path');
var router = express.Router();
var Q = require('q');
var conString = require(path.join(__dirname, '../', '../', 'config')).connectionString;
var uiinfologger = require(path.join(__dirname, '../', '../', 'config')).uiinfologger;
var uierrorlogger = require(path.join(__dirname, '../', '../', 'config')).ccderrorlogger;
var client = require(path.join(__dirname, '../', '../', 'config')).client;



exports.ChangeMobile = function (newmobile, loginuserid) {
    uiinfologger.debug(
        "Entering into change Mobile method.." + { newmobile, loginuserid }
    );
    var deferred = Q.defer();
    var query =
        "select * from unic_sol.fn_update_mobile($1,$2)";
    client.query(query, [newmobile, loginuserid], function (err, rows) {
        if (err) {
            uierrorlogger.error("change newmobile  Problem with Postgresql.." + err);
            deferred.reject(err);
        } else {
            console.log("saved newmobilel successfully!");
            deferred.resolve(rows);
        }
    });
    return deferred.promise;
};

exports.getPhonecode = function (loginuserid) {

    uiinfologger.debug("Entering into getphonecode method.." + loginuserid);
    var deferred = Q.defer();


    var query = "select phonecode from unic_sol.fn_get_user_profile($1)";
    client.query(query, [loginuserid], function (err, rows) {
        if (err) {
            uierrorlogger.error("getprofile  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {

            //  console.log("getprofile server rows" + rows);
            //console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}


exports.getIdforEmail = function (userid) {
    var deferred = Q.defer();
    try {
        uiinfologger.info("Entering into getIdforMail .." + userid);
        var query = "select initcap(firstname ||' '|| lastname) as name, email from unic_sol.lu_user_profile  where user_registration_id=$1";
        client.query(query, [userid], function (err, rows) {
            if (err) {

                uierrorlogger.error("Error in getIdforMail Problem with DB.." + err);
                deferred.reject(err);

            } else {
                uiinfologger.info("Leaving from getIdforMail  .." + JSON.stringify(rows.rows));
                deferred.resolve(rows);
            }
        })
    } catch (error) {
        uierrorlogger.error("Error catch expection  in getIdforMail ..." + error);
        deferred.reject(error);
    }
    return deferred.promise;
}



exports.getregistartiondetails = function (loginuserid) {
    uiinfologger.debug("Entering into getregistartiondetails method.." + loginuserid);
    var deferred = Q.defer();


    var query = "select * from unic_sol.lu_user_registration where user_registration_id=$1";
    client.query(query, [loginuserid], function (err, rows) {
        if (err) {
            uierrorlogger.error("getregistartiondetails  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {

            //  console.log("getregistartiondetails server rows" + rows);
            console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}


exports.getprofile = function (loginuserid) {
    uiinfologger.debug("Entering into getprofile method.." + loginuserid);
    var deferred = Q.defer();


    var query = "select * from unic_sol.fn_get_user_profile($1)";
    client.query(query, [loginuserid], function (err, rows) {
        if (err) {
            uierrorlogger.error("getprofile  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {

            //  console.log("getprofile server rows" + rows);
            //console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}
exports.viewProfileCount = function (loginuserid, viewer_id) {
    uiinfologger.debug("Entering into viewProfileCount method..");
    var deferred = Q.defer();


    var query = "select * from unic_sol.fn_profileview_count($1,$2,$3)";
    client.query(query, [viewer_id, loginuserid, viewer_id], function (err, rows1) {
        if (err) {
            uierrorlogger.error("viewProfileCount  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {

            console.log("viewProfileCount  rows ---------------------->" + JSON.stringify(rows1));
            console.log(JSON.stringify(rows1.rows1));
            deferred.resolve(rows1);
        }

    })
    return deferred.promise;
}
exports.getfunctinalareadata = function () {
    uiinfologger.debug("Entering into getfunctinalareadata method..");
    var deferred = Q.defer();


    var query = "select * from unic_sol.lu_functional_area";
    client.query(query, [], function (err, rows) {
        if (err) {
            uierrorlogger.error("getfunctinalareadata  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {

            // console.log("getfunctinalareadataofile server rows" + rows);
            // console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}

exports.Onlinestatus = function (loginuserid) {
    uiinfologger.debug("Entering into Onlinestatus method.." + loginuserid);
    var deferred = Q.defer();
    var query = "update unic_sol.lu_user_registration set status=1 where user_registration_id=$1";
    client.query(query, [loginuserid], function (err, rows) {
        if (err) {
            uierrorlogger.error("Onlinestatus  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            //  console.log("displaycompanydetails server rows" + rows);
            //  console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}
exports.displaycompanydetails = function (loginuserid) {
    uiinfologger.debug("Entering into displaycompanydetails method.." + loginuserid);
    var deferred = Q.defer();
    var query = "select * from unic_sol.lu_organization where user_registration_id=$1";
    client.query(query, [loginuserid], function (err, rows) {
        if (err) {
            uierrorlogger.error("displaycompanydetails  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            //  console.log("displaycompanydetails server rows" + rows);
            //  console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}
exports.getfunctinalareadata = function () {
    uiinfologger.debug("Entering into getfunctinalareadata method..");
    var deferred = Q.defer();


    var query = "select * from unic_sol.lu_functional_area";
    client.query(query, [], function (err, rows) {
        if (err) {
            uierrorlogger.error("getfunctinalareadata  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {


            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}

//getlogincount

exports.getlogincount = function (loginuserid) {
    uiinfologger.debug("Entering into getlogincount>>>> method.." + loginuserid);
    var deferred = Q.defer();

    var query = 'select * from unic_sol.fn_get_logincount($1)';


    client.query(query, [loginuserid], function (err, rows) {
        if (err) {
            uierrorlogger.error("getlogincount  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("saved successfully!");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}

exports.socialusercheck = function (id) {
    uiinfologger.debug("Entering into socialusercheck method..");
    var deferred = Q.defer();


    var query = 'select user_registration_id from unic_sol.lu_user_registration where socialid=$1';
    client.query(query, [id], function (err, rows) {
        if (err) {
            uierrorlogger.error("socialusercheck  Problem with Postgresql.." + err);
            deferred.reject(err);
        } else {

            //   console.log("getfunctinalareadataofile server rows" + rows);
            //   console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}

exports.getOldemail = function (loginuserid) {
    uiinfologger.debug("Entering into getoldemail method..");
    var deferred = Q.defer();


    var query = "select email from unic_sol.lu_user_profile where user_registration_id=$1";
    client.query(query, [loginuserid], function (err, rows) {
        if (err) {
            uierrorlogger.error("get old Email Problem with Postgresql.." + err);
            deferred.reject(err);
        } else {

            //  console.log("getting old email problem server rows" + rows);
            //  console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}
exports.getCurrentMobile = function (loginuserid) {
    uiinfologger.debug("Entering into getCurrentMobile method..");
    var deferred = Q.defer();
    var query = "select phone from unic_sol.lu_user_profile where user_registration_id=$1 ";
    client.query(query, [loginuserid], function (err, rows) {
        if (err) {
            uierrorlogger.error(
                "get Current Mobile  Problem with Postgresql.." + err);
            deferred.reject(err);
        } else {
            //  console.log("getting Current Mobile problem server rows" + rows);
            //  console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }
    });
    return deferred.promise;
};




exports.changePassword = function (loginuserid, oldpassword, password) {
    uiinfologger.debug("Entering into changepassword method.." + loginuserid + oldpassword + password);
    var deferred = Q.defer();

    //   var query = "update  unic_sol.lu_user_registration set password=$1  where user_registration_id=$2";
    var query = "select * from unic_sol.fn_user_changepassword($1,$2,$3)";
    client.query(query, [loginuserid, oldpassword, password], function (err, rows) {
        if (err) {
            uierrorlogger.error("change password  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("changepassword sucess in Database.js");
            deferred.resolve(rows);
        }

    })

    return deferred.promise;
}


exports.getroledata = function (fn_area_id) {
    uiinfologger.debug("Entering into getroledata method.." + fn_area_id);
    var deferred = Q.defer();

    var query = "select role_id,role_name from unic_sol.lu_role where fn_area_id=$1";
    client.query(query, [fn_area_id], function (err, rows) {
        if (err) {
            uierrorlogger.error("getroledata  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {

            // console.log("getroledata server rows" + rows);
            // console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}
exports.getstatesdata = function () {
    uiinfologger.debug("Entering into getstatesdata method..");
    var deferred = Q.defer();
    var query = "select state_id,state_name from unic_sol.lu_state limit 500 ORDER BY state_name";
    client.query(query, [], function (err, rows) {
        if (err) {
            uierrorlogger.error("getstatesdata  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {

            //  console.log("getstatesdata server rows" + rows);
            //  console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}
//change Query
exports.geteducationBoardsforssc = function () {
    uiinfologger.debug("Entering into   geteducationBoardsforssc method..");
    var deferred = Q.defer();
    var query = "select state_id,state_name from unic_sol.lu_state limit 500 ORDER BY state_name ASC";
    client.query(query, [], function (err, rows) {
        if (err) {
            uierrorlogger.error("geteducationBoardsforssc  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {

            //  console.log("geteducationBoardsforssc server rows" + rows);
            ///   console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}

//Add certifications

exports.Addcertifications = function (loginuserid,
    certification_name, certification_body, year, id) {
    uiinfologger.debug("Entering into Addcertifications method.." + loginuserid,
        certification_name, certification_body, year, id);
    var deferred = Q.defer();
    var query = "SELECT * FROM unic_sol.fn_save_certification($1,$2,$3,$4,$5)";
    client.query(query, [loginuserid,
        certification_name, certification_body, year, id], function (err, rows) {
            if (err) {
                uierrorlogger.error("Addcertifications  Problem with Postgresql.." + err);
                deferred.reject(err);
            }
            else {
                console.log("Add certifications sucess in Database.js" + rows);
                deferred.resolve(rows);
            }

        })
    return deferred.promise;

}


// getcertifications 

exports.getcertifications = function (loginuserid) {
    uiinfologger.debug("Entering into getcertifications>>>> method.." + loginuserid);
    var deferred = Q.defer();
    var query = "SELECT * FROM unic_sol.fn_get_certificationdetails($1)";
    client.query(query, [loginuserid], function (err, rows) {
        if (err) {
            uierrorlogger.error("getcertifications  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log(" getcertifications successfully!");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}





exports.deletecertifications = function (loginuserid, id) {
    uiinfologger.debug("Entering into deletebyid>>>> method.." + id, loginuserid);
    var deferred = Q.defer();
    var query = "delete from unic_sol.lu_user_certification where id=$1 and user_registration_id=$2";
    client.query(query, [id, loginuserid], function (err, rows) {
        if (err) {
            uierrorlogger.error("deletebyid  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("deleted successfully!");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}
exports.addlanguagesdetails = function (loginuserid, language_name, is_speak, is_read, is_write) {
    uiinfologger.debug("Entering into addlanguagesdetails>>>> method.." + loginuserid, language_name, is_speak, is_read, is_write);
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_save_language($1,$2,$3,$4,$5)";
    client.query(query, [loginuserid, language_name, is_speak, is_read, is_write], function (err, rows) {
        if (err) {
            uierrorlogger.error("addlanguagesdetails  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("saved successfully!");
            deferred.resolve(rows);
        }
    })
    return deferred.promise;

}

exports.editlanguagesdetails = function (loginuserid, language_id, language_name, is_speak, is_read, is_write) {
    uiinfologger.debug("Entering into editlanguagesdetails>>>> method.." + language_id
        , loginuserid, language_name, is_speak, is_read, is_write);
    var deferred = Q.defer();

    var query = "update unic_sol.lu_user_language set user_registration_id=$1,language_name=$3,is_speak=$4,is_read=$5,is_write=$6 where language_id=$2";
    client.query(query, [loginuserid, language_id, language_name, is_speak, is_read, is_write], function (err, rows) {
        if (err) {
            uierrorlogger.error("editlanguagesdetails  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("edit language details successfully!");
            deferred.resolve(rows);
        }
    })
    return deferred.promise;

}
exports.addskilldetails = function (loginuserid, skill_name, version, last_used, exp_in_years, exp_in_months) {
    uiinfologger.debug("Entering into addskilldetails>>>> method.." + loginuserid, skill_name, version, last_used, exp_in_years, exp_in_months);
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_save_skill($1,$2,$3,$4,$5,$6)";
    client.query(query, [loginuserid, skill_name, version, last_used, exp_in_years, exp_in_months], function (err, rows) {
        if (err) {
            uierrorlogger.error("add skills data  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("saved successfully!");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}
exports.orgdetails = function (loginuserid,
    current_company,
    overview_company,
    Established_year,
  
    type_company,
    key_persons,
    noof_employees,
    financial_info,
    office_address,
    locations_any,
    cmmi_level,
    prestigious_projects,
    awards,
    certifications,
    clients,
    participations
) {
    var deferred = Q.defer();


    var query = "select * from unic_sol.fn_save_organization($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16)";
    client.query(query, [loginuserid, current_company, overview_company, Established_year, type_company, key_persons, noof_employees, financial_info, office_address, locations_any, cmmi_level, prestigious_projects, awards,
        certifications,
        clients,
        participations], function (err, rows) {
            if (err) {
                uierrorlogger.error("orgdetails  Problem with Postgresql..Retrivedudetails Database" + err);
                deferred.reject(err);
            }
            else {

                // console.log("server rows Retrivedudetails Database" + rows);
                //console.log(JSON.stringify(rows.rows));
                deferred.resolve(rows);
            }

        })
    return deferred.promise;
}




exports.getlanguagesdetailsbyid = function (loginuserid) {
    uiinfologger.debug("Entering into getlanguagesdetailsbyid>>>> method.." + loginuserid);
    var deferred = Q.defer();
    var query = "select user_registration_id,language_id,language_name, is_speak, is_read, is_write, is_read from unic_sol.lu_user_language where user_registration_id=$1 and is_active=true order by language_name desc";
    client.query(query, [loginuserid], function (err, rows) {
        if (err) {
            uierrorlogger.error("getlanguagesdetailsbyid  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("saved successfully!");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}
exports.getallskills = function (loginuserid) {
    uiinfologger.debug("Entering into getallskills>>>> method.." + loginuserid);
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_get_skilldetails($1)";
    client.query(query, [loginuserid], function (err, rows) {
        if (err) {
            uierrorlogger.error("getallskills  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("get skills successfully!");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}

exports.editskillsdetails = function (loginuserid, skill_name, version, last_used, exp_in_years, exp_in_months, skill_id) {
    uiinfologger.debug("Entering into editskillsdetails>>>> method.." +
        loginuserid, skill_name, version, last_used, exp_in_years, exp_in_months, skill_id);
    var deferred = Q.defer();

    var query = "select * from unic_sol.fn_save_skill($1,$2,$3,$4,$5,$6,$7)";
    client.query(query, [loginuserid, skill_name, version, last_used, exp_in_years, exp_in_months, skill_id], function (err, rows) {
        if (err) {
            uierrorlogger.error("editskillsdetails  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("edit skills details successfully!");
            deferred.resolve(rows);
        }
    })
    return deferred.promise;

}

exports.loadstates = function () {
    uiinfologger.debug("Entering into loadstates method..");
    var deferred = Q.defer();


    var query = "select state_id,state_name,country_id from unic_sol.lu_state order by state_name limit 500 ";
    client.query(query, [], function (err, rows) {
        if (err) {
            uierrorlogger.error("login  Problem with Postgresql..loadcstatedropdown Database" + err);
            deferred.reject(err);
        }
        else {

            //   console.log("server rows loadstatesdropdown Database" + rows);
            //  console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}


exports.loadstatesbynames = function (statename) {
    uiinfologger.debug("Entering into loadstatesbynames method..",statename);
    var deferred = Q.defer();


    var query = "select * from unic_sol.fn_getstates($1) ";
    client.query(query, [statename], function (err, rows) {
        if (err) {
            uierrorlogger.error("login  Problem with Postgresql..loadstatesbynames Database" + err);
            deferred.reject(err);
        }
        else {

            //   console.log("server rows loadstatesdropdown Database" + rows);
            //  console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}

//load areas 
exports.loadareas = function () {
    uiinfologger.debug("Entering into loadareas method..");
    var deferred = Q.defer();


    var query = "select * from unic_sol.lu_locality limit 50";
    client.query(query, [], function (err, rows) {
        if (err) {
            uierrorlogger.error("login  Problem with Postgresql..loadareas Database" + err);
            deferred.reject(err);
        }
        else {

            //   console.log("server rows loadareas Database" + rows);
            //  console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}

exports.loadalllocalities = function (name) {
    uiinfologger.debug("Entering into loadalllocalities method..",name);
    var deferred = Q.defer();


    var query = "select * from unic_sol.fn_getlocalities($1)";
    client.query(query, [name], function (err, rows) {
        if (err) {
            uierrorlogger.error("login  Problem with Postgresql..loadalllocalities Database" + err);
            deferred.reject(err);
        }
        else {

            //   console.log("server rows loadareas Database" + rows);
            //  console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}
exports.loadcities = function (name) {
    uiinfologger.debug("Entering into loadcities method..");
    var deferred = Q.defer();


    var query = "select * from unic_sol.fn_getcities($1)";
    client.query(query, [name], function (err, rows) {
        if (err) {
            uierrorlogger.error("login  Problem with Postgresql..loadcities Database" + err);
            deferred.reject(err);
        }
        else {

            //     console.log("server rows loadcities Database" + rows);
            //    console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}
exports.loadmaritalstatus = function () {
    uiinfologger.debug("Entering into loadmaritalstatus method..");
    var deferred = Q.defer();


    var query = "select * from unic_sol.lu_maritalstatus";
    client.query(query, function (err, rows) {
        if (err) {
            uierrorlogger.error("login  Problem with Postgresql..loadmaritalstatus Database" + err);
            deferred.reject(err);
        }
        else {

            //    console.log("server rows loadmaritalstatus Database" + rows);
            //    console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}


exports.loadgender = function () {
    uiinfologger.debug("Entering into loadgender method..");
    var deferred = Q.defer();


    var query = "select * from unic_sol.lu_gender";
    client.query(query, function (err, rows) {
        if (err) {
            uierrorlogger.error("login  Problem with Postgresql..loadgender Database" + err);
            deferred.reject(err);
        }
        else {

            //     console.log("server rows loadgender Database" + rows);
            //     console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}
exports.loadsalinlakhs = function () {
    uiinfologger.debug("Entering into loadsalinlakhs method..");
    var deferred = Q.defer();


    var query = "select * from unic_sol.lu_salary_in_lakhs";
    client.query(query, function (err, rows) {
        if (err) {
            uierrorlogger.error("login  Problem with Postgresql..loadsalinlakhs Database" + err);
            deferred.reject(err);
        }
        else {

            //      console.log("server rows loadsalinlakhs Database" + rows);
            //console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}

exports.loadjobtype = function () {
    uiinfologger.debug("Entering into loadjobtype method..");
    var deferred = Q.defer();


    var query = "select * from unic_sol.lu_jobtype";
    client.query(query, function (err, rows) {
        if (err) {
            uierrorlogger.error("login  Problem with Postgresql..load jobtype Database" + err);
            deferred.reject(err);
        }
        else {

            //     console.log("server rows load jobtype Database" + rows);
            //     console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}

exports.loadsalinthousands = function () {
    uiinfologger.debug("Entering into loadsalinthousands method..");
    var deferred = Q.defer();


    var query = "select * from unic_sol.lu_salary_in_thousands";
    client.query(query, function (err, rows) {
        if (err) {
            uierrorlogger.error("login  Problem with Postgresql.. loadsalinthousands Database" + err);
            deferred.reject(err);
        }
        else {

            //     console.log("server rows loadsalinthousands Database" + rows);
            //    console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}

exports.loademptype = function () {
    uiinfologger.debug("Entering into loademptype method..");
    var deferred = Q.defer();


    var query = "select * from unic_sol.lu_employmenttype";
    client.query(query, function (err, rows) {
        if (err) {
            uierrorlogger.error("login  Problem with Postgresql.. loademptype Database" + err);
            deferred.reject(err);
        }
        else {

            //    console.log("server rows loademptype Database" + rows);
            //     console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}

exports.loaddesiredshift = function () {
    uiinfologger.debug("Entering into loaddesiredshift method..");
    var deferred = Q.defer();


    var query = "select * from unic_sol.lu_desiredshift";
    client.query(query, function (err, rows) {
        if (err) {
            uierrorlogger.error("login  Problem with Postgresql.. loaddesiredshift Database" + err);
            deferred.reject(err);
        }
        else {

            //    console.log("server rows loaddesiredshift Database" + rows);
            //    console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}

exports.loadexpyears = function () {
    uiinfologger.debug("Entering into loadexpyears method..");
    var deferred = Q.defer();


    var query = "select * from unic_sol.lu_exp_year";
    client.query(query, function (err, rows) {
        if (err) {
            uierrorlogger.error("login  Problem with Postgresql..load noticeperiod Database" + err);
            deferred.reject(err);
        }
        else {

            //    console.log("server rows loadexpyears Database" + rows);
            //     console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}

exports.loadexpmonths = function () {
    uiinfologger.debug("Entering into loadexpmonths method..");
    var deferred = Q.defer();


    var query = "select * from unic_sol.lu_exp_month";
    client.query(query, function (err, rows) {
        if (err) {
            uierrorlogger.error("login  Problem with Postgresql..load noticeperiod Database" + err);
            deferred.reject(err);
        }
        else {

            //   console.log("server rows loadexpmonths Database" + rows);
            //   console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}
exports.getlanguagenames = function () {
    uiinfologger.debug("Entering into getlanguagenames>>>> method..");
    var deferred = Q.defer();
    var query = "select * from unic_sol.lu_m_languages ";
    client.query(query, [], function (err, rows) {
        if (err) {
            uierrorlogger.error("getlanguagenames  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("saved successfully!");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}
exports.loaddesignations = function () {
    uiinfologger.debug("Entering into loaddesignations method..");
    var deferred = Q.defer();


    var query = "select * from unic_sol.lu_designations";
    client.query(query, function (err, rows) {
        if (err) {
            uierrorlogger.error("login  Problem with Postgresql..load noticeperiod Database" + err);
            deferred.reject(err);
        }
        else {

            //   console.log("server rows loaddesignations Database" + rows);
            //   console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}

exports.getskillnames = function (name) {
    var deferred = Q.defer();
    var query = 'select * from unic_sol.fn_getskills($1)';
    client.query(query, [name], function (err, rows) {
        if (err) {
            uierrorlogger.error("getskillnames  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("saved successfully!");
            deferred.resolve(rows);
        }
    })
    return deferred.promise;
}

// all skills data for mobile api
exports.getallskillnames = function () {
    var deferred = Q.defer();
    var query = 'select * from unic_sol.lu_functional_area_related_skills';
    client.query(query, [], function (err, rows) {
        if (err) {
            uierrorlogger.error("getallskillnames  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("saved successfully!");
            deferred.resolve(rows);
        }
    })
    return deferred.promise;
}

exports.getskillnames1 = function (fn_area_id) {
    //uiinfologger.debug("Entering into getskillnames>>>> method.." + skillname);
    var deferred = Q.defer();

    var query = 'select * from unic_sol.fn_get_skills_by_fn_area_id($1)';
    //uiinfologger.debug("select skill_id,skill_name from unic_sol.lu_m_skills where skill_name ilike '$1' limit 10");

    client.query(query, [fn_area_id], function (err, rows) {
        if (err) {
            uierrorlogger.error("getskillnames  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("saved successfully!");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}
exports.getskillnames2 = function (fn_area_id, name) {
    //uiinfologger.debug("Entering into getskillnames>>>> method.." + skillname);
    var deferred = Q.defer();
    var query = 'select * from unic_sol.fn_get_skills_by_fn_area_id($1, $2)';
    //uiinfologger.debug("select skill_id,skill_name from unic_sol.lu_m_skills where skill_name ilike '$1' limit 10");
    client.query(query, [fn_area_id, name], function (err, rows) {
        if (err) {
            uierrorlogger.error("getskillnames  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("saved successfully!");
            deferred.resolve(rows);
        }
    })
    return deferred.promise;
}



exports.deleteskillsbyid = function (loginuserid, skill_id, skill_name) {
    uiinfologger.debug("Entering into deleteskillsbyid>>>> method.." + skill_id, loginuserid, skill_name);
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_delete_skills($1,$2,$3)";
    client.query(query, [loginuserid, skill_id, null], function (err, rows) {
        if (err) {
            uierrorlogger.error("deleteskillsbyid  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("deleted successfully!");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}

exports.deleteprofileimage= function (loginuserid,profileimage) {
    uiinfologger.debug("Entering into deleteprofileimage>>>> method.." +  loginuserid,profileimage);
    var deferred = Q.defer();
    
    var query = "update unic_sol.lu_user_profile set image=$2 where user_registration_id=$1";
    client.query(query, [loginuserid,null], function (err, rows) {
        if (err) {
            uierrorlogger.error("delete profile image  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("deleted successfully!");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}
exports.deletelanguagesdetailsbyid = function (loginuserid, language_id) {
    uiinfologger.debug("Entering into deletelanguagesdetailsbyid>>>> method.." + language_id, loginuserid);
    var deferred = Q.defer();
    var query = "delete from unic_sol.lu_user_language where language_id=$1 and user_registration_id=$2";
    client.query(query, [language_id, loginuserid], function (err, rows) {
        if (err) {
            uierrorlogger.error("deletelanguagesdetailsbyid  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("deleted successfully!");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}

exports.addgallery = function (loginuserid, name, pt1) {

    uiinfologger.debug("Entering into addgallery >>>> method.." + loginuserid, name, pt1);
    var deferred = Q.defer();
    var query = "insert into unic_sol.lu_user_gallery(user_registration_id,name,image) values($1,$2,$3)";
    client.query(query, [loginuserid, name, pt1], function (err, rows) {
        if (err) {
            uierrorlogger.error("addgallery  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("saved successfully!");
            deferred.resolve(rows);
        }
    })
    return deferred.promise;

}

exports.getgallery = function (loginuserid) {

    uiinfologger.debug("Entering into getgallery>>>> method.." + loginuserid);
    var deferred = Q.defer();
    var query = "select * from unic_sol.lu_user_gallery where user_registration_id=$1";
    client.query(query, [loginuserid], function (err, rows) {
        if (err) {
            uierrorlogger.error("getgallery Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("saved successfully!");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}

exports.deletegallery = function (loginuserid, id) {
    uiinfologger.debug("Entering into deletegallery>>>> method.." + id, loginuserid);
    var deferred = Q.defer();
    var query = "delete from unic_sol.lu_user_gallery where id=$1 and user_registration_id=$2";
    client.query(query, [id, loginuserid], function (err, rows) {
        if (err) {
            uierrorlogger.error("deletegallery  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("deleted successfully!");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}

exports.deletevideoresume = function (loginuserid, videoid) {
    uiinfologger.debug("Entering into delete video resume>>>> method.." + videoid, loginuserid);
    var deferred = Q.defer();
    var query = "delete from unic_sol.lu_user_videoresume where id=$1 and user_registration_id=$2";
    client.query(query, [videoid, loginuserid], function (err, rows) {
        if (err) {
            uierrorlogger.error("delete video resume Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("deleted successfully!");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}

exports.addvideoresume = function (loginuserid, video) {

    uiinfologger.debug("Entering into addvideoresume details>>>> method.." + loginuserid, video);
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_save_videoresume($1,$2,$3)";
    client.query(query, [loginuserid, video, null], function (err, rows) {
        if (err) {
            uierrorlogger.error("addvideoresume  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("saved successfully!");
            deferred.resolve(rows);
        }
    })
    return deferred.promise;

}
exports.getvideoresume = function (loginuserid) {

    uiinfologger.debug("Entering into getvideoresume>>>> method.." + loginuserid);
    var deferred = Q.defer();
    var query = "select * from unic_sol.lu_user_videoresume where user_registration_id=$1";
    client.query(query, [loginuserid], function (err, rows) {
        if (err) {
            uierrorlogger.error("getvideoresume  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("saved successfully!");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}
exports.addachievementdetails = function (loginuserid, name, description, pt) {

    uiinfologger.debug("Entering into add achievements details>>>> method.." + loginuserid, name, description, pt);
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_save_achievements($1,$2,$3,$4)";
    client.query(query, [loginuserid, name, description, pt], function (err, rows) {
        if (err) {
            uierrorlogger.error("addachievementsdetails  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("saved successfully!");
            deferred.resolve(rows);
        }
    })
    return deferred.promise;

}

exports.editachievementdetails = function (loginuserid, id, name, description) {
    uiinfologger.debug("Entering into editskillsdetails>>>> method.." + id
        , loginuserid, name, description);
    var deferred = Q.defer();

    var query = "update unic_sol.lu_user_achievements set user_registration_id=$1,name=$3,description=$4 where id=$2";
    client.query(query, [loginuserid, id, name, description], function (err, rows) {
        if (err) {
            uierrorlogger.error("editachievementdetails  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("edit achievement details successfully!");
            deferred.resolve(rows);
        }
    })
    return deferred.promise;

}

exports.getachievementdetails = function (loginuserid) {

    uiinfologger.debug("Entering into getachievementdetails>>>> method.." + loginuserid);
    var deferred = Q.defer();
    var query = "select * from unic_sol.lu_user_achievements where user_registration_id=$1";
    client.query(query, [loginuserid], function (err, rows) {
        if (err) {
            uierrorlogger.error("getachievementdetails  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("saved successfully!");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}
exports.deleteachievementdetails = function (loginuserid, id) {
    uiinfologger.debug("Entering into deleteachievementdetails>>>> method.." + id, loginuserid);
    var deferred = Q.defer();
    var query = "delete from unic_sol.lu_user_achievements where id=$1 and user_registration_id=$2";
    client.query(query, [id, loginuserid], function (err, rows) {
        if (err) {
            uierrorlogger.error("deleteachievementdetails  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("deleted successfully!");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}
//edit

exports.editprofile = function (loginuserid, resume_headline, preferred_location,
    permanent_address, hometown, pin_code, marital_status, prefered_jobtype, pan_no, passportno, aadhar_no, currentsalary, expectedsalary_min, expectedsalary_max, employment_type, desired_shift, alternate_contact_number, blood_group, hobbies, physical_status, total_exp_in_yrs, total_exp_in_months, date_of_birth, locality,functional_area,fresher_or_exp) {
    uiinfologger.debug("Entering into edit profile details>>>> method.." + loginuserid, resume_headline, preferred_location,
        permanent_address, hometown, pin_code, marital_status, prefered_jobtype, pan_no, passportno, aadhar_no, currentsalary, expectedsalary_min, expectedsalary_max, employment_type, desired_shift, alternate_contact_number, blood_group, hobbies, physical_status, total_exp_in_yrs, total_exp_in_months, date_of_birth, locality,functional_area,fresher_or_exp);
    var deferred = Q.defer();

    var query = "select * from  unic_sol.fn_save_profile($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25,$26) limit 1"

    client.query(query, [loginuserid, resume_headline, preferred_location,
        permanent_address, hometown, pin_code, marital_status, prefered_jobtype, pan_no, passportno, aadhar_no, currentsalary, expectedsalary_min, expectedsalary_max, employment_type, desired_shift, alternate_contact_number, blood_group, hobbies, physical_status, total_exp_in_yrs, total_exp_in_months, date_of_birth, locality,functional_area,fresher_or_exp], function (err, rows) {
            if (err) {
                uierrorlogger.error("editprofiledetails  Problem with Postgresql.." + err);
                deferred.reject(err);
            }
            else {
                console.log("edit profile details successfully!");
                deferred.resolve(rows);
            }
        })
    return deferred.promise;

}


exports.addpresentempdetails = function (loginuserid, organization_name, designation, from_date, to_date, description, notice_period, other_emp, employment, employment_type) {
    uiinfologger.debug("Entering into addpresentempdetails>>>> method.." + loginuserid, organization_name, designation, from_date, to_date, description, notice_period, other_emp, employment_type);
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_save_empdetails($1,$2,$3,$4,$5,$6,$7,$8,$9,$10)";
    client.query(query, [loginuserid, organization_name, designation, from_date, to_date, description, notice_period, other_emp, employment, employment_type], function (err, rows) {
        if (err) {
            uierrorlogger.error("add emp data  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("saved successfully!");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}
exports.addpreviousempdetails = function (loginuserid, organization_name, designation, from_date, to_date, description, notice_period, other_emp, employment, employment_type) {
    uiinfologger.debug("Entering into addpreviousempdetails>>>> method.." + loginuserid, organization_name, designation, from_date, to_date, description, notice_period, other_emp, employment, employment_type);
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_save_previousempdetails($1,$2,$3,$4,$5,$6,$7,$8,$9,$10)";
    client.query(query, [loginuserid, organization_name, designation, from_date, to_date, description, notice_period, other_emp, employment, employment_type], function (err, rows) {
        if (err) {
            uierrorlogger.error("add emp data  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("saved successfully!");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}
exports.getpresentemp = function (employment, loginuserid) {
    uiinfologger.debug("Entering into getpresentemp>>>> method.." + employment, loginuserid);
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_get_empdetails($1,$2)";
    client.query(query, [employment, loginuserid], function (err, rows) {
        if (err) {
            uierrorlogger.error("getpresentemp  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("get presentemp successfully!");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}

exports.getpreviousemp = function (employment, loginuserid) {
    uiinfologger.debug("Entering into getpreviousemp>>>> method.." + employment, loginuserid);
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_get_empdetails($1,$2)";
    client.query(query, [employment, loginuserid], function (err, rows) {
        if (err) {
            uierrorlogger.error("getpreviousemp  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("get previousemp successfully!");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}

exports.getotheremp = function (employment, loginuserid) {
    uiinfologger.debug("Entering into getotheremp>>>> method.." + employment, loginuserid);
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_get_empdetails($1,$2)";
    client.query(query, [employment, loginuserid], function (err, rows) {
        if (err) {
            uierrorlogger.error("getotheremp  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("get otheremp successfully!");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}

exports.editpresentemp = function (loginuserid, organization_name, designation, from_date, to_date, description, notice_period, other_emp, employment, employment_type, id) {
    uiinfologger.debug("Entering into editpresentemp>>>> method.." + loginuserid, organization_name, designation, from_date, to_date, description, notice_period, other_emp, employment, employment_type, id);
    var deferred = Q.defer();

    var query = "select * from unic_sol.fn_save_empdetails($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11)";
    client.query(query, [loginuserid, organization_name, designation, from_date, null, description, notice_period, null, employment, employment_type, id], function (err, rows) {
        if (err) {
            uierrorlogger.error("editpresentemp  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("edit presentemp details successfully!");
            deferred.resolve(rows);
        }
    })
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_save_empdetails($1,$2,$3,$4,$5,$6,$7,$8)";
    return deferred.promise;

}

exports.editpreviousemp = function (loginuserid, organization_name, designation, from_date, to_date, description, employment, employment_type, id) {
    uiinfologger.debug("Entering into editpresentemp>>>> method.." + loginuserid, organization_name, designation, from_date, to_date, description, employment, employment_type, id);
    var deferred = Q.defer();

    var query = "update unic_sol.lu_user_empdetails set organization_name=$2,designation=$3,from_date=$4,to_date=$5,description=$6,employment=$7,employment_type=$8 where id=$9 and user_registration_id=$1";
    client.query(query, [loginuserid, organization_name, designation, from_date, to_date, description, employment, employment_type, id], function (err, rows) {
        if (err) {
            uierrorlogger.error("editpreviousemp  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("edit previousemp details successfully!");
            deferred.resolve(rows);
        }
    })
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_save_empdetails($1,$2,$3,$4,$5,$6,$7,$8)";
    return deferred.promise;

}

exports.editotheremp = function (loginuserid, other_emp, employment, employment_type, id) {
    uiinfologger.debug("Entering into editotheremp>>>> method.." + loginuserid, other_emp, employment, employment_type, id);
    var deferred = Q.defer();

    var query = "update unic_sol.lu_user_empdetails set other_emp=$2,employment=$3,employment_type=$4 where id=$5 and user_registration_id=$1";
    client.query(query, [loginuserid, other_emp, employment, employment_type, id], function (err, rows) {
        if (err) {
            uierrorlogger.error("editotheremp  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("edit otheremp details successfully!");
            deferred.resolve(rows);
        }
    })
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_save_empdetails($1,$2,$3,$4,$5,$6,$7,$8)";
    return deferred.promise;

}
exports.deletepresentempbyid = function (loginuserid, id, organization_name, designation, notice_period) {
    uiinfologger.debug("Entering into deletepresentempbyid>>>> method.." + loginuserid, id, organization_name, designation, notice_period);
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_delete_currentempdetails($1,$2,$3,$4,$5)";
    client.query(query, [loginuserid, id, null, null, null], function (err, rows) {
        if (err) {
            uierrorlogger.error("deletepresentempbyid  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("deleted successfully!");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}

exports.deletepreviousempbyid = function (loginuserid, id) {
    uiinfologger.debug("Entering into deletepreviousempbyid>>>> method.." + loginuserid, id);
    var deferred = Q.defer();
    var query = "delete from unic_sol.lu_user_empdetails where id=$1 and user_registration_id=$2";
    client.query(query, [id, loginuserid], function (err, rows) {
        if (err) {
            uierrorlogger.error("deletepreviousempbyid  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("deleted successfully!");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}
exports.deleteotherempbyid = function (loginuserid, id) {
    uiinfologger.debug("Entering into deleteotherempbyid>>>> method.." + loginuserid, id);
    var deferred = Q.defer();
    var query = "delete from unic_sol.lu_user_empdetails where id=$1 and user_registration_id=$2";
    client.query(query, [id, loginuserid], function (err, rows) {
        if (err) {
            uierrorlogger.error("deleteotherempbyid  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("deleted successfully!");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}

exports.AddEducationdetails = function (user_registration_id, qualification, Specialization, University_or_Institute, year, grading_system, marks, medium, natureof_job) {
    uiinfologger.debug("Entering into AddEducationdetails method.." + user_registration_id, qualification, Specialization, University_or_Institute, year, grading_system, marks, medium, natureof_job);
    var deferred = Q.defer();

    var query = "select * from unic_sol.fn_save_education($1,$2,$3,$4,$5,$6,$7,$8,$9)";
    client.query(query, [user_registration_id, University_or_Institute, year, natureof_job, qualification, Specialization, grading_system, marks, medium], function (err, rows) {
        if (err) {
            uierrorlogger.error("login  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("AddEducationdetails sucess in Database.js");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}


exports.saveProfileImage = function (loginuserid1, image) {

    uiinfologger.debug("Entering into saveProfileImage method..", loginuserid1, image);
    var deferred = Q.defer();

    var query = "update unic_sol.lu_user_profile set image=$2 where user_registration_id=$1";


    client.query(query, [loginuserid1, image], function (err, rows) {
        if (err) {
            uierrorlogger.error("saveProfileImage  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("saveProfileImage sucess in Database.js");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}



exports.getimage = function (loginuserid) {
    uiinfologger.debug("Entering into get image method.." + loginuserid);
    var deferred = Q.defer();


    var query = "select image from unic_sol.lu_user_profile where user_registration_id=$1";
    client.query(query, [loginuserid], function (err, rows) {
        if (err) {
            uierrorlogger.error("getprofile  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {

            //  console.log("get profile image server rows" + rows);
            //  console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}
exports.loadcountrydropdown = function () {
    uiinfologger.debug("Entering into loadcountrydropdown method..");
    var deferred = Q.defer();


    var query = "select DISTINCT * from unic_sol.lu_country ORDER BY country_name ASC";
    client.query(query, function (err, rows) {
        if (err) {
            uierrorlogger.error("login  Problem with Postgresql..loadcountrydropdown Database" + err);
            deferred.reject(err);
        }
        else {

            //   console.log("server rows loadcountrydropdown Database" + rows);
            //   console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}



exports.loadstatesdropdown = function (country_id) {
    uiinfologger.debug("Entering into loadstatesdropdown method.." + country_id);
    var deferred = Q.defer();


    var query = "select state_id,state_name,country_id from unic_sol.lu_state where country_id=$1 ";
    client.query(query, [country_id], function (err, rows) {
        if (err) {
            uierrorlogger.error("login  Problem with Postgresql..loadcstatedropdown Database" + err);
            deferred.reject(err);
        }
        else {

            //      console.log("server rows loadstatesdropdown Database" + rows);
            //     console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}



exports.loadcitydropdown = function (state_id) {
    uiinfologger.debug("Entering into loadcitydropdown method.." + state_id);
    var deferred = Q.defer();


    var query = "select city_id,city_name,state_id from unic_sol.lu_city where state_id=$1 ORDER BY city_name ASC";
    client.query(query, [state_id], function (err, rows) {
        if (err) {
            uierrorlogger.error("login  Problem with Postgresql..loadcitydropdown Database" + err);
            deferred.reject(err);
        }
        else {

            //        console.log("server rows loadcitydropdown Database" + rows);
            //         console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}


exports.loadnoticeperiod = function () {
    uiinfologger.debug("Entering into loadnoticeperiod method..");
    var deferred = Q.defer();


    var query = "select * from unic_sol.lu_notice_period";
    client.query(query, function (err, rows) {
        if (err) {
            uierrorlogger.error("login  Problem with Postgresql..load noticeperiod Database" + err);
            deferred.reject(err);
        }
        else {

            //console.log("server rows loadnoticeperiod Database" + rows);
            //     console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}
exports.loadbloodgroups = function () {
    uiinfologger.debug("Entering into loadbloodgroups method..");
    var deferred = Q.defer();


    var query = "select * from unic_sol.lu_bloodgroup";
    client.query(query, function (err, rows) {
        if (err) {
            uierrorlogger.error("login  Problem with Postgresql.. loadbloodgroups Database" + err);
            deferred.reject(err);
        }
        else {

            //       console.log("server rows loadbloodgroups Database" + rows);
            //       console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}

exports.loadphysicalstatus = function () {
    uiinfologger.debug("Entering into loadphysicalstatus method..");
    var deferred = Q.defer();


    var query = "select * from unic_sol.lu_physicalstatus";
    client.query(query, function (err, rows) {
        if (err) {
            uierrorlogger.error("login  Problem with Postgresql.. loadphysicalstatus Database" + err);
            deferred.reject(err);
        }
        else {

            //     console.log("server rows loadphysicalstatus Database" + rows);
            //    console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}

exports.loadstatus = function () {
    uiinfologger.debug("Entering into loadstatus method..");
    var deferred = Q.defer();


    var query = "select * from unic_sol.lu_fresher_or_exp";
    client.query(query, function (err, rows) {
        if (err) {
            uierrorlogger.error("login  Problem with Postgresql.. loadstatus Database" + err);
            deferred.reject(err);
        }
        else {

            //    console.log("server rows loadstatus Database" + rows);
            //   console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}
//getadviceoptionsdata
exports.getadviceoptionsdata = function () {
    uiinfologger.debug("Entering into getadviceoptionsdata method..");
    var deferred = Q.defer();


    var query = "select id,options from unic_sol.lu_advic_options";
    client.query(query, [], function (err, rows) {
        if (err) {
            uierrorlogger.error("getadviceoptionsdata  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {

            //   console.log("getadviceoptionsdataofile server rows" + rows);
            //   console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}





exports.jobposting = function (
    jobtitle,
    companyname,
    expyear,
    expmonth,
    keyskills,
    location,
    jobpost,
    maxsalaryl,
    maxsalaryt,
    minsalaryt,
    minsalaryt,
    location,
    natureofjob,
    educationdetails,
    vacancy,
    tilldate,
    keyskills,
    description,
    jobpostid) {

    uiinfologger.debug("Entering into jobpostdb method.." +
        jobtitle,
        companyname,
        expyear,
        expmonth,
        keyskills,
        location,
        jobpost,
        maxsalaryl,
        maxsalaryt,
        minsalaryt,
        minsalaryt,
        location,
        natureofjob,
        educationdetails,
        vacancy,
        tilldate,
        keyskills,
        description);
    var deferred = Q.defer();


    var query = "select * from unic_sol.fn_save_posting($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17)";
    client.query(query, [loginuserid, "", location, location, "", minsalary, maxsalary, natureofjob, description, "", "", "", educationdetails, jobtitle, jobpostid, experience, walkin], function (err, rows) {
        if (err) {
            uierrorlogger.error("jobpostdb  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {

            ///console.log("jobpostdb server rows" + rows);
            //     console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}

exports.getmyposts = function (loginuserid) {
    uiinfologger.debug("Entering into getmyposts method.." + loginuserid);
    var deferred = Q.defer();


    var query = "select * from unic_sol.fn_get_posting($1)";
    client.query(query, [loginuserid], function (err, rows) {
        if (err) {
            uierrorlogger.error("getmyposts  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {

            //   console.log("getmyposts server rows" + rows);
            //  console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}









exports.expyear = function () {
    uiinfologger.debug("enter into get expyear method..");
    var deferred = Q.defer();

    var query = "select * from unic_sol.lu_exp_year";
    client.query(query, [], function (err, rows) {
        if (err) {
            uierrorlogger.error("getjobappliedusers  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("expyear sucess in jobposting.js");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}
exports.expmonth = function () {
    uiinfologger.debug("enter into get expmonth method..");
    var deferred = Q.defer();

    var query = "select * from unic_sol.lu_exp_month";
    client.query(query, [], function (err, rows) {
        if (err) {
            uierrorlogger.error("getjobappliedusers  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("expyear sucess in jobposting.js");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}

exports.natureofjob = function () {
    uiinfologger.debug("enter into get natureofjob method..");
    var deferred = Q.defer();

    var query = "select * from unic_sol.lu_nature_of_job";
    client.query(query, [], function (err, rows) {
        if (err) {
            uierrorlogger.error("natureofjob  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("natureofjob sucess in jobposting.js");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}

exports.getcity = function () {
    uiinfologger.debug("enter into get getcity method..");
    var deferred = Q.defer();

    var query = "select * from unic_sol.lu_city limit 500";
    client.query(query, [], function (err, rows) {
        if (err) {
            uierrorlogger.error("getcity  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("getcity sucess in jobpostingdb.js");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}


exports.getallprojects = function (loginuserid) {
    uiinfologger.debug("Entering into getallprojects>>>> method.." + loginuserid);
    var deferred = Q.defer();
    var query = "select * from unic_sol.lu_user_project where user_registration_id=$1";
    client.query(query, [loginuserid], function (err, rows) {
        if (err) {
            uierrorlogger.error("getallprojects  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("get projects successfully!");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}

exports.deleteprojectbyid = function (loginuserid, id) {
    uiinfologger.debug("Entering into deleteprojectbyid>>>> method.." + id, loginuserid);
    var deferred = Q.defer();
    var query = "delete from unic_sol.lu_user_project where id=$1 and user_registration_id=$2";
    client.query(query, [id, loginuserid], function (err, rows) {
        if (err) {
            uierrorlogger.error("deleteprojectbyid  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("deleted successfully!");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}


exports.addproject = function (name, team, description, duration, skills, loginuserid) {
    uiinfologger.debug("Entering into addprojectdetails-------------->>>> method.." + name, team, description, duration, skills, loginuserid);
    var deferred = Q.defer();
    var query = "select * from  unic_sol.fn_save_projectdetails($1,$2,$3,$4,$5,$6,$7)";
    client.query(query, [name, team, description, duration, skills, loginuserid, null], function (err, rows) {
        if (err) {
            uierrorlogger.error("add Project data  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("saved successfully!");
            deferred.resolve(rows);
        }
    })
    return deferred.promise;
}

exports.editprojectdetails = function (loginuserid, id, name, team, description, duration, skills) {
    uiinfologger.debug("Entering into editprojectdetails>>>> method.." + loginuserid, id, name, team, description, duration, skills);
    var deferred = Q.defer();
    var query = "update unic_sol.lu_user_project set user_registration_id=$1,name=$3,team=$4,description=$5,duration=$6,skills=$7 where id=$2";
    client.query(query, [loginuserid, id, name, team, description, duration, skills], function (err, rows) {
        if (err) {
            uierrorlogger.error("edit project details  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("edit project details successfully!");
            deferred.resolve(rows);
        }
    })
    return deferred.promise;

}

exports.allcourses = function () {
    uiinfologger.debug("enter into get allcourses method..");
    var deferred = Q.defer();

    var query = "SELECT DISTINCT coursename,id,courseid FROM unic_sol.lu_courses where id=$1 or id=$2 or id=$3 or id=$4 or id=$5 ORDER BY coursename ASC ";
    client.query(query, [3, 4, 5,6,7], function (err, rows) {
        if (err) {
            uierrorlogger.error("allcourses  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("allcourses sucess in jobpostingdb.js");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}

exports.allcourses1 = function (coursename) {
    uiinfologger.debug("SELECT coursename,id,courseid FROM unic_sol.lu_courses where upper(coursename) like upper('" + coursename + "%')");
    var deferred = Q.defer();

    var query = "SELECT * FROM unic_sol.fn_course_search($1)";
    client.query(query, [coursename], function (err, rows) {
        if (err) {
            uierrorlogger.error("allcourses  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("allcourses sucess in jobpostingdb.js");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}
exports.getinterviewmode = function () {
    uiinfologger.debug("Entering into getinterviewmode method..");
    var deferred = Q.defer();
    var query = "SELECT * FROM unic_sol.lu_mode_of_interview";
    client.query(query, [], function (err, rows) {
        if (err) {
            uierrorlogger.error("getinterviewmode  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {

            //       console.log("getinterviewmode server rows" + rows);
            //      console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}

exports.getinterviewtype = function () {
    uiinfologger.debug("Entering into getinterviewtype method..");
    var deferred = Q.defer();
    var query = " SELECT * FROM unic_sol.lu_type_of_interview;";
    client.query(query, [], function (err, rows) {
        if (err) {
            uierrorlogger.error("getinterviewtype  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {

            //   console.log("getinterviewtype server rows" + rows);
            //   console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}

