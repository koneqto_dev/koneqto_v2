var express = require('express');
var path = require('path');
var router = express.Router();
var Q = require('q');
var conString = require(path.join(__dirname, '../', '../', 'config')).connectionString;
var uiinfologger = require(path.join(__dirname, '../', '../', 'config')).uiinfologger;
var uierrorlogger = require(path.join(__dirname, '../', '../', 'config')).ccderrorlogger;
var client = require(path.join(__dirname, '../', '../', 'config')).client;


// sendmypost
exports.sendmypost = function (recruiter_id,postedjobid,profile_id,send_job_description) {
    uiinfologger.debug("Entering into walkindb==> sendmypost database method.."+recruiter_id,postedjobid,profile_id,send_job_description );
    var deferred = Q.defer();
    var query = "select * from  unic_sol.fn_sendjobpost_email_recuit($1,$2,$3,$4,$5)";
    client.query(query, [recruiter_id,postedjobid,profile_id,recruiter_id,send_job_description], function (err, rows) {
        if (err) {
            uierrorlogger.error("sendmypost  Problem with Postgresql..sendmypost Database" + err);
            deferred.reject(err);
        }
        else {
               console.log("server rows sendmypost Database" + rows);
               console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}
exports.getIdforEmail = function (userid,jobid) {
    var deferred = Q.defer();
    try {
        uiinfologger.info("Entering into getIdforMail .." + userid,jobid);
        var query = "select * from unic_sol.fn_jobpostrecuit_email($1,$2)";
        client.query(query, [userid,jobid], function (err, rows) {
            if (err) {

                uierrorlogger.error("Error in getIdforMail Problem with DB.." + err);
                deferred.reject(err);

            } else {
                uiinfologger.info("Leaving from getIdforMail  .." + JSON.stringify(rows.rows));
                deferred.resolve(rows);
            }
        })
    } catch (error) {
        uierrorlogger.error("Error catch expection  in getIdforMail ..." + error);
        deferred.reject(error);
    }
    return deferred.promise;
}

exports.loadwalkins = function () {
    uiinfologger.debug("Entering into walkindb==> loadwalkins database method.." );
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_walkinjob_search($1,$2,$3,$4)";
    client.query(query, [null, null, null, null], function (err, rows) {
        if (err) {
            uierrorlogger.error("loadwalkins  Problem with Postgresql..loadwalkins Database" + err);
            deferred.reject(err);
        }
        else {
            //    console.log("server rows loadwalkins Database" + rows);
            //    console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}


exports.walkinsearch = function ( skill_name, location, functionalarea ) {
    uiinfologger.debug("Entering into walkindb==>walkinsearch/load database method..",skill_name, location, functionalarea);
    var deferred = Q.defer();
    var query = "select * from  unic_sol.fn_walkinjob_search($1,$2,$3,$4)";
    client.query(query, [skill_name, location, functionalarea,null], function (err, rows) {
        if (err) {
            uierrorlogger.error("walkindb==>walkinsearch  Problem with Postgresql.. Database" + err);
            deferred.reject(err);
        }
        else {

            //console.log("walkindb==>walkinsearch server rows  Database" + rows);
            //  console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}

exports.loadjobsinotherloc = function (p_user_registration_id) {
    uiinfologger.debug("Entering into walkindb==> loadjobsinotherloc database method.." + p_user_registration_id);
    var deferred = Q.defer();

    var query = "select * from unic_sol.fn_job_search_other_location($1,$2,$3,$4,$5)";
    client.query(query, [p_user_registration_id, null, null, null, null], function (err, rows) {
        if (err) {
            uierrorlogger.error("loadjobsinotherloc==>loadjobsinotherloc  Problem with Postgresql..loadjobsinotherloc Database" + err);
            deferred.reject(err);
        }
        else {
            //   console.log("server rows loadjobsinotherloc==>loadjobsinotherloc Database" + rows);
            //    console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}

exports.oprsearch = function (p_user_registration_id, skills, location, experience) {
    uiinfologger.debug("Entering into walkindb==>oprsearch database method.." + p_user_registration_id, skills, location, experience);
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_job_search_other_location($1,$2,$3,$4,$5)";
    client.query(query, [p_user_registration_id, skills, location, experience, null], function (err, rows) {
        if (err) {
            uierrorlogger.error("walkindb==>oprsearch  Problem with Postgresql.. Database" + err);
            deferred.reject(err);
        }
        else {

            //    console.log("walkindb==>oprsearch server rows  Database" + rows);
            //    console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}



exports.loadadvancejobs = function (loginuserid) {
    uiinfologger.debug("Entering into walkindb==> loadadvancedjob database method.." + loginuserid);
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_get_all_user_profile($1)";
    client.query(query, [loginuserid], function (err, rows) {
        if (err) {
            uierrorlogger.error("loadadvancedjob==>advancedjob  Problem with Postgresql..loadadvancedjob Database" + err);
            deferred.reject(err);
        }
        else {
            //    console.log("server rows loadadvancedjob==>loadadvancedjob Database" + rows);
            //    console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}




exports.profileoverview = function (p_registration_id) {
    uiinfologger.debug("Entering into walkindb==> profileoverview database method.." + p_registration_id);
    var deferred = Q.defer();
    var query = "select * from  unic_sol.fn_get_user_profile($1)";
    client.query(query, [p_registration_id], function (err, rows) {
        if (err) {
            uierrorlogger.error("loadwalkins==>advancedjob  Problem with Postgresql..profileoverview Database" + err);
            deferred.reject(err);
        }
        else {
            //  console.log("server rows loadwalkins==>profileoverview Database" + rows);
            //console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}


exports.advancejobsearch = function (loginuserid,firstname, skill_name01,skill_name02,skill_name03,skill_name04,skill_name1, current_location, total_exp_in_yrs, gender, preferred_location, marital_status, employment_type, notice_period, qualification, type_of_interview,physical_status, year,locality) {
    uiinfologger.debug("Entering into walkindb==>advancejobsearch database method.." + loginuserid,skill_name01,skill_name02,skill_name03,skill_name04,skill_name1, current_location, total_exp_in_yrs, gender, preferred_location, marital_status, employment_type, notice_period, qualification, type_of_interview,physical_status, year,locality);
    var deferred = Q.defer();
    

    var query = "select * from unic_sol.fn_advanced_job_search($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19)";
    client.query(query, [loginuserid,firstname, skill_name01,skill_name02,skill_name03,skill_name04,skill_name1, current_location, total_exp_in_yrs, gender, preferred_location, marital_status, employment_type, notice_period, qualification, type_of_interview, physical_status, year,  locality], function (err, rows) {
        if (err) {
            uierrorlogger.error("walkindb==>advancedjobsearch  Problem with Postgresql.. Database" + err);
            deferred.reject(err);
        }
        else {
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}
// exports.advancejobsearch = function (loginuserid,firstname, skill_name,skill_name1, current_location, total_exp_in_yrs, gender, preferred_location, marital_status, employment_type, notice_period, qualification, type_of_interview,physical_status, year,locality) {
    
//     uiinfologger.debug("Entering into walkindb==>advancejobsearch database method.." + loginuserid,skill_name,skill_name1, current_location, total_exp_in_yrs, gender, preferred_location, marital_status, employment_type, notice_period, qualification, type_of_interview,physical_status, year,locality);
//     var deferred = Q.defer();
//     // if (current_location == undefined) {
//     //     current_location = null
//     // }
//     // if (total_exp_in_yrs == undefined) {
//     //     total_exp_in_yrs = null
//     // }
//     var query = "select * from unic_sol.fn_advanced_job_search($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16)";
//     client.query(query, [loginuserid,firstname, skill_name,skill_name1, current_location, total_exp_in_yrs, gender, preferred_location, marital_status, employment_type, notice_period, qualification, type_of_interview, physical_status, year,  locality], function (err, rows) {
//         if (err) {
//             uierrorlogger.error("walkindb==>advancedjobsearch  Problem with Postgresql.. Database" + err);
//             deferred.reject(err);
//         }
//         else {

//             //      console.log("walkindb==>advancejobsearch server rows  Database" + rows);
//             //     console.log(JSON.stringify(rows.rows));
//             deferred.resolve(rows);
//         }

//     })
//     return deferred.promise;

// }






exports.advanceprofileSearch = function (loginuserid,mailid, phone) {
    uiinfologger.debug("Entering into walkindb==>advanceprofileSearch/load database method..");
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_get_user_profile_search_by_email_and_mobile($1,$2,$3)";
    client.query(query, [loginuserid,mailid, phone], function (err, rows) {
        if (err) {
            uierrorlogger.error("walkindb==>advanceprofileSearch  Problem with Postgresql.. Database" + err);
            deferred.reject(err);
        } else {

            // console.log("walkindb==>advanceprofileSearch server rows  Database" + rows);
            //   console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}
