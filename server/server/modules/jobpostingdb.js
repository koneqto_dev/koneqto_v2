var express = require('express');
var path = require('path');
var router = express.Router();
var Q = require('q');
var conString = require(path.join(__dirname, '../', '../', 'config')).connectionString;
var uiinfologger = require(path.join(__dirname, '../', '../', 'config')).uiinfologger;
var uierrorlogger = require(path.join(__dirname, '../', '../', 'config')).ccderrorlogger;
var client = require(path.join(__dirname, '../', '../', 'config')).client;






exports.profilevisibilityenable = function (loginuserid, value) {
    
    uiinfologger.debug("Entering into profilevisibilityenable method.." + loginuserid + " " + value);
    var deferred = Q.defer();
    
    var query = 'select * from unic_sol.fn_profile_visibility($1, $2);';

    // 'select * from unic_sol.fn_profile_visibility($1)'
    client.query(query, [loginuserid, value], function (err, rows) {
        if (err) {
            uierrorlogger.error("profilevisibilityenable  Problem with Postgresql.." + err);
            deferred.reject(err);
        } else {
            deferred.resolve(rows);
        }
    })
    return deferred.promise;
}



exports.report = function (p_registration_id, id, type, description, loginuserid) {
    
    uiinfologger.debug("Entering into report method.." + p_registration_id, id, type, description);
    var deferred = Q.defer();
    
    var query = "INSERT INTO unic_sol.t_txn_spam_reports (registration_id,posting_id,type,created_by,description) values($1,$2,$3,$4,$5)";


    client.query(query, [p_registration_id, id, type, loginuserid, description], function (err, rows) {
        if (err) {

            deferred.reject(err);
        } else {
            deferred.resolve(rows);
        }
    })
    return deferred.promise;
}


exports.deleteexpertAdviceonly = function (Id) {
    console.log("Entering into deleteexpertAdviceonly routes method |||||||||>" + Id);

    uiinfologger.debug("deleteexpertAdviceonly  method.." + Id);
    var deferred = Q.defer();
    var query = "DELETE FROM unic_sol.t_txn_advice_options  WHERE id = $1";
    client.query(query, [Id], function (err, rows) {
        if (err) {
            uierrorlogger.error("deleteexpertAdviceonly *****************  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("deleteexpertAdviceonly sucess in Database.js");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}


exports.getexpertadviceOnly = function (loginuserid) {
    console.log("Entering into getexpertadviceOnly routes method |||||||||>" + loginuserid);

    uiinfologger.debug("getexpertadviceOnly  method.." + loginuserid);
    var deferred = Q.defer();
    var query = "SELECT * FROM unic_sol.fn_get_my_advice($1,$2,$3)";
    client.query(query, [loginuserid, null, null], function (err, rows) {
        if (err) {
            uierrorlogger.error("getexpertadviceOnly *****************  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("getexpertadviceOnly sucess in Database.js");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}


exports.getallmixedposts = function (p_registration_id, search) {
    uiinfologger.debug("Entering into getallmixedposts method.." + p_registration_id + "Search :" + search);
    var deferred = Q.defer();
    // var query = "select * from unic_sol.fn_all_mixed_search($1);";
    var query = 'select * from  unic_sol.fn_all_mixed_posts($1,$2)'
    client.query(query, [p_registration_id, search], function (err, rows) {
        if (err) {
            uierrorlogger.error("getallmixedposts  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            deferred.resolve(rows);
        }
    })
    return deferred.promise;
}


exports.jobposting = function (
    loginuserid,
    jobtitle,
    natureofjob,
    companyname,
    functionalarea,
    educationdetails,
    keyskills,
    optionalskills,
    country,

    state,
    fresher,
    minexp,
    maxexp,
    maxsalary,

    minsalary,
    gender,
    noticeperiod,
    vacancy,
    startdate,
    enddate,
    phy_status,
    contactperson,
    contactno,

    description,
    certification,
    specialmention,
    venue,
    photo,
    video,
    interviewdetails,
    first_question,
    second_question,
    third_question,
    fourth_question,
    fifth_question,
    jobpostid,
    curency,
    locality,
    location,
    salarytype) {

    var deferred = Q.defer();


    var query = "select * from  unic_sol.fn_save_posting($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25,$26, $27, $28,$29, $30, $31,  $32, $33, $34, $35,$36,$37,$38,$39,$40,$41,$42,$43,$44)";
    client.query(query, [loginuserid, "department", state, "ref", minsalary, maxsalary, vacancy, enddate, natureofjob, description, functionalarea, "role", keyskills, educationdetails, jobtitle, jobpostid, minexp, maxexp, interviewdetails, country, venue, video, photo, optionalskills, fresher, gender, startdate, phy_status, contactperson, contactno, "educationStream", specialmention, certification, loginuserid, first_question, second_question, third_question, fourth_question, fifth_question, curency, noticeperiod, locality, location,salarytype], function (err, rows) {
        if (err) {
            uierrorlogger.error("jobpostdb  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {

            // //console.log("jobpostdb server rows" + rows);
            ////console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}


exports.postcharity = function (
    charityname, charitycategory, address, charityidproof, mobilenumber, startdate, enddate, charityfund, termsandconditions, charitydescription, volunteersnames, volunteersmobile, supporting_documents, pt1, supporting_video, charitysubcategory, loginuserid, account_number, account_name, account_type, ifsc_code, firstname, bankname) {
    uiinfologger.debug("Entering into postcharity method.." +
        charityname, charitycategory, address, charityidproof, mobilenumber, startdate, enddate, charityfund, termsandconditions, charitydescription, volunteersnames, volunteersmobile, supporting_documents, pt1, supporting_video, charitysubcategory, loginuserid, account_number, account_name, account_type, ifsc_code, firstname, bankname

    );
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_save_charity($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23)";
    client.query(query, [charityname, charitycategory, address, charityidproof, mobilenumber, startdate, enddate, charityfund, termsandconditions, charitydescription, volunteersnames, volunteersmobile, null, pt1, null, charitysubcategory, loginuserid, account_number, account_name, account_type, ifsc_code, firstname, bankname], function (err, rows) {
        if (err) {
            uierrorlogger.error("postcharity  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("postcharity server rows" + rows);
            console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }
    })
    return deferred.promise;
}


exports.getcharitydetails = function (loginuserid) {
    
    uiinfologger.debug("Entering into getcharitydetails>>>> method.." + loginuserid);
    var deferred = Q.defer();
    var query = "SELECT * FROM unic_sol.lu_charity where user_registration_id=$1";
    client.query(query, [loginuserid], function (err, rows) {
        if (err) {
            uierrorlogger.error("getcharitydetails  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("saved successfully!");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}







exports.updatejob = function (


    loginuserid,
    optional_skills,
    maxsalary,
    minsalary,
    gender,
    notice_period,
    enddate,
    phy_status,
    contactperson,
    photo,
    video,
    jobpostid) {

    var deferred = Q.defer();


    var query = "select * from  unic_sol.fn_update_posting($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12)";
    client.query(query, [loginuserid, minsalary, maxsalary, enddate, optional_skills, jobpostid, video, photo, gender, phy_status, contactperson, notice_period], function (err, rows) {
        if (err) {
            uierrorlogger.error("updatejob  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {

            // //console.log("jobpostdb server rows" + rows);
            ////console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}

// update charity

exports.updatecharity = function (


    loginuserid,
    charity_name,
    charity_address,
    charity_mobile_no,
    charity_help_start_date,
    charity_help_end_date,
    charity_description,
    volunteers_name,
    volunteers_mobile,

    account_number,
    account_name,
    account_type,
    ifsc_code,
    firstname, charity_id, bankname) {
    uiinfologger.debug("Entering into updatecharity method.." + loginuserid,
        charity_name,
        charity_address,
        charity_mobile_no,
        charity_help_start_date,
        charity_help_end_date,
        charity_description,
        volunteers_name,
        volunteers_mobile,

        account_number,
        account_name,
        account_type,
        ifsc_code,
        firstname, charity_id, bankname);
    var deferred = Q.defer();


    var query = "select * from unic_sol.fn_update_charity($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16)";
    client.query(query, [loginuserid,
        charity_name,
        charity_address,
        charity_mobile_no,
        charity_help_start_date,
        charity_help_end_date,
        charity_description,
        volunteers_name,
        volunteers_mobile,
        account_number,
        account_name,
        account_type,
        ifsc_code,
        firstname,
        charity_id, bankname], function (err, rows) {
            if (err) {
                uierrorlogger.error("updatecharity  Problem with Postgresql.." + err);
                deferred.reject(err);
            }
            else {

                // //console.log("jobpostdb server rows" + rows);
                ////console.log(JSON.stringify(rows.rows));
                deferred.resolve(rows);
            }

        })
    return deferred.promise;
}

// delete charity
exports.deletecharity = function (loginuserid, charity_id) {
    uiinfologger.debug("Entering into deletecharity method.." + loginuserid, charity_id);
    var deferred = Q.defer();
    is_active = false

    var query = "update unic_sol.t_txn_user_postings set is_active=$1 where posting_id=$2";
    client.query(query, [loginuserid, charity_id], function (err, rows) {
        if (err) {
            uierrorlogger.error("deletecharity  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {

            ////console.log("deletecharity server rows" + rows);
            // //console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}

exports.getphysicalstatus = function () {
    uiinfologger.debug("Entering into get physical status method..");
    var deferred = Q.defer();


    var query = "select * from unic_sol.lu_physicalstatus";
    client.query(query, function (err, rows) {
        if (err) {
            uierrorlogger.error("get physical status Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {

            ////console.log("GetApplyjob server rows" + rows);
            ////console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}



exports.updateadvice = function (advice_options, functional_area, advice_message, id) {
    // console.log("Entering into deleteexpertAdviceonly routes method |||||||||>" + id);

    uiinfologger.debug("deleteexpertAdviceonly  method.." + advice_options, functional_area, advice_message, id);
    var deferred = Q.defer();
    var query = "update unic_sol.t_txn_advice_options set advice_options=$1,functional_area=$2,advice_message=$3 WHERE id = $4";
    client.query(query, [advice_options, functional_area, advice_message, id], function (err, rows) {
        if (err) {
            uierrorlogger.error("deleteexpertAdviceonly *****************  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("deleteexpertAdviceonly sucess in Database.js");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}



exports.deleteexpertAdviceonly = function (Id) {
    console.log("Entering into deleteexpertAdviceonly routes method |||||||||>" + Id);

    uiinfologger.debug("deleteexpertAdviceonly  method.." + Id);
    var deferred = Q.defer();
    var query = "DELETE FROM unic_sol.t_txn_advice_options  WHERE id = $1";
    client.query(query, [Id], function (err, rows) {
        if (err) {
            uierrorlogger.error("deleteexpertAdviceonly *****************  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("deleteexpertAdviceonly sucess in Database.js");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}


exports.loadcharitycategories = function () {
    uiinfologger.debug("Entering into loadcharitycategories method..");
    var deferred = Q.defer();
    var query = "SELECT * FROM unic_sol.lu_charity_category";
    client.query(query, function (err, rows) {
        if (err) {
            uierrorlogger.error("Problem with Postgresql..loadcharitycategories Database" + err);
            deferred.reject(err);
        }
        else {
            //console.log("server rows loadcharitycategories Database" + rows);
            //    console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }
    })
    return deferred.promise;
}

exports.getallcharityposts = function () {
    uiinfologger.debug("enter into get getallcharityposts method..");
    var deferred = Q.defer();

    var query = "select * from unic_sol.fn_get_charity_log()";
    client.query(query, [], function (err, rows) {
        if (err) {
            uierrorlogger.error("getallcharityposts  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("getallcharityposts sucess in jobpostingdb.js");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}



exports.getmycharityposts = function (loginuserid) {
    uiinfologger.debug("enter into get getmycharityposts method.." + loginuserid);
    var deferred = Q.defer();

    var query = "select * from unic_sol.fn_getmycharityposts($1)";
    client.query(query, [loginuserid], function (err, rows) {
        if (err) {
            uierrorlogger.error("getmycharityposts  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("getmycharityposts sucess in jobpostingdb.js");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}


exports.getallcharityPostsview = function (charity_id) {

    uiinfologger.debug("enter into get getallcharitypostsview method..");
    var deferred = Q.defer();

    var query = "SELECT * FROM unic_sol.lu_charity where charity_id=$1";
    client.query(query, [charity_id], function (err, rows) {
        if (err) {
            uierrorlogger.error("getallcharitypostsview  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("getallcharitypostsview sucess in jobpostingdb.js");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}





exports.savedonaters = function (
    user_registration_id,
    donaters_name,
    donaters_mobile_no,
    donaters_email,
    donaters_address,
    donation_amount,
    frequently_donation,
    supporting_for,
    comments) {
    uiinfologger.debug("Entering into savedonaters method.." +
        user_registration_id,
        donaters_name,
        donaters_mobile_no,
        donaters_email,
        donaters_address,
        donation_amount,
        frequently_donation,
        supporting_for,
        comments
    );
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_save_donaters($1,$2,$3,$4,$5,$6,$7,$8,$9)";
    client.query(query, [user_registration_id, donaters_name, donaters_mobile_no, donaters_email, donaters_address, donation_amount, frequently_donation, supporting_for, comments], function (err, rows) {
        if (err) {
            uierrorlogger.error("savedonaters  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("savedonaters server rows" + rows);
            console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }
    })
    return deferred.promise;
}





exports.loadsubcharitydropdown = function (charity_category_id) {
    uiinfologger.debug("Entering into loadsubcharitydropdown method.." + charity_category_id);
    var deferred = Q.defer();


    var query = "SELECT * FROM unic_sol.lu_charity_sub_category where charity_category_id=$1";
    client.query(query, [charity_category_id], function (err, rows) {
        if (err) {
            uierrorlogger.error("login  Problem with Postgresql..loadsubcharitydropdown Database" + err);
            deferred.reject(err);
        }
        else {

            console.log("server rows loadsubcharitydropdown Database" + rows);
            console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}
exports.getapply = function (loginuserid, jobid) {
    uiinfologger.debug("Entering into Applyjob method.." + loginuserid, jobid);
    var deferred = Q.defer();


    var query = "select * from unic_sol.fn_get_apply_posting($1,$2)";
    client.query(query, [loginuserid, jobid], function (err, rows) {
        if (err) {
            uierrorlogger.error("GetApplyjob  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {

            ////console.log("GetApplyjob server rows" + rows);
            ////console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}

exports.getquestions = function (jobid) {
    uiinfologger.debug("Entering into get questions method.." + jobid);
    var deferred = Q.defer();


    var query = "select * from unic_sol.t_txn_job_posting_question where posting_id=$1";
    client.query(query, [jobid], function (err, rows) {
        if (err) {
            uierrorlogger.error("get questions  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {

            ////console.log("GetApplyjob server rows" + rows);
            ////console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}
exports.getmyposts = function (loginuserid, jobid,search,pagesize,pagelength) {
    uiinfologger.debug("Entering into getmyposts method.." + loginuserid, jobid);
    var deferred = Q.defer();


    var query = "select * from unic_sol.fn_get_posting($1,$2,$3,$4,$5)";
    client.query(query, [loginuserid, jobid,search,pagesize,pagelength], function (err, rows) {
        if (err) {
            uierrorlogger.error("getmyposts  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {

            // //console.log("getmyposts server rows" + rows);
            ////console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}

exports.Applyjob = function (loginuserid, jobid, first, second, third, fourth, fifth) {
    uiinfologger.debug("Entering into Applyjob method.." + loginuserid, jobid, first, second, third, fourth, fifth);
    var deferred = Q.defer();

    type = "post"
    var query = "select * from unic_sol.fn_job_apply($1,$2,$3,$4,$5,$6,$7,$8)";
    client.query(query, [loginuserid, jobid, type, first, second, third, fourth, fifth], function (err, rows) {
        if (err) {
            uierrorlogger.error("Applyjob  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {

            ////console.log("Applyjob server rows" + rows);
            ////console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}

exports.unapplyjob = function (loginuserid, jobid, registration_id) {
    uiinfologger.debug("Entering into unapplyjob method.." + loginuserid, jobid);
    var deferred = Q.defer();

    type = "post"
    var query = "select * from unic_sol.fn_job_apply($1,$2)";
    client.query(query, [loginuserid, jobid], function (err, rows) {
        if (err) {
            uierrorlogger.error("unapplyjob  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {

            ////console.log("Applyjob server rows" + rows);
            ////console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}

exports.deletejob = function (loginuserid, jobid) {
    uiinfologger.debug("Entering into deletejob method.." + loginuserid, jobid);
    var deferred = Q.defer();
    is_active = false

    var query = "update unic_sol.t_txn_user_postings set is_active=$1 where posting_id=$2";
    client.query(query, [is_active, jobid], function (err, rows) {
        if (err) {
            uierrorlogger.error("deletejob  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {

            ////console.log("deletejob server rows" + rows);
            // //console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}

//jobs in other category
exports.getmypostingsinothrcat = function (p_registration_id, p_category) {
    uiinfologger.debug("Entering into getmypostingsinothrcat method.." + p_registration_id);
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_other_job_category($1, $2);"

    client.query(query, [p_registration_id, p_category], function (err, rows) {
        if (err) {
            uierrorlogger.error("getmypostingsinothrcat  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {

            console.log("getmyposts server rows" + rows);
            console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}

//jobs in other location
exports.getmypostingsinothrloc = function (p_registration_id, p_location) {
    uiinfologger.debug("Entering into getmypostingsinothrloc method.." + p_registration_id, p_location);
    var deferred = Q.defer();


    var query = "select * from unic_sol.fn_jobs_in_other_location($1, $2);"

    client.query(query, [p_registration_id, p_location], function (err, rows) {
        if (err) {
            uierrorlogger.error("getmypostingsinothrloc  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {

            // //console.log("getmyposts server rows" + rows);
            ////console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}

///////////////////////////////////////////////  Events Functions  ///////////////////////////////////////////////////////


//posting events function
exports.saveevent = function (eventid,
    p_registration_id,
    eventname,
    eventtype,
    eventdescription,
    eventfunctionalarea,
    eventlocation,
    eventlocality,
    eventimage,
    eventaddress,
    eventstartdate,
    eventenddate,
    eventvideo,
    country,
    state) {
    //to display
    uiinfologger.debug("Entering into saveevent-postingdb module method.." + eventid,
        p_registration_id,
        eventname,
        eventtype,
        eventdescription,
        eventfunctionalarea,
        eventlocation,
        eventlocality,
        eventaddress,
        eventstartdate,
        eventenddate,
        country,
        state);

    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_save_event($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15)";
    client.query(query, [eventid,
        p_registration_id,
        eventname,
        eventtype,
        eventdescription,
        eventfunctionalarea,
        eventlocation,
        eventlocality,
        eventimage,
        eventaddress,
        eventstartdate,
        eventenddate,
        eventvideo,
        country,
        state

    ], function (err, rows) {
        if (err) {
            uierrorlogger.error("saveevent-postingdb module Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {

            // console.log("saveevent-postingdb module server rows" + rows);
            //console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}

//loadeventtypes
exports.loadeventtypes = function () {
    uiinfologger.debug("Entering into loadeventtypes method..");
    var deferred = Q.defer();

    var query = "SELECT event_type_id,event_type FROM unic_sol.lu_event_types where is_active = true";
    client.query(query, [], function (err, rows) {
        if (err) {
            uierrorlogger.error("login Problem with Postgresql..loadeventtypes Database" + err);
            deferred.reject(err);
        }
        else {

            //  console.log("server rows loadeventtypes Database" + rows);
            // console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}

//Get states by country_id
exports.getstate = function (country_id) {
    uiinfologger.debug("enter into get getstate method.." + country_id);
    var deferred = Q.defer();
    var query = "select c.currencycode, s.state_id,s.state_name from  unic_sol.lu_state s join unic_sol.lu_country c  on s.country_id=c.country_id where c.country_id=$1 order by s.state_name asc";
    client.query(query, [country_id], function (err, rows) {
        if (err) {
            uierrorlogger.error("getstate  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            //console.log("getcity sucess in jobpostingdb.js");
            deferred.resolve(rows);
        }
    })
    return deferred.promise;
}

//loadcityinevents
exports.loadcityinevents = function () {
    uiinfologger.debug("Entering into loadcityinevents method..");
    var deferred = Q.defer();

    var query = "select city_id,city_name from unic_sol.lu_city ORDER BY city_name ASC limit 50";
    client.query(query, [], function (err, rows) {
        if (err) {
            uierrorlogger.error("login Problem with Postgresql..loadcityinevents Database" + err);
            deferred.reject(err);
        }
        else {

            //      console.log("server rows loadcityinevents Database" + rows);
            //   console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }
    })
    return deferred.promise;
}

//Get cities by state_id
exports.getcity = function (state_id) {
    //uiinfologger.debug("select * from unic_sol.lu_city where city_name ilike '" + name + "' and state_id=$1" + state_id, name);
    //console.log("entering to get city db")
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_get_city_by_state_id($1)";
    client.query(query, [state_id], function (err, rows) {
        if (err) {
            uierrorlogger.error("getcity  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            //console.log("getcity sucess in jobpostingdb.js");
            deferred.resolve(rows);
        }
    })
    return deferred.promise;
}


// get cities by id for mobile api 
exports.getmobileapicity = function (state_id) {
    //uiinfologger.debug("select * from unic_sol.lu_city where city_name ilike '" + name + "' and state_id=$1" + state_id, name);
    //console.log("entering to getmobileapicity db")
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_get_city_by_state_id($1)";
    client.query(query, [state_id], function (err, rows) {
        if (err) {
            uierrorlogger.error("getmobileapicity  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            //console.log("getmobileapicity sucess in jobpostingdb.js");
            deferred.resolve(rows);
        }
    })
    return deferred.promise;
}

// get allcities  for mobile api 
exports.getmobileapiallcities = function () {
    //uiinfologger.debug("select * from unic_sol.lu_city where city_name ilike '" + name + "' and state_id=$1" + state_id, name);
    //console.log("entering to getmobileapiallcities db")
    var deferred = Q.defer();
    var query = "select * from unic_sol.lu_city";
    client.query(query, [], function (err, rows) {
        if (err) {
            uierrorlogger.error("getmobileapiallcities  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            //console.log("getmobileapiallcities sucess in jobpostingdb.js");
            deferred.resolve(rows);
        }
    })
    return deferred.promise;
}


//Get cities by search
exports.getcity1 = function (state_id, name) {
    uiinfologger.debug("select * from unic_sol.lu_city where city_name ilike '" + name + "' and state_id=$1" + state_id, name);
    console.log("entering to get city db")
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_get_city_by_state_id($1,$2)";
    client.query(query, [state_id, name], function (err, rows) {
        if (err) {
            uierrorlogger.error("getcity  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            //console.log("getcity sucess in jobpostingdb.js");
            deferred.resolve(rows);
        }
    })
    return deferred.promise;
}

//Get localities by city_id
exports.loadlocalities1 = function (city_id) {
    uiinfologger.debug("Entering into loadeventlocalities method..",city_id);
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_get_locality_by_city_id($1)";
    client.query(query, [city_id], function (err, rows) {
        if (err) {
            uierrorlogger.error("login Problem with Postgresql..loadeventlocalities Database" + err);
            deferred.reject(err);
        }
        else {
            deferred.resolve(rows);
        }
    })
    return deferred.promise;
}

// get localities by id for mobile api 
exports.mobileapilocalities = function (city_id) {
    uiinfologger.debug("Entering into mobileapilocalities method..",city_id);
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_get_mobilelocalityapi_by_city_id($1)";
    client.query(query, [city_id], function (err, rows) {
        if (err) {
            uierrorlogger.error("login Problem with Postgresql..mobileapilocalities Database" + err);
            deferred.reject(err);
        }
        else {
            deferred.resolve(rows);
        }
    })
    return deferred.promise;
}

// getall skills for mobile api 
exports.mobileapiallskills = function (fn_area_id) {
    //uiinfologger.debug("Entering into mobileapiskills>>>> method.." + skillname);
    var deferred = Q.defer();

    var query = 'select * from  unic_sol.fn_get_mobileapi_allskills($1)';
    client.query(query, [fn_area_id], function (err, rows) {
        if (err) {
            uierrorlogger.error("mobileapiskills  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("saved successfully!");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}

//Get localities by search
exports.loadlocalities2 = function (city_id, name) {
    uiinfologger.debug("Entering into loadeventlocalities2 method..",city_id,name);
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_get_locality_by_city_id($1, $2)";
    client.query(query, [city_id, name], function (err, rows) {
        if (err) {
            uierrorlogger.error("Problem with Postgresql..loadeventlocalities Database" + err);
            deferred.reject(err);
        }
        else {
            deferred.resolve(rows);
        }
    })
    return deferred.promise;
}

//Getting latest events
exports.allsavedevents = function (loginuserid, pagesize, pagelength) {
    console.log("*********************" + loginuserid, pagesize, pagelength);
    //uiinfologger.debug("Entering into allsavedevents database method..\n");
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_get_latest_events($1,$2,$3)";
    client.query(query, [loginuserid, pagesize, pagelength], function (err, rows) {
        if (err) {
            uierrorlogger.error("allsavedevents Problem with Postgresql Database" + err);
            deferred.reject(err);
        }
        else {
            // console.log("These are server rows of allsavedevents from Database in jobpostingdb" + rows);            
            deferred.resolve(rows);
        }
    })
    return deferred.promise;
}

//interested in event click
exports.interestedinevent = function (event_id, p_registration_id, type) {
    uiinfologger.debug("Entering into interestedinevent db method.." + event_id, p_registration_id, type);
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_event_apply($1,$2,$3)";
    client.query(query, [p_registration_id, event_id, type], function (err, rows) {
        if (err) {
            uierrorlogger.error("interestedineventdb  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {

            //  console.log("interestedineventdb server rows" + rows);
            //  console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}

//uninterested in event click
exports.uninterestedinevent = function (event_id, p_registration_id, type, active) {
    uiinfologger.debug("Entering into uninterestedinevent db method.." + event_id, p_registration_id, type, active);
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_event_apply($1,$2,$3,$4)";
    client.query(query, [p_registration_id, event_id, type, active], function (err, rows) {
        if (err) {
            uierrorlogger.error("uninterestedineventdb  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {

            //  console.log("uninterestedineventdb server rows" + rows);
            //  console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}

//Getting my events
exports.mysavedevents = function (p_registration_id,search,pagesize,pagelength) {
    //uiinfologger.debug("Entering into mysavedevents database method.."+p_registration_id);
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_get_my_events($1,$2,$3,$4)";
    client.query(query, [p_registration_id,search,pagesize,pagelength], function (err, rows) {
        if (err) {
            uierrorlogger.error("mysavedevents  Problem with Postgresql Database" + err);
            deferred.reject(err);
        }
        else {
            //console.log("server rows mysavedevents Database" + rows);
            //console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }
    })
    return deferred.promise;
}

//Deleting my event
exports.deletemyevent = function (event_id) {
    uiinfologger.debug("Entering into deletemyevent db method.." + event_id);
    var deferred = Q.defer();
    //is_active
    var query = "update unic_sol.t_txn_user_event set is_active=false where event_id=$1";
    client.query(query, [event_id], function (err, rows) {
        if (err) {
            uierrorlogger.error("deletemyeventdb  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {

            // console.log("deletemyeventdb server rows" + rows);
            // console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }
    })
    return deferred.promise;
}

//Get interested users for my events
exports.getmyeventappliedusers = function (p_type_id) {
    uiinfologger.debug("Entering into getmyeventappliedusers db method.." + p_type_id);
    var deferred = Q.defer();
    var p_type = 'event';
    var query = "select * from unic_sol.fn_get_applied_users_for_my_event($1,$2)";
    //client.query(query, [p_registration_id,event_id,p_type], function (err, rows) {
    client.query(query, [p_type_id, p_type], function (err, rows) {
        if (err) {
            uierrorlogger.error("getmyeventappliedusers db Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {

            // console.log("getmyeventappliedusers db server rows" + rows);
            // console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}

//Getting events interested by me
exports.getappliedevents = function (p_registration_id) {
    uiinfologger.debug("Entering into getappliedevents db method.." + p_registration_id);
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_get_my_interested_events($1)";
    client.query(query, [p_registration_id], function (err, rows) {
        if (err) {
            uierrorlogger.error("getappliedevents db Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {

            //  console.log("getappliedevents db server rows" + rows);
            //  console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}




exports.geteventsearch = function (p_skills, p_location, p_event_date) {
    uiinfologger.debug("Entering into jobpostingdb==> geteventsearch db method.." + p_skills, p_location, p_event_date);
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_event_search($1,$2,$3)";
    client.query(query, [p_skills, p_location, p_event_date], function (err, rows) {
        if (err) {
            uierrorlogger.error("jobpostingdb==>geteventsearch db Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {

            console.log("jobpostingdb==>geteventsearch db server rows" + rows);
            console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}


exports.loadfriends = function (p_registration_id) {
    uiinfologger.debug("Entering into jobpostingdb==> loadfriends database method.." + p_registration_id);
    var deferred = Q.defer();


    var query = "select * from unic_sol.lu_user_profile where user_profile_id!=$1 limit 2";
    client.query(query, [p_registration_id], function (err, rows) {
        if (err) {
            uierrorlogger.error("jobpostingdb==>loadfriends  Problem with Postgresql..savedevents Database" + err);
            deferred.reject(err);
        }
        else {

            console.log("server rows jobpostingdb==>loadfriends Database" + rows);
            console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}
exports.getpPhonecode = function (country_id) {
    uiinfologger.debug("enter into get getphonecode method.." + country_id);
    var deferred = Q.defer();

    // var query = "select c.currencycode, ci.city_id,ci.city_name from unic_sol.lu_country as c join unic_sol.lu_state as s on c.country_id=s.country_id join unic_sol.lu_city as ci on s.state_id=ci.state_id where c.country_id=$1";
    var query = "select * from unic_sol.lu_country where country_id=$1 ORDER BY country_name ASC";

    client.query(query, [country_id], function (err, rows) {
        if (err) {
            uierrorlogger.error("getphonecode  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            //console.log("getcity sucess in jobpostingdb.js");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}
// exports.jobsearch = function (skills, location, experience, maxsalry, loginuserid) {
//     uiinfologger.debug("Entering into jobsearch database method..");
//     var deferred = Q.defer();
//     var query = "select * from unic_sol.fn_job_search($1,$2,$3,$4,$5)";
//     client.query(query, [skills, location, experience, maxsalry, loginuserid], function (err, rows) {
//         if (err) {
//             uierrorlogger.error("jobpostingdb==>jobsearch  Problem with Postgresql..jobsearch Database" + err);
//             deferred.reject(err);
//         }
//         else {

//             //console.log("jobpostingdb==>server rows jobsearch Database" + rows);
//             //console.log(JSON.stringify(rows.rows));
//             deferred.resolve(rows);
//         }

//     })
//     return deferred.promise;

// }


// job search id for mobile api
exports.jobsearchandroidid = function ( p_posting_id) {

    uiinfologger.debug("Entering into jobsearch database method.."+ p_posting_id);
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_job_search_for_android_api($1)";
    client.query(query, [p_posting_id], function (err, rows) {
        if (err) {
            uierrorlogger.error("jobpostingdb==>jobsearch  Problem with Postgresql..jobsearch Database" + err);
            deferred.reject(err);
        }
        else {

            //console.log("jobpostingdb==>server rows jobsearch Database" + rows);
            //console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}

exports.jobsearchid = function (p_registration_id,skill_name01,skill_name02,skill_name03,skill_name04, p_jobid) {

    uiinfologger.debug("Entering into jobsearch database method.."+p_registration_id+"     "+skill_name01+"    "+skill_name02+"     "+skill_name03 +"   " +skill_name04+"   "+ p_jobid);
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_job_search2($1,$2,$3,$4,$5,$6)";
    client.query(query, [p_jobid,p_registration_id,skill_name01,skill_name02,skill_name03,skill_name04], function (err, rows) {
        if (err) {
            uierrorlogger.error("jobpostingdb==>jobsearch  Problem with Postgresql..jobsearch Database" + err);
            deferred.reject(err);
        }
        else {

            //console.log("jobpostingdb==>server rows jobsearch Database" + rows);
            //console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}

exports.jobsearch = function (skills, location, experience, maxsalry, loginuserid, pagesize, pagelength) {

    uiinfologger.debug("Entering into jobsearch database method..");
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_job_search($1,$2,$3,$4,$5,$6,$7)";
    client.query(query, [skills, location, experience, maxsalry, loginuserid, pagesize, pagelength], function (err, rows) {
        if (err) {
            uierrorlogger.error("jobpostingdb==>jobsearch  Problem with Postgresql..jobsearch Database" + err);
            deferred.reject(err);
        }
        else {

            //console.log("jobpostingdb==>server rows jobsearch Database" + rows);
            //console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}




exports.appliedjobs = function (loginuserid) {
    uiinfologger.debug("Entering into appliedjobs method.." + loginuserid);
    var deferred = Q.defer();
    // (p_University_or_Institute ,p_year ,p_natureof_job ,
    //   p_qualification ,p_Specialization ,p_grading_system ,p_marks ,p_medium 
    //unic_sol.fn_get_apply_posting(registration_ids
    var query = "select * from  unic_sol.fn_get_apply_posting($1)";
    client.query(query, [loginuserid], function (err, rows) {
        if (err) {
            uierrorlogger.error("appliedjobs  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            //console.log("appliedjobs sucess in Database.js");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}
exports.appliedevents = function (loginuserid) {
    uiinfologger.debug("appliedevents into appliedevents method.." + loginuserid);
    var deferred = Q.defer();
    // (p_University_or_Institute ,p_year ,p_natureof_job ,
    //   p_qualification ,p_Specialization ,p_grading_system ,p_marks ,p_medium 
    //unic_sol.fn_get_apply_posting(registration_ids
    var query = "select * from  unic_sol.fn_get_apply_event($1)";
    client.query(query, [loginuserid], function (err, rows) {
        if (err) {
            uierrorlogger.error("appliedevents  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("appliedevents sucess in Database.js");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}





exports.getjobappliedusers = function (jobid, type) {
    uiinfologger.debug("getjobappliedusers into getjobappliedusers method.." + jobid, type);
    var deferred = Q.defer();
    // (p_University_or_Institute ,p_year ,p_natureof_job ,
    //   p_qualification ,p_Specialization ,p_grading_system ,p_marks ,p_medium 
    //unic_sol.fn_get_apply_posting(registration_ids
    var query = "select * from unic_sol.fn_get_job_applied_user($1,$2)";
    client.query(query, [jobid, type], function (err, rows) {
        if (err) {
            uierrorlogger.error("getjobappliedusers  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            //console.log("getjobappliedusers sucess in Database.js");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}
//advice expert post - save advice
// advice_options: this.expadvice.adviceoption,
// functional_area: this.expadvice.functionalarea,
// advice_message: this.expadvice.advicemessage
exports.saveAdvice = function (p_registration_id, advice_options, functional_area, advice_message) {
    uiinfologger.debug("Entering into  save expert advice method.." + p_registration_id, advice_options, functional_area, advice_message);
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_save_advice($1,$2,$3,$4)";
    client.query(query, [p_registration_id, advice_options, functional_area, advice_message],
        function (err, rows) {
            if (err) {
                uierrorlogger.error("jobpostdb==>saveAdvice  Problem with Postgresql..updated by" + err);
                deferred.reject(err);
            }
            else {

                console.log("server rows jobpostdb==> saveAdvice Database" + rows);
                console.log(JSON.stringify(rows.rows));
                deferred.resolve(rows);
            }

        })
    return deferred.promise;
}

//Get Expert Advice.......

exports.getexpertadvice = function (loginuserid, pagesize, pagelength) {
    console.log("Entering into getexpertadvice routes method |||||||||>" + loginuserid, pagesize, pagelength);

    uiinfologger.debug("getexpertadvice  method.." + loginuserid, pagesize, pagelength);
    var deferred = Q.defer();
    var query = "SELECT * FROM unic_sol.fn_get_advice($1,$2,$3)";
    client.query(query, [loginuserid, pagesize, pagelength], function (err, rows) {
        if (err) {
            uierrorlogger.error("getexpertadvice *****************  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("getexpertadvice sucess in Database.js");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}


exports.shortlist = function (jobid, element) {
    uiinfologger.debug("Entering into jobpostingdb==> shortlist database method.." + jobid, element);
    var deferred = Q.defer();
    shortlist = 1
    date = new Date()

    for (let index = 0; index < element.length; index++) {



        var query = "update unic_sol.t_txn_job_apply set status=$1,updated_on=$2 where registration_id=$3 AND master_id=$4";
        client.query(query, [shortlist, date, element[index], jobid], function (err, rows) {
            if (err) {
                uierrorlogger.error("jobpostingdb==>shortlist  Problem with Postgresql..savedevents Database" + err);
                deferred.reject(err);
            }
            else {

                //console.log("jobpostingdb==>server rows shortlist Database" + rows);
                //console.log(JSON.stringify(rows.rows));
                deferred.resolve(rows);
            }

        })
    }
    return deferred.promise;

}

exports.getshortlist = function (jobid) {
    uiinfologger.debug("Entering into jobpostingdb==> get shortlist database method.." + jobid);
    var deferred = Q.defer();



    var query =
        " select * from  unic_sol.fn_get_shortlisted_applicants($1,$2)";
    client.query(query, [jobid, 'post'], function (err, rows) {
        if (err) {
            uierrorlogger.error("jobpostingdb==>shortlist  Problem with Postgresql..savedevents Database" + err);
            deferred.reject(err);
        }
        else {

            //console.log("jobpostingdb==>server rows shortlist Database" + rows);
            //console.log(JSON.stringify(rows.rows)); scheduledlist
            deferred.resolve(rows);
        }

    })

    return deferred.promise;

}
exports.scheduledlist = function (jobid) {
    uiinfologger.debug("Entering into jobpostingdb==> get scheduledlist database method.." + jobid);
    var deferred = Q.defer();



    var query =
        "select * from  unic_sol.fn_get_scheduled_applicants($1,$2)";
    client.query(query, [jobid, 'post'], function (err, rows) {
        if (err) {
            uierrorlogger.error("jobpostingdb==>scheduledlist  Problem with Postgresql..savedevents Database" + err);
            deferred.reject(err);
        }
        else {

            //console.log("jobpostingdb==>server rows shortlist Database" + rows);
            //console.log(JSON.stringify(rows.rows)); 
            deferred.resolve(rows);
        }

    })

    return deferred.promise;

}
exports.progressed = function (jobid) {
    uiinfologger.debug("Entering into jobpostingdb==> get progressed database method.." + jobid);
    var deferred = Q.defer();



    var query =
        "select * from unic_sol.fn_get_progresed_applicants($1,$2);";
    client.query(query, [jobid, 'post'], function (err, rows) {
        if (err) {

        }
        else {

            deferred.resolve(rows);
        }

    })

    return deferred.promise;

}





exports.schedule = function (
    title,
    schedule_date,
    schedule_time,
    interviewer,
    type,
    location,
    mode,
    notify_me,
    created_by,
    schedule_user,
    schedulejobid) {
    var deferred = Q.defer();

    for (let index = 0; index < schedule_user.length; index++) {
        var query =
            "select * from unic_sol.fn_schedule_posting($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11)";
        client.query(query, [schedulejobid, schedule_user[index], title, schedule_date, schedule_time, interviewer, type, location, mode, notify_me, created_by], function (err, rows) {
            if (err) {
                uierrorlogger.error("jobpostingdb==>schedule  Problem with Postgresql..schedule Database" + err);
                deferred.reject(err);
            }
            else {

                //console.log("jobpostingdb==>server rows schedule Database" + rows);
                //console.log(JSON.stringify(rows.rows));
                deferred.resolve(rows);
            }

        })
    }
    return deferred.promise;

}
exports.nextround = function (
    title,
    schedule_date,
    schedule_time,
    interviewer,
    type,
    location,
    mode,
    notify_me,
    created_by,
    schedule_user,
    schedulejobid) {
    uiinfologger.debug("Entering into jobpostingdb==> get schedule database method..");
    var deferred = Q.defer();

    for (let index = 0; index < schedule_user.length; index++) {
        var query =
            "select * from unic_sol.fn_second_interview_schedule($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11)";
        client.query(query, [schedulejobid, schedule_user[index], title, schedule_date, schedule_time, interviewer, type, location, mode, notify_me, created_by], function (err, rows) {
            if (err) {
                uierrorlogger.error("jobpostingdb==>schedule  Problem with Postgresql..schedule Database" + err);
                deferred.reject(err);
            }
            else {

                //console.log("jobpostingdb==>server rows schedule Database" + rows);
                //console.log(JSON.stringify(rows.rows));
                deferred.resolve(rows);
            }

        })
    }
    return deferred.promise;

}
exports.changestatus = function (user, jobid, status) {
    uiinfologger.debug("Entering into jobpostingdb==> get changestatus database method.." + user, jobid, status);
    var deferred = Q.defer();
    status = eval(status)


    for (let index = 0; index < user.length; index++) {
        var query =
            "select * from unic_sol.fn_change_status($1,$2,$3)";
        client.query(query, [user[index], jobid, status], function (err, rows) {
            if (err) {
                uierrorlogger.error("jobpostingdb==>schedule  Problem with Postgresql..changestatus Database" + err);
                deferred.reject(err);
            }
            else {

                //console.log("jobpostingdb==>server rows schedule Database" + rows);
                //console.log(JSON.stringify(rows.rows));
                deferred.resolve(rows);
            }

        })
    }
    return deferred.promise;

}
//////get applied 

exports.getapplied = function (jobid) {
    uiinfologger.debug("Entering into get applied method.." + jobid);
    var deferred = Q.defer();
    type = "post"

    var query = "select * from unic_sol.fn_get_job_applied_user($1,$2)";
    client.query(query, [jobid, type], function (err, rows) {
        if (err) {
            uierrorlogger.error("get applied  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {

            //console.log("get applied server rows" + rows);
            //console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}
/* =======================jobposting=========================== */
exports.appliedjobs = function (loginuserid) {
    uiinfologger.debug("Entering into appliedjobs method.." + loginuserid);
    var deferred = Q.defer();
    // (p_University_or_Institute ,p_year ,p_natureof_job ,
    //   p_qualification ,p_Specialization ,p_grading_system ,p_marks ,p_medium 
    //unic_sol.fn_get_apply_posting(registration_ids
    var query = "select * from  unic_sol.fn_get_apply_posting($1)";
    client.query(query, [loginuserid], function (err, rows) {
        if (err) {
            uierrorlogger.error("appliedjobs  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            //console.log("appliedjobs sucess in Database.js");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}




exports.natureofjob = function () {
    uiinfologger.debug("enter into get natureofjob method..");
    var deferred = Q.defer();

    var query = "select * from unic_sol.lu_nature_of_job";
    client.query(query, [], function (err, rows) {
        if (err) {
            uierrorlogger.error("natureofjob  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            //console.log("natureofjob sucess in jobposting.js");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}






exports.loadlocalities = function () {
    uiinfologger.debug("Entering into loadeventlocalities method..");
    var deferred = Q.defer();

    var query = "select id,city_id,area,zipcode from unic_sol.lu_locality where is_active = true";
    client.query(query, [city_id], function (err, rows) {
        if (err) {
            uierrorlogger.error("login Problem with Postgresql..loadeventlocalities Database" + err);
            deferred.reject(err);
        }
        else {

            //      console.log("server rows loadeventlocalities Database" + rows);
            //   console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }
    })
    return deferred.promise;
}

exports.getjobapplicantstatus = function (loginuserid, jobid, registration_id) {
    uiinfologger.debug("enter into get getjobapplicantstatus method.." + registration_id, jobid);
    var deferred = Q.defer();

    var query = "	select * from unic_sol.t_txn_user_rating where user_registration_id=$1 and posting_id=$2";


    client.query(query, [loginuserid, jobid], function (err, rows) {
        if (err) {
            uierrorlogger.error("getjobapplicantstatus  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            //console.log("getcity sucess in jobpostingdb.js");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}
exports.getanswer = function (p_registration_id, posting_id) {
    uiinfologger.debug("enter into get getanswer method.." + p_registration_id, posting_id);
    var deferred = Q.defer();

    var query = "	select * from unic_sol.t_txn_job_posting_answer where registration_id=$1 and posting_id=$2";


    client.query(query, [p_registration_id, posting_id], function (err, rows) {
        if (err) {
            uierrorlogger.error("getanswer  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            //console.log("getcity sucess in jobpostingdb.js");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}


exports.getIdforEmail = function (userid) {
    var deferred = Q.defer();
    try {
        uiinfologger.info("Entering into getIdforMail .." + userid);
        var query = "select * from unic_sol.fn_jobpost_email($1)";
        client.query(query, [userid], function (err, rows) {
            if (err) {

                uierrorlogger.error("Error in getIdforMail Problem with DB.." + err);
                deferred.reject(err);

            } else {
                uiinfologger.info("Leaving from getIdforMail  .." + JSON.stringify(rows.rows));
                deferred.resolve(rows);
            }
        })
    } catch (error) {
        uierrorlogger.error("Error catch expection  in getIdforMail ..." + error);
        deferred.reject(error);
    }
    return deferred.promise;
}
//intrestedEmail
exports.getEmailIdForIntrested = function (userid, eid) {
    var deferred = Q.defer();
    try {
        uiinfologger.info("Entering into getIdforMail .." + userid, eid);
        var query = "select * from unic_sol.fn_eventintrested_email($1,$2)";
        client.query(query, [userid, eid], function (err, rows) {
            if (err) {

                uierrorlogger.error("Error in getIdforMail Problem with DB.." + err);
                deferred.reject(err);

            } else {
                uiinfologger.info("Leaving from getIdforMail ************* .." + JSON.stringify(rows.rows));
                deferred.resolve(rows);
            }
        })
    } catch (error) {
        uierrorlogger.error("Error catch expection  in getIdforMail ..." + error);
        deferred.reject(error);
    }
    return deferred.promise;
}
//interviewschedulemail

exports.getEmailIdInterviewSh = function (userid, sid) {
    var deferred = Q.defer();
    try {
        uiinfologger.info("Entering into getIdforMail .." + userid, sid);
        var query = "select * from unic_sol.fn_schedulejobpost_email($1,$2);";
        client.query(query, [userid, sid], function (err, rows) {
            if (err) {

                uierrorlogger.error("Error in getIdforMail Problem with DB.." + err);
                deferred.reject(err);

            } else {
                uiinfologger.info("Leaving from getIdforMail  .." + JSON.stringify(rows.rows));
                deferred.resolve(rows);
            }
        })
    } catch (error) {
        uierrorlogger.error("Error catch expection  in getIdforMail ..." + error);
        deferred.reject(error);
    }
    return deferred.promise;
}
//next round
exports.getEmailIdInterviewSh1 = function (userid, sid) {
    var deferred = Q.defer();
    try {
        uiinfologger.info("Entering into getIdforMail .." + userid, sid);
        var query = "select * from unic_sol.fn_schedulejobpost_next_email($1,$2);"; 
        client.query(query, [userid, sid], function (err, rows) {
            if (err) {

                uierrorlogger.error("Error in getIdforMail Problem with DB.." + err);
                deferred.reject(err);

            } else {
                uiinfologger.info("Leaving from getIdforMail  .." + JSON.stringify(rows.rows));
                deferred.resolve(rows);
            }
        })
    } catch (error) {
        uierrorlogger.error("Error catch expection  in getIdforMail ..." + error);
        deferred.reject(error);
    }
    return deferred.promise;
}

// exports.getEmailIdforApplyJobPost = function (userid) {
//     var deferred = Q.defer();
//     try {
//         uiinfologger.info("Entering into getEmailIdforApplyJobPost .." + userid);
//         var query = "select * from unic_sol.fn_applyjobpost_email($1);";
//         client.query(query, [userid], function (err, rows) {
//             if (err) {

//                 uierrorlogger.error("Error in getEmailIdforApplyJobPost Problem with DB.." + err);
//                 deferred.reject(err);

//             } else {
//                 uiinfologger.info("Leaving from getEmailIdforApplyJobPost  .." + JSON.stringify(rows.rows));
//                 deferred.resolve(rows);
//             }
//         })
//     } catch (error) {
//         uierrorlogger.error("Error catch expection  in getEmailIdforApplyJobPost ..." + error);
//         deferred.reject(error);
//     }
//     return deferred.promise;
// }

exports.getIdforEmailevent = function (userid) {
    var deferred = Q.defer();
    try {
        uiinfologger.info("Entering into getIdforMail .." + userid);
        var query = "select * from unic_sol.fn_eventpost_email($1)";

        client.query(query, [userid], function (err, rows) {
            if (err) {

                uierrorlogger.error("Error in getIdforMail Problem with DB.." + err);
                deferred.reject(err);

            } else {
                uiinfologger.info("Leaving from getIdforMail  .." + JSON.stringify(rows.rows));
                deferred.resolve(rows);
            }
        })
    } catch (error) {
        uierrorlogger.error("Error catch expection  in getIdforMail ..." + error);
        deferred.reject(error);
    }
    return deferred.promise;
}







//////////////////////////////////////////getIdforEmail for Expert Advice///////////////////////////////////////////////
exports.getIdforEmailadvice = function (userid) {
    var deferred = Q.defer();
    try {
        uiinfologger.info("Entering into getIdforMail .." + userid);
        var query = "select * from  unic_sol.fn_advicepost_email($1)";


        client.query(query, [userid], function (err, rows) {
            if (err) {

                uierrorlogger.error("Error in getIdforMail Problem with DB.." + err);
                deferred.reject(err);

            } else {
                uiinfologger.info("Leaving from getIdforMail  .." + JSON.stringify(rows.rows));
                deferred.resolve(rows);
            }
        })
    } catch (error) {
        uierrorlogger.error("Error catch expection  in getIdforMail ..." + error);
        deferred.reject(error);
    }
    return deferred.promise;
}

exports.checkapplicants = function (jobid, type) {
    var deferred = Q.defer();
    try {
        uiinfologger.info("Entering into checkapplicants .." + jobid);
        var query = "select count(*) from unic_sol.t_txn_job_apply where master_id=$1 and type=$2";


        client.query(query, [jobid, type], function (err, rows) {
            if (err) {

                uierrorlogger.error("Error in checkapplicants Problem with DB.." + err);
                deferred.reject(err);

            } else {
                uiinfologger.info("Leaving from checkapplicants  .." + JSON.stringify(rows.rows));
                deferred.resolve(rows);
            }
        })
    } catch (error) {
        uierrorlogger.error("Error catch expection  in checkapplicants ..." + error);
        deferred.reject(error);
    }
    return deferred.promise;
}

exports.loadsalarytype = function (jobtype) {
    var deferred = Q.defer();
    try {
        uiinfologger.info("Entering into loadsalarytype .." + jobtype);
        var query = "select * from  unic_sol.fn_get_salary_type($1)";


        client.query(query, [jobtype], function (err, rows) {
            if (err) {

                uierrorlogger.error("Error in loadsalarytype Problem with DB.." + err);
                deferred.reject(err);

            } else {
                uiinfologger.info("Leaving from loadsalarytype  .." + JSON.stringify(rows.rows));
                deferred.resolve(rows);
            }
        })
    } catch (error) {
        uierrorlogger.error("Error catch expection  in loadsalarytype ..." + error);
        deferred.reject(error);
    }
    return deferred.promise;
}



exports.checkreport = function (jobid,registration_id,loginuserid) {
    var deferred = Q.defer();
    try {
        uiinfologger.info("Entering into checkreport .." );
        var query = "select count(*) from unic_sol.t_txn_spam_reports where registration_id=$1 and created_by=$2 and posting_id=$3";


        client.query(query, [registration_id,loginuserid,jobid], function (err, rows) {
            if (err) {

                uierrorlogger.error("Error in checkreport Problem with DB.." + err);
                deferred.reject(err);

            } else {
                uiinfologger.info("Leaving from checkreport  .." + JSON.stringify(rows.rows));
                deferred.resolve(rows);
            }
        })
    } catch (error) {
        uierrorlogger.error("Error catch expection  in checkreport ..." + error);
        deferred.reject(error);
    }
    return deferred.promise;
}


///////////////////////////
// p_registration_id integer,getVideoResume
// p_department character varying,
// p_state character varying,
// p_district character varying,
// p_referrence_id character varying,
// p_min_salary numeric,
// p_max_salary numeric,
// p_naturof_job character varying,
// p_summary character varying,
// p_functional_area character varying,
// p_role character varying,
// p_skill character varying,
// p_education_details character varying)