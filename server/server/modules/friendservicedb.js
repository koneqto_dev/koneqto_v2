
var express = require('express');
var path = require('path');
var router = express.Router();
var Q = require('q');
var conString = require(path.join(__dirname, '../', '../', 'config')).connectionString;
var uiinfologger = require(path.join(__dirname, '../', '../', 'config')).uiinfologger;
var uierrorlogger = require(path.join(__dirname, '../', '../', 'config')).ccderrorlogger;
var client = require(path.join(__dirname, '../', '../', 'config')).client;
// loadconnections

exports.loadconnections = function (search) {
    console.log("Entering into loadconnections routes method |||||||||>" + search);

    uiinfologger.debug("loadconnections  method.." + search);
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_find_connections($1)";
    client.query(query, [search], function (err, rows) {
        if (err) {
            uierrorlogger.error("loadconnections *****************  Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("loadconnections sucess in Database.js");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}
exports.getfriendjobposts = function (loginuserid, psize, plength) {
    uiinfologger.debug("Entering into  save getfriendjobposts method.." + loginuserid, psize, plength);
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_friend_jobpost($1,$2,$3)";
    client.query(query, [loginuserid, psize, plength],
        function (err, rows) {
            if (err) {
                uierrorlogger.error("friendsdb==>getfriendjobposts  Problem with Postgresql..updated by" + err);
                deferred.reject(err);
            }
            else {

                // console.log("server rows friendsdb==> getfriendjobposts Database" + rows);
                // console.log(JSON.stringify(rows.rows));
                deferred.resolve(rows);
            }

        })
    return deferred.promise;
}
exports.friendsearch = function (user_name, loginuserid) {
    uiinfologger.debug("Entering into friendsearch database method..............." + user_name, loginuserid);
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_friend_search12($1,$2) ";
    client.query(query, [user_name, loginuserid], function (err, rows) {
        if (err) {
            uierrorlogger.error("jobpostingdb==>friendsearch  Problem with Postgresql..friendsearch Database" + err);
            deferred.reject(err);
        }
        else {
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}
exports.getIdforEmail = function (userid) {
    var deferred = Q.defer();
    try {
        uiinfologger.info("Entering into getIdforMail .." + userid);
        var query = "select * from unic_sol.fn_jobpost_email($1)";
        client.query(query, [userid], function (err, rows) {
            if (err) {

                uierrorlogger.error("Error in getIdforMail Problem with DB.." + err);
                deferred.reject(err);

            } else {
                uiinfologger.info("Leaving from getIdforMail  .." + JSON.stringify(rows.rows));
                deferred.resolve(rows);
            }
        })
    } catch (error) {
        uierrorlogger.error("Error catch expection  in getIdforMail ..." + error);
        deferred.reject(error);
    }
    return deferred.promise;
}
exports.friendprofileoverview = function (user_reg_id) {
    uiinfologger.debug("Entering into friendsdb==> friendprofileoverview database method.." + user_reg_id);
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_get_user_Profile($1)";
    client.query(query, [user_reg_id], function (err, rows) {
        if (err) {
            uierrorlogger.error("friendsdb==>advancedjob  Problem with Postgresql..friendprofileoverview Database" + err);
            deferred.reject(err);
        }
        else {
            console.log("server rows friendsdb==>friendprofileoverview Database" + rows);
            console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}
//loadfrnds
exports.loadfrnds = function (p_registration_id) {
    uiinfologger.debug("Entering into friendsdb==> loadfrnds database method.." + p_registration_id);
    var deferred = Q.defer();


    var query = "select * from unic_sol.fn_accepted_friends_list_json($1)";
    client.query(query, [p_registration_id], function (err, rows) {
        if (err) {
            uierrorlogger.error("friendsdb==>loadfrnds  Problem with Postgresql..loadfrnds Database" + err);
            deferred.reject(err);
        }
        else {

            //    console.log("server rows friendsdb==>loadfindconnections Database" + rows);`
            //    console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}
//loadfindconnections
exports.loadfindconnections = function (p_registration_id) {
    uiinfologger.debug("Entering into friendsdb==> loadfindconnections database method.." + p_registration_id);
    var deferred = Q.defer();


    var query = "select * from unic_sol.fn_suggested_friends($1) order by user_name limit 10";
    client.query(query, [p_registration_id], function (err, rows) {
        if (err) {
            uierrorlogger.error("friendsdb==>loadfindconnections  Problem with Postgresql..loadfindconnections Database" + err);
            deferred.reject(err);
        }
        else {

            //    console.log("server rows friendsdb==>loadfindconnections Database" + rows);`
            //    console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}
//order by random() 
exports.loadsuggestedfrnds = function (p_registration_id) {
    uiinfologger.debug("Entering into friendsdb==> loadsuggestedfrnds database method.." + p_registration_id);
    var deferred = Q.defer();


    var query = "select * from unic_sol.fn_suggested_friends($1) limit 5 ";
    client.query(query, [p_registration_id], function (err, rows) {
        if (err) {
            uierrorlogger.error("friendsdb==>loadsuggestedfrnds  Problem with Postgresql..loadsuggestedfrnds Database" + err);
            deferred.reject(err);
        }
        else {

            //    console.log("server rows friendsdb==>loadsuggestedfrnds Database" + rows);`
            //    console.log(JSON.stringify(rows.rows)); 
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}
exports.loadreqnotif = function (p_registration_id) {
    uiinfologger.debug("Entering into friendsdb==> loadreqnotification database method.." + p_registration_id);
    var deferred = Q.defer();



    var query = "select * from unic_sol.fn_get_applied_jobs_users_notification_list($1)"
    client.query(query, [p_registration_id], function (err, rows) {
        if (err) {
            uierrorlogger.error("friendsdb==>loadreqnotification  Problem with Postgresql..loadreqfriends Database" + err);
            deferred.reject(err);
        }
        else {

            //  console.log("server rows friendsdb==>loadreqnotification Database" + rows);
            //console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}
exports.loadreqfriends = function (p_registration_id) {
    uiinfologger.debug("Entering into friendsdb==> loadreqfriends database method.." + p_registration_id);
    var deferred = Q.defer();


    var query = "select * from unic_sol.fn_requested_friends($1)";
    client.query(query, [p_registration_id], function (err, rows) {
        if (err) {
            uierrorlogger.error("friendsdb==>loadreqfriends  Problem with Postgresql..loadreqfriends Database" + err);
            deferred.reject(err);
        }
        else {

            //  console.log("server rows friendsdb==>loadreqfriends Database" + rows);
            //     console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}
exports.friendrequeststatus = function (p_request_to, p_request_from, p_is_accepted, p_is_rejected) {
    uiinfologger.debug("Entering into friendsdb==> friendrequeststatus database method.." + p_request_to, p_request_from, p_is_accepted, p_is_rejected);
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_friendrequest_accept_reject($1,$2,$3,$4)";

    client.query(query, [p_request_to, p_request_from, p_is_accepted, p_is_rejected], function (err, rows) {
        if (err) {
            uierrorlogger.error("friendsdb==>friendrequeststatus  Problem with Postgresql..updated by" + err);
            deferred.reject(err);
        }
        else {

            //   console.log("server rows friendsdb==> friendrequeststatus Database" + rows);
            //   console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}
exports.friendrequeststatus1 = function (p_request_to, p_request_from, p_is_accepted, p_is_rejected) {
    uiinfologger.debug("Entering into friendsdb==> friendrequeststatus1 database method.." + p_request_to, p_request_from, p_is_accepted, p_is_rejected);
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_friendrequest_accept_reject($1,$2,$3,$4)";

    client.query(query, [p_request_to, p_request_from, p_is_accepted, p_is_rejected], function (err, rows) {
        if (err) {
            uierrorlogger.error("friendsdb==>friendrequeststatus1  Problem with Postgresql..updated by" + err);
            deferred.reject(err);
        }
        else {

            //   console.log("server rows friendsdb==> friendrequeststatus1 Database" + rows);
            //   console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}
exports.sendfriendrequest = function (p_request_to, p_request_from) {
    uiinfologger.debug("Entering into friendsdb==> sendfriendrequest database method.." + p_request_to, p_request_from);
    var deferred = Q.defer();
    var query = "select * from unic_sol.fn_friendship($1,$2,$3,$4,$5,$6,$7,$8)";
    client.query(query, [p_request_to, p_request_from, true, p_request_from, p_request_from, false, false, p_request_from], function (err, rows) {
        if (err) {
            uierrorlogger.error("friendsdb==>sendfriendrequest  Problem with Postgresql..updated by" + err);
            deferred.reject(err);
        }
        else {

            console.log("server rows friendsdb==> sendfriendrequest Database" + rows);
            console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}
exports.loadacceptedfriends = function (p_registration_id) {
    uiinfologger.debug("Entering into friendsdb==> loadacceptedfriends database method.." + p_registration_id);
    var deferred = Q.defer();


    var query = "select * from unic_sol.fn_Accepted_friends_list($1)";
    client.query(query, [p_registration_id], function (err, rows) {
        if (err) {
            uierrorlogger.error("friendsdb==>loadacceptedfriends  Problem with Postgresql..loadacceptedfriends Database" + err);
            deferred.reject(err);
        }
        else {

            //    console.log("server rows friendsdb==>loadacceptedfriends Database" + rows);
            console.log(JSON.stringify(rows.rows));
            deferred.resolve(rows);
        }

    })
    return deferred.promise;
}
exports.invitemail = function (loginuserid, name, mail, mobile) {
    uiinfologger.debug("Entering into invitemail >>>> method.." +
        loginuserid, name, mail, mobile);
    var deferred = Q.defer();
    var query = "insert into unic_sol.t_txn_invite_friends (user_registration_id,name,mail,mobile) values ($1,$2,$3,$4)";
    client.query(query, [loginuserid, name, mail, mobile], function (err, rows) {
        if (err) {
            uierrorlogger.error("invitemail Problem with Postgresql.." + err);
            deferred.reject(err);
        }
        else {
            console.log("saved successfully!");
            deferred.resolve(rows);
        }

    })
    return deferred.promise;

}

// getEmailIdforinvitefriend
exports.getEmailIdforinvitefriend = function (userid) {
    var deferred = Q.defer();
    try {
        uiinfologger.info("Entering into getEmailIdforinvitefriend .." + userid);
        var query = "select * from unic_sol.fn_invitefriend_email($1);";
        client.query(query, [userid], function (err, rows) {
            if (err) {

                uierrorlogger.error("Error in getEmailIdforinvitefriend Problem with DB.." + err);
                deferred.reject(err);

            } else {
                uiinfologger.info("Leaving from getEmailIdforinvitefriend  .." + JSON.stringify(rows.rows));
                deferred.resolve(rows);
            }
        })
    } catch (error) {
        uierrorlogger.error("Error catch expection  in getEmailIdforinvitefriend ..." + error);
        deferred.reject(error);
    }
    return deferred.promise;
}