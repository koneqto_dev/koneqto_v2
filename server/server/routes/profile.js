var express = require('express');
var path = require('path');
var router = express.Router();
var multer = require('multer');
var profiledb = require(path.join(__dirname, '../', 'modules', 'profiledb'));
var database = require(path.join(__dirname, '../', 'modules', 'database'));
var uiinfologger = require(path.join(__dirname, '../', '../', 'config')).uiinfologger;
var uierrorlogger = require(path.join(__dirname, '../', '../', 'config')).uierrorlogger;
var nodemailer = require(path.join(__dirname, '../', '../', 'config')).nodemailer;
var http = require("http");
var request = require('request');
var aws = require('aws-sdk')
var multer = require('multer')
var multerS3 = require('multer-s3')
var bodyParser = require('body-parser');
var s3 = new aws.S3({ /* ... */ })
var md5 = require('md5');
const fs = require('fs');
var log4js = require('log4js');

var upload = multer({
    storage: multerS3({
        s3: s3,
        bucket: 'some-bucket',
        metadata: function (req, file, cb) {
            cb(null, { fieldName: file.fieldname });
        },
        key: function (req, file, cb) {
            cb(null, Date.now().toString())
        }
    })
})

//server
// var AchievementImages='./publish/assets/achievements/'
// var myGAlleryPath='./publish/assets/mygallery/'
// var ProfileImagesPath='./publish/assets/profileimage/'
// var videoResumePath='./publish/assets/videoresume/'

// local
var AchievementImages = '../server/public/achievements/'
var myGAlleryPath = '../server/public/mygallery/'
// var ProfileImagesPath ='./server/public/profileimage/'
var ProfileImagesPath = '../server/public/profileimage'

var videoResumePath = '../server/public/videoresume'

router.use(function (req, res, next) { //allow cross origin requests
    res.setHeader("Access-Control-Allow-Methods", "POST, PUT, OPTIONS, DELETE, GET");
     //res.header("Access-Control-Allow-Origin", "http://koneqto.com");
      res.header("Access-Control-Allow-Origin", "http://localhost:4200");

    //res.header("Access-Control-Allow-Origin", "http://13.58.33.113");
    //res.header("Access-Control-Allow-Origin", "https://www.koneqto.com");
   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Credentials", true);
    next();
});


/** Serving from the same express Server
No cors required */
router.use(express.static('../Client'));
router.use(bodyParser.json());


router.get('/socialusercheck', function (req, res, next) {
    try {
        //    console.log("getsocialid data");
        profiledb.socialusercheck(req.query.id).then(function (rows) {
            if (rows) {

                res.send(JSON.stringify(rows.rows));
            }
        }).fail(function (err) {
            uierrorlogger.error("api socialusercheck..." + err.body);
            //next(Errormessage("Create New-Password ", err));
        });
    } catch (error) {
        uierrorlogger.error("Error catch expection api socialusercheck..." + error);

    }
});

router.get('/getOldemail', function (req, res, next) {
    try {
        console.log("getoldemail data " + req.query.loginuserid);
        profiledb.getOldemail(req.query.loginuserid).then(function (rows) {
            if (rows) {
                res.end(JSON.stringify(rows.rows));
            }
        }).fail(function (err) {
            uierrorlogger.error("api get old email..." + err.body);

        });
    } catch (error) {
        uierrorlogger.error("Error catch get oldemail..." + error);

    }
});


router.get('/getprofiledetails', function (req, res, next) {
    try {
        this.loginuserid1 = req.query.loginuserid
        var viewer_id = req.query.viewer_id
        var loginuserid = req.query.loginuserid
        var loginusername = req.query.loginusername
        var name = req.body.name;
        console.log('safdasfsdafsadgfdaghsfgjhsfgj' + loginuserid, viewer_id, loginusername)

        profiledb.getprofile(req.query.loginuserid, req.query.viewer_id).then(function (rows) {
            if (rows) {
                if (viewer_id != loginuserid) {
                    console.log('if' + loginuserid, viewer_id)

                    viewProfileCount(loginuserid, viewer_id, name, loginusername);




                }

                res.end(JSON.stringify(rows.rows));
            }
        }).fail(function (err) {
            uierrorlogger.error("api getprofile..." + err.body);

        });
    } catch (error) {
        uierrorlogger.error("Error catch expection api SavenewPassword..." + error);
        next(Errormessage("Create New-Pass.loginword", error));
    }
});
// let 
function viewProfileCount(loginuserid, viewer_id, name, loginusername) {

    console.log(loginuserid, viewer_id)
    profiledb.viewProfileCount(loginuserid, viewer_id)
        .then(function (rows) {
            if (rows) {
                var gguser = JSON.stringify(rows.rows[0].fn_profileview_count);
                console.log(gguser)
                if (gguser == 'Success fully saved') {
                    viewprofileByuser(loginuserid, name, loginusername);
                }
                return;

            }
        })
        .fail(function (err) {
            uierrorlogger.error("profile view count..." + err.body);

        });

}

////////////////////
function viewprofileByuser(loginuserid, name, loginusername) {
    try {
        console.log("Friend request Email....." + loginuserid, name, loginusername);
        profiledb.getIdforEmail(loginuserid, name, loginusername).then(function (rows) {
            if (rows) {
                var emailid = JSON.parse(JSON.stringify(rows.rows))[0].email;
                var name = JSON.parse(JSON.stringify(rows.rows))[0].name;
                console.log("..............." + emailid)
                var varlenth = JSON.parse(JSON.stringify(rows.rows)).length;
                var fullUrl = ' http://koneqto.com';
                if (parseInt(varlenth) != 0) {
                    console.log('hieee');
                    var output =
                        '<table  align="center" style="width:600px;height:100px;font-size:13px;font-family:arial"><tr><td  style="border-bottom: #ccc solid 1px;"><a href=' + fullUrl + '><img width="230" src="http://unicsol.com/wp-content/uploads/2018/05/Koneqto-e1525862695762.png"></a></td></tr><tr><td><p style="font-size: 18px" ><b> Hi ' + name + '!</b></p><p style="font-size: 16px"><b style=" text-transform: capitalize;">' + loginusername + '</b> has viewed your profile.</p> </td></tr><tr><td style="background-color: #ccc; padding: 10px;">Copy Rights By <a style="color:#3b5998" href=' + fullUrl + '>   KONEQTO </a> </td> </tr></table>';

                    let mailOptions = {
                        from: nodemailer.email,
                        to: emailid,
                        subject: 'View Profile',
                        text: '',
                        html: output
                    };
                    nodemailer.sendMail(mailOptions, function (err, info) {
                        if (err) {
                            uierrorlogger.error("api sendmail...emil" + err);
                            //     next(Errormessage("sendmail", err));
                        }
                        else {

                            console.log('Message sent: %s', info.messageId);
                            console.log(info);
                            return
                            //console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
                        }
                    });

                }
                else {
                    console.log("entring into Email does't found case");
                    res.status(500).send({
                        success: false,
                        message: "Email doesn't found, please enter correct email"
                    });
                }
            }
        }).fail(function (err) {
            uierrorlogger.error("api sendmail...fail" + err);
        });
    } catch (error) {
        uierrorlogger.error("Error catch expection api sendmail..." + error);
    }
    return

}
///////////////////
router.get('/getcurrentMobile', function (req, res, next) {
    try {
        console.log("get Current Mobile data " + req.query.loginuserid);
        // this.loginuserid1=req.query.loginuserid;
        profiledb.getCurrentMobile(req.query.loginuserid)
            .then(function (rows) {
                if (rows) {
                    res.end(JSON.stringify(rows.rows));
                }
            })
            .fail(function (err) {
                uierrorlogger.error("api Current Mobile Number email..." + err.body);

            });
    } catch (error) {
        uierrorlogger.error("Error catch get Current Mobile Number..." + error);

    }
});


router.get('/getregistartiondetails', function (req, res, next) {
    try {
        this.loginuserid1 = req.query.loginuserid
        profiledb.getregistartiondetails(req.query.loginuserid).then(function (rows) {

            if (rows) {
                res.end(JSON.stringify(rows.rows));
                console.log(JSON.stringify(rows.rows))
            }
        }).fail(function (err) {
            uierrorlogger.error("api getregistartiondetails..." + err.body);

        });
    } catch (error) {
        uierrorlogger.error("Error catch expection api SavenewPassword..." + error);
        next(Errormessage("Create New-Pass.loginword", error));
    }
});



router.post('/postCompanyDetails', function (req, res) {
    profiledb.orgdetails(req.body.loginuserid,
        req.body.current_company,
        req.body.overview_company,
        req.body.established_year,
       
        req.body.type_company,
        req.body.key_persons,
        req.body.noof_employees,
        req.body.financial_info,
        req.body.office_address,
        req.body.locations_any,
        req.body.cmmi_level,
        req.body.prestigious_projects,
        req.body.awards,
        req.body.certifications,
        req.body.clients,
        req.body.participations)
        .then(function (rows) {
            if (rows) {
                console.log("add Organizationdetails details Sucessful")
                res.status(200).send({
                    success: true,
                    message: 'Sucessfull'
                });
            }
            else {
                console.log("Organizationdetails details falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'add Organizationdetails Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error(" addprofiledetails failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'Organizationdetails Method Error' + err
            });
        });
});



router.get('/displaycompanydetails', function (req, res, next) {
    try {

        profiledb.displaycompanydetails(req.query.loginuserid).then(function (rows) {
            if (rows) {
                res.end(JSON.stringify(rows.rows));
            }
        }).fail(function (err) {
            uierrorlogger.error("api displaycompanydetails..." + err.body);
        });
    } catch (error) {
        uierrorlogger.error("Error catch expection api displaycompanydetails..." + error);
        next(Errormessage("displaycompanydetails", error));
    }
});

router.get('/getfunctinalareadata', function (req, res, next) {
    try {
        console.log("getfunctinalareadata data");
        profiledb.getfunctinalareadata().then(function (rows) {
            if (rows) {

                res.send(JSON.stringify(rows.rows));
            }
        }).fail(function (err) {
            uierrorlogger.error("api getfunctinalareadata..." + err.body);
        });
    } catch (error) {
        uierrorlogger.error("Error catch expection api SavenewPassword..." + error);

    }
});



router.get('/getroledata', function (req, res, next) {
    try {
        console.log("getroledata data" + req.query.fn_area_id);
        profiledb.getroledata(req.query.fn_area_id).then(function (rows) {
            if (rows) {
                res.end(JSON.stringify(rows.rows));
            }
        }).fail(function (err) {
            uierrorlogger.error("api getroledata..." + err.body);
        });
    } catch (error) {
        uierrorlogger.error("Error catch expection api getroledata..." + error);


    }
});

router.get('/getstatesdata', function (req, res, next) {
    try {
        console.log("getstatesdata data");
        profiledb.getstatesdata().then(function (rows) {
            if (rows) {
                res.end(JSON.stringify(rows.rows));
            }
        }).fail(function (err) {
            uierrorlogger.error("api getstatesdata..." + err.body);
        });
    } catch (error) {
        uierrorlogger.error("Error catch expection api getstatesdata..." + error);

    }
});
router.get('/geteducationBoardsforssc', function (req, res, next) {
    try {
        console.log("geteducationBoardsforssc");
        profiledb.geteducationBoardsforssc().then(function (rows) {
            if (rows) {
                res.end(JSON.stringify(rows.rows));
            }
        }).fail(function (err) {
            uierrorlogger.error("api geteducationBoardsforssc..." + err.body);
        });
    } catch (error) {
        uierrorlogger.error("Error catch expection api geteducationBoardsforssc..." + error);

    }
});




router.post('/addlanguagesdetails', function (req, res) {
    console.log("req body", JSON.stringify(req.body.loginuserid, req.body.language_name, req.body.is_speak, req.body.is_read, req.body.is_write))
    profiledb.addlanguagesdetails(req.body.loginuserid, req.body.language_name, req.body.is_speak, req.body.is_read, req.body.is_write)
        .then(function (rows) {
            if (rows) {
                console.log("add laNGuage details Sucessful")
                res.status(200).send({
                    success: true,
                    message: 'Sucessfull'
                });
            }
            else {
                console.log("addLanguage details falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'add languagedetails Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error(" addprofiledetails failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'addprofiledetails Method Error' + err
            });
        });
});


router.post('/onlinestatus', function (req, res) {
    console.log("req body", JSON.stringify(req.body.loginuserid))
    profiledb.Onlinestatus(req.body.loginuserid)
        .then(function (rows) {
            if (rows) {
                console.log("onlinestatus Sucessful")
                res.status(200).send({
                    success: true,
                    message: 'Sucessfull'
                });
            }
            else {
                console.log("onlinestatus falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'onlinestatus Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error(" onlinestatus failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'onlinestatus Method Error' + err
            });
        });
});

var videostorage = multer.diskStorage({ //multers disk storage settings
    destination: function (req, file, cb) {
        cb(null, videoResumePath);
    },
    filename: function (req, file, cb) {
        var datetimestamp = Date.now();
        cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1]);
    }
});
var videoupload = multer({ //multer settings
    storage: videostorage
}).single('file');

var video;
/** API path that will upload the files */
router.post('/videouploads', function (req, res) {

    console.log("file sizze==", req.file)
    videoupload(req, res, function (err) {
        console.log("file", req.file);
        if (err) {
            res.json({ error_code: 1, err_desc: err });
            return;
        }
        res.json({ error_code: 0, err_desc: null });
        //console.log("path ---",req.file.path)
        //var path=JSON.stringify(req.file.filename);
        video = req.file.filename
        //path.push(req.file.filename);

        //var path1='uploads/'+path;

        console.log("File name ", video)
    });
});

router.post('/addvideoresume', function (req, res) {

    vid_data = req.body.video

    var pt = '';
    var dt = new Date();
    video = req.body.loginusername + '-' + 'videoresume' + '-' + dt.getFullYear() + "" + dt.getMonth() + "" + dt.getMilliseconds() + "" + vid_data.type;
    path = './public/videoresume/' + video;
    fs.writeFile(path, vid_data.vid_data, 'base64', (err) => {
        if (err)
            console.log(err)
        else {
            console.log('video Saved Success...');
        }

    })

    console.log("req body", JSON.stringify(req.body.loginuserid, video))
    profiledb.addvideoresume(req.body.loginuserid, video)
        .then(function (rows) {
            if (rows) {
                console.log("add addvideoresume details Sucessful")
                res.status(200).send({
                    success: true,
                    message: 'Sucessfull'
                });
            }
            else {
                console.log("add addvideoresume  details falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'add addvideoresume details Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error(" addvideoresume failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'addvideoresume Method Error' + err
            });
        });
});

router.get('/getvideoresume', function (req, res) {

    console.log("Entering into getvideoresume>>>> method", req.query.loginuserid);
    profiledb.getvideoresume(req.query.loginuserid)
        .then(function (rows) {
            //   console.log("1>>",JSON.stringify(rows))
            //  console.log("2>>",JSON.stringify(rows.rows))
            if (rows) {
                console.log("getvideoresume Sucessful")
                res.send(rows.rows)
            }
            else {
                console.log("getvideoresume falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'get getvideoresume  Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error("getvideoresume failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'getvideoresume Method Error' + err
            });
        });
});
router.post('/addgallery', function (req, res) {
    console.log('save image')
    var data = []
    var achievimage = []
    data = req.body.filetype
    achievimage = req.body.image
    console.log(achievimage,"achievements")

    var pt1 = [];
    for (let i = 0; i < data.length; i++) {

        console.log('save image', data[i])
        // console.log('save image', achievimage[i])

        var dt = new Date();

        ptr = req.body.loginusername + '-' + dt.getFullYear() + "" + dt.getMonth() + "" + dt.getMilliseconds() + "" + i + '.' + data[i];
        console.log(ptr, "ptr")
        pts = './public/mygallery/' + ptr;
        fs.writeFile(pts, achievimage[i], 'base64', (err) => {
            if (err)
                console.log(err)
            else {

                console.log('Image Saved Success...');
            }

        })
        pt1.push(ptr)
        console.log(pt1)
    }
    profiledb.addgallery(req.body.loginuserid, req.body.name, pt1)
        .then(function (rows) {
            if (rows) {
                console.log("add gallery details Sucessful")
                res.status(200).send({
                    success: true,
                    message: 'Sucessfull'
                });

            }
            else {
                console.log("add gallery  details falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'add gallery details Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error("add gallery failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'add gallery Method Error' + err
            });
        });
});

galleryimages = []
var gallery
router.post('/deletegallery', function (req, res) {
    console.log("Entering into deletegallery>>>> method", req.body.loginuserid, req.body.id);
    profiledb.deletegallery(req.body.loginuserid, req.body.id)
        .then(function (rows) {

            //  console.log("1>>",JSON.stringify(rows))
            //  console.log("2>>",JSON.stringify(rows.rows))
            if (rows) {
                gallery = req.body.image;
                // galleryimages = gallery.split(' ');
                console.log("images===", galleryimages)
                // for (var i = 0; i < gallery.length - 1; i++) {
                //     fs.unlink(myGAlleryPath + gallery[i], (err) => {
                //         if (err) throw err;

                //     },

                //         console.log("deleted the image" + galleryimages[i])

                //     );

                // }
                console.log("deletegallerydetails Sucessful")
                res.send(rows)
            }
            else {
                console.log("deletegallerydetails failed" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'deletegallerydetails Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error("deletegallerydetails failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'deletegallerydetails Method Error' + err
            });
        });
});


router.post('/deletevideoresume', function (req, res) {
    console.log("Entering into delete video>>>> method", req.body.loginuserid, req.body.videoid);
    profiledb.deletevideoresume(req.body.loginuserid, req.body.videoid)
        .then(function (rows) {

            //  console.log("1>>",JSON.stringify(rows))
            //  console.log("2>>",JSON.stringify(rows.rows))
            if (rows) {
                // gallery = req.body.image;
                // galleryimages = gallery.split(' ');
                // console.log("images===", galleryimages)
                // for (var i = 0; i < gallery.length - 1; i++) {
                //     fs.unlink(myGAlleryPath + gallery[i], (err) => {
                //         if (err) throw err;

                //     },

                //         console.log("deleted the image" + galleryimages[i])

                //     );

                // }
                console.log("delete video resume details Sucessful")
                res.send(rows)
            }
            else {
                console.log("delete video resume details failed" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'delete video resume details Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error("deletegallerydetails failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'deletegallerydetails Method Error' + err
            });
        });
});


router.get('/getgallery', function (req, res) {


    console.log("Entering into getgallery>>>> method", req.query.loginuserid);
    profiledb.getgallery(req.query.loginuserid)
        .then(function (rows) {
            //  this.achievements=rows.rows;
            // console.log("achievements===",achievements)
            //   console.log("1>>",JSON.stringify(rows))
            //  console.log("2>>",JSON.stringify(rows.rows))
            if (rows) {
                console.log("getgallery Sucessful")
                res.send(rows.rows)

            }
            else {
                console.log("getgallery falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'get present emp Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error("getgallery failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'getgallery Method Error' + err
            });
        });
});


var gallerystorage = multer.diskStorage({ //multers disk storage settings
    destination: function (req, file, cb) {
        cb(null, myGAlleryPath);
    },
    filename: function (req, file, cb) {
        var datetimestamp = Date.now();
        cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1]);
    }
});


//loginuserid: localStorage.getItem('loginuserid')

var galleryupload = multer({ //multer settings
    storage: gallerystorage
}).single('file');
var galleryimages = "";
/** API path that will upload the files */
router.post('/galleryuploads', function (req, res) {

    console.log("/uploads req====", galleryimages)
    galleryupload(req, res, function (err) {
        console.log("file", req.file);
        if (err) {
            res.json({ error_code: 1, err_desc: err });

            return;
        }
        res.json({ error_code: 0, err_desc: null });
        //console.log("path ---",req.file.path)
        //var image=JSON.stringify(req.file.filename);
        //image=req.file.filename
        //image.push(req.file.filename);
        galleryimages = galleryimages + req.file.filename + " "
        //var path1='uploads/'+image;

        console.log("File name ", image)
    });
});

var storage = multer.diskStorage({ //multers disk storage settings
    destination: function (req, file, cb) {
        cb(null, AchievementImages);
    },
    filename: function (req, file, cb) {
        var datetimestamp = Date.now();
        cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1]);
    }
});


//loginuserid: localStorage.getItem('loginuserid')

var imageupload = multer({ //multer settings
    storage: storage
}).single('file');
var image = "";
/** API path that will upload the files */
router.post('/uploads', function (req, res) {


    imageupload(req, res, function (err) {
        console.log("file", req.file);
        if (err) {
            res.json({ error_code: 1, err_desc: err });
            return;
        }
        res.json({ error_code: 0, err_desc: null });
        //console.log("path ---",req.file.path)
        //var image=JSON.stringify(req.file.filename);
        //image=req.file.filename
        //image.push(req.file.filename);
        image = image + req.file.filename + " "
        //var path1='uploads/'+image;

        console.log("File name ", image)
    });
});
router.post('/addachievementdetails', function (req, res) {
    console.log('save image')
    var data = []
    var achievimage = []
    data = req.body.filetype
    achievimage = req.body.image

    var pt = [];
    if(data){
    for (let i = 0; i < data.length; i++) {

        console.log('save image', data[i])
        // console.log('save image', achievimage[i])

        var dt = new Date();

        ptr = req.body.loginusername + '-' + dt.getFullYear() + "" + dt.getMonth() + "" + dt.getMilliseconds() + "" + i + '.' + data[i];
        console.log(ptr, "ptr")
        pts = './public/achievements/' + ptr;
        fs.writeFile(pts, achievimage[i], 'base64', (err) => {
            if (err)
                console.log(err)
            else {

                console.log('Image Saved Success...');
            }

        })
        pt.push(ptr)
        console.log(pt,"pt")
    }
}

    profiledb.addachievementdetails(req.body.loginuserid, req.body.name, req.body.description, pt)
        .then(function (rows) {
            if (rows) {
                console.log("add achievement details Sucessful")
                res.status(200).send({
                    success: true,
                    message: 'Sucessfull'
                });

            }
            else {
                console.log("add achievement  details falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'add achievement details Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error(" addachievementdetails failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'addachievementdetails Method Error' + err
            });
        });
});
// router.post('/addachievementdetails', function (req, res) {
//     var aimages = [];
//     var arenamedImages = '';

//     //    var datetimestamp = Date.now();
//     // var galleryImage;
//     console.log("req body", JSON.stringify(req.body.loginuserid, image, req.body.loginusername))
//     // galleryImage=galleryimages;
//     console.log("Achievement Image===", image)
//     aimages = image.split(' ');
//     var count = 0;
//     for (var i = 0; i < aimages.length - 1; i++) {
//         var nfile = req.body.loginusername + '-' + Date.now() + '-' + count++ + '.png'
//         fs.rename(AchievementImages + aimages[i], AchievementImages + nfile, function (err) {
//             if (err) console.log('ERROR: ' + err);

//         },
//         );
//         arenamedImages = arenamedImages + nfile + ' ';
//         console.log("Renamed images(inside for)==", arenamedImages)


//     }
//     console.log("Renamed images==", arenamedImages)
//     console.log("req body", JSON.stringify(req.body.loginuserid, req.body.name, req.body.description, arenamedImages))
//     profiledb.addachievementdetails(req.body.loginuserid, req.body.name, req.body.description, arenamedImages)
//         .then(function (rows) {
//             if (rows) {
//                 console.log("add achievement details Sucessful")
//                 res.status(200).send({
//                     success: true,
//                     message: 'Sucessfull'
//                 });
//                 image = ''
//             }
//             else {
//                 console.log("add achievement  details falied" + err.body)
//                 res.status(500).send({
//                     success: false,
//                     message: 'add achievement details Method Error' + err
//                 });
//             }
//         }).fail(function (err) {
//             uierrorlogger.error(" addachievementdetails failed..." + err.body);
//             res.status(500).send({
//                 success: false,
//                 message: 'addachievementdetails Method Error' + err
//             });
//         });
// });
router.post('/editachievementdetails', function (req, res) {
    console.log("Entering into editachievementdetails>>>> method", req.body.loginuserid, req.body.id, req.body.name, req.body.description);
    profiledb.editachievementdetails(req.body.loginuserid, req.body.id, req.body.name, req.body.description)
        .then(function (rows) {
            if (rows) {
                console.log("editachievementdetails Sucessful")
                res.status(200).send({
                    success: true,
                    message: 'Sucessfull'
                });
            }
            else {
                console.log("edited achievement details falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'edit achievement details Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error(" add skills details failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'editskillsdetails Method Error' + err
            });
        });
});

router.get('/getachievementdetails', function (req, res) {

    console.log("Entering into getachievementdetails>>>> method", req.query.loginuserid);
    profiledb.getachievementdetails(req.query.loginuserid)
        .then(function (rows) {
            //   console.log("1>>",JSON.stringify(rows))
            //  console.log("2>>",JSON.stringify(rows.rows))
            if (rows) {
                console.log("getachievementdetails Sucessful")
                res.send(rows.rows)
            }
            else {
                console.log("getachievementdetails falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'get present emp Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error("getachievementdetails failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'getachievementdetails Method Error' + err
            });
        });
});

achievementimages = [];
var achievements
router.post('/deleteachievementdetails', function (req, res) {
    console.log("Entering into deleteachievementdetails>>>> method", req.body.loginuserid, req.body.id);
    profiledb.deleteachievementdetails(req.body.loginuserid, req.body.id)
        .then(function (rows) {
            //  console.log("1>>",JSON.stringify(rows))
            //  console.log("2>>",JSON.stringify(rows.rows))
            if (rows) {
                achievements = req.body.image;
                // achievementimages = achievements.split(' ');
                // console.log("images===", achievementimages)
            //     if(req.body.image.length>0){
            //     for (var i = 0; i < achievements.length - 1; i++) {
            //         fs.unlink(AchievementImages + achievements[i], (err) => {
            //             if (err) throw err;

            //         },

            //             console.log("deleted the image" + achievementimages[i])

            //         );

            //     }
            // }
                console.log("deleteachievementdetails Sucessful")
                res.send(rows)
            }
            else {
                console.log("deleteachievementdetails failed" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'deleteachievementdetails Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error("deleteachievementdetails failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'deleteachievementdetails Method Error' + err
            });
        });
});

//getlogincount
router.get('/getlogincount', function (req, res) {
    console.log("Entering into getlogincount>>>> method", req.query.loginuserid);
    profiledb.getlogincount(req.query.loginuserid)
        .then(function (rows) {
            // console.log("1>>>>>",JSON.stringify(rows))
            // console.log("2>>>>>",JSON.stringify(rows.rows))
            if (rows) {
                console.log("getlogincount Sucessful")
                res.send(rows.rows)
            }
            else {
                console.log("getlogincount falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: ' getlogincount Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error("getlogincount failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'getlogincount Method Error' + err
            });
        });
});


//edit

router.post('/edit', function (req, res) {


    profiledb.editprofile(req.body.loginuserid, req.body.resume_headline, req.body.preferred_location,
        req.body.permanent_address, req.body.hometown, req.body.pin_code, req.body.marital_status, req.body.prefered_jobtype, req.body.pan_no, req.body.passportno, req.body.aadhar_no, req.body.currentsalary, req.body.expectedsalary_min, req.body.expectedsalary_max, req.body.employment_type, req.body.desired_shift, req.body.alternate_contact_number, req.body.blood_group, req.body.hobbies, req.body.physical_status, req.body.total_exp_in_yrs, req.body.total_exp_in_months, req.body.date_of_birth, req.body.locality,req.body.functional_area,req.body.fresher_or_exp)

        .then(function (rows) {
            if (rows) {
                console.log("edit profile details Sucessful")
                res.status(200).send({
                    success: true,
                    message: 'Sucessfull'
                });
            }
            else {
                console.log("edit profile details falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'edit profile details Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error(" edit profile details failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'edit profile details Method Error' + err
            });
        });
});


router.post('/changepassword', function (req, res) {
    console.log(req.body.loginuserid, md5(req.body.oldpassword), md5(req.body.password));
    profiledb.changePassword(req.body.loginuserid, md5(req.body.oldpassword), md5(req.body.password))

        .then(function (rows) {
            if (rows) {
                console.log("changepassword Sucessful")
                res.status(200).send({
                    // success: true,
                    message: JSON.parse(JSON.stringify(rows.rows))[0].message,
                });
            }
            else {
                console.log("changepassword falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'changepassword Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error(" changepassword failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'changepassword Method Error' + err
            });
        });
});

////////////////////////////////////Change Password Email/////////////////////////////////////////

// router.post('/changepassword', function (req, res) {
//     var usermailid = req.body.loginuserid;
//     
//     console.log("Entering into  changepassword api" + req.body.loginuserid,md5(req.body.oldpassword),md5(req.body.password));
//     

//     profiledb.changePassword(req.body.loginuserid,md5(req.body.oldpassword),md5(req.body.password))
//         .then(function (rows) {
//             if (rows) {
//                 res.send(rows.rows)
//                 pchangepasswordemail(usermailid);
//                 //console.log("edit profile details Sucessful")
//                 res.status(200).send({
//                     success: true,
//                     message: 'Sucessfull'
//                 });

//             }
//             else {
//                 console.log("Message saving  falied" + err.body)
//                 res.status(500).send({
//                     success: false,
//                     message: 'ChangePassword message  Method Error' + err
//                 });
//             }
//         }).fail(function (err) {
//             uierrorlogger.error(" ChangePassword routes failed..." + err.body);
//             res.status(500).send({
//                 success: false,
//                 message: 'ChangePassword routes Method Error' + err
//             });
//         });


// });

// function pchangepasswordemail(usermailid) {


//     try {
//         console.log("Profile Api image send mail ....." + usermailid);
//         profiledb.getIdforEmail(usermailid).then(function (rows) {
//             if (rows) {
//                 var emailid = JSON.parse(JSON.stringify(rows.rows))[0].email;
//                 console.log("...............=============================>>>>>>>>>>>>>" + usermailid)
//                 var varlenth = JSON.parse(JSON.stringify(rows.rows)).length;
//                 if (parseInt(varlenth) != 0) {
//                     console.log('hieee');
//                     var body =
//                         '<table align="center" style="width: 600px; height: 100px;font-size: 13px;font-family: arial;margin: 30px;"><tr><td  style="border-bottom: #ccc solid 1px;"><img width="230" src="http://unicsol.com/wp-content/uploads/2018/05/Koneqto-e1525862695762.png"></td></tr><tr ><td><p style="font-size: 18px;">Hi <b>XXXXXXXX</b>,</p><p style="font-size: 14px" >The password for your <b>Koneqto</b> account <b> xxxxxxxxxxx(Email Id/username/UserId)</b> was recently changed .</p> <br> <p style="font-size: 14px" ><b>Don&#39;t recognize this activity?</b></p><p style="font-size: 14px" >click <a href="/">here</a> for more information on how to recover your account</p><br>  <p style="font-size: 14px" >The Koneqto Accounts Team</p></td></tr><tr><td style="background-color: #ccc; padding: 10px;">Copy Rights By<a style="color: #3B5998;"href="#">KONEQTO </a> </td></tr></table>';


//                     let mailOptions = {
//                         from: nodemailer.email,
//                         to: emailid,
//                         subject: "Intrested Event",
//                         html: body,
//                     };
//                     nodemailer.sendMail(mailOptions, function (err, info) {
//                         if (err) {
//                             uierrorlogger.error("Profile Api sendmail..." + err.body);

//                         }
//                         else {
//                             console.log('Message sent: %s', info.messageId);
//                             console.log(info);
//                             console.log("Success...." + info);
//                             // res.status(200).send({
//                             //     success: true,
//                             //     message: "Email sent sucessfully"
//                             // });
//                         }
//                     });
//                 }
//                 else {
//                     console.log("entring into Email does't found case");

//                 }
//             }
//         }).fail(function (err) {
//             uierrorlogger.error("Profile Api sendmail...fail" + err);

//         });
//     } catch (error) {
//         uierrorlogger.error("Error catch expection Profile Api sendmail..." + error);

//     }


// }
////////////////////////////////////////////////////////////////////////////////////////////////

router.post('/editpresentemp', function (req, res) {
    console.log("Entering into editpresentemp>>>> method", req.body.loginuserid, req.body.organization_name, req.body.designation, req.body.from_date, req.body.to_date, req.body.description, req.body.notice_period, req.body.other_emp, req.body.employment, req.body.employment_type, req.body.id);
    profiledb.editpresentemp(req.body.loginuserid, req.body.organization_name, req.body.designation, req.body.from_date, req.body.to_date, req.body.description, req.body.notice_period, req.body.other_emp, req.body.employment, req.body.employment_type, req.body.id)
        .then(function (rows) {
            if (rows) {
                console.log("edit present emp Sucessful")
                res.status(200).send({
                    success: true,
                    message: 'Sucessfull'
                });
            }
            else {
                console.log("edited present emp details falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'edit present emp Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error(" edit presentemp details failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'editskillsdetails Method Error' + err
            });
        });
});

router.post('/editpreviousemp', function (req, res) {
    console.log("Entering into editpreviousemp>>>> method", req.body.loginuserid, req.body.organization_name, req.body.designation, req.body.from_date, req.body.to_date, req.body.description, req.body.employment, req.body.employment_type, req.body.id);
    profiledb.editpreviousemp(req.body.loginuserid, req.body.organization_name, req.body.designation, req.body.from_date, req.body.to_date, req.body.description, req.body.employment, req.body.employment_type, req.body.id)
        .then(function (rows) {
            if (rows) {
                console.log("edit previous emp Sucessful")
                res.status(200).send({
                    success: true,
                    message: 'Sucessfull'
                });
            }
            else {
                console.log("edited previous emp details falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'edit previous emp Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error(" edit previousemp details failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'editpreviousempdetails Method Error' + err
            });
        });
});

router.post('/editotheremp', function (req, res) {
    console.log("Entering into editotheremp>>>> method", req.body.loginuserid, req.body.other_emp, req.body.employment, req.body.employment_type, req.body.id);
    profiledb.editotheremp(req.body.loginuserid, req.body.other_emp, req.body.employment, req.body.employment_type, req.body.id)
        .then(function (rows) {
            if (rows) {
                console.log("edit other emp Sucessful")
                res.status(200).send({
                    success: true,
                    message: 'Sucessfull'
                });
            }
            else {
                console.log("edited other emp details falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'edit other emp Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error(" edit otheremp details failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'editotherdetails Method Error' + err
            });
        });
});

router.post('/deletepresentempbyid', function (req, res) {
    console.log("Entering into deletepresentempbyid>>>> method", req.body.loginuserid, req.body.id, req.body.organization_name, req.body.designation, req.body.notice_period);
    profiledb.deletepresentempbyid(req.body.loginuserid, req.body.id, req.body.organization_name, req.body.designation, req.body.notice_period)
        .then(function (rows) {
            // console.log("1>>",JSON.stringify(rows))
            //console.log("2>>",JSON.stringify(rows.rows))
            if (rows) {
                console.log("deletepresentempbyid Sucessful")
                res.send(rows)
            }
            else {
                console.log("deletepresentempbyid failed" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'delete presentemp Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error("delete presentemp failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'delete present emp Method Error' + err
            });
        });
});


router.post('/deletepreviousempbyid', function (req, res) {
    console.log("Entering into deletepreviousempbyid>>>> method", req.body.loginuserid, req.body.id);
    profiledb.deletepreviousempbyid(req.body.loginuserid, req.body.id)
        .then(function (rows) {
            //    console.log("1>>",JSON.stringify(rows))
            //   console.log("2>>",JSON.stringify(rows.rows))
            if (rows) {
                console.log("deletepreviousempbyid Sucessful")
                res.send(rows)
            }
            else {
                console.log("deletepreviousempbyid failed" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'delete previousemp Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error("delete previousemp failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'delete previous emp Method Error' + err
            });
        });
});


router.post('/deleteotherempbyid', function (req, res) {
    console.log("Entering into deleteotherempbyid>>>> method", req.body.loginuserid, req.body.id);
    profiledb.deleteotherempbyid(req.body.loginuserid, req.body.id)
        .then(function (rows) {
            //     console.log("1>>",JSON.stringify(rows))
            //    console.log("2>>",JSON.stringify(rows.rows))
            if (rows) {
                console.log("deleteotherempbyid Sucessful")
                res.send(rows)
            }
            else {
                console.log("deleteotherempbyid failed" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'delete otheremp Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error("delete otheremp failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'delete other emp Method Error' + err
            });
        });
});



router.post('/addpresentempdetails', function (req, res) {
    console.log("Entering into addpresentempdetails>>>> method", req.body.loginuserid, req.body.organization_name, req.body.designation, req.body.from_date, req.body.to_date, req.body.description, req.body.notice_period, req.body.other_emp, req.body.employment, req.body.employment_type);
    profiledb.addpresentempdetails(req.body.loginuserid, req.body.organization_name, req.body.designation, req.body.from_date, req.body.to_date, req.body.description, req.body.notice_period, req.body.other_emp, req.body.employment, req.body.employment_type)
        .then(function (rows) {
            if (rows) {
                console.log("addpresentempdetails Sucessful")
                res.status(200).send({
                    success: true,
                    message: 'Sucessfull'
                });
            }
            else {
                console.log("add emp details falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'add empdetails Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error(" add emp details failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'addempdetails Method Error' + err
            });
        });
});

router.post('/addpreviousempdetails', function (req, res) {
    console.log("Entering into addpreviousempdetails>>>> method", req.body.loginuserid, req.body.organization_name, req.body.designation, req.body.from_date, req.body.to_date, req.body.description, req.body.notice_period, req.body.other_emp, req.body.employment, req.body.employment_type);
    profiledb.addpreviousempdetails(req.body.loginuserid, req.body.organization_name, req.body.designation, req.body.from_date, req.body.to_date, req.body.description, req.body.notice_period, req.body.other_emp, req.body.employment, req.body.employment_type)
        .then(function (rows) {
            if (rows) {
                console.log("addpresentempdetails Sucessful")
                res.status(200).send({
                    success: true,
                    message: 'Sucessfull'
                });
            }
            else {
                console.log("add emp details falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'add empdetails Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error(" add emp details failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'addempdetails Method Error' + err
            });
        });
});


router.get('/getpresentemp', function (req, res) {
    console.log("Entering into getpresentemp>>>> method", req.query.employment, req.query.loginuserid);
    profiledb.getpresentemp(req.query.employment, req.query.loginuserid)
        .then(function (rows) {
            //    console.log("1>>",JSON.stringify(rows))
            //    console.log("2>>",JSON.stringify(rows.rows))
            if (rows) {
                console.log("getpresentemp Sucessful")
                res.send(rows.rows)
            }
            else {
                console.log("getpresentemp falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'get present emp Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error("getpresentemp failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'getpresentemp Method Error' + err
            });
        });
});


router.get('/getpreviousemp', function (req, res) {
    console.log("Entering into getpreviousemp>>>> method", req.query.employment, req.query.loginuserid);
    profiledb.getpreviousemp(req.query.employment, req.query.loginuserid)
        .then(function (rows) {
            //  console.log("1>>",JSON.stringify(rows))
            //  console.log("2>>",JSON.stringify(rows.rows))
            if (rows) {
                console.log("getpreviousemp Sucessful")
                res.send(rows.rows)
            }
            else {
                console.log("getpreviousemp falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'getpreviousemp Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error("getpreviousemp failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'getpreviousemp Method Error' + err
            });
        });
});


router.get('/getotheremp', function (req, res) {
    console.log("Entering into getotheremp>>>> method", req.query.employment, req.query.loginuserid);
    profiledb.getotheremp(req.query.employment, req.query.loginuserid)
        .then(function (rows) {
            //   console.log("1>>",JSON.stringify(rows))
            //  console.log("2>>",JSON.stringify(rows.rows))
            if (rows) {
                console.log("getotheremp Sucessful")
                res.send(rows.rows)
            }
            else {
                console.log("getotheremp falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'getotheremp Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error("getotheremp failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'getotheremp Method Error' + err
            });
        });
});


router.post('/addskilldetails', function (req, res) {
    console.log("Entering into addskilldetails>>>> method", req.body.loginuserid, req.body.skill_name, req.body.version, req.body.last_used, req.body.exp_in_years, req.body.exp_in_months);
    profiledb.addskilldetails(req.body.loginuserid, req.body.skill_name, req.body.version, req.body.last_used, req.body.exp_in_years, req.body.exp_in_months)
        .then(function (rows) {
            if (rows) {
                console.log("addskilldetails Sucessful")
                res.status(200).send({
                    success: true,
                    message: 'Sucessfull'
                });
            }
            else {
                console.log("add skills details falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'add languagedetails Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error(" add skills details failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'addskillsdetails Method Error' + err
            });
        });
});
router.get('/getlanguagenames', function (req, res) {
    console.log("Entering into getlanguagenames>>>> method");
    profiledb.getlanguagenames()
        .then(function (rows) {
            //console.log("1>>>>>",JSON.stringify(rows))

            if (rows) {
                console.log("getlanguagenames Sucessful")
                res.send(rows.rows)
            }
            else {
                console.log("getlanguagenames falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'get languagenames Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error("getlanguagenames failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'getlanguagenames Method Error' + err
            });
        });
});

router.get('/loadexpyears', function (req, res) {
    console.log("Entering into expyears methode");

    profiledb.loadexpyears()
        .then(function (rows) {
            if (rows) {

                res.end(JSON.stringify(rows.rows))


            }
            else {

                console.log("loadexpyears" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'loadexpyears api Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'loadexpyears api Method Error' + err
            });
        });

});

router.get('/loadexpmonths', function (req, res) {
    console.log("Entering into expmonths methode");

    profiledb.loadexpmonths()
        .then(function (rows) {
            if (rows) {

                res.end(JSON.stringify(rows.rows))


            }
            else {

                console.log("loadexpmonths" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'loadexpmonths api Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'loadexpmonths api Method Error' + err
            });
        });

});
router.get('/loaddesignations', function (req, res) {
    console.log("Entering into loaddeignations methode");

    profiledb.loaddesignations()
        .then(function (rows) {
            if (rows) {

                res.end(JSON.stringify(rows.rows))


            }
            else {

                console.log("loaddesignations" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'loaddesignations api Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'loaddesignations api Method Error' + err
            });
        });

});

router.get('/loadbloodgroups', function (req, res) {
    console.log("Entering into loadbloodgroups methode");

    profiledb.loadbloodgroups()
        .then(function (rows) {
            if (rows) {

                res.end(JSON.stringify(rows.rows))


            }
            else {

                console.log("loadbloodgroups" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'loadbloodgroups api Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'loadbloodgroups api Method Error' + err
            });
        });

});

router.get('/loadphysicalstatus', function (req, res) {
    console.log("Entering into loadphysicalstatus methode");

    profiledb.loadphysicalstatus()
        .then(function (rows) {
            if (rows) {

                res.end(JSON.stringify(rows.rows))


            }
            else {

                console.log("loadphysicalstatus" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'loadphysicalstatus api Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'loadphysicalstatus api Method Error' + err
            });
        });

});

router.get('/loadstatus', function (req, res) {
    console.log("Entering into loadstatus methode");

    profiledb.loadstatus()
        .then(function (rows) {
            if (rows) {

                res.end(JSON.stringify(rows.rows))


            }
            else {

                console.log("loadstatus" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'loadstatus api Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'loadstatus api Method Error' + err
            });
        });

});
router.get('/loadcities', function (req, res) {
    console.log("Entering into loadcities methode");
    profiledb.loadcities(req.query.name)
        .then(function (rows) {
            if (rows) {
                res.end(JSON.stringify(rows.rows))
            }
            else {
                console.log("loadcities" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'loadcities api Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'loadcities api Method Error' + err
            });
        });

});

router.get('/getskillnames', function (req, res) {
    profiledb.getskillnames(req.query.name)
        .then(function (rows) {
            if (rows) {
                console.log("getskillnames Sucessful")
                res.send(rows.rows)
            }
            else {
                console.log("getskillnames falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'get skillnames Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error("getskillnames failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'getskillnames Method Error' + err
            });
        });
});

// all skills data for mobile api
router.get('/getallskillnames', function (req, res) {
    profiledb.getallskillnames()
        .then(function (rows) {
            if (rows) {
                console.log("getallskillnames Sucessful")
                res.send(rows.rows)
            }
            else {
                console.log("getallskillnames falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'get skillnames Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error("getallskillnames failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'getallskillnames Method Error' + err
            });
        });
});



router.get('/getskillnames1', function (req, res) {
    //console.log("Entering into getskillnames1>>>> method", req.query.skillname);
    profiledb.getskillnames1(req.query.fn_area_id)
        .then(function (rows) {
            // console.log("1>>>>>",JSON.stringify(rows))
            // console.log("2>>>>>",JSON.stringify(rows.rows))
            if (rows) {
                console.log("getskillnames1 Sucessful")
                res.send(rows.rows)
            }
            else {
                console.log("getskillnames1 falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'get skillnames Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error("getskillnames1 failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'getskillnames1 Method Error' + err
            });
        });
});

router.get('/getskillnames2', function (req, res) {
    //console.log("Entering into getskillnames2>>>> method", req.query.skillname);
    profiledb.getskillnames2(req.query.fn_area_id, req.query.name)
        .then(function (rows) {
            // console.log("1>>>>>",JSON.stringify(rows))
            // console.log("2>>>>>",JSON.stringify(rows.rows))
            if (rows) {
                console.log("getskillnames2 Sucessful")
                res.send(rows.rows)
            }
            else {
                console.log("getskillnames2 falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'get skillnames Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error("getskillnames2 failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'getskillnames2 Method Error' + err
            });
        });
});

router.post('/deleteskillsbyid', function (req, res) {
    console.log("Entering into deleteskillsbyid>>>> method", req.body.loginuserid, req.body.skill_id, req.body.skill_name);
    profiledb.deleteskillsbyid(req.body.loginuserid, req.body.skill_id, req.body.skill_name)
        .then(function (rows) {
            //  console.log("1>>",JSON.stringify(rows))
            //   console.log("2>>",JSON.stringify(rows.rows))
            if (rows) {
                console.log("deleteskillsbyid Sucessful")
                res.send(rows)
            }
            else {
                console.log("deleteskillsbyid falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'delete skills Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error("deleteskills failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'deleteskills Method Error' + err
            });
        });
});

router.post('/deleteprofileimage', function (req, res) {
    console.log("Entering into deleteprofileimage>>>> method", req.body.loginuserid,req.body.profileimage);
    profiledb.deleteprofileimage(req.body.loginuserid,req.body.profileimage)
        .then(function (rows) {
            //  console.log("1>>",JSON.stringify(rows))
            //   console.log("2>>",JSON.stringify(rows.rows))
            if (rows) {
                console.log("delete profile image Sucessful")
                res.send(rows)
            }
            else {
                console.log("delete profile image falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'delete profile image Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error("delete profile image failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'delete profile image Method Error' + err
            });
        });
});
router.post('/editskillsdetails', function (req, res) {
    console.log("Entering into editskilldetails>>>> method", req.body.loginuserid, req.body.skill_name, req.body.version, req.body.last_used, req.body.exp_in_years, req.body.exp_in_months, req.body.skill_id);
    profiledb.editskillsdetails(req.body.loginuserid, req.body.skill_name, req.body.version, req.body.last_used, req.body.exp_in_years, req.body.exp_in_months, req.body.skill_id)
        .then(function (rows) {
            if (rows) {
                console.log("editskilldetails Sucessful")
                res.status(200).send({
                    success: true,
                    message: 'Sucessfull'
                });
            }
            else {
                console.log("edited skills details falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'edit languagedetails Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error(" edit skills details failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'editskillsdetails Method Error' + err
            });
        });
});

router.get('/loadstates', function (req, res) {
    console.log("Entering into loadstates methode");

    profiledb.loadstates()
        .then(function (rows) {
            if (rows) {

                res.end(JSON.stringify(rows.rows))


            }
            else {

                console.log("loadstatesdropdown" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'loadstatesdropdown api Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'loadstatesdropdown api Method Error' + err
            });
        });

});

router.get('/loadstatesbyname', function (req, res) {
    console.log("Entering into loadstatesbynames methode",req.query.statename);

    profiledb.loadstatesbynames(req.query.statename)
        .then(function (rows) {
            if (rows) {

                res.end(JSON.stringify(rows.rows))


            }
            else {

                console.log("loadstatesbynames" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'loadstatesbynames api Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'loadstatesbynames api Method Error' + err
            });
        });

});

// load areas
router.get('/loadareas', function (req, res) {
    console.log("Entering into loadareas methode");

    profiledb.loadareas()
        .then(function (rows) {
            if (rows) {

                res.end(JSON.stringify(rows.rows))


            }
            else {

                console.log("loadareasdropdown" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'loadareasdropdown api Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'loadareasdropdown api Method Error' + err
            });
        });

});

router.get('/loadalllocalities', function (req, res) {
    console.log("Entering into load all localities methode",req.query.name);

    profiledb.loadalllocalities(req.query.name)
        .then(function (rows) {
            if (rows) {

                res.end(JSON.stringify(rows.rows))


            }
            else {

                console.log("loadalllocalities" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'loadalllocalities api Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'loadalllocalities api Method Error' + err
            });
        });

});
router.get('/loadmaritalstatus', function (req, res) {
    console.log("Entering into loadstates methode");

    profiledb.loadmaritalstatus()
        .then(function (rows) {
            if (rows) {

                res.end(JSON.stringify(rows.rows))


            }
            else {

                console.log("loadstatesdropdown" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'loadstatesdropdown api Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'loadstatesdropdown api Method Error' + err
            });
        });

});

router.get('/loadgender', function (req, res) {
    console.log("Entering into loadgender methode");

    profiledb.loadgender()
        .then(function (rows) {
            if (rows) {

                res.end(JSON.stringify(rows.rows))


            }
            else {

                console.log("loadgenderdropdown" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'loadgenderdropdown api Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'loadgenderdropdown api Method Error' + err
            });
        });

});

router.get('/loadjobtype', function (req, res) {
    console.log("Entering into loadjobtype methode");

    profiledb.loadjobtype()
        .then(function (rows) {
            if (rows) {

                res.end(JSON.stringify(rows.rows))


            }
            else {

                console.log("loadjobtypedropdown" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'loadjobtypedropdown api Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'loadjobtypedropdown api Method Error' + err
            });
        });

});


router.get('/loadsalinlakhs', function (req, res) {
    console.log("Entering into loadsalinlakhs methode");

    profiledb.loadsalinlakhs()
        .then(function (rows) {
            if (rows) {

                res.end(JSON.stringify(rows.rows))


            }
            else {

                console.log("loadsalinlakhs" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'loadsalinlakhs api Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'loadsalinlakhs api Method Error' + err
            });
        });

});

router.get('/loadsalinthousands', function (req, res) {
    console.log("Entering into loadsalinthousands methode");

    profiledb.loadsalinthousands()
        .then(function (rows) {
            if (rows) {

                res.end(JSON.stringify(rows.rows))


            }
            else {

                console.log("loadsalinthousands" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'loadsalinthousands api Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'loadsalinthousands api Method Error' + err
            });
        });

});


router.get('/loademptype', function (req, res) {
    console.log("Entering into loademptype methode");

    profiledb.loademptype()
        .then(function (rows) {
            if (rows) {

                res.end(JSON.stringify(rows.rows))


            }
            else {

                console.log("loademptype" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'loademptype api Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'loademptype api Method Error' + err
            });
        });

});

router.get('/loaddesiredshift', function (req, res) {
    console.log("Entering into loaddesiredshift methode");

    profiledb.loaddesiredshift()
        .then(function (rows) {
            if (rows) {

                res.end(JSON.stringify(rows.rows))


            }
            else {

                console.log("loaddesiredshift" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'loaddesiredshift api Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'loaddesiredshift api Method Error' + err
            });
        });

});


router.get('/getlanguagesdetailsbyid', function (req, res) {
    console.log("Entering into getlanguagesdetailsbyid>>>> method", req.query.loginuserid);
    profiledb.getlanguagesdetailsbyid(req.query.loginuserid)
        .then(function (rows) {
            //       console.log("1>>>>>",JSON.stringify(rows))
            //     console.log("2>>>>>",JSON.stringify(rows.rows))
            if (rows) {
                console.log("getlanguagesdetailsbyid Sucessful")
                // res.send(rows)
                res.end(JSON.stringify(rows.rows))
            }
            else {
                console.log("getlanguagesdetailsbyid falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'get languagedetails Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error("getlanguagesdetailsbyid failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'getlanguagesdetailsbyid Method Error' + err
            });
        });
});
router.get('/getallskills', function (req, res) {
    console.log("Entering into getskilldetailsbyid>>>> method", req.query.loginuserid);
    profiledb.getallskills(req.query.loginuserid)
        .then(function (rows) {
            //  console.log("1>>",JSON.stringify(rows))
            // console.log("2>>",JSON.stringify(rows.rows))
            if (rows) {
                console.log("getallskills Sucessful")
                res.end(JSON.stringify(rows.rows))
                // res.send(rows)
                // res.status(200).send({
                //     success: true,
                //     message: 'Sucessfull'
                // });
            }
            else {
                console.log("getallskills falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'get allskills Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error("getskilldetailsbyid failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'getskilldetailsbyid Method Error' + err
            });
        });
});



router.post('/deletelanguagesdetailsbyid', function (req, res) {
    console.log("Entering into deletelanguagesdetailsbyid>>>> method", req.body.loginuserid, req.body.language_id);
    profiledb.deletelanguagesdetailsbyid(req.body.loginuserid, req.body.language_id)
        .then(function (rows) {
            // console.log("1>>",JSON.stringify(rows))
            //console.log("2>>",JSON.stringify(rows.rows))
            if (rows) {
                console.log("deletelanguagesdetailsbyid Sucessful")
                // res.send(rows)
                res.status(200).send({
                    success: true,
                    message: 'Sucessfull'
                });
            }
            else {
                console.log("deletelanguagesdetailsbyid falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'delete languagedetails Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error("deletelanguagesdetailsbyid failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'deletelanguagesdetailsbyid Method Error' + err
            });
        });
});

router.post('/editlanguagesdetails', function (req, res) {
    console.log("Entering into editskilldetails>>>> method", req.body.loginuserid, req.body.language_id, req.body.language_name, req.body.is_speak, req.body.is_read, req.body.is_write);
    profiledb.editlanguagesdetails(req.body.loginuserid, req.body.language_id, req.body.language_name, req.body.is_speak, req.body.is_read, req.body.is_write)
        .then(function (rows) {
            if (rows) {
                console.log("editskilldetails Sucessful")
                res.status(200).send({
                    success: true,
                    message: 'Sucessfull'
                });
            }
            else {
                console.log("edited skills details falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'edit languagedetails Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error(" add skills details failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'editskillsdetails Method Error' + err
            });
        });
});

router.post('/AddEducationdetails', function (req, res) {

    console.log("Entering into AddEducationdetailsApi    method", +req.body.user_registration_id, req.body.qualification, req.body.Specialization, req.body.University_or_Institute, req.body.year, req.body.grading_system, req.body.marks, req.body.medium, req.body.natureof_job);
    profiledb.AddEducationdetails(req.body.user_registration_id, req.body.qualification, req.body.Specialization, req.body.University_or_Institute, req.body.year, req.body.grading_system, req.body.marks, req.body.medium, req.body.natureof_job)
        .then(function (rows) {
            if (rows) {

                console.log("AddEducationdetails Api Sucessful")
                res.status(200).send({
                    success: true,
                    message: 'Sucessfull'
                });


            }
            else {
                console.log("AddEducationdetails Api falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'AddEducationdetails APi Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error(" AddEducationdetails Api failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'AddEducationdetails Method Error' + err
            });

        });

});

router.get('/loadcountrydropdown', function (req, res) {
    console.log("Entering into loadcountrydropdown methode");

    profiledb.loadcountrydropdown()
        .then(function (rows) {
            if (rows) {

                console.log("loadcountrydropdown Api Sucessful")
                res.end(JSON.stringify(rows.rows))


            }
            else {

                console.log("loadcountrydropdown" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'loadcountrydropdown api Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'loadcountrydropdown api Method Error' + err
            });
        });

});


router.get('/loadstatesdropdown', function (req, res) {
    console.log("Entering into loadstatesdropdown methode" + req.query.country_id);

    profiledb.loadstatesdropdown(req.query.country_id)
        .then(function (rows) {
            if (rows) {

                res.end(JSON.stringify(rows.rows))


            }
            else {

                console.log("loadstatesdropdown" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'loadstatesdropdown api Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'loadstatesdropdown api Method Error' + err
            });
        });

});





router.get('/loadcitydropdown', function (req, res) {
    console.log("Entering into loadcitydropdown methode" + req.query.state_id);

    profiledb.loadcitydropdown(req.query.state_id)
        .then(function (rows) {
            if (rows) {

                res.end(JSON.stringify(rows.rows))


            }
            else {

                console.log("loadcitydropdown" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'loadcitydropdown api Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'loadcitydropdown api Method Error' + err
            });
        });

});



router.get('/loadnoticeperiod', function (req, res) {
    console.log("Entering into noticeperiod methode");

    profiledb.loadnoticeperiod()
        .then(function (rows) {
            if (rows) {

                res.end(JSON.stringify(rows.rows))


            }
            else {

                console.log("loadnoticeperiod" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'loadnoticeperiod api Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'loadnoticeperiod api Method Error' + err
            });
        });

});
var profileimagestorage = multer.diskStorage({ //multers disk storage settings
    destination: function (req, file, cb) {
        cb(null, ProfileImagesPath);
    },
    filename: function (req, file, cb) {
        var datetimestamp = Date.now();
        cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1]);
    }
});
var profileupload = multer({ //multer settings
    storage: profileimagestorage
}).single('file');

var profileimage;
/** API path that will upload the files */
// router.post('/profileimageuploads', function (req, res) {
//     

//     profileupload(req, res, function (err) {
//         console.log("file profile image", req.file);
//         if (err) {
//             res.json({ error_code: 1, err_desc: err });
//             return;
//         }
//         res.json({ error_code: 0, err_desc: null ,image:req.file.filename});
//         //console.log("path ---",req.file.path)
//         //var path=JSON.stringify(req.file.filename);
//         profileimage = req.file.filename
//         //path.push(req.file.filename);

//         //var path1='uploads/'+path;

//         console.log("File name ", profileimage)

//         profiledb.saveProfileImage(this.loginuserid1,profileimage).then(function (rows) {
//             if (rows) {

//                 console.log("Profile api  Sucessful")
//                 res.status(200).send({
//                     success: true,
//                     message: 'Sucessfull'
//                 });

//                 profileimageupdatemail(usermailid)
//             }
//             else {
//                 console.log("Profile Api falied" + err.body)
//                 res.status(500).send({
//                     success: false,
//                     message: 'Profile APi Method Error' + err
//                 });

//             }

//         }).fail(function (err) {
//             uierrorlogger.error("Profile Api failed..." + err.body);
//             res.status(500).send({
//                 success: false,
//                 message: 'Profile APi Method Error' + err
//             });

//         });

//     });
// });

router.post('/profileimage', function (req, res) {
    var usermailid = req.body.loginuserid;
    var data
    image
    data = req.body.profile_image


    var pt = '';
    var dt = new Date();
    ptr = req.body.username + '-' + dt.getFullYear() + "" + dt.getMonth() + "" + dt.getMilliseconds() + "" + data.type;
    pts = './public/profileimage/' + ptr;
    fs.writeFile(pts, data.img_data, 'base64', (err) => {
        if (err)
            console.log(err)
        else {
            console.log('Image Svaed Success...');
        }

    })
    image = ptr
    console.log("Entering into edit profile details >>>> method", req.body.loginuserid, image)
    profiledb.saveProfileImage(req.body.loginuserid, image).then(function (rows) {
        if (rows) {

            console.log("Profile api  Sucessful")
            res.status(200).send({
                success: true,
                message: 'Sucessfull'
            });

            profileimageupdatemail(usermailid)
        }
        else {
            console.log("Profile Api falied" + err.body)
            res.status(500).send({
                success: false,
                message: 'Profile APi Method Error' + err
            });

        }

    }).fail(function (err) {
        uierrorlogger.error("Profile Api failed..." + err.body);
        res.status(500).send({
            success: false,
            message: 'Profile APi Method Error' + err
        });

    });
});


//profile image change email
function profileimageupdatemail(usermailid) {
    try {
        console.log("Profile Api image send mail ....." + usermailid);
        profiledb.getIdforEmail(usermailid).then(function (rows) {
            if (rows) {
                var emailid = JSON.parse(JSON.stringify(rows.rows))[0].email;
                console.log("...............=============================>>>>>>>>>>>>>" + usermailid)
                var varlenth = JSON.parse(JSON.stringify(rows.rows)).length;
                if (parseInt(varlenth) != 0) {
                    console.log('hieee');
                    var body =
                        '<table align="center" style="width: 600px; height: 100px; font-size: 13px; font-family: arial;" table, th, td {   ble, th, td {   padding: 50px;   margin: 130px;><tr> <td  style="border-bottom: #ccc solid 1px;"><a href=' + fullUrl + '><img width="230" src="http://unicsol.com/wp-content/uploads/2018/05/Koneqto-e1525862695762.png"></a></td> </tr>  <tr ><td>    <p style="color:green;font-size: 18px" ><b>Nice shot!</b></p><p style="font-size: 16px" >You have successfully updated the profile picture.</p></td></tr><tr><td style="background-color: #ccc; padding: 10px;">Copy Rights By<a  style="color: #3B5998;"href="#">   KONEQTO </a> </td></tr></table>';

                    let mailOptions = {
                        from: nodemailer.email,
                        to: emailid,
                        subject: "Profile pic",
                        html: body,
                    };
                    nodemailer.sendMail(mailOptions, function (err, info) {
                        if (err) {
                            uierrorlogger.error("Profile Api sendmail..." + err.body);

                        }
                        else {
                            console.log('Message sent: %s', info.messageId);
                            console.log(info);
                            console.log("Success...." + info);
                            // res.status(200).send({
                            //     success: true,
                            //     message: "Email sent sucessfully"
                            // });
                        }
                    });
                }
                else {
                    console.log("entring into Email does't found case");

                }
            }
        }).fail(function (err) {
            uierrorlogger.error("Profile Api sendmail...fail" + err);

        });
    } catch (error) {
        uierrorlogger.error("Error catch expection Profile Api sendmail..." + error);

    }


}
/////////////////////////////////////////////////////////////////////////////////////////////

router.get('/profile_image', function (req, res) {
    console.log("Entering into get profile image api", req.query.loginuserid);
    profiledb.getimage(req.query.loginuserid)
        .then(function (rows) {

            if (rows) {
                console.log("get image Sucessful")
                res.send(rows.rows)
            }
            else {
                console.log("get image falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'get image Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error("get image failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'get image Method Error' + err
            });
        });
});

//getadvice options 
router.get('/getadviceoptionsdata', function (req, res, next) {
    try {
        console.log("getadviceoptionsdata data");
        profiledb.getadviceoptionsdata().then(function (rows) {
            if (rows) {
                res.send(JSON.stringify(rows.rows));
            }
        }).fail(function (err) {
            uierrorlogger.error("api getadviceoptionsdata..." + err.body);
            //next(Errormessage("Create New-Password ", err));
        });
    } catch (error) {
        uierrorlogger.error("Error catch expection api getadviceoptionsdata..." + error);

    }
});



router.get('/getsalarylakh', function (req, res, next) {
    try {
        console.log("getsalaylakh data" + req.query.loginuserid);
        profiledb.getsalaylakh().then(function (rows) {
            if (rows) {
                res.end(JSON.stringify(rows.rows));
            }
        }).fail(function (err) {
            uierrorlogger.error("api getsalaylakh..." + err.body);

        });
    } catch (error) {
        uierrorlogger.error("Error catch expection api SavenewPassword..." + error);
        next(Errormessage("Create New-Pass.loginword", error));
    }
});

router.get('/getsalarythousand', function (req, res, next) {
    try {
        console.log("getsalarythousand data" + req.query.loginuserid);
        profiledb.getsalarythousand().then(function (rows) {
            if (rows) {
                res.end(JSON.stringify(rows.rows));
            }
        }).fail(function (err) {
            uierrorlogger.error("api getsalarythousand..." + err.body);
            //next(Errormessage("Create New-Password ", err));
        });
    } catch (error) {
        uierrorlogger.error("Error catch expection api SavenewPassword..." + error);
        next(Errormessage("Create New-Pass.loginword", error));
    }
});

router.get('/allcourses', function (req, res, next) {
    try {

        profiledb.allcourses().then(function (rows) {
            if (rows) {
                res.end(JSON.stringify(rows.rows));
            }
        }).fail(function (err) {
            uierrorlogger.error("api allcourses..." + err.body);

        });
    } catch (error) {
        uierrorlogger.error("Error catch expection api SavenewPassword..." + error);
        next(Errormessage("Create New-Pass.loginword", error));
    }
});
router.get('/allcourses1', function (req, res, next) {
    try {
        console.log("inside js", req.query.coursename)
        profiledb.allcourses1(req.query.coursename).then(function (rows) {
            if (rows) {
                res.end(JSON.stringify(rows.rows));
            }
        }).fail(function (err) {
            uierrorlogger.error("api allcourses..." + err.body);

        });
    } catch (error) {
        uierrorlogger.error("Error catch expection api SavenewPassword..." + error);
        next(Errormessage("Create New-Pass.loginword", error));
    }
});

router.post('/editprojectdetails', function (req, res) {
    console.log("Entering into edit project details >>>> method", req.body.loginuserid, req.body.id, req.body.name, req.body.team, req.body.description, req.body.duration, req.body.skills);
    profiledb.editprojectdetails(req.body.loginuserid, req.body.id, req.body.name, req.body.team, req.body.description, req.body.duration, req.body.skills)
        .then(function (rows) {
            if (rows) {
                console.log("edit project details  Sucessful")
                res.status(200).send({
                    success: true,
                    message: 'Sucessfull'
                });
            }
            else {
                console.log("edited project details falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'edit languagedetails Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error(" add project details failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'editskillsdetails Method Error' + err
            });
        });
});

router.post('/addprojectdetails', function (req, res) {
    console.log("Entering into profile.js addprojectdetails>>>> method", req.body.name, req.body.team, req.body.description, req.body.duration, req.body.skills, req.body.loginuserid);
    profiledb.addproject(req.body.name, req.body.team, req.body.description, req.body.duration, req.body.skills, req.body.loginuserid)
        .then(function (rows) {
            if (rows) {
                console.log("addprojectdetails Sucessful")
                res.status(200).send({
                    success: true,
                    message: 'Sucessfull'
                });
            }
            else {
                console.log("add emp details falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'add empdetails Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error(" add emp details failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'addprojectdetails Method Error' + err
            });
        });
});

router.post('/deleteprojectbyid', function (req, res) {
    console.log("Entering into deleteprojectbyid>>>> method", req.body.loginuserid, req.body.id);
    profiledb.deleteprojectbyid(req.body.loginuserid, req.body.id)
        .then(function (rows) {
            //            console.log("2>>", JSON.stringify(rows.rows))
            if (rows) {
                console.log("deleteprojectbyid Sucessful")
                res.send(rows)
            }
            else {
                console.log("deleteprojectbyid falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'delete project Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error("deleteproject failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'deleteproject Method Error' + err
            });
        });
});
router.get('/getallprojects', function (req, res) {
    console.log("Entering into getskilldetailsbyid>>>> method", req.query.loginuserid);
    profiledb.getallprojects(req.query.loginuserid)
        .then(function (rows) {
            // console.log("1>>", JSON.stringify(rows))
            //console.log("2>>", JSON.stringify(rows.rows))
            if (rows) {
                console.log("getallprojects Sucessful")
                res.send(rows.rows)
            }
            else {
                console.log("getallprojects falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'get allprojects Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error("getskilldetailsbyid failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'getskilldetailsbyid Method Error' + err
            });
        });
});

//Add certifications
router.post('/Addcertifications', function (req, res) {
    console.log("Entering into Addcertifications>>>> method", req.body.loginuserid, req.body.certification_name, req.body.certification_body, req.body.year, req.body.id);
    profiledb.Addcertifications(req.body.loginuserid, req.body.
        certification_name, req.body.certification_body, req.body.year, req.body.id)
        .then(function (rows) {
            //console.log("1>>",JSON.stringify(rows))
            //console.log("2>>",JSON.stringify(rows.rows))
            if (rows) {
                console.log("Addcertifications Sucessful")
                res.send(rows)
            }
            else {
                console.log("Addcertifications failed" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'Add certifications  Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error("delete certification failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'delete certification Method Error' + err
            });
        });
});



//get certifications
router.get('/getcertifications', function (req, res) {
    console.log("Entering into getcertifications>>>> method", req.query.loginuserid);
    profiledb.getcertifications(req.query.loginuserid)
        .then(function (rows) {
            // console.log("1>>", JSON.stringify(rows))
            //console.log("2>>", JSON.stringify(rows.rows))
            if (rows) {
                console.log("getcertifications Sucessful")
                res.send(rows.rows)
            }
            else {
                console.log("getcertifications falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'getcertifications Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error("getskilldetails failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'getskilldetails Method Error' + err
            });
        });
});




//Delete Certifications


router.post('/deletecertifications', function (req, res) {
    console.log("Entering into deletebyid>>>> method", req.body.loginuserid, req.body.id);
    profiledb.deletecertifications(req.body.loginuserid, req.body.id)
        .then(function (rows) {
            //console.log("1>>", JSON.stringify(rows))
            //console.log("2>>", JSON.stringify(rows.rows))
            if (rows) {
                console.log("deletebyid Sucessful")
                res.send(rows)
            }
            else {
                console.log("deletebyid falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'delete certifications Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error("deletebyid failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'deletebyid Method Error' + err
            });
        });
});


// app.post('/upload', upload.array('photos', 3), function(req, res, next) {
//     res.send('Successfully uploaded ' + req.files.length + ' files!')
//   })


router.post('/getpresignedurl', function (req, res, next) {
    console.log("req.body.filename" + req.body.filename, req.body.data)
    aws.config.update({
        region: 'us-east-1',
        credentials: new aws.Credentials('AKIAJX2AYO6PE5Q7E7RQ', 'LtsGivq0trFeSGpdY3Cr4I13+k/+LIL0mHbiPlcv')

    })
    let params = {
        Bucket: 'elasticbeanstalk-us-east-2-084872120391',
        key: req.body.filename,
        Body: req.body.data,
    }
    var s3 = new aws.S3({ apiVersion: 'v4' });

    s3.listBuckets(function (err, data) {
        if (err) {
            console.log("Error", err);
        } else {
            console.log("Bucket List", data.Buckets);
        }
    });


    s3.getSignedUrl('putobject', params, (err, url) => {

        if (url) {
            log
            console.log("generated url" + url)
        }
        else {
            console.log("v........s3" + err)
        }
    })
})
router.get('/getinterviewmode', function (req, res, next) {
    try {
        console.log("getinterviewmode data");
        profiledb.getinterviewmode().then(function (rows) {
            if (rows) {

                res.send(JSON.stringify(rows.rows));
            }
        }).fail(function (err) {
            uierrorlogger.error("api getinterviewmode..." + err.body);

        });
    } catch (error) {
        uierrorlogger.error("Error catch expection api getinterviewmode..." + error);

    }
});
router.get('/getinterviewtype', function (req, res, next) {
    try {
        console.log("getinterviewtype data");
        profiledb.getinterviewtype().then(function (rows) {
            if (rows) {

                res.send(JSON.stringify(rows.rows));
            }
        }).fail(function (err) {
            uierrorlogger.error("api getinterviewtype..." + err.body);

        });
    } catch (error) {
        uierrorlogger.error("Error catch expection api getinterviewtype..." + error);

    }
});

router.get('/getphonecode', function (req, res, next) {

    try {
        console.log("get Phoncode data " + req.query.loginuserid);
        profiledb.getPhonecode(req.query.loginuserid)
            .then(function (rows) {
                if (rows) {
                    res.end(JSON.stringify(rows.rows));
                }
            })
            .fail(function (err) {
                uierrorlogger.error("api getphonecode..." + err.body);
            });
    } catch (error) {
        uierrorlogger.error("Error catch getphonecode..." + error);

    }
});

router.post('/changemobile', function (req, res) {
    var emailid = req.body.loginuserid;
    var loginusername = req.body.loginusername;
    var newmobile = req.body.newmobile
    console.log("Entering into projile js  Change Mobile" + req.body.newmobile, req.body.loginuserid);
    profiledb
        .ChangeMobile(req.body.newmobile, req.body.loginuserid)
        .then(function (rows) {
            if (rows) {
                console.log("change mobile Sucessful")
                res.status(200).send({
                    success: true,
                    message: 'Sucessfull'
                });
                changemobilemail(emailid, loginusername, newmobile);
                console.log('&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&')

            }
            else {
                console.log("change mobile" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'change mobile Methode Error' + err
                });


            }

        })
});
//change mobile mail
function changemobilemail(emailid, loginusername, mobileno) {

    try {
        console.log("###########changemobilemail###########" + emailid, loginusername, mobileno);
        database.getIdforEmail(emailid, loginusername, mobileno).then(function (rows) {
            if (rows) {
                var emailid = JSON.parse(JSON.stringify(rows.rows))[0].email;
                var varlenth = JSON.parse(JSON.stringify(rows.rows)).length;
                var fullUrl = ' http://koneqto.com';
                if (parseInt(varlenth) != 0) {
                    console.log("Entering into form the url 333")

                    var body = '<table align="center" style="width:600px;height:100px;font-size:13px;font-family:arial"><tr><td  style="border-bottom: #ccc solid 1px;"><a href=' + fullUrl + '><img width="230" src="http://unicsol.com/wp-content/uploads/2018/05/Koneqto-e1525862695762.png"></a></td></tr> <tr ><td><p style="font-size: 18px;">Hi <b>' + loginusername + '</b>,</p><p style="font-size: 14px" >Your current mobile number of <b>Koneqto</b> account is recently changed to  <b>' + mobileno + '(new mobile number)</b></p><p style="font-size: 14px" >Now, get free access of the latest job updates and alerts on your mobile.</p><br> <p style="font-size: 14px" ><b>Don&#39;t recognize this activity?</b></p><p style="font-size: 14px" >click <a href="/">here</a> for more information on how to recover your account</p><br><p style="font-size: 14px" >The Koneqto Accounts Team</p></td></tr><tr ><td style="background-color: #ccc; padding: 10px;">Copy Rights By <a style="color:#3b5998" href=' + fullUrl + '>KONEQTO </a> </td> </tr></table>';

                    let mailOptions = {
                        from: nodemailer.email,
                        to: emailid,
                        subject: "Change Mobile",
                        html: body,
                    };
                    nodemailer.sendMail(mailOptions, function (err, info) {
                        if (err) {
                            uierrorlogger.error("api sendmail..." + err.body);
                        }
                        else {
                            console.log('Message sent: %s', info.messageId);
                            console.log(info);
                            console.log("Success...." + info);
                            // res.status(200).send({
                            //     success: true,
                            //     message: "Email sent sucessfully"
                            // });
                        }
                    });
                }
                else {
                    console.log("entring into Email does't found case");
                }
            }
        }).fail(function (err) {
            uierrorlogger.error("api sendmail...fail" + err);
        });
    } catch (error) {
        uierrorlogger.error("Error catch expection api sendmail..." + error);
    }


}


module.exports = router;