var express = require('express');
var path = require('path');
var router = express.Router();
var multer = require('multer');
var friendsdb = require(path.join(__dirname, '../', 'modules', 'friendservicedb'));
var uiinfologger = require(path.join(__dirname, '../', '../', 'config')).uiinfologger;
var uierrorlogger = require(path.join(__dirname, '../', '../', 'config')).uierrorlogger;
var nodemailer = require(path.join(__dirname, '../', '../', 'config')).nodemailer;
var http = require("http");
var request = require('request');
var log4js = require('log4js');
// loadconnections

router.post('/loadconnections', function (req, res) {
    console.log("Entering into loadconnections routes method ----------->" + req.body.search);
    friendsdb.loadconnections(req.body.search)
        .then(function (rows) {
            if (rows) {
                res.end(JSON.stringify(rows.rows))
            }
            else {
                console.log("loadconnections routes" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'loadconnections routes Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'loadconnections routes Method Error' + err
            });
        });
});
router.post('/getfriendjobposts', function (req, res) {
    console.log("Entering into getfriendjobposts routes method ----------->" + req.body.loginuserid, req.body.psize, req.body.plength);

    friendsdb.getfriendjobposts(req.body.loginuserid, req.body.psize, req.body.plength)
        .then(function (rows) {
            if (rows) {

                res.end(JSON.stringify(rows.rows))


            }
            else {

                console.log("getfriendjobposts routes" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'getfriendjobposts routes Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'getfriendjobposts routes Method Error' + err
            });
        });

});

router.post('/friendsearch', function (req, res) {
    console.log("Entering into  friendsearch **********" + req.body.job_title, req.body.loginuserid);
    friendsdb.friendsearch(req.body.job_title, req.body.loginuserid)
        .then(function (rows) {
            if (rows) {

                res.send(rows.rows);

            }
            else {

                res.status(500).send({
                    success: false,
                    message: 'friendsearch  Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error(" friendsearch routes failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'friendsearch routes Method Error' + err
            });
        });

})
router.get('/viewProfilefriend', function (req, res) {
    console.log("Entering into==>friendsevice==> friendprofileoverview routes method", req.query.user_reg_id);

    friendsdb.friendprofileoverview(req.query.user_reg_id)
        .then(function (rows) {
            if (rows) {

                res.end(JSON.stringify(rows.rows))
            }
            else {

                console.log("friendsevice==> friendprofileoverview  routes" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'friendsevice==> friendprofileoverview routes Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: '==>friendsevice==> friendprofileoverview routes Method Error' + err
            });
        });

});
// loadfindconnections

router.get('/loadfindconnections', function (req, res) {
    console.log("Entering into friendsevice==> loadfindconnections routes method", req.query.p_registration_id);

    friendsdb.loadfindconnections(req.query.p_registration_id)
        .then(function (rows) {
            if (rows) {

                res.end(JSON.stringify(rows.rows))


            }
            else {

                console.log("friendsevice==>loadfindconnections routes" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'loadfindconnections routes Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'friendsevice==>loadfindconnections routes Method Error' + err
            });
        });

});
//loadfrnds
router.get('/loadfrnds', function (req, res) {
    console.log("Entering into friendsevice==> loadfrnds routes method", req.query.p_registration_id);

    friendsdb.loadfrnds(req.query.p_registration_id)
        .then(function (rows) {
            if (rows) {

                res.end(JSON.stringify(rows.rows))


            }
            else {

                console.log("friendsevice==>loadfrnds routes" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'loadfrnds routes Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'friendsevice==>loadfrnds routes Method Error' + err
            });
        });

});
////1////
router.get('/loadsuggestedfrnds', function (req, res) {
    console.log("Entering into friendsevice==> loadsuggestedfrnds routes method", req.query.p_registration_id);

    friendsdb.loadsuggestedfrnds(req.query.p_registration_id)
        .then(function (rows) {
            if (rows) {

                res.end(JSON.stringify(rows.rows))


            }
            else {

                console.log("friendsevice==>loadsuggestedfrnds routes" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'loadsuggestedfrnds routes Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'friendsevice==>loadsuggestedfrnds routes Method Error' + err
            });
        });

});

router.get('/loadreqnotif', function (req, res) {
    console.log("Entering into friendsevice==> loadreqnotif routes method", req.query.p_registration_id);

    friendsdb.loadreqnotif(req.query.p_registration_id)
        .then(function (rows) {
            if (rows) {

                res.end(JSON.stringify(rows.rows))


            }
            else {

                console.log("friendsevice==>loadreqnotif routes" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'loadreqnotif routes Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'friendsevice==>loadreqnotif routes Method Error' + err
            });
        });

});


////2////
router.get('/loadreqfriends', function (req, res) {
    console.log("Entering into friendsevice==> loadreqfriends routes method", req.query.p_registration_id);

    friendsdb.loadreqfriends(req.query.p_registration_id)
        .then(function (rows) {
            if (rows) {

                res.end(JSON.stringify(rows.rows))


            }
            else {

                console.log("friendsevice==>loadreqfriends routes" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'loadreqfriends routes Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'friendsevice==>loadreqfriends routes Method Error' + err
            });
        });

});


router.get('/loadfriendrequest', function (req, res) {
    console.log("Entering into friendsevice==> loadfriendrequest routes method", req.query.p_registration_id);

    friendsdb.loadfriendrequest(req.query.p_registration_id)
        .then(function (rows) {
            if (rows) {

                res.end(JSON.stringify(rows.rows))


            }
            else {

                console.log("friendsevice==>loadfriendrequest routes" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'friendsevice==>loadfriendrequest routes Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'friendsevice==>loadfriendrequest routes Method Error' + err
            });
        });

});

router.post('/sendfriendrequest', function (req, res) {

    console.log("Entering into  sendfriendrequest" + req.body.p_request_to, req.body.p_request_from, req.query.AuthToken);
    friendsdb.sendfriendrequest(req.body.p_request_to, req.body.p_request_from)
        .then(function (rows) {
            if (rows) {
                console.log("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" + JSON.stringify(rows.rows))
                // res.send(rows.rows)                
                res.status(200).send({
                    success: true,
                    message: JSON.parse(JSON.stringify(rows.rows))
                });


            }
            else {
                console.log("friendsevice==>sendfriendrequest failed" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'friendsevice==>sendfriendrequest  Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error("friendsevice==> sendfriendrequest routes failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'friendsevice==>sendfriendrequest routes Method Error' + err
            });
        });

})


/////////////////////////// Send Friend Request Mail///////////////////////////////////////////
router.post('/friendrequeststatus', function (req, res) {
    var name = req.body.name
    var frommailID = req.body.p_request_from;
    var loginusername = req.body.loginusername;
    console.log("Entering into  friendrequeststatus" + req.body.p_request_to, req.body.p_request_from, req.body.p_is_accepted, req.body.p_is_rejected);
    friendsdb.friendrequeststatus(req.body.p_request_to,
        req.body.p_request_from,
        req.body.p_is_accepted,
        req.body.p_is_rejected)
        .then(function (rows) {
            if (rows) {
                res.send(rows.rows);
                sendfriendReq(frommailID, name, loginusername);
            }
            else {
                console.log("friendsevice==>friendrequeststatus failed" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'friendsevice==>friendrequeststatus  Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error("friendsevice==> friendrequeststatus routes failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'friendsevice==>friendrequeststatus routes Method Error' + err
            });
        });

});


function sendfriendReq(frommailID, name, loginusername) {
    try {
        console.log("Friend request Email....." + frommailID, name, loginusername);
        friendsdb.getIdforEmail(frommailID, name, loginusername).then(function (rows) {
            if (rows) {
                var emailid = JSON.parse(JSON.stringify(rows.rows))[0].email;
                var name = JSON.parse(JSON.stringify(rows.rows))[0].name;
                console.log("..............." + emailid)
                var varlenth = JSON.parse(JSON.stringify(rows.rows)).length;
                var fullUrl = 'http://koneqto.com';
                if (parseInt(varlenth) != 0) {
                    console.log('hieee');
                    var output =
                        '<table align="center" style="width: 600px; height: 100px;font-size: 13px; font-family: arial; table, th, td {  ble, th, td { padding: 50px;  margin: 130px; }"><tr><td  style="border-bottom: #ccc solid 1px;"><a href=' + fullUrl + '><img width="230" src="http://unicsol.com/wp-content/uploads/2018/05/Koneqto-e1525862695762.png"></a></td></tr><tr><td><p style="font-size: 18px" ><b> Hi ' + name + '!</b></p><p style="font-size: 16px"><b style=" text-transform: capitalize;">' + loginusername + '</b> accepted your friend request.  Be the first one to share job updates and alerts.</p> </td></tr><tr><td style="background-color: #ccc; padding: 10px;">Copy Rights By <a style="color:#3b5998" href=' + fullUrl + '> KONEQTO </a> </td> </tr></table>';

                    let mailOptions = {
                        from: nodemailer.email,
                        to: emailid,
                        subject: 'friend request',
                        text: '',
                        html: output
                    };
                    nodemailer.sendMail(mailOptions, function (err, info) {
                        if (err) {
                            uierrorlogger.error("api sendmail...emil" + err);
                            //     next(Errormessage("sendmail", err));
                        }
                        else {

                            console.log('Message sent: %s', info.messageId);
                            console.log(info);
                            //console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
                        }
                    });

                }
                else {
                    console.log("entring into Email does't found case");
                    res.status(500).send({
                        success: false,
                        message: "Email doesn't found, please enter correct email"
                    });
                }
            }
        }).fail(function (err) {
            uierrorlogger.error("api sendmail...fail" + err);
        });
    } catch (error) {
        uierrorlogger.error("Error catch expection api sendmail..." + error);
    }

}
////////////////////Decline Friend request ////////////////////////
router.post('/friendrequeststatus1', function (req, res) {
    var name = req.body.name
    var frommailID = req.body.p_request_from;
    var loginusername = req.body.loginusername;
    console.log("Entering into  friendrequeststatus1" + req.body.p_request_to, req.body.p_request_from, req.body.p_is_accepted, req.body.p_is_rejected);
    friendsdb.friendrequeststatus1(req.body.p_request_to,
        req.body.p_request_from,
        req.body.p_is_accepted,
        req.body.p_is_rejected)
        .then(function (rows) {
            if (rows) {
                res.send(rows.rows);
                // sendfriendReq(frommailID, name, loginusername);
            }
            else {
                console.log("friendsevice==>friendrequeststatus1 failed" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'friendsevice==>friendrequeststatus1  Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error("friendsevice==> friendrequeststatus1 routes failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'friendsevice==>friendrequeststatus1 routes Method Error' + err
            });
        });

});

/////////////////////////////////////////////////////////////////////////////////////////////

router.get('/loadacceptedfriends', function (req, res) {
    console.log("Entering into friendsevice==> loadacceptedfriends routes method", req.query.p_registration_id);

    friendsdb.loadacceptedfriends(req.query.p_registration_id)
        .then(function (rows) {
            if (rows) {

                res.end(JSON.stringify(rows.rows))


            }
            else {

                console.log("friendsevice==>loadacceptedfriends routes" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'friendsevice==>loadacceptedfriends routes Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'friendsevice==>loadacceptedfriends routes Method Error' + err
            });
        });

});



router.post('/profileviwedby', function (req, res) {

    friendsdb.profileviwedby(req.body.loginuserid, req.body.otheruserid)
        .then(function (rows) {
            if (rows) {

                res.send(rows.rows);

            }
            else {
                console.log("profileviwedby falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'profileviwedby  Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error(" profileviwedby routes failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'profileviwedby routes Method Error' + err
            });
        });

})


//INVITE FRIEND MAIL...!
router.post('/invitemail', function (req, res) {
    var usermailid = req.body.loginuserid
    var name = req.body.name;
    var email = req.body.mail;
    var fname = req.body.fname;

    console.log("Entering into invitemail>>>> method",
        req.body.loginuserid,
        req.body.loginusername,
        req.body.name,
        req.body.mail,
        req.body.mobile
    )
    friendsdb.invitemail(
        req.body.loginuserid,
        req.body.name,
        req.body.mail,
        req.body.mobile)
        .then(function (rows) {
            if (rows) {

                res.status(200).send({
                    success: true,
                    message: 'Sucessfull'
                });
                console.log("invitemail Sucessful")
                sendinviteemail(usermailid, name, fname, email)
            }
            else {
                console.log("invitemail falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'invitemail Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error(" invitemail failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'invitemail Method Error' + err
            });
        });
});





function sendinviteemail(usermailid, name, fname, email) {
    try {
        console.log("Profile Api image send mail ....." + usermailid, name, fname, email);
        friendsdb.getEmailIdforinvitefriend(usermailid).then(function (rows) {
            if (rows) {
                var emailid = email;
                var name = JSON.parse(JSON.stringify(rows.rows))[0].name;
                var fname = JSON.parse(JSON.stringify(rows.rows))[0].fname;
                var currenturl = 'http://koneqto.com';
                var fullUrl = 'http://koneqto.com';
                console.log("...............=============================>>>>>>>>>>>>>" + usermailid,emailid)
                var varlenth = JSON.parse(JSON.stringify(rows.rows)).length;
                if (parseInt(varlenth) != 0) {
                    console.log('hieee');
                    var body =
                        '<table align="center" style="width:600px;height:100px;font-size:13px;font-family:arial"><tr><td style="border-bottom:#ccc solid 1px"><a href=' + fullUrl + '><img width="230" src="http://unicsol.com/wp-content/uploads/2018/05/Koneqto-e1525862695762.png"></a></td></tr><tr><td> <p style="font-size:18px"><b> Dear ' + fname + ', </b></p><p style="font-size:16px"> You have been invited by <b>' + name + '</b> to join <b> koneqto</b>. The one stop destination to fulfill all your career needs.To accept this invitation please <a href= ' + currenturl + ' >Click here</a></p><br></td></tr><tr> <td><p style="font-size:16px">Regards, <br>Team Koneqto </p></td></tr><tr><td style="background-color:#ccc;padding:10px;font-size:16px"> Copy Rights By <a style="color:#3b5998" href=' + fullUrl + '> KONEQTO </a>  </td></tr> </table>';

                    let mailOptions = {
                        from: nodemailer.email,
                        to: emailid,
                        subject: "Friend Invitation",
                        html: body,
                    };
                    nodemailer.sendMail(mailOptions, function (err, info) {
                        if (err) {
                            uierrorlogger.error("Profile Api sendmail..." + err.body);

                        }
                        else {
                            console.log('Message sent: %s', info.messageId);
                            console.log(info);
                            console.log("Success...." + info);
                            // res.status(200).send({
                            //     success: true,
                            //     message: "Email sent sucessfully"
                            // });
                        }
                    });
                }
                else {
                    console.log("entring into Email does't found case");

                }
            }
        }).fail(function (err) {
            uierrorlogger.error("Profile Api sendmail...fail" + err);

        });
    } catch (error) {
        uierrorlogger.error("Error catch expection Profile Api sendmail..." + error);

    }


}



module.exports = router;
