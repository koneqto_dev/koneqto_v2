var express = require('express');
var path = require('path');
var router = express.Router();
var multer = require('multer');
var jobpostingdb = require(path.join(__dirname, '../', 'modules', 'jobpostingdb'));
var uiinfologger = require(path.join(__dirname, '../', '../', 'config')).uiinfologger;
var uierrorlogger = require(path.join(__dirname, '../', '../', 'config')).uierrorlogger;
var http = require("http");
var request = require('request');
const fs = require('fs');
var nodemailer = require(path.join(__dirname, '../', '../', 'config')).nodemailer;
router.use(express.static('../../client'));
const { TwoFactor } = require('@twofactor/sdk/lib');
const https = require('https');
var log4js = require('log4js');//logger file
// const { TwoFactor } = require( '@twofactor/sdk' );
//creating logger file
//var today_Date = new Date();
//logger_file_name = today_Date.getFullYear() + "-" + today_Date.getMonth() + "-" + today_Date.getDate() + " log file" + '.txt';
//logger_file_name_path = './public/log_files/' + logger_file_name;
/* log4js.configure({
    appenders: [
        { type: 'console' },
        { type: 'file', filename: logger_file_name_path }
    ]
}); */



router.post('/profilevisibilityenable', function (req, res, next) {

    console.log("profilevisibilityenable data" + req.query.loginuserid, req.query.value);
    jobpostingdb.profilevisibilityenable(req.body.loginuserid, req.body.value)
        .then(function (rows) {
            if (rows) {
                res.send(rows.rows);
                //res.end(JSON.stringify(rows.rows))
            } else {
                console.log("profilevisibilityenable falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'profilevisibilityenable Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error(" profilevisibilityenable routes failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'profilevisibilityenable routes Method Error' + err
            });
        });
})



router.post('/report', function (req, res, next) {

    // console.log("report data" + req.body.p_registration_id, req.body.id, req.body.type, req.body.description, req.body.loginuserid);
    jobpostingdb.report(req.body.p_registration_id, req.body.id, req.body.type, req.body.description, req.body.loginuserid)
        .then(function (rows) {
            if (rows) {
                res.send(rows.rows);
                //res.end(JSON.stringify(rows.rows))
            } else {
                //  console.log("report faliedsfsafsafsaf" + err.body)
                res.send();
            }
        }).fail(function (err) {

            res.send([{ message: "Already Reported" }]);
        });
})



router.post('/getexpertadviceonly', function (req, res) {
    console.log("Entering into getexpertadviceonly routes method ----------->" + req.body.p_registration_id);

    jobpostingdb.getexpertadviceOnly(req.body.p_registration_id)
        .then(function (rows) {
            if (rows) {

                res.end(JSON.stringify(rows.rows))


            }
            else {

                console.log("getexpertadviceOnly routes" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'getexpertadviceOnly routes Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'getexpertadviceOnly routes Method Error' + err
            });
        });

});
router.post('/deleteexpertAdviceonly', function (req, res) {
    console.log("Entering into deleteexpertAdviceonly routes method ----------->" + req.Id);

    jobpostingdb.deleteexpertAdviceonly(req.body.Id)
        .then(function (rows) {
            if (rows) {

                res.end(JSON.stringify(rows.rows))


            }
            else {

                console.log("deleteexpertAdviceonly routes" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'deleteexpertAdviceonly routes Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'deleteexpertAdviceonly routes Method Error' + err
            });
        });

});
router.post('/getallmixedposts', function (req, res, next) {
    //console.log("getallmixedposts data" + req.query.p_registration_id+" "+req.query.p_location);
    jobpostingdb.getallmixedposts(req.body.loginuserid, req.body.search)
        .then(function (rows) {
            if (rows) {
                res.send(rows.rows);
            }
            else {
                console.log("getallmixedposts falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'getallmixedposts  Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error(" getallmixedposts routes failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'getallmixedposts routes Method Error' + err
            });
        });
})

router.use(function (req, res, next) { //allow cross origin requests
    res.setHeader("Access-Control-Allow-Methods", "POST, PUT, OPTIONS, DELETE, GET");
    res.header("Access-Control-Allow-Origin", "http://localhost:4200");
    //res.header("Access-Control-Allow-Origin", "https://www.koneqto.com");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Credentials", true);
    next();
});

//server
//   var jobPostsvideo='./publish/assets/jobposts/videos'
//   var jobpostImage='./publish/assets/jobposts/images'
//   var EventImages='./publish/assets/events/images'
//   var EventVideo='./publish/assets/events/videos'

// local
var jobPostsvideo = '../server/public/jobposts/videos'
var jobpostImages = '../server/public/jobposts/images'
var EventImage = '../server/public/events/images'
var EventVideo = '../server/public/events/videos'
var charityimage = '../server/public/charity'


//////////////////////////////Job Posting/////////////////////////////////////
router.post('/jobpost', function (req, res) {
    var usermailid = req.body.loginuserid;
    var name = req.body.name;
    var jobtitle = req.body.jobtitle
    data = req.body.photo
    vid_data = req.body.video
    var pt = '';
    var dt = new Date();
    vidname = ''
    imagename = ''
    if (data) {
        if (data.img_data != "" && data.type != "") {
            imagename = req.body.name + '-' + dt.getFullYear() + "" + dt.getMonth() + "" + dt.getMilliseconds() + "" + req.body.loginuserid + data.type;
            path = './public/jobposts/images/' + imagename;
            fs.writeFile(path, data.img_data, 'base64', (err) => {
                if (err)
                    console.log(err)
                else {
                    console.log('Image Svaed Success...');
                }

            })
        }
        var pt = '';
        var dt = new Date();
        if (vid_data) {
            if (vid_data.type != "" && vid_data.vid_data != "") {
                vidname = req.body.name + '-' + dt.getFullYear() + "" + dt.getMonth() + "" + dt.getMilliseconds() + "" + req.body.loginuserid + vid_data.type;
                path = './public/jobposts/videos/' + vidname;
                fs.writeFile(path, vid_data.vid_data, 'base64', (err) => {
                    if (err)
                        console.log(err)
                    else {
                        console.log('video Saved Success...');
                    }

                })
            }
        }

        jobpostingdb.jobposting(
            req.body.loginuserid,
            req.body.jobtitle,
            req.body.natureofjob,
            req.body.companyname,
            req.body.functionalarea,
            req.body.educationdetails,
            req.body.keyskills,
            req.body.optionalskills,
            req.body.country,

            req.body.state,
            req.body.fresher,
            req.body.minexp,
            req.body.maxexp,
            req.body.maxsalary,

            req.body.minsalary,
            req.body.gender,
            req.body.noticeperiod,
            req.body.vacancy,
            req.body.startdate,
            req.body.enddate,
            req.body.phy_status,
            req.body.contactperson,
            req.body.contactno,

            req.body.description,
            req.body.certification,
            req.body.specialmention,
            req.body.venue,
            imagename,
            // this.picname,
            // this.vname,
            vidname,
            req.body.interviewdetails,
            req.body.first_question,
            req.body.second_question,
            req.body.third_question,
            req.body.fourth_question,
            req.body.fifth_question,
            req.body.jobpostid,
            req.body.currency,
            req.body.locality,
            req.body.location,
            req.body.salarytype)
            .then(function (rows) {
                if (rows, res) {
                    //console.log("jobposting Sucessful")
                    res.status(200).send({
                        success: true,
                        message: 'Sucessfull'
                    });
                    sendmailforjobpost(usermailid, name, jobtitle)
                    this.picname = ''
                    this.vname = ''
                }
                else {
                    //console.log("jobposting falied" + err.body)
                    res.status(500).send({
                        success: false,
                        message: 'jobpost Method Error' + err
                    });
                    this.picname = ''
                    this.vname = ''
                }
            }).fail(function (err) {
                uierrorlogger.error(" jobposting failed..." + err.body);
                res.status(500).send({
                    success: false,
                    message: 'jobpost Method Error' + err

                });
                this.picname = ''
                this.vname = ''

            });
    }
});

// Sending mail for Job post
function sendmailforjobpost(usermailid, name, jobtitle) {
    try {
        console.log(" Job post Email....." + usermailid, name, jobtitle);
        jobpostingdb.getIdforEmail(usermailid, name).then(function (rows) {
            if (rows) {
                var emailid = JSON.parse(JSON.stringify(rows.rows))[0].email;
                var name = JSON.parse(JSON.stringify(rows.rows))[0].name;
                console.log("...............=============================>>>>>>>>>>>>>" + usermailid, name, jobtitle)
                var fullUrl = ' http://koneqto.com';
                var varlenth = JSON.parse(JSON.stringify(rows.rows)).length;
                if (parseInt(varlenth) != 0) {
                    console.log('hieee');
                    var body =
                        '<table align="center" style="width:600px;height:100px;font-size:13px;font-family:arial"><tr><td  style="border-bottom: #ccc solid 1px;"><a href=' + fullUrl + '><img width="230" src="http://unicsol.com/wp-content/uploads/2018/05/Koneqto-e1525862695762.png"></a> </td></tr><tr><td><p style="font-size: 18px;">Hi <b>' + name + '</b>,</p><p style="font-size: 14px" >Thank you!</p><p style="font-size: 14px" >Your job on <b style=" text-transform: capitalize;">' + jobtitle + '</b> is successfully posted. Now, people can view and reach you in seconds.</p><p style="font-size: 14px" >Keep Posting!</p><p style="font-size: 14px">Team Koneqto</p></td></tr><tr><td style="background-color: #ccc; padding: 10px;">Copy Rights By <a style="color:#3b5998" href=' + fullUrl + '> KONEQTO </a></td></tr></table>';

                    let mailOptions = {
                        from: nodemailer.email,
                        to: emailid,
                        subject: "Job post",
                        html: body,
                    };
                    nodemailer.sendMail(mailOptions, function (err, info) {
                        if (err) {
                            uierrorlogger.error("jobpostningjs sendmail..." + err.body);

                        }
                        else {
                            console.log('Message sent: %s', info.messageId);
                            console.log(info);
                            console.log("Success...." + info);
                            // res.status(200).send({
                            //     success: true,
                            //     message: "Email sent sucessfully"
                            // });
                        }
                    });
                }
                else {
                    console.log("entring into Email does't found case");
                    res.status(200).send({
                        success: true,
                        message: "Email sent failed, Error in sending email"
                    });

                }
            }

        }).fail(function (err) {
            uierrorlogger.error("jobpostingjs sendmail...fail" + err);

        });
    } catch (error) {
        uierrorlogger.error("Error catch expection jobpostingjs sendmail..." + error);

    }

}
//////////////////////////////////////////////////////////////////////////////
var jpvstorage = multer.diskStorage({ //multers disk storage settings
    destination: function (req, file, cb) {
        cb(null, jobPostsvideo);
    },
    filename: function (req, file, cb) {
        console.log("original name--", file.originalname)
        //console.log("original file name--",file.filename)
        var datetimestamp = Date.now();
        cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1]);
    }
});
var jpvupload = multer({ //multer settings
    storage: jpvstorage
}).single('file');
var vname;
router.post('/vjobposts', function (req, res) {
    jpvupload(req, res, function (err) {
        console.log("req.file", req.file);
        if (err) {
            res.json({ error_code: 1, err_desc: err });
            return;
        }
        res.json({ error_code: 0, err_desc: null, jobvideo: req.file.filename });
        //console.log("path ---",req.file.path)
        //var path=JSON.stringify(req.file.filename);
        this.vname = req.file.filename

    });
});
//images
var jpistorage = multer.diskStorage({ //multers disk storage settings
    destination: function (req, file, cb) {
        cb(null, jobpostImages);
    },
    filename: function (req, file, cb) {
        console.log("original name--", file.originalname)
        //console.log("original file name--",file.filename)
        var datetimestamp = Date.now();
        cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1]);
    }
});
var jpiupload = multer({ //multer settings
    storage: jpistorage
}).single('file');
router.post('/ijobposts', function (req, res) {
    jpiupload(req, res, function (err) {
        console.log("req.file", req.file);
        if (err) {
            res.json({ error_code: 1, err_desc: err });
            return;
        }
        res.json({ error_code: 0, err_desc: null, jobimage: req.file.filename });
        //console.log("path ---",req.file.path)
        //var path=JSON.stringify(req.file.filename);
        this.picname = req.file.filename

    });
});
// router.get('/getmyposts', function (req, res, next) {
//     try {
//         console.log("getmyposts data" + req.query.loginuserid);
//         jobpostingdb.getmyposts(req.query.loginuserid, req.query.jobid).then(function (rows) {
//             if (rows) {
//                 res.end(JSON.stringify(rows.rows));
//             }
//         }).fail(function (err) {
//             uierrorlogger.error("api getmyposts..." + err.body);
//             //next(Errormessage("Create New-Password ", err));
//         });
//     } catch (error) {
//         uierrorlogger.error("Error catch expection api getmyposts..." + error);
//         next(Errormessage("Create New-Pass.loginword", error));
//     }
// });




router.post('/getmyposts', function (req, res, next) {
    //console.log("getallmixedposts data" + req.query.p_registration_id+" "+req.query.p_location);
    jobpostingdb.getmyposts(req.body.loginuserid,req.body.jobid, req.body.search,req.body.pagesize,req.body.pagelength)
        .then(function (rows) {
            if (rows) {
                res.send(rows.rows);
            }
            else {
                console.log("getmyposts falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'getmyposts  Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error(" getmyposts routes failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'getmyposts routes Method Error' + err
            });
        });
})
//Apply job
router.post('/applyjob', function (req, res, next) {
    var registration_id = req.body.registration_id;
    var jobid = req.body.jobid;
    var uname = req.body.loginusername;
    var usermailid = req.body.loginuserid;
    var jobtitle = req.body.jobtitle;
    var description = req.body.description;
    var mandatoryskills = req.body.mandatoryskills;
    skills = []
    mandatoryskillsNames = []
    for (let i = 0; i < mandatoryskills.length; i++) {
        console.log(mandatoryskills[i], "mandatoryskills[i]")
        skills.push(mandatoryskills[i].split(':'))
        console.log(skills, "skills")
        // mandatoryskillsNames.push(skills[0][1])

    }
    let j = 0
    while (j < skills.length) {
        // var sep=''
        // if(skills[j+1]){
        //     sep=','
        // }
        mandatoryskillsNames.push(skills[j][1])
        j++
        console.log(mandatoryskillsNames, "mandatoryskillsNames")
    }
    console.log("data=====================================!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!>" + registration_id, jobid, jobtitle, uname, description, mandatoryskillsNames);
    try {
        //console.log("applyjob data" + req.body.loginuserid, req.body.jobid);first:answer.firstanswer,second:answer.secondanswer,third:answer.thirdanswer,fourth:answer.fourthanswer,fifth:answer.fifthanswer
        jobpostingdb.Applyjob(req.body.loginuserid, req.body.jobid, req.body.first, req.body.second, req.body.third, req.body.fourth, req.body.fifth).then(function (rows) {
            if (rows) {
                res.end(JSON.stringify(rows.rows));
                applyjobmail(usermailid, jobtitle, description, mandatoryskillsNames)
                posteduserjobmail(registration_id, uname, jobtitle)
                console.log("email checking passing or not")
            }
        }).fail(function (err) {
            uierrorlogger.error("api applyjob..." + err.body);
            //next(Errormessage("Create New-Password ", err));
        });
    } catch (error) {
        uierrorlogger.error("Error catch expection api applyjob..." + error);
        next(Errormessage("Create New-Pass.loginword", error));
    }
});
//sending mail for apply job mail
function applyjobmail(usermailid, jobtitle, description, mandatoryskillsNames) {
    try {
        console.log(" Job post Email....." + usermailid, jobtitle, description, mandatoryskillsNames);
        jobpostingdb.getIdforEmail(usermailid).then(function (rows) {
            if (rows) {
                var emailid = JSON.parse(JSON.stringify(rows.rows))[0].email;
                var name = JSON.parse(JSON.stringify(rows.rows))[0].name;
                // var name = JSON.parse(JSON.stringify(rows.rows))[0].name;
                console.log("...............=============================>>>>>>>>>>>>>" + usermailid)
                var fullUrl = ' http://koneqto.com';
                var varlenth = JSON.parse(JSON.stringify(rows.rows)).length;
                if (parseInt(varlenth) != 0) {
                    console.log('hieee');
                    var body =
                        '<table align="center" style="width:600px;height:100px;font-size:13px;font-family:arial"><tr><td style="border-bottom:#ccc solid 1px"><a href=' + fullUrl + '><img width="230" src="http://unicsol.com/wp-content/uploads/2018/05/Koneqto-e1525862695762.png"></a></td></tr><tr><td><p style="font-size:18px"><b> Dear ' + name + ',</b></p><p style="font-size:16px;">You have applied to below mentioned job </p></td></tr><tr><td><br><table><tr><td style="vertical-align: -webkit-baseline-middle;"><h4 >Job Title: </h4></td><td><h4>' + jobtitle + ' </h4></td></tr><tr><td style="vertical-align: -webkit-baseline-middle;"><h4>Description: </h4></td><td><h4>' + description + ' </h4></td></tr><tr><td style="vertical-align: -webkit-baseline-middle; width: 112px"><h4>Mandatory Skills:</h4></td><td><h4>' + mandatoryskillsNames + '</h4></td></tr></table></td><tr><td><p style="font-size:16px;"> If the recruiters at <b>Koneqto</b> find your profile suitable, they&#39;ll connect with you. <tr><td><br><p style="font-size:16px;">Regards, <br> <b>Team Koneqto.</b></p></td></tr></tr><tr><td style="background-color:#ccc;padding:10px;font-size:16px">Copy Rights By<a style="color:#3b5998" href=' + fullUrl + '> KONEQTO </a></td></tr></table>';
                    let mailOptions = {
                        from: nodemailer.email,
                        to: emailid,
                        subject: "Job post",
                        html: body,
                    };
                    nodemailer.sendMail(mailOptions, function (err, info) {
                        if (err) {
                            uierrorlogger.error("jobpostningjs sendmail..." + err.body);

                        }
                        else {
                            console.log('Message sent: %s', info.messageId);
                            console.log(info);
                            console.log("Success...." + info);
                            // res.status(200).send({
                            //     success: true,
                            //     message: "Email sent sucessfully"
                            // });
                        }
                    });
                }
                else {
                    console.log("entring into Email does't found case");
                    res.status(200).send({
                        success: true,
                        message: "Email doesn't found, please enter correct email"
                    });

                }
            }
        }).fail(function (err) {
            uierrorlogger.error("api sendmail...fail" + err);

        });
    } catch (error) {
        uierrorlogger.error("Error catch expection api sendmail..." + error);

    }

}
//sending mail for posteduserjobmail 
function posteduserjobmail(registration_id, uname, jobtitle) {
    try {
        console.log("some one has applied tou your job Email....." + registration_id);
        jobpostingdb.getIdforEmail(registration_id).then(function (rows) {
            if (rows) {
                var emailid = JSON.parse(JSON.stringify(rows.rows))[0].email;
                var name = JSON.parse(JSON.stringify(rows.rows))[0].name;
                console.log("..............." + emailid)
                var fullUrl = ' http://koneqto.com';
                if (emailid) {
                    console.log('hieee');
                    var output =
                        '<table align="center" style="width:600px;height:100px;font-size:13px;font-family:arial"> <tr><td  style="border-bottom: #ccc solid 1px;"><a href=' + fullUrl + '><img width="230" src="http://unicsol.com/wp-content/uploads/2018/05/Koneqto-e1525862695762.png"></a></td></tr><tr><td><p style="font-size: 18px" ><b> Hi ' + name + '!</b></p><br></td></tr><tr><td><p style="font-size:17px"><b style=" text-transform: capitalize;">' + uname + '</b> has applied for your job post <b style=" text-transform: capitalize;">' + jobtitle + '.</b> </p><br></td></tr><tr><td><p style="font-size:16px">Best Regards, <br> <b>Team Koneqto</b> </p>  </td></tr></tr><tr><td style="background-color: #ccc; padding: 10px;">Copy Rights By <a style="color:#3b5998" href=' + fullUrl + '>KONEQTO</a> </td></tr></table>';




                    let mailOptions = {
                        from: nodemailer.email,
                        to: emailid,
                        subject: 'Applied job post',
                        text: '',
                        html: output
                    };

                    nodemailer.sendMail(mailOptions, function (err, info) {
                        if (err) {
                            uierrorlogger.error("jobpostningjs sendmail...emil" + err);

                        }
                        else {

                            console.log('Message sent: %s', info.messageId);
                            console.log(info);

                        }
                    });

                } else {
                    console.log("entring into Email does't found case");
                    res.status(500).send({
                        success: false,
                        message: "Email doesn't found, please enter correct email"
                    });
                }
            }
        }).fail(function (err) {
            uierrorlogger.error("api sendmail...fail" + err);
        });
    } catch (error) {
        uierrorlogger.error("Error catch expection api sendmail..." + error);
    }

}
///////////////////////////////////////////////////////////////////////////////////
router.get('/getquestions', function (req, res) {
    console.log("Entering into getquestions method");
    jobpostingdb.getquestions(req.query.jobid)
        .then(function (rows) {
            if (rows) {

                res.end(JSON.stringify(rows.rows))


            }
            else {

                console.log("getquestions" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'getquestions api Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'getquestions api Method Error' + err
            });
        });

});
router.post('/deletejob', function (req, res, next) {
    try {
        //console.log("Deletejob data" + req.body.loginuserid, req.body.jobid);
        jobpostingdb.deletejob(req.query.loginuserid, req.body.jobid, req.body.image).then(function (rows) {
            if (rows) {
                // if(req.body.image){
                // jobimage = req.body.image;
                // // achievementimages = achievements.split(' ');
                // // console.log("images===", achievementimages)
                // // for (var i = 0; i < achievements.length - 1; i++) {
                //     fs.unlink(jobpostImages+'/'+ jobimage, (err) => {
                //         if (err) throw err;

                //     },

                //         console.log("deleted the image" + jobimage)

                //     );
                // }
                // if(req.body.video){
                //     jobvideo = req.body.video;
                //     // achievementimages = achievements.split(' ');
                //     // console.log("images===", achievementimages)
                //     // for (var i = 0; i < achievements.length - 1; i++) {
                //         fs.unlink(jobPostsvideo+'/'+ jobvideo, (err) => {
                //             if (err) throw err;

                //         },

                //             console.log("deleted the image" + jobvideo)

                //         );
                // }
                // }
                res.end(JSON.stringify(rows.rows));
            }
        }).fail(function (err) {
            uierrorlogger.error("api Deletejob..." + err.body);
            //next(Errormessage("Create New-Password ", err));
        });
    } catch (error) {
        uierrorlogger.error("Error catch expection api Deletejob..." + error);
        next(Errormessage("Deletejob", error));
    }
});
//jobs in other category
router.post('/getmypostingsinothrcat', function (req, res, next) {
    //console.log("getmypostingsinothrcat data" + req.query.p_registration_id+" "+req.query.p_location);
    jobpostingdb.getmypostingsinothrcat(req.body.p_registration_id, req.body.p_category)
        .then(function (rows) {
            if (rows) {
                res.send(rows.rows);
            }
            else {
                console.log("getmypostingsinothrcat falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'getmypostingsinothrcat  Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error(" getmypostingsinothrcat routes failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'getmypostingsinothrcat routes Method Error' + err
            });
        });
})
//jobs other location
router.post('/getmypostingsinothrloc', function (req, res, next) {
    //console.log("getmypostingsinothrloc data" + req.query.p_registration_id+" "+req.query.p_location);
    jobpostingdb.getmypostingsinothrloc(req.body.p_registration_id, req.body.p_location)
        .then(function (rows) {
            if (rows) {
                res.send(rows.rows);
            }
            else {
                console.log("getmypostingsinothrloc falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'getmypostingsinothrloc  Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error(" getmypostingsinothrloc routes failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'getmypostingsinothrloc routes Method Error' + err
            });
        });
})
///////////////////////////////////////////////  Events Functions  ///////////////////////////////////////////////////////
//posting events function
router.post('/saveevent', function (req, res) {
    var useremailid = req.body.p_registration_id;
    var name = req.body.name;
    console.log(name, "name")
    var etype = req.body.event_type;
    var eventname = req.body.event_name;
    var startdate = req.body.eventstartdate;
    var enddate = req.body.eventenddate;
    var location = req.body.event_location;
    var iext = req.body.eimageextType;
    var vext = req.body.evideoextType;

    //eventimage
    var data1;
    data1 = req.body.eventimage;
    var idt = new Date();
    ipattern = req.body.name + '-' + idt.getFullYear() + "" + idt.getMonth() + "" + idt.getMilliseconds() + "" + iext;
    console.log('Image ptr...' + iext);
    ipath = './public/events/images/' + ipattern;
    fs.writeFile(ipath, data1, 'base64', (err) => {
        if (err)
            console.log(err)
        else {
            // console.log('Image Svaed Success...');
        }
    })

    //eventvideo
    if (req.body.eventvideo != null && req.body.eventvideo) {
        var data2;
        data2 = req.body.eventvideo;
        var vdt = new Date();
        vpattern = req.body.name + '-' + vdt.getFullYear() + "" + vdt.getMonth() + "" + vdt.getMilliseconds() + "" + vext;
        console.log('video ptr...' + vext);
        vpath = './public/events/videos/' + vpattern;
        fs.writeFile(vpath, data2, 'base64', (err) => {
            if (err)
                console.log(err)
            else {
                //console.log('Video Svaed Success...');
            }
        })
    }
    else {
        vpattern = null;
    }



    if (req.body.eventvideo == null || !req.body.eventvideo) {
        eventvideo = null;
        jobpostingdb.saveevent(req.body.eventid,
            req.body.p_registration_id,
            req.body.eventname,
            req.body.eventtype,
            req.body.eventdescription,
            req.body.eventfunctionalarea,
            req.body.eventlocation,
            req.body.eventlocality,
            ipattern,
            req.body.eventaddress,
            req.body.eventstartdate,
            req.body.eventenddate,
            vpattern,
            req.body.country,
            req.body.state)
            .then(function (rows) {

                if (rows) {
                    console.log("saveevent-posting routes Sucessful")
                    res.status(200).send({
                        success: true,
                        message: 'Sucessfull'
                    });
                    eventvideo = '';
                    eventimage = ''
                    eventpostmail(useremailid, name, eventname, etype, startdate, enddate, location)

                }
                else {
                    console.log("saveevent-posting routes falied" + err.body)
                    res.status(500).send({
                        success: false,
                        message: 'saveevent-posting routes Method Error' + err
                    });
                    eventvideo = '';
                    eventimage = ''
                }
            }).fail(function (err) {
                uierrorlogger.error("saveevent-posting routes failed..." + err.body);
                res.status(500).send({
                    success: false,
                    message: 'saveevent-posting routes Method Error' + err
                });
            });
    }

    else {
        jobpostingdb.saveevent(req.body.eventid,
            req.body.p_registration_id,
            req.body.eventname,
            req.body.eventtype,
            req.body.eventdescription,
            req.body.eventfunctionalarea,
            req.body.eventlocation,
            req.body.eventlocality,
            ipattern,
            req.body.eventaddress,
            req.body.eventstartdate,
            req.body.eventenddate,
            vpattern,
            req.body.country,
            req.body.state)
            .then(function (rows) {

                if (rows) {
                    console.log("saveevent-posting routes Sucessful")
                    res.status(200).send({
                        success: true,
                        message: 'Sucessfull'
                    });
                    eventvideo = '';
                    eventimage = ''
                    eventpostmail(useremailid, name, eventname, etype, startdate, enddate, location)

                }
                else {
                    console.log("saveevent-posting routes falied" + err.body)
                    res.status(500).send({
                        success: false,
                        message: 'saveevent-posting routes Method Error' + err
                    });
                    eventvideo = '';
                    eventimage = ''
                }
            }).fail(function (err) {
                uierrorlogger.error("saveevent-posting routes failed..." + err.body);
                res.status(500).send({
                    success: false,
                    message: 'saveevent-posting routes Method Error' + err
                });
            });
    }


    eventvideo = '';
    eventimage = ''
})
//eventimage
var estorage = multer.diskStorage({ //multers disk storage settings
    destination: function (req, file, cb) {
        cb(null, EventImage);
    },
    filename: function (req, file, cb) {
        console.log("original name--", file.originalname)
        //console.log("original file name--",file.filename)
        var datetimestamp = Date.now();
        cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1]);
    }
});

var eiupload = multer({ //multer settings
    storage: estorage
}).single('file');
var eventimage;
router.post('/ieventposts', function (req, res) {
    eiupload(req, res, function (err) {
        console.log("req.file", req.file);
        if (err) {
            res.json({ error_code: 1, err_desc: err });
            return;
        }
        res.json({ error_code: 0, err_desc: null, image: req.file.filename });
        //console.log("path ---",req.file.path)
        //var path=JSON.stringify(req.file.filename);
        eventimage = req.file.filename

    });
});
//eventvideo
var evstorage = multer.diskStorage({ //multers disk storage settings
    destination: function (req, file, cb) {
        cb(null, EventVideo);
    },
    filename: function (req, file, cb) {
        console.log("original name--", file.originalname)
        //console.log("original file name--",file.filename)
        var datetimestamp = Date.now();
        cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1]);
    }
});
var evupload = multer({ //multer settings
    storage: evstorage
}).single('file');
var eventvideo;
router.post('/veventposts', function (req, res) {
    evupload(req, res, function (err) {
        console.log("req.file", req.file);
        if (err) {
            res.json({ error_code: 1, err_desc: err });
            return;
        }
        res.json({ error_code: 0, err_desc: null, video: req.file.filename });
        //console.log("path ---",req.file.path)
        //var path=JSON.stringify(req.file.filename);
        eventvideo = req.file.filename


    });
});

//loadeventtypes
router.get('/loadeventtypes', function (req, res) {
    console.log("Entering into loadeventtypes method");
    jobpostingdb.loadeventtypes()
        .then(function (rows) {
            if (rows) {
                res.end(JSON.stringify(rows.rows))
            }
            else {
                console.log("loadeventtypes" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'loadeventtypes api Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'loadeventtypes api Method Error' + err
            });
        });
});

//Get states by country_id
router.get('/state', function (req, res) {
    country_id = req.query.country_id
    jobpostingdb.getstate(country_id)
        .then(function (rows) {
            if (rows) {
                res.end(JSON.stringify(rows.rows))
            }
            else {
                //console.log("city routes" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'getstate routes Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'getstate routes Method Error' + err
            });
        });
});

//Get cities by state_id
router.get('/city', function (req, res) {
    console.log("entering into /city js", req.query.state_id)
    state_id = req.query.state_id
    jobpostingdb.getcity(state_id)
        .then(function (rows) {
            if (rows) {
                res.end(JSON.stringify(rows.rows))
            }
            else {
                //console.log("city routes" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'city routes Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'city routes Method Error' + err
            });
        });
});

// get cities by id for mobile api
router.get('/mobileapicity', function (req, res) {
    console.log("entering into /mobileapicity js", req.query.state_id)
    state_id = req.query.state_id
    jobpostingdb.getmobileapicity(state_id)
        .then(function (rows) {
            if (rows) {
                res.end(JSON.stringify(rows.rows))
            }
            else {
                //console.log("mobileapicity routes" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'mobileapicity routes Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'mobileapicity routes Method Error' + err
            });
        });
});

// get allcities for mobile api
router.get('/getallcitiesformobileapi', function (req, res) {
    console.log("entering into /getallcitiesformobileapi js")
    // state_id = req.query.state_id
    jobpostingdb.getmobileapiallcities()
        .then(function (rows) {
            if (rows) {
                res.end(JSON.stringify(rows.rows))
            }
            else {
                //console.log("getmobileapiallcities routes" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'getmobileapiallcities routes Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'getmobileapiallcities routes Method Error' + err
            });
        });
});

//Get cities by search
router.get('/city1', function (req, res) {
    console.log("entering into /city js", req.query.state_id, req.query.name)
    state_id = req.query.state_id
    req.query.name
    // jobpostingdb.getcity1(req.query.state_id,req.query.name)
    state_id = req.query.state_id
    jobpostingdb.getcity1(state_id, req.query.name)
        .then(function (rows) {
            if (rows) {
                res.end(JSON.stringify(rows.rows))
            }
            else {
                //console.log("city routes" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'city routes Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'city routes Method Error' + err
            });
        });
});

//Get localities by city_id
router.get('/loadeventlocalities1', function (req, res) {
    console.log("Entering into loadeventlocalities method");
    city_id = req.query.city_id
    jobpostingdb.loadlocalities1(city_id)
        .then(function (rows) {
            if (rows) {
                res.end(JSON.stringify(rows.rows))
            }
            else {
                console.log("loadeventlocalities" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'loadeventlocalities api Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'loadeventlocalities api Method Error' + err
            });
        });
});

// get localities by id for mobile api
router.get('/mobileapilocalities', function (req, res) {
    console.log("Entering into mobileapilocalities method");
    city_id = req.query.city_id
    jobpostingdb.mobileapilocalities(city_id)
        .then(function (rows) {
            if (rows) {
                res.end(JSON.stringify(rows.rows))
            }
            else {
                console.log("mobileapilocalities" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'mobileapilocalities api Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'mobileapilocalities api Method Error' + err
            });
        });
});

// getall mobileapiskills  for mobile api
router.get('/mobileapiskills', function (req, res) {
    //console.log("Entering into getskillnames1>>>> method", req.query.skillname);
    jobpostingdb.mobileapiallskills(req.query.fn_area_id)
        .then(function (rows) {
            // console.log("1>>>>>",JSON.stringify(rows))
            // console.log("2>>>>>",JSON.stringify(rows.rows))
            if (rows) {
                console.log("mobileapiskills Sucessful")
                res.send(rows.rows)
            }
            else {
                console.log("mobileapiskills falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'get skillnames Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error("mobileapiskills failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'mobileapiskills Method Error' + err
            });
        });
});


//Get localities by search
router.get('/loadeventlocalities2', function (req, res) {
    console.log("Entering into loadeventlocalities2 method", city_id, req.query.name);
    city_id = req.query.city_id
    jobpostingdb.loadlocalities2(city_id, req.query.name)
        .then(function (rows) {
            if (rows) {
                res.end(JSON.stringify(rows.rows))
            }
            else {
                console.log("loadeventlocalities" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'loadeventlocalities api Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'loadeventlocalities api Method Error' + err
            });
        });
});

//event posting mail
function eventpostmail(useremailid, name, eventname, ettype, startdate, enddate, location) {

    try {
        //console.log(" Event post Email....." + useremailid, name, eventname, ettype, startdate, enddate, location);

        jobpostingdb.getIdforEmailevent(useremailid, name, eventname, ettype, startdate, enddate, location).then(function (rows) {
            if (rows) {

                //function to format date
                var sd = formattedate;
                var ed = formattedate;
                function formattedate(startdate) {
                    var d = new Date(startdate),
                        month = '' + (d.getMonth() + 1),
                        day = '' + d.getDate(),
                        year = d.getFullYear();
                    if (month.length < 2) month = '0' + month;
                    if (day.length < 2) day = '0' + day;
                    this.formattedate = [day, month, year].join('-')
                    return this.formattedate;

                }
                // console.log(sd(startdate)+"EVENT START DATE");
                // console.log(ed(enddate)+"EVENT END DATE");
                var emailid = JSON.parse(JSON.stringify(rows.rows))[0].email;
                var name = JSON.parse(JSON.stringify(rows.rows))[0].name;
                var eventname = JSON.parse(JSON.stringify(rows.rows))[0].event_name;
                var ettype = JSON.parse(JSON.stringify(rows.rows))[0].event_type;
                var location = JSON.parse(JSON.stringify(rows.rows))[0].event_location;
                // var loginusername= JSON.parse(JSON.stringify(rows.rows))[0].loginusername;
                // console.log("...............=============================>>>>>>>>>>>>>" + useremailid, name, eventname, ettype, sd(startdate), ed(enddate), location)
                var varlenth = JSON.parse(JSON.stringify(rows.rows)).length;
                var fullUrl = ' http://koneqto.com';
                if (parseInt(varlenth) != 0) {
                    //console.log('hieee');
                    var body =
                        '<table align="center" style="width:600px;height:100px;font-size:13px;font-family:arial"><tr><td  style="border-bottom: #ccc solid 1px;"><a href=' + fullUrl + '><img width="230" src="http://unicsol.com/wp-content/uploads/2018/05/Koneqto-e1525862695762.png"></a></td></tr><tr><td><p style="font-size: 18px;">Hello <b>' + name + '!</b></p><p style="font-size: 20px;" align="center" ><b>Your Event is on Track!</b></p><p style="font-size: 14px" >That’s Amazing from you. Your <b>' + eventname + '</b> event on <b>' + ettype + '</b> which is being held between <b>(' + sd(startdate) + ' to ' + ed(enddate) + ')</b> in <b>' + location + '</b> is succesfully posted. </p><p style="font-size: 14px" >That&#39;s a fantastic effort from you.Keep posting the latest event updates.</p></td></tr><tr><td style="background-color: #ccc; padding: 10px;">Copy Rights By <a style="color:#3b5998" href=' + fullUrl + '>   KONEQTO </a> </td></tr></table>';

                    let mailOptions = {
                        from: nodemailer.email,
                        to: emailid,
                        subject: "Event Post",
                        html: body,

                    };
                    nodemailer.sendMail(mailOptions, function (err, info) {
                        if (err) {
                            uierrorlogger.error("jobpostningjs sendmail..." + err.body);

                        }
                        else {
                            //console.log('Message sent: %s', info.messageId);
                            //console.log(info);
                            // console.log("Success...." + info);
                            // res.status(200).send({
                            //     success: true,
                            //     message: "Email sent sucessfully"
                            // });
                        }
                    });
                }
                else {
                    //console.log("entring into Email does't found case");

                }
            }
        }).fail(function (err) {
            uierrorlogger.error("api sendmail...fail" + err);

        });
    } catch (error) {
        uierrorlogger.error("Error catch expection api sendmail..." + error);

    }

}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// p_registration_id: eval(loginuserid),
// pagesize: psize,
// pagelength: plength
//Getting latest events
router.post('/allsavedevents', function (req, res) {
    console.log("Entering into allsavedevents routes method\n" + req.body.p_registration_id, req.body.pagesize, req.body.pagelength);
    jobpostingdb.allsavedevents(req.body.p_registration_id, req.body.pagesize, req.body.pagelength)
        .then(function (rows) {
            console.log("getallevents Successful")
            //console.log(JSON.stringify(rows))
            if (rows) {
                //console.log("getallevents Sucessful")
                res.send(rows.rows)
            }
            else {
                //console.log("allsavedevents routes" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'allsavedevents routes Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'allsavedevents routes Method Error' + err
            });
        });

});
//interested in event click 
router.post('/interestedinevent', function (req, res) {
    var usermailid = req.body.p_registration_id;
    var registration_id = req.body.registration_id;
    var event_image = req.body.event_image;
    var loginusername = req.body.loginusername;
    var event_name = req.body.event_name;
    var event_description = req.body.event_description;
    var event_location = req.body.eventlocation;
    var event_start_date = req.body.event_start_date;
    var event_end_date = req.body.event_end_date;
    var event_type = req.body.event_type;
    var functional_area = req.body.functional_area;
    var eid = req.body.event_id
    var event_address = req.body.event_address

    console.log("Entering into  interestedinevent" + req.body.event_id, req.body.p_registration_id, req.body.event_name, req.body.type, req.body.event_description, req.body.event_location, req.body.event_start_date, req.body.event_end_date, req.body.event_type, req.body.functional_area, req.body.event_image, event_address);

    jobpostingdb.interestedinevent(req.body.event_id, req.body.p_registration_id, req.body.type)
        .then(function (rows) {
            if (rows) {
                console.log("inetrestedinevent routes Sucessful")
                /*     res.status(200).send({
                      success: true,
                      message: 'Sucessfull'
                  }); */
                res.end(JSON.stringify(rows.rows))
                intrestedeventpostmail(registration_id, loginusername, event_name)
                intrestedeventpostmail1(usermailid, loginusername, event_name, event_description, event_location, event_start_date, event_end_date, event_type, functional_area, event_image, event_address)
            }
            else {
                console.log("interestedinevent falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'interestedinevent routes Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error(" interestedinevent routes failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'interestedinevent routes Method Error' + err
            });
        });

})
//User Intrested event mail  <img width="150" src=' + event_image + ' /> <tr><td><p style="font-size:16px">Event Type : <b>' + event_type + '</b> <br><br>Functional Area :<b>' + functional_area + '</p></td></tr>
function intrestedeventpostmail1(usermailid, loginusername, event_name, event_description, event_location, event_start_date, event_end_date, event_type, functional_area, event_image, event_address) {
    try {
        console.log(" Intrested Event post Email....." + usermailid, loginusername, event_name, event_description, event_location, event_start_date, event_end_date, event_type, functional_area, event_image, event_address);
        jobpostingdb.getIdforEmail(usermailid).then(function (rows) {
            if (rows) {
                var emailid = JSON.parse(JSON.stringify(rows.rows))[0].email;
                var name = JSON.parse(JSON.stringify(rows.rows))[0].name;
                var event_type = JSON.parse(JSON.stringify(rows.rows))[0].event_type;
                // var event_description = JSON.parse(JSON.stringify(rows.rows))[0].event_description;
                var eventlocation = JSON.parse(JSON.stringify(rows.rows))[0].eventlocation;
                var functional_area = JSON.parse(JSON.stringify(rows.rows))[0].fname;
                console.log("...............=============================>>>>>>>>>>>>" + usermailid, loginusername, event_name, event_description, event_location, event_start_date, event_end_date, event_type, functional_area, event_image)
                var varlenth = JSON.parse(JSON.stringify(rows.rows)).length;
                var fullUrl = ' http://koneqto.com';
                if (parseInt(varlenth) != 0) {
                    console.log('hieee');
                    var body =
                        '<table align="center" style="width:700px;height:100px;font-size:13px;font-family:arial"><tr><td style="border-bottom:#ccc solid 1px"><a href=' + fullUrl + '><img width="230" src="http://unicsol.com/wp-content/uploads/2018/05/Koneqto-e1525862695762.png"></a></td> </tr><tr><td><p style="font-size:18px"> Dear <b style=" text-transform: capitalize;">  ' + loginusername + '</b>,</p><p> You may be interested in <b style=" text-transform: capitalize;">' + event_name + '</b>.</p></td></tr><tr><td><table><tr><td style="vertical-align: -webkit-baseline-middle;"><h4>Summary:</h4></td><td><h4>' + event_description + '</h4></td></tr><tr><td style="vertical-align: -webkit-baseline-middle;"><h4>Starts From:</h4></td><td><h4>' + event_start_date + '</h4></td></tr><tr><td style="vertical-align: -webkit-baseline-middle;"><h4>Ends On:</h4></td><td><h4>' + event_end_date + '</h4></td></tr><tr><td style="vertical-align: -webkit-baseline-middle;"><h4>Venue:</h4></td><td><h4>' + event_address + '</h4></td></tr></table></td></tr><tr><td><br><p style="font-size:16px;">Regards, <br> <b>Team Koneqto.</b></p></td></tr><tr><td style="background-color:#ccc;padding:10px;font-size:16px">Copy Rights By <a style="color:#3b5998" href=' + fullUrl + '>KONEQTO </a></td></tr></table>';

                    let mailOptions = {
                        from: nodemailer.email,
                        to: emailid.trim(),
                        subject: "Intrested Event",
                        html: body,
                    };
                    nodemailer.sendMail(mailOptions, function (err, info) {
                        if (err) {
                            uierrorlogger.error("jobpostningjs sendmail..." + err.body);
                        }
                        else {
                            console.log('Message sent: %s', info.messageId);
                            console.log(info);
                            console.log("Success...." + info);

                        }
                    });
                }
                else {
                    console.log("entring into Email does't found case");
                }
            }
        }).fail(function (err) {
            uierrorlogger.error("api sendmail...fail" + err);
        });
    } catch (error) {
        uierrorlogger.error("Error catch expection api sendmail..." + error);

    }

}
//Your Intrested Event mail
function intrestedeventpostmail(registration_id, loginusername, event_name) {
    try {
        console.log("Applied Intrested Event post Email....." + registration_id, loginusername, event_name);
        jobpostingdb.getIdforEmail(registration_id).then(function (rows) {
            if (rows) {
                var emailid = JSON.parse(JSON.stringify(rows.rows))[0].email;
                // var name = JSON.parse(JSON.stringify(rows.rows))[0].name;
                console.log("...............=============================>>>>>>>>>>>>>" + emailid, registration_id, loginusername, event_name)
                var varlenth = JSON.parse(JSON.stringify(rows.rows)).length;
                var fullUrl = ' http://koneqto.com';
                if (parseInt(varlenth) != 0) {
                    console.log('hieee');
                    var body =
                        '<table align="center" style="width: 600px; height: 100px;font-size: 13px; font-family: arial;" table, th, td { ble, th,td { padding: 50px; margin: 130px;><tr><td style="border-bottom: #ccc solid 1px;"><a href=' + fullUrl + '><img width="230" src="http://unicsol.com/wp-content/uploads/2018/05/Koneqto-e1525862695762.png"></a></td></tr><tr><td><br><p style="color:green;font-size: 18px ;text-align: center"><b>Another one to go!</b></p><p style="font-size: 15px"><b style=" text-transform: capitalize;"> ' + loginusername + ' </b> has showed interest in <b style=" text-transform: capitalize;">' + event_name + '.</b></p></td></tr><tr><td><br><p style="font-size:15px;">   Regards, <br> <b> Team Koneqto.</b></p></td></tr><tr><td style="background-color: #ccc; padding: 10px;">Copy Rights By <a style="color:#3b5998" href=' + fullUrl + '> KONEQTO </a> </td></tr></table>';

                    let mailOptions = {
                        from: nodemailer.email,
                        to: emailid,
                        subject: "Intrested Event",
                        html: body,
                    };
                    nodemailer.sendMail(mailOptions, function (err, info) {
                        if (err) {
                            uierrorlogger.error("jobpostningjs sendmail..." + err.body);

                        }
                        else {
                            console.log('Message sent: %s', info.messageId);
                            console.log(info);
                            console.log("Success...." + info);
                        }
                    });
                }
                else {
                    console.log("entring into Email does't found case");

                }
            }
        }).fail(function (err) {
            uierrorlogger.error("api sendmail...fail" + err);

        });
    } catch (error) {
        uierrorlogger.error("Error catch expection api sendmail..." + error);

    }


}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//uninterestedinevent in event click
router.post('/uninterestedinevent', function (req, res) {
    var usermailid = req.body.p_registration_id;

    var registration_id = req.body.registration_id;
    console.log("Entering into  uninterestedinevent" + req.body.event_id, req.body.p_registration_id, req.body.type, req.body.active);
    // p_registration_id ,p_event_name,p_event_type ,p_envet_desription ,p_functional_area,p_event_time
    jobpostingdb.uninterestedinevent(req.body.event_id, req.body.p_registration_id, req.body.type, req.body.active)
        .then(function (rows) {
            if (rows) {
                console.log("uninterestedinevent routes Sucessful")
                /*     res.status(200).send({
                        success: true,
                        message: 'Sucessfull'
                    }); */
                res.end(JSON.stringify(rows.rows))
            }
            else {
                console.log("uninterestedinevent falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'uninterestedinevent routes Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error(" uninterestedinevent routes failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'uninterestedinevent routes Method Error' + err
            });
        });
})
//Getting my events
// router.get('/mysavedevents', function (req, res) {
//     //console.log("Entering into mysavedevents routes method", req.query.p_registration_id);
//     jobpostingdb.mysavedevents(req.query.p_registration_id)
//         .then(function (rows) {
//             if (rows) {
//                 res.end(JSON.stringify(rows.rows))
//             }
//             else {
//                 //console.log("mysavedevents routes" + err.body)
//                 res.status(500).send({
//                     success: false,
//                     message: 'mysavedevents routes Method Error' + err
//                 });
//             }
//         }).fail(function (err) {
//             res.status(500).send({
//                 success: false,
//                 message: 'mysavedevents routes Method Error' + err
//             });
//         });
// });


router.post('/mysavedevents', function (req, res, next) {
    console.log("mysavedevents data" + req.query.p_registration_id+" "+req.query.search+ " " + req.query.pagesize+" " + req.body.pagelength);
    jobpostingdb.mysavedevents(req.body.loginuserid, req.body.search,req.body.pagesize,req.body.pagelength)
        .then(function (rows) {
            if (rows) {
                res.send(rows.rows);
            }
            else {
                console.log("mysavedevents falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'mysavedevents  Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error(" mysavedevents routes failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'mysavedevents routes Method Error' + err
            });
        });
})
//Deleting my event
router.post('/deletemyevent', function (req, res) {
    console.log("Entering into  deletemyevent jobposting.js" + req.body.event_id, req.body.eventimage, req.body.eventvideo);
    jobpostingdb.deletemyevent(req.body.event_id, req.body.eventimage)
        .then(function (rows) {
            if (rows) {

                eventimage = req.body.eventimage;
                // achievementimages = achievements.split(' ');
                // console.log("images===", achievementimages)
                // for (var i = 0; i < achievements.length - 1; i++) {
                // fs.unlink(EventImage +'/'+ eventimage, (err) => {
                //     if (err) throw err;
                // },                    
                //     console.log("deleted the image" + eventimage)
                // );
                eventvideo = req.body.eventvideo
                // if(req.body.eventvideo){
                //     fs.unlink(EventVideo +'/'+ eventvideo, (err) => {
                //         if (err) throw err;
                //     },                    
                //         console.log("deleted the video" + eventvideo)
                //     );
                // }

                // }

                console.log("deletemyevent routes Sucessful")
                res.status(200).send({
                    success: true,
                    message: 'Sucessfull'
                });
            }
            else {
                console.log("deletemyevent failed" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'deletemyevent routes Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error(" deletemyevent routes failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'deletemyevent routes Method Error' + err
            });
        });
})
//Get interested users for my events
router.get('/getmyeventappliedusers', function (req, res) {
    console.log("Entering into getmyeventappliedusers routes method ", req.query.p_type_id);

    jobpostingdb.getmyeventappliedusers(req.query.p_type_id)
        .then(function (rows) {
            if (rows) {

                res.end(JSON.stringify(rows.rows))
                console.log("getmyeventappliedusers data entering into routes... " + rows)
            }
            else {

                console.log("getmyeventappliedusers routes" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'getmyeventappliedusers routes Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'getmyeventappliedusers routes Method Error' + err
            });
        });
});
//Getting events interested by me
router.get('/getappliedevents', function (req, res) {
    console.log("Entering into getappliedevents routes method", req.query.p_registration_id);

    jobpostingdb.getappliedevents(req.query.p_registration_id)
        .then(function (rows) {
            if (rows) {

                res.end(JSON.stringify(rows.rows))
                console.log("getappliedevents data entering into routes... " + rows)
            }
            else {

                console.log("getappliedevents routes" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'getappliedevents routes Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'getappliedevents routes Method Error' + err
            });
        });
});
//charity image
var chstorage = multer.diskStorage({ //multers disk storage settings
    destination: function (req, file, cb) {
        cb(null, charityimage);
    },
    filename: function (req, file, cb) {
        console.log("original name--", file.originalname)
        //console.log("original file name--",file.filename)
        var datetimestamp = Date.now();
        cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1]);
    }
});
var chupload = multer({ //multer settings
    storage: chstorage
}).single('file');
// var charityimages = "";
// router.post('/charityimgposts', function (req, res) {
//     chupload(req, res, function (err) {
//         console.log("req.file", req.file);
//         if (err) {
//             res.json({ error_code: 1, err_desc: err });
//             return;
//         }
//         res.json({ error_code: 0, err_desc: null });
//         //console.log("path ---",req.file.path)
//         //var path=JSON.stringify(req.file.filename);

//         charityimages = charityimages + req.file.filename + " "


//     });
// });

router.post('/updateadvice', function (req, res) {
    console.log("Entering into deleteexpertAdviceonly routes method ----------->" + req.body.advice_options, req.body.functional_area, req.body.advice_message, req.body.id);

    jobpostingdb.updateadvice(req.body.advice_options, req.body.functional_area, req.body.advice_message, req.body.id)
        .then(function (rows) {
            if (rows) {

                res.end(JSON.stringify(rows.rows))


            }
            else {

                console.log("deleteexpertAdviceonly routes" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'deleteexpertAdviceonly routes Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'deleteexpertAdviceonly routes Method Error' + err
            });
        });

});
router.post('/deleteexpertAdviceonly', function (req, res) {
    console.log("Entering into deleteexpertAdviceonly routes method ----------->" + req.Id);

    jobpostingdb.deleteexpertAdviceonly(req.body.Id)
        .then(function (rows) {
            if (rows) {

                res.end(JSON.stringify(rows.rows))


            }
            else {

                console.log("deleteexpertAdviceonly routes" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'deleteexpertAdviceonly routes Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'deleteexpertAdviceonly routes Method Error' + err
            });
        });

});
///////////////////////////////
router.get('/getcharitydetails', function (req, res) {

    console.log("Entering into getcharitydetails>>>> method", req.query.loginuserid);
    jobpostingdb.getcharitydetails(req.query.loginuserid)
        .then(function (rows) {
            if (rows) {
                console.log("getcharitydetails Sucessful")
                res.send(rows.rows)
            }
            else {
                console.log("getcharitydetails falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'get present emp Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error("getcharitydetails failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'getcharitydetails Method Error' + err
            });
        });
});
//////////////////////////////// Post charity /////////////////////
router.post('/postcharity', function (req, res) {
    var usermailid = req.body.loginuserid;
    var name = req.body.name;
    var charityname = req.body.charityname;
    console.log('save image')
    var type = []
    var achievimage = []
    type = req.body.type
    achievimage = req.body.supporting_image

    var pt1 = [];
    if (req.body.supporting_image) {
        for (let i = 0; i < type.length; i++) {

            console.log('save image', type[i])
            // console.log('save image', achievimage[i])

            var dt = new Date();

            ptr = req.body.loginusername + '-' + dt.getFullYear() + "" + dt.getMonth() + "" + dt.getMilliseconds() + "" + i + '.' + type[i];
            console.log(ptr, "ptr")
            pts = './public/charity/' + ptr;
            fs.writeFile(pts, achievimage[i], 'base64', (err) => {
                if (err)
                    console.log(err)
                else {

                    console.log('Image Saved Success...');
                }

            })
            pt1.push(ptr)
            console.log(pt1, "pt1")
        }
    }


    // console.log("entering into Charity Posting Method" +
    //     req.body.charityname,
    //     req.body.charitycategory,
    //     req.body.address,
    //     req.body.charityidproof,
    //     req.body.mobilenumber,
    //     req.body.startdate,
    //     req.body.enddate,
    //     req.body.charityfund,
    //     req.body.termsandconditions,
    //     req.body.charitydescription,
    //     req.body.volunteersnames,
    //     req.body.volunteersmobile,
    //     req.body.supporting_documents,
    //     charityimages,
    //     req.body.supporting_video,
    //     req.body.charitysubcategory,
    //     req.body.loginuserid,
    //     req.body.account_number,
    //     req.body.account_name,
    //     req.body.account_type,
    //     req.body.ifsc_code,
    //     req.body.firstname,
    //     req.body.bankname


    // );

    jobpostingdb.postcharity(
        req.body.charityname,
        req.body.charitycategory,
        req.body.address,
        req.body.charityidproof,
        req.body.mobilenumber,
        req.body.startdate,
        req.body.enddate,
        req.body.charityfund,
        req.body.termsandconditions,
        req.body.charitydescription,
        req.body.volunteersnames,
        req.body.volunteersmobile,
        req.body.supporting_documents,
        pt1,
        req.body.supporting_video,
        req.body.charitysubcategory,
        req.body.loginuserid,
        req.body.account_number,
        req.body.account_name,
        req.body.account_type,
        req.body.ifsc_code,
        req.body.firstname,
        req.body.bankname).then(function (rows) {
            if (rows) {
                res.status(200).send({
                    success: true,
                    message: 'Sucessfull'
                });
                chanamedImages = ''
                charityimages = ''
                // charityimages
                sendmailforcharitypost(usermailid, name, charityname)

            }
            else {
                res.status(500).send({
                    success: false,
                    message: 'Post Charity Method Error' + err
                });
                chanamedImages = ''
                charityimages = ''

            }
        }).fail(function (err) {
            uierrorlogger.error(" Post Charity failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'Post Charity Method Error' + err
            });
            chanamedImages = ''
            charityimages = ''
        });
});
//mail for post charity
function sendmailforcharitypost(usermailid, name, charityname) {
    try {
        console.log(" Job post Email....." + usermailid, name, charityname);
        jobpostingdb.getIdforEmail(usermailid, name).then(function (rows) {
            if (rows) {
                var emailid = JSON.parse(JSON.stringify(rows.rows))[0].email;
                var name = JSON.parse(JSON.stringify(rows.rows))[0].name;
                console.log("...............=============================>>>>>>>>>>>>>" + usermailid, name, charityname)
                var varlenth = JSON.parse(JSON.stringify(rows.rows)).length;
                var fullUrl = ' http://koneqto.com';
                if (parseInt(varlenth) != 0) {
                    console.log('hieee');
                    var body =
                        '<table align="center" style="width:600px;height:100px;font-size:13px;font-family:arial"><tr><td  style="border-bottom: #ccc solid 1px;"><a href=' + fullUrl + '><img width="230" src="http://unicsol.com/wp-content/uploads/2018/05/Koneqto-e1525862695762.png"></a></td></tr><tr><td><p style="font-size: 18px;">Hi <b>' + name + '</b>,</p><p style="font-size: 14px" >Thank you!</p><p style="font-size: 14px" >Your charity on <b>' + charityname + '</b> is successfully posted. Now, people can view and reach you.</p><p style="font-size: 14px" >Keep Posting!</p><br><p style="font-size: 14px">Regards,</p><p style="font-size: 14px"><b>Team Koneqto.</b></p></td></tr><tr><td style="background-color: #ccc; padding: 10px;"> Copy Rights By <a style="color:#3b5998" href=' + fullUrl + '>KONEQTO</a></td></tr></table>';

                    let mailOptions = {
                        from: nodemailer.email,
                        to: emailid,
                        subject: "Charity post",
                        html: body,
                    };
                    nodemailer.sendMail(mailOptions, function (err, info) {
                        if (err) {
                            uierrorlogger.error("charitypostningjs sendmail..." + err.body);

                        }
                        else {
                            console.log('Message sent: %s', info.messageId);
                            console.log(info);
                            console.log("Success...." + info);
                            // res.status(200).send({
                            //     success: true,
                            //     message: "Email sent sucessfully"
                            // });
                        }
                    });
                }
                else {
                    console.log("entring into Email does't found case");
                    res.status(200).send({
                        success: true,
                        message: "Email sent failed, Error in sending email"
                    });

                }
            }

        }).fail(function (err) {
            uierrorlogger.error("charitypostingjs sendmail...fail" + err);

        });
    } catch (error) {
        uierrorlogger.error("Error catch expection charitypostingjs sendmail..." + error);

    }

}
//////////////////////////////////////////////////////////////////
router.get('/loadcharitycategories', function (req, res) {
    console.log("Entering into loadcharitycategories methode");

    jobpostingdb.loadcharitycategories()
        .then(function (rows) {
            if (rows) {
                res.end(JSON.stringify(rows.rows))
            }
            else {
                console.log("loadcharitycategories" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'loadcharitycategories api Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'loadcharitycategories api Method Error' + err
            });
        });

});
router.get('/getallcharityposts', function (req, res) {
    console.log("Entering into getallcharityposts methode");

    jobpostingdb.getallcharityposts()
        .then(function (rows) {
            if (rows) {
                res.end(JSON.stringify(rows.rows))
            }
            else {
                console.log("getallcharityposts" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'getallcharityposts api Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'getallcharityposts api Method Error' + err
            });
        });
});
router.get('/getmycharityposts', function (req, res) {
    console.log("Entering into getmycharityposts methode", req.query.loginuserid);

    jobpostingdb.getmycharityposts(req.query.loginuserid)
        .then(function (rows) {
            if (rows) {
                res.end(JSON.stringify(rows.rows))
            }
            else {
                console.log("getmycharityposts" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'getmycharityposts api Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'getmycharityposts api Method Error' + err
            });
        });
});
router.get('/getallcharitypostsview', function (req, res) {

    console.log("Entering into getallcharitypostsview methode" + req.query.charity_id);

    jobpostingdb.getallcharityPostsview(req.query.charity_id)
        .then(function (rows) {
            if (rows) {
                res.end(JSON.stringify(rows.rows))
            }
            else {
                console.log("getallcharitypostsview" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'getallcharitypostsview api Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'getallcharitypostsview api Method Error' + err
            });
        });
});
router.post('/savedonaters', function (req, res) {
    console.log("entering into savedonaters Method" +
        req.body.loginuserid,
        req.body.donatorsname,
        req.body.donatorsmobilenumber,
        req.body.donatoremail,
        req.body.donatoraddress,
        req.body.donationamount,
        req.body.frequentlydonation,
        req.body.supportingfor,
        req.body.comments,
    );

    jobpostingdb.savedonaters(
        req.body.loginuserid,
        req.body.donatorsname,
        req.body.donatorsmobilenumber,
        req.body.donatoremail,
        req.body.donatoraddress,
        req.body.donationamount,
        req.body.frequentlydonation,
        req.body.supportingfor,
        req.body.comments,

    ).then(function (rows) {
        if (rows) {
            res.status(200).send({
                success: true,
                message: 'Sucessfull'
            });
        }
        else {
            res.status(500).send({
                success: false,
                message: 'Post savedonaters Method Error' + err
            });
        }
    }).fail(function (err) {
        uierrorlogger.error(" Post savedonaters failed..." + err.body);
        res.status(500).send({
            success: false,
            message: 'Post savedonaters Method Error' + err
        });
    });
});
router.get('/loadsubcharitydropdown', function (req, res) {
    console.log("Entering into getallcharityposts methode");

    jobpostingdb.loadsubcharitydropdown(req.query.charity_category_id)
        .then(function (rows) {
            if (rows) {
                res.end(JSON.stringify(rows.rows))
            }
            else {
                console.log("loadsubcharitydropdown" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'loadsubcharitydropdown api Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'loadsubcharitydropdown api Method Error' + err
            });
        });
});
router.get('/geteventsearch', function (req, res) {
    console.log("Entering into geteventsearch routes method", req.query.p_skills, req.query.p_location, req.query.p_event_date);

    jobpostingdb.geteventsearch(req.query.p_skills, req.query.p_location, req.query.p_event_date)
        .then(function (rows) {
            if (rows) {

                res.end(JSON.stringify(rows.rows))


            }
            else {

                console.log("geteventsearch routes" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'geteventsearch routes Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'geteventsearch routes Method Error' + err
            });
        });

});
router.get('/loadfriends', function (req, res) {
    console.log("Entering into savedevents routes method", req.query.p_registration_id);

    jobpostingdb.loadfriends(req.query.p_registration_id)
        .then(function (rows) {
            if (rows) {

                res.end(JSON.stringify(rows.rows))


            }
            else {

                console.log("savedevents routes" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'savedevents routes Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'savedevents routes Method Error' + err
            });
        });

});
router.get('/phonecode', function (req, res) {
    //console.log("Entering into city routes method");
    country_id = req.query.country_id
    jobpostingdb.getpPhonecode(country_id)
        .then(function (rows) {
            if (rows) {

                res.end(JSON.stringify(rows.rows))


            }
            else {

                //console.log("city routes" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'getphonecode routes Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'getphonecode routes Method Error' + err
            });
        });

})

// job search by id for mobile api
router.post('/searchandroidid', function (req, res) {
    console.log("Entering into  searchid" + req.body.p_posting_id);
    jobpostingdb.jobsearchandroidid(req.body.p_posting_id)
        .then(function (rows) {
            if (rows) {

                res.send(rows.rows);

            }
            else {
                //console.log("jobsearch falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'jobsearch  Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error(" jobsearch routes failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'jobsearch routes Method Error' + err
            });
        });

})


router.post('/searchid', function (req, res) {
    console.log("Entering into  searchid" + req.body.p_registration_id, req.body.skill_name01, req.body.skill_name02, req.body.skill_name03, req.body.skill_name04, req.body.p_jobid);
    jobpostingdb.jobsearchid(req.body.p_registration_id, req.body.skill_name01, req.body.skill_name02, req.body.skill_name03, req.body.skill_name04, req.body.p_jobid)
        .then(function (rows) {
            if (rows) {

                res.send(rows.rows);

            }
            else {
                //console.log("jobsearch falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'jobsearch  Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error(" jobsearch routes failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'jobsearch routes Method Error' + err
            });
        });

})



router.post('/jobsearch', function (req, res) {
    ////console.log("Entering into  intrestedinevent"+req.body.event_id,req.body.p_registration_id,req.body.type);
    // p_registration_id ,p_event_name,p_event_type ,p_envet_desription ,p_functional_area,p_event_time
    jobpostingdb.jobsearch(req.body.skills, req.body.loaction, req.body.experience, req.body.maxsalary, req.body.p_registration_id, req.body.pagesize, req.body.pagelength)
        .then(function (rows) {
            if (rows) {

                res.send(rows.rows);

            }
            else {
                //console.log("jobsearch falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'jobsearch  Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error(" jobsearch routes failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'jobsearch routes Method Error' + err
            });
        });

})
router.get('/appliedjobs', function (req, res) {
    // //console.log("Entering into appliedjobs routes method", req.query.p_registration_id);

    jobpostingdb.loadfriends(req.query.p_registration_id)
        .then(function (rows) {
            if (rows) {

                res.end(JSON.stringify(rows.rows))


            }
            else {

                //console.log("appliedjobs routes" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'appliedjobs routes Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'appliedjobs routes Method Error' + err
            });
        });

});
//getApplyjob
router.get('/getApplyjob', function (req, res, next) {
    try {
        console.log("getApplyjob data" + req.query.jobid);
        jobpostingdb.getapply(req.query.loginuserid, req.query.jobid).then(function (rows) {
            if (rows) {
                res.end(JSON.stringify(rows.rows));
            }
        }).fail(function (err) {
            uierrorlogger.error("api getApplyjob..." + err.body);
            //next(Errormessage("Create New-Password ", err));
        });
    } catch (error) {
        uierrorlogger.error("Error catch expection api getApplyjob..." + error);
        next(Errormessage("Create New-Pass.loginword", error));
    }
});

router.get('/appliedevents', function (req, res) {
    console.log("Entering into appliedevents routes method", req.query.p_registration_id);

    jobpostingdb.loadfriends(req.query.p_registration_id)
        .then(function (rows) {
            if (rows) {

                res.end(JSON.stringify(rows.rows))


            }
            else {

                console.log("appliedevents routes" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'appliedevents routes Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'appliedevents routes Method Error' + err
            });
        });

});


//////////////////////////////Expert Advice////////////////////////////////
//get expert advice
router.post('/getexpertadvice', function (req, res) {
    console.log("Entering into getexpertadvice routes method ----------->" + req.body.p_registration_id, req.body.pagesize, req.body.pagelength);

    jobpostingdb.getexpertadvice(req.body.p_registration_id, req.body.pagesize, req.body.pagelength)
        .then(function (rows) {
            if (rows) {

                res.end(JSON.stringify(rows.rows))


            }
            else {

                console.log("getexpertadvice routes" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'getexpertadvice routes Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'getexpertadvice routes Method Error' + err
            });
        });

});

//post expert advice
router.post('/advice', function (req, res) {
    var emailid = req.body.p_registration_id;
    var name = req.body.name;
    var options = req.body.options;

    console.log("Entering into  Advice api" + req.body.p_registration_id, req.body.advice_options, req.body.functional_area, req.body.advice_message);
    jobpostingdb.saveAdvice(req.body.p_registration_id, req.body.advice_options, req.body.functional_area, req.body.advice_message)
        .then(function (rows) {
            if (rows) {
                res.send(rows.rows);
                expertadviceemail(emailid, name, options);
            }
            else {
                console.log("Message saving  falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'Advice message  Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error(" Advice Message routes failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'advice message routes Method Error' + err
            });
        });

});
//Mail function for posting Expert Advice.....
function expertadviceemail(emailid, name, options) {
    try {
        console.log("###########Expert Advice###########" + emailid, name, options);
        jobpostingdb.getIdforEmailadvice(emailid).then(function (rows) {
            if (rows) {
                var emailid = JSON.parse(JSON.stringify(rows.rows))[0].email;
                // var id = JSON.parse(JSON.stringify(rows.rows))[0].id;
                var name = JSON.parse(JSON.stringify(rows.rows))[0].name;
                var options = JSON.parse(JSON.stringify(rows.rows))[0].options;
                var varlenth = JSON.parse(JSON.stringify(rows.rows)).length;
                var fullUrl = ' http://koneqto.com';

                console.log("-------------------" + name, options, emailid)
                if (parseInt(varlenth) != 0) {
                    console.log("Entering into Expert Advice sending mail");
                    var body = '<table align="center" style="width:600px;height:100px;font-size:13px;font-family:arial"><tr><td  style="border-bottom: #ccc solid 1px;"><img width="230" src="http://unicsol.com/wp-content/uploads/2018/05/Koneqto-e1525862695762.png"></td> </tr><tr><td><p style="font-size: 18px;">Hello <b>' + name + '!</b></p><p style="font-size: 20px;" align="center" ><b>Mission Succesful</b></p> <p style="font-size: 14px" >It&#39;s been Amazing from you. Your advice on <b>' + options + '</b>  is succesfully posted </p><p style="font-size: 14px" >we really hope that the advice posted by you will enrich people to choose the right career path</p></td></tr><tr><td style="background-color: #ccc; padding: 10px;">Copy Rights By <a style="color:#3b5998" href=' + fullUrl + '>   KONEQTO </a> </td></tr></table>';
                    let mailOptions = {
                        from: nodemailer.email,
                        to: emailid,
                        subject: "Expert Advice",
                        html: body,
                    };
                    nodemailer.sendMail(mailOptions, function (err, info) {
                        if (err) {
                            uierrorlogger.error("api sendmail..." + err.body);
                        }
                        else {
                            console.log('Message sent: %s', info.messageId);
                            console.log(info);
                            console.log("Success...." + info);
                        }
                    });
                }
                else {
                    console.log("entring into Email does't found case");
                }
            }
        }).fail(function (err) {
            uierrorlogger.error("api sendmail...fail" + err);
        });
    } catch (error) {
        uierrorlogger.error("Error catch expection api sendmail..." + error);
    }
}
//////////////////////////////////////////////////////////////////////////





//////////////////getApplied
router.get('/getapplied', function (req, res, next) {
    try {
        //  //console.log("get applied data" + req.query.jobid);
        jobpostingdb.getapplied(req.query.jobid).then(function (rows) {
            if (rows) {
                res.end(JSON.stringify(rows.rows));
            }
        }).fail(function (err) {
            uierrorlogger.error("api get applied..." + err.body);

        });
    } catch (error) {
        uierrorlogger.error("Error catch expection api applied users..." + error);

    }
});
router.get('/getshortlist', function (req, res, next) {
    try {
        //console.log("get shortlist data" + req.query.jobid);
        jobpostingdb.getshortlist(req.query.jobid).then(function (rows) {
            if (rows) {
                res.end(JSON.stringify(rows.rows));
            }
        }).fail(function (err) {
            // uierrorlogger.error("api get shortlist..." + err.body);

        });
    } catch (error) {
        //uierrorlogger.error("Error catch expection api shortlist users..." + error);

    }
});
router.get('/scheduledlist', function (req, res, next) {
    try {
        //console.log("get scheduledlist data" + req.query.jobid);
        jobpostingdb.scheduledlist(req.query.jobid).then(function (rows) {
            if (rows) {
                res.end(JSON.stringify(rows.rows));
            }
        }).fail(function (err) {
            uierrorlogger.error("api get scheduledlist..." + err.body);

        });
    } catch (error) {
        uierrorlogger.error("Error catch expection api scheduledlist users..." + error);

    }
});

router.get('/progressed', function (req, res, next) {
    try {
        console.log("get progressed data" + req.query.jobid);
        jobpostingdb.progressed(req.query.jobid).then(function (rows) {
            if (rows) {
                res.end(JSON.stringify(rows.rows));
            }
        }).fail(function (err) {
            uierrorlogger.error("api get progressed..." + err.body);

        });
    } catch (error) {
        uierrorlogger.error("Error catch expection api progressed users..." + error);

    }
});



/* ========================jobposting============================= */

router.get('/appliedjobs', function (req, res) {
    // //console.log("Entering into appliedjobs routes method", req.query.p_registration_id);

    jobpostingdb.loadfriends(req.query.p_registration_id)
        .then(function (rows) {
            if (rows) {

                res.end(JSON.stringify(rows.rows))


            }
            else {

                //console.log("appliedjobs routes" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'appliedjobs routes Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'appliedjobs routes Method Error' + err
            });
        });

});


////////////////////////// shortlisting Email////////////////////////////////////////////
router.post('/shortlist', function (req, res) {
    var usermailid = req.body.element;
    var email = req.body.email;
    var name = req.body.name;
    var jobtitle = req.body.jobtitle;
    var mobile_no = req.body.mobile_no
    //console.log('jobposting js' + req.body.userid, req.body.selected)
    jobpostingdb.shortlist(req.body.jobid, req.body.element)

        .then(function (rows) {
            if (rows) {

                res.end(JSON.stringify(rows.rows));
                for (var i = 0; i < usermailid.length; i++) {
                    shortlistemail(usermailid[i], email[i], name[i], jobtitle);
                    shortlistSms(mobile_no[i], jobtitle, name[i]);
                }

                console.log("************************************************************************************************************************")

            }
            else {
                //console.log("ShortList falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'shortlist  Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error(" shortlist routes failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'shortlist routes Method Error' + err
            });
        });

})
const shortlistSms = async (mobile_no, jobtitle, name) => {

    const apiInstance = new TwoFactor('96ab36da-f79d-11e8-a895-0200cd936042');


    try {

        const response = await apiInstance.Transactional.sendMessage({
            to: mobile_no,
            // name: name[index],
            from: 'KONQTO',
            // templateName: 'open',
            templateType: apiInstance.Transactional.TemplateTypes.dynamic,

            templateName: 'shortlisting',
            var1: name,
            var2: jobtitle
            // date:toString( Date()),
            // job_title: toString(jobtitle)
        });



    } catch (error) {

        console.log('Error: ', error);

    }

};
// const testFunction = async (mobile_no,jobtitle,name) => {
// console.log(mobile_no,jobtitle,name)
//     const apiObject = new TwoFactor('96ab36da-f79d-11e8-a895-0200cd936042');

//     try {

//             const response = await apiObject.Transactional.sendMessage({
//                 to: '7097158384',
//                 name: name[index],
//                 from: 'Koneqto',
//                 templateName: 'Shortlist',
//                 date: Date(),
//                 job_title: jobtitle
//             }).then(console.log (response)).catch(console.error);



//         // const response = await apiObject.OTP.sendOtp({
//         //     phoneNumber: "" + req.body.CountryCode + req.body.phonenumber,
//         //     template: 'koneqto_new',
//         //     deliveryType: apiObject.OTP.DeliveryTypes.sms

//         //     // deliveryType: apiInstance.DeliveryTypes.voice
//         // });
//         // res.send({
//         //     success: true,
//         //     sessionId: response.Details
//         // });
//         // console.log(response.Details);
//         // console.log(apiObject)

//     } catch (error) {
//         console.log('Error: ', error);
//     }
// }

function shortlistemail(usermailid, email, name, jobtitle) {
    try {
        console.log("Email sent sucessfully" + usermailid, email, name, jobtitle);
        jobpostingdb.getIdforEmail(usermailid).then(function (rows) {
            if (rows) {
                var emailid = JSON.parse(JSON.stringify(rows.rows))[0].email;
                var name = JSON.parse(JSON.stringify(rows.rows))[0].name;
                console.log("...............=============================>>>>>>>>>>>>>" + usermailid, email, name, jobtitle)
                var varlenth = JSON.parse(JSON.stringify(rows.rows)).length;
                var fullUrl = ' http://koneqto.com';
                if (parseInt(varlenth) != 0) {
                    console.log('hieee');
                    var body =
                        '<table align="center" style="width:600px;height:100px;font-size:13px;font-family:arial"><tr><td style="border-bottom:#ccc solid 1px"><a href=' + fullUrl + '><img width="230" src="http://unicsol.com/wp-content/uploads/2018/05/Koneqto-e1525862695762.png"></a></td></tr><tr><td><p style="font-size:18px"><b>Dear ' + name + ',</b></p><p style="color:green;font-size:18px" align="center"><b>Congratulations!!!</b></p><p style="font-size:18px">You Are Shortlisted For <b style=" text-transform: capitalize;">' + jobtitle + '.</b> </p></td></tr><tr><td><br><p style="font-size:15px;">   Regards, <br> <b> Team Koneqto.</b></p></td></tr><tr><td style="background-color:#ccc;padding:10px">Copy Rights By <a style="color:#3b5998" href=' + fullUrl + '>KONEQTO</a></td></tr></table>';

                    let mailOptions = {
                        from: nodemailer.email,
                        to: emailid,
                        subject: "Short Listed",
                        html: body,
                    };
                    nodemailer.sendMail(mailOptions, function (err, info) {
                        if (err) {
                            uierrorlogger.error("jobpostningjs sendmail..." + err.body);

                        }
                        else {
                            console.log('Message sent: %s', info.messageId);
                            console.log(info);
                            console.log("Success...." + info);
                            // res.status(200).send({
                            //     success: true,
                            //     message: "Email sent sucessfully"
                            // });
                        }
                    });
                }
                else {
                    console.log("entring into Email does't found case");
                    res.status(200).send({
                        success: true,
                        message: "Email doesn't found, please enter correct email"
                    });

                }
            }

        }).fail(function (err) {
            uierrorlogger.error("api sendmail...fail" + err);

        });
    } catch (error) {
        uierrorlogger.error("Error catch expection api sendmail..." + error);

    }
}
///////////////////////////////////////////////////////////////////////////////////////////


////////////////////////// scheduling interview/////////////////////////////////////////////

router.post('/schedule', function (req, res, next) {
    var usermailid = req.body.schedule_user;
    var contactperson = req.body.interviewer;
    var date = req.body.schedule_date;
    var time = req.body.schedule_time;
    var type = req.body.type_mode_name;
    var mode = req.body.interview_mode_name;
    var title = req.body.title
    //var interviewer =req.body.interviewer;
    var location = req.body.location
    var sid = req.body.schedulejobid
    var mobile_no = req.body.mobile_no
    var name = req.body.name
    // var name = req.body.name

    try {

        console.log("schedule interview data" +
            req.body.title,
            req.body.schedule_date,
            req.body.schedule_time,
            req.body.interviewer,
            req.body.type,
            req.body.location,
            req.body.mode,
            req.body.notify_me,
            req.body.created_by,
            req.body.schedule_user,
            req.body.schedulejobid)
        jobpostingdb.schedule(
            req.body.title,
            req.body.schedule_date,
            req.body.schedule_time,
            req.body.interviewer,
            req.body.type,
            req.body.location,
            req.body.mode,
            req.body.notify_me,
            req.body.created_by,
            req.body.schedule_user,
            req.body.schedulejobid).then(function (rows) {
                if (rows) {
                    res.end(JSON.stringify(rows.rows));
                    for (var i = 0; i < usermailid.length; i++) {
                        schedulemail(usermailid[i], date, time, location, mode, type, contactperson, title, sid)
                        scheduleSms(mobile_no[i], title, name[i], date, time, mode, type, contactperson, location)
                    }
                }
            }).fail(function (err) {
                uierrorlogger.error("api schedule..." + err.body);

            });
    } catch (error) {
        uierrorlogger.error("Error catch expection api schedule..." + error);

    }
});
function schedulemail(usermailid, date, time, location, mode, type, contactperson, title, sid) {
    try {
        console.log(" scheduling interview Email....." + usermailid, date, time, location, mode, type, contactperson, title, sid);
        jobpostingdb.getEmailIdInterviewSh(usermailid, sid).then(function (rows) {
            if (rows) {
                //function to format date
                // var sd = formattedate;
                // // var ed = formattedate;
                // function formattedate(date) {
                //     var d = new Date(date),
                //         month = '' + (d.getMonth() + 1),
                //         day = '' + d.getDate(),
                //         year = d.getFullYear();
                //     if (month.length < 2) month = '0' + month;
                //     if (day.length < 2) day = '0' + day;
                //     this.formattedate = [day, month, year].join('-')
                //     return this.formattedate;


                // }
                // console.log("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"+sd(date))
                var emailid = JSON.parse(JSON.stringify(rows.rows))[0].email;
                var name = JSON.parse(JSON.stringify(rows.rows))[0].name;
                var mode = JSON.parse(JSON.stringify(rows.rows))[0].interview_mode_name;
                var date = JSON.parse(JSON.stringify(rows.rows))[0].schedule_date;
                var time = JSON.parse(JSON.stringify(rows.rows))[0].schedule_time;
                var type = JSON.parse(JSON.stringify(rows.rows))[0].type_mode_name;
                console.log("...............=============================>>>>>>>>>>>>>" + usermailid, date, time, location, mode, type, contactperson, title)
                var varlenth = JSON.parse(JSON.stringify(rows.rows)).length;
                var fullUrl = ' http://koneqto.com';
                if (parseInt(varlenth) != 0) {
                    console.log('hieee');
                    var body =
                        '<table align="center" style="width:800px;height:100px;font-size:13px;font-family:arial"><tr><td style="border-bottom: #ccc solid 1px;"><a href=' + fullUrl + '><img width="230" src="http://unicsol.com/wp-content/uploads/2018/05/Koneqto-e1525862695762.png"></a></td></tr><tr><td><p style="font-size: 18px;">Dear <b>' + name + ',</b></p><p style="font-size: 14px">Greetings from <b style=" text-transform: capitalize;"> ' + contactperson + ',</b></p><p style="font-size: 14px">Hi we are glad to inform you that you are one step ahead. Your profile seems like a good fit for the position of <b style=" text-transform: capitalize;">' + title + '.</b> We’d like to schedule an interview to know you better.</p><p style="font-size: 14px">Your first round of interview for <b style=" text-transform: capitalize;"> ' + title + ' </b> has been scheduled as follows:</p><table style="border-collapse: collapse;"><tr><td style="vertical-align: -webkit-baseline-middle;"><h4 style="vertical-align: -webkit-baseline-middle;">Date of Inteview:</h4></td><td style="vertical-align: -webkit-baseline-middle;"><h4>' + date + '</h4></td></tr><tr><td style="vertical-align: -webkit-baseline-middle;"><h4>Time:</h4></td><td><h4>' + time + '</h4></td></tr><tr><td style="vertical-align: -webkit-baseline-middle;"><h4>Contact Person:</h4></td><td><h4 style=" text-transform: capitalize;">' + contactperson + '</h4></td></tr><tr><td style="vertical-align: -webkit-baseline-middle;"><h4>Venue:</h4></td><td><h4 style=" text-transform: capitalize;">' + location + '</h4><br></td></tr><tr><td style="vertical-align: -webkit-baseline-middle;"><h4>Mode of Interview:</h4></td><td><h4>' + mode + '</h4></td></tr><tr><td style="vertical-align: -webkit-baseline-middle;"><h4>Type of Interview:</h4></td><td><h4>' + type + '</h4></td></tr></table><br><p style="font-size: 14px">Hope to see you there.</p><p style="font-size: 14px">Good Luck !</p><br><p style="font-size: 14px">Regards,</p><p style="font-size: 14px"><b>Team Koneqto.</b></p></td></tr><tr><td style="background-color: #ccc; padding: 10px;">Copy Rights By <a style="color:#3b5998" href=' + fullUrl + '>KONEQTO</a></td></tr></table>';



                    let mailOptions = {
                        from: nodemailer.email,
                        to: emailid,
                        subject: "scheduling interview ",
                        html: body,
                    };
                    nodemailer.sendMail(mailOptions, function (err, info) {
                        if (err) {
                            uierrorlogger.error("jobpostningjs sendmail..." + err.body);

                        }
                        else {
                            console.log('Message sent: %s', info.messageId);
                            console.log(info);
                            console.log("Success...." + info);
                            // res.status(200).send({
                            //     success: true,
                            //     message: "Email sent sucessfully"
                            // });
                        }
                    });
                }
                else {
                    console.log("entring into Email does't found case");
                    res.status(200).send({
                        success: true,
                        message: "Email doesn't found, please enter correct email"
                    });

                }
            }

        }).fail(function (err) {
            uierrorlogger.error("api sendmail...fail" + err);

        });
    } catch (error) {
        uierrorlogger.error("Error catch expection api sendmail..." + error);

    }
}

const scheduleSms = async (mobile_no, jobtitle, name, date, time, mode, type, contactperson, location) => {

    const apiInstance = new TwoFactor('96ab36da-f79d-11e8-a895-0200cd936042');


    try {

        const response = await apiInstance.Transactional.sendMessage({
            to: mobile_no,

            from: 'KONQTO',

            templateType: apiInstance.Transactional.TemplateTypes.dynamic,

            templateName: 'schedule_interview',
            var1: name,
            var2: jobtitle,
            var3: date.split('T')[0],
            var4: time.split('T')[1],
            var5: location,
            var6: contactperson

        });



    } catch (error) {

        console.log('Error: ', error);

    }

};


////////////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////// Next Round mail //////////////////////////////////////
router.post('/nextround', function (req, res, next) {
    var usermailid = req.body.schedule_user;
    var interviewername = req.body.interviewer;
    var title = req.body.title;
    var date = req.body.schedule_date;
    var time = req.body.schedule_time;
    var location = req.body.location;
    var type = req.body.type_mode_name;
    var mode = req.body.interview_mode_name;
    var sid = req.body.schedulejobid
    var mobile_no = req.body.mobile_no
    var name = req.body.name

    try {

        console.log("nextround interview data" +
            req.body.title,
            req.body.schedule_date,
            req.body.schedule_time,
            req.body.interviewer,
            req.body.type,
            req.body.location,
            req.body.mode,
            req.body.notify_me,
            req.body.created_by,
            req.body.schedule_user,
            req.body.schedulejobid)
        jobpostingdb.nextround(
            req.body.title,
            req.body.schedule_date,
            req.body.schedule_time,
            req.body.interviewer,
            req.body.type,
            req.body.location,
            req.body.mode,
            req.body.notify_me,
            req.body.created_by,
            req.body.schedule_user,
            req.body.schedulejobid).then(function (rows) {
                if (rows) {
                    res.end(JSON.stringify(rows.rows));
                    for (var i = 0; i < usermailid.length; i++) {
                        nextroundemail(usermailid[i], interviewername, title, date, time, location, type, mode, sid)
                        scheduleSms(mobile_no[i], title, name[i], date, time, mode, type, interviewername, location)
                    }
                }
            }).fail(function (err) {
                uierrorlogger.error("api nextround..." + err.body);

            });
    } catch (error) {
        uierrorlogger.error("Error catch expection api nextround..." + error);

    }
});
function nextroundemail(usermailid, interviewername, title, date, time, location, type, mode, sid) {
    try {
        console.log(" nextround  Email....." + usermailid, interviewername, title, date, time, location, type, mode, sid);
        jobpostingdb.getEmailIdInterviewSh1(usermailid, sid).then(function (rows) {
            if (rows) {
                //function to format date
                // var sd = formattedate;
                // // var ed = formattedate;
                // function formattedate(date) {
                //     var d = new Date(date),
                //         month = '' + (d.getMonth() + 1),
                //         day = '' + d.getDate(),
                //         year = d.getFullYear();
                //     if (month.length < 2) month = '0' + month;
                //     if (day.length < 2) day = '0' + day;
                //     this.formattedate = [day, month, year].join('-')
                //     return this.formattedate;

                // }
                var fullUrl = ' http://koneqto.com';
                var emailid = JSON.parse(JSON.stringify(rows.rows))[0].email;
                var name = JSON.parse(JSON.stringify(rows.rows))[0].name;
                var mode = JSON.parse(JSON.stringify(rows.rows))[0].interview_mode_name;
                var type = JSON.parse(JSON.stringify(rows.rows))[0].type_mode_name;
                var date = JSON.parse(JSON.stringify(rows.rows))[0].schedule_date;
                var time = JSON.parse(JSON.stringify(rows.rows))[0].schedule_time;
                console.log("...............=============================>>>>>>>>>>>>>" + usermailid, interviewername, title, date, time, location, type, mode)
                var varlenth = JSON.parse(JSON.stringify(rows.rows)).length;
                if (parseInt(varlenth) != 0) {
                    console.log('hieee');
                    var body =
                        '<table align="center" style="width: 900px; height: 100px;font-size: 13px;font-family: arial;"><tr><td style="border-bottom: #ccc solid 1px;"><a href=' + fullUrl + '><img width="230" src="http://unicsol.com/wp-content/uploads/2018/05/Koneqto-e1525862695762.png"></a></td></tr><tr><td><p style="font-size: 14px;">Dear <b>' + name + '!</b></p><p style="font-size: 14px">Greetings from <b> ' + interviewername + '!</b></p><p style="font-size: 14px">Take a step forward!</p><p style="font-size: 14px">Your resume has been shortlisted for the next round </p><br><p style="font-size: 16px"><b><u>Scheduled Details:</u></b></p><table style=" border-collapse: collapse;"><tr><td> <h4>Position:</h4> </td><td> <h4>' + title + '</h4></td></tr><tr><td><h4>Mode of Interview:</h4></td><td> <h4>' + mode + '</h4></td></tr><tr><td><h4>Type of Interview:</h4></td><td><h4>' + type + '</h4></td></tr><tr><td><h4>Date of Inteview:</h4></td><td> <h4>' + date + '</h4></td></tr><tr><td><h4>Time:</h4></td><td><h4>' + time + '</h4></td></tr><tr><td><h4>Venue:</h4> </td><td> <h4>' + location + '</h4></td></tr></table><p style="font-size: 14px"><h4>Regards,</h4></p><p style="font-size: 14px">' + interviewername + '</p><p style="font-size: 14px">Team Koneqto</p></td></tr><tr><td style="background-color: #ccc; padding: 10px;">Copy Rights By <a style="color:#3b5998" href=' + fullUrl + '>KONEQTO </a></td></tr></table>';

                    let mailOptions = {
                        from: nodemailer.email,
                        to: emailid,
                        subject: "nextround",
                        html: body,
                    };
                    nodemailer.sendMail(mailOptions, function (err, info) {
                        if (err) {
                            uierrorlogger.error("nextround sendmail..." + err.body);

                        }
                        else {
                            console.log('Message sent: %s', info.messageId);
                            console.log(info);
                            console.log("Success...." + info);
                            // res.status(200).send({
                            //     success: true,
                            //     message: "Email sent sucessfully"
                            // });
                        }
                    });
                }
                else {
                    console.log("entring into Email does't found case");
                    res.status(200).send({
                        success: true,
                        message: "Email doesn't found, please enter correct email"
                    });

                }
            }

        }).fail(function (err) {
            uierrorlogger.error("api sendmail...fail" + err);

        });
    } catch (error) {
        uierrorlogger.error("Error catch expection api sendmail..." + error);

    }
}
////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////// Change Status Mail ///////////////////////////
router.post('/changestatus', function (req, res, next) {
    var usermailid = req.body.user;
    var status = req.body.status;
    var jobtitle = req.body.title;
    var mobile_no = req.body.mobile_no
    var name = req.body.name

    try {
        jobpostingdb.changestatus(req.body.user, req.body.jobid, req.body.status).then(function (rows) {
            if (rows) {
                res.end(JSON.stringify(rows.rows));

                if (status == 8) {
                    for (var i = 0; i < usermailid.length; i++) {
                        selectedmail(usermailid[i], jobtitle)
                        changestatus_sms(mobile_no[i], jobtitle, name[i], 'Selected ')
                    }
                }
                if (status == 2) {
                    for (var i = 0; i < usermailid.length; i++) {
                        rejectedmail(usermailid[i], jobtitle)
                        // changestatus_sms('Sorry to inform',mobile_no[i],jobtitle,name[i],'Selected ')
                    }
                }
                if (status == 3) {
                    for (var i = 0; i < usermailid.length; i++) {
                        onholdmail(usermailid[i], jobtitle)


                        changestatus_sms(mobile_no[i], jobtitle, name[i], ' put on hold ')
                    }
                }
                if (status == 4) {
                    for (var i = 0; i < usermailid.length; i++) {
                        futureusemail(usermailid[i], jobtitle)
                        changestatus_sms(mobile_no[i], jobtitle, name[i], 'put for future use ')
                    }
                }
                if (status == 5) {
                    for (var i = 0; i < usermailid.length; i++) {
                        irrelevantprofile(usermailid[i], jobtitle)
                        //changestatus_sms(mobile_no[i],jobtitle,name[i],'Selected ')

                    }
                }
            }
        }).fail(function (err) {
            uierrorlogger.error("api nextround..." + err.body);

        });
    } catch (error) {
        uierrorlogger.error("Error catch expection api nextround..." + error);

    }
});
const changestatus_sms = async (mobile_no, title, name, status) => {

    const apiInstance = new TwoFactor('96ab36da-f79d-11e8-a895-0200cd936042');


    try {

        const response = await apiInstance.Transactional.sendMessage({
            to: mobile_no,
            // name: name[index],
            from: 'KONQTO',
            // templateName: 'open',
            templateType: apiInstance.Transactional.TemplateTypes.dynamic,

            templateName: 'change_job_status',
            var1: name,
            var2: status,
            var3: title

            // date:toString( Date()),
            // job_title: toString(jobtitle)
        });



    } catch (error) {

        console.log('Error: ', error);

    }

};
//selected
function selectedmail(usermailid, jobtitle) {
    try {
        console.log(" selectedmail  Email....." + usermailid, jobtitle);
        jobpostingdb.getIdforEmail(usermailid).then(function (rows) {
            if (rows) {
                var emailid = JSON.parse(JSON.stringify(rows.rows))[0].email;
                var name = JSON.parse(JSON.stringify(rows.rows))[0].name;
                console.log("...............=============================>>>>>>>>>>>>>" + usermailid, jobtitle)
                var varlenth = JSON.parse(JSON.stringify(rows.rows)).length;
                var fullUrl = ' http://koneqto.com';
                if (parseInt(varlenth) != 0) {
                    console.log('hieee');
                    var body =
                        '<table align="center" style="width:600px;height:100px;font-size:13px;font-family:arial"><tr><td  style="border-bottom: #ccc solid 1px;"><a href=' + fullUrl + '><img width="230" src="http://unicsol.com/wp-content/uploads/2018/05/Koneqto-e1525862695762.png"></a></td></tr><tr><td><p style="font-size: 18px;">Dear <b>' + name + '!,</b></p><p style="font-size: 14px" > <b> Congratulations!!</b></p><p style="font-size: 14px">Good news for you! It is our pleasure to inform you that you have been selected for the <b>' + jobtitle + '</b> role.</p><p style="font-size: 14px" >Happy Working!</p><br><p style="font-size: 14px" >Team Koneqto</p></td></tr><tr><td style="background-color: #ccc; padding: 10px;">Copy Rights By <a style="color:#3b5998" href=' + fullUrl + '>KONEQTO </a> </td></tr></table>';


                    let mailOptions = {
                        from: nodemailer.email,
                        to: emailid,
                        subject: "selectedmail",
                        html: body,
                    };
                    nodemailer.sendMail(mailOptions, function (err, info) {
                        if (err) {
                            uierrorlogger.error("selectedmail sendmail..." + err.body);

                        }
                        else {
                            console.log('Message sent: %s', info.messageId);
                            console.log(info);
                            console.log("Success...." + info);
                            // res.status(200).send({
                            //     success: true,
                            //     message: "Email sent sucessfully"
                            // });
                        }
                    });
                }
                else {
                    console.log("entring into Email does't found case");
                    res.status(200).send({
                        success: true,
                        message: "Email doesn't found, please enter correct email"
                    });

                }
            }

        }).fail(function (err) {
            uierrorlogger.error("api sendmail...fail" + err);

        });
    } catch (error) {
        uierrorlogger.error("Error catch expection api sendmail..." + error);

    }
}

//Rejected
function rejectedmail(usermailid, jobtitle) {
    try {
        console.log(" rejectedmail  Email....." + usermailid, jobtitle);
        jobpostingdb.getIdforEmail(usermailid).then(function (rows) {
            if (rows) {
                var emailid = JSON.parse(JSON.stringify(rows.rows))[0].email;
                var name = JSON.parse(JSON.stringify(rows.rows))[0].name;
                console.log("...............=============================>>>>>>>>>>>>>" + usermailid, jobtitle)
                var varlenth = JSON.parse(JSON.stringify(rows.rows)).length;
                var fullUrl = ' http://koneqto.com';
                if (parseInt(varlenth) != 0) {
                    console.log('hieee');
                    var body =
                        '<table align="center" style="width:600px;height:100px;font-size:13px;font-family:arial"><tr><td  style="border-bottom: #ccc solid 1px;"><a href=' + fullUrl + '><img width="230" src="http://unicsol.com/wp-content/uploads/2018/05/Koneqto-e1525862695762.png"></a></td></tr><tr><td><p style="font-size: 18px;">Dear <b>' + name + ',!</b></p> <p style="font-size: 14px" >Thank you for taking the time to meet with our team for the role <b>' + jobtitle + '</b>. It was a pleasure to learn more about your skills and accomplishments.</p> <p style="font-size: 14px" >We would like to inform you that we have filled the position. However, we will keep your application on file for consideration if there is a future  opening that may be a fit for you.</p> <p style="font-size: 14px" >All the Best!</p><br><p style="font-size: 14px" >Team Koneqto</p></td></tr><tr> <td style="background-color: #ccc; padding: 10px;">Copy Rights By <a style="color:#3b5998" href=' + fullUrl + '>KONEQTO </a> </td></tr></table>';


                    let mailOptions = {
                        from: nodemailer.email,
                        to: emailid,
                        subject: "rejectedmail",
                        html: body,
                    };
                    nodemailer.sendMail(mailOptions, function (err, info) {
                        if (err) {
                            uierrorlogger.error("rejectedmail sendmail..." + err.body);

                        }
                        else {
                            console.log('Message sent: %s', info.messageId);
                            console.log(info);
                            console.log("Success...." + info);
                            // res.status(200).send({
                            //     success: true,
                            //     message: "Email sent sucessfully"
                            // });
                        }
                    });
                }
                else {
                    console.log("entring into Email does't found case");
                    res.status(200).send({
                        success: true,
                        message: "Email doesn't found, please enter correct email"
                    });

                }
            }

        }).fail(function (err) {
            uierrorlogger.error("api sendmail...fail" + err);

        });
    } catch (error) {
        uierrorlogger.error("Error catch expection api sendmail..." + error);

    }
}

//On Hold
function onholdmail(usermailid, jobtitle) {
    try {
        console.log(" onholdmail  Email....." + usermailid, jobtitle);
        jobpostingdb.getIdforEmail(usermailid).then(function (rows) {
            if (rows) {
                var emailid = JSON.parse(JSON.stringify(rows.rows))[0].email;
                var name = JSON.parse(JSON.stringify(rows.rows))[0].name;
                console.log("...............=============================>>>>>>>>>>>>>" + usermailid, jobtitle)
                var varlenth = JSON.parse(JSON.stringify(rows.rows)).length;
                var fullUrl = ' http://koneqto.com';
                if (parseInt(varlenth) != 0) {
                    console.log('hieee');
                    var body =
                        '<table align="center" style="width:600px;height:100px;font-size:13px;font-family:arial"><tr><td  style="border-bottom: #ccc solid 1px;"><a href=' + fullUrl + '><img width="230" src="http://unicsol.com/wp-content/uploads/2018/05/Koneqto-e1525862695762.png"></a></td></tr><tr><td><p style="font-size: 18px;">Dear <b>' + name + ',</b></p> <p style="font-size: 14px" >Thank you for your interest in the following position <b>' + jobtitle + '</b>. The purpose of this message is to inform  that yours application is still under consideration at this point of time .</p><br><p style="font-size: 14px" >Team Koneqto</p></td></tr><tr><td style="background-color: #ccc; padding: 10px;">Copy Rights By <a style="color:#3b5998" href=' + fullUrl + '>KONEQTO </a> </td></tr></table>';


                    let mailOptions = {
                        from: nodemailer.email,
                        to: emailid,
                        subject: "onholdmail",
                        html: body,
                    };
                    nodemailer.sendMail(mailOptions, function (err, info) {
                        if (err) {
                            uierrorlogger.error("onholdmail sendmail..." + err.body);

                        }
                        else {
                            console.log('Message sent: %s', info.messageId);
                            console.log(info);
                            console.log("Success...." + info);
                            // res.status(200).send({
                            //     success: true,
                            //     message: "Email sent sucessfully"
                            // });
                        }
                    });
                }
                else {
                    console.log("entring into Email does't found case");
                    res.status(200).send({
                        success: true,
                        message: "Email doesn't found, please enter correct email"
                    });

                }
            }

        }).fail(function (err) {
            uierrorlogger.error("api sendmail...fail" + err);

        });
    } catch (error) {
        uierrorlogger.error("Error catch expection api sendmail..." + error);

    }
}

//Future Use
function futureusemail(usermailid, jobtitle) {
    try {
        console.log(" futureusemail  Email....." + usermailid, jobtitle);
        jobpostingdb.getIdforEmail(usermailid).then(function (rows) {
            if (rows) {
                var emailid = JSON.parse(JSON.stringify(rows.rows))[0].email;
                var name = JSON.parse(JSON.stringify(rows.rows))[0].name;
                console.log("...............=============================>>>>>>>>>>>>>" + usermailid, jobtitle)
                var varlenth = JSON.parse(JSON.stringify(rows.rows)).length;
                var fullUrl = ' http://koneqto.com';
                if (parseInt(varlenth) != 0) {
                    console.log('hieee');
                    var body =
                        ' <table align="center" style="width:600px;height:100px;font-size:13px;font-family:arial"><tr> <td  style="border-bottom: #ccc solid 1px;"><a href=' + fullUrl + '><img width="230" src="http://unicsol.com/wp-content/uploads/2018/05/Koneqto-e1525862695762.png"></a></td></tr><tr><td><p style="font-size: 18px;"> <b>Hi ' + name + ',</b></p> <p style="font-size: 14px" >After careful deliberation, we have been unable to find an ideal match between the candidate’s background and our current needs. Presently, there are no openings for role <b>' + jobtitle + '</b>.</p><p style="font-size: 14px" >We’ll definitely contact you for the future openings and drives. </p> <p style="font-size: 14px" >Wishing you the best of luck on our behalf.</p><br><p style="font-size: 14px" >Sincerely,</p><p style="font-size: 14px" >Team Koneqto</p></td></tr><tr><td style="background-color: #ccc; padding: 10px;">Copy Rights By <a style="color:#3b5998" href=' + fullUrl + '>KONEQTO </a> </td> </tr></table>';


                    let mailOptions = {
                        from: nodemailer.email,
                        to: emailid,
                        subject: "futureusemail",
                        html: body,
                    };
                    nodemailer.sendMail(mailOptions, function (err, info) {
                        if (err) {
                            uierrorlogger.error("futureusemail sendmail..." + err.body);

                        }
                        else {
                            console.log('Message sent: %s', info.messageId);
                            console.log(info);
                            console.log("Success...." + info);
                            // res.status(200).send({
                            //     success: true,
                            //     message: "Email sent sucessfully"
                            // });
                        }
                    });
                }
                else {
                    console.log("entring into Email does't found case");
                    res.status(200).send({
                        success: true,
                        message: "Email doesn't found, please enter correct email"
                    });

                }
            }

        }).fail(function (err) {
            uierrorlogger.error("api sendmail...fail" + err);

        });
    } catch (error) {
        uierrorlogger.error("Error catch expection api sendmail..." + error);

    }
}
//Irrelevant profile
function irrelevantprofile(usermailid, jobtitle) {
    try {
        console.log(" irrelevantprofile  Email....." + usermailid, jobtitle);
        jobpostingdb.getIdforEmail(usermailid).then(function (rows) {
            if (rows) {
                var emailid = JSON.parse(JSON.stringify(rows.rows))[0].email;
                var name = JSON.parse(JSON.stringify(rows.rows))[0].name;
                console.log("...............=============================>>>>>>>>>>>>>" + usermailid, jobtitle)
                var varlenth = JSON.parse(JSON.stringify(rows.rows)).length;
                var fullUrl = ' http://koneqto.com';
                if (parseInt(varlenth) != 0) {
                    console.log('hieee');
                    var body =
                        ' <table align="center" style="width:600px;height:100px;font-size:13px;font-family:arial"><tr><td  style="border-bottom: #ccc solid 1px;"><a href=' + fullUrl + '><img width="230" src="http://unicsol.com/wp-content/uploads/2018/05/Koneqto-e1525862695762.png"></a></td></tr><tr><td><p style="font-size: 16px;">Hi <b>' + name + '</b>,</p><p style="font-size: 14px" >We considered your profile as irrelevant as your experiences and skills set doesn’t match our requirements for the role <b>' + jobtitle + '</b>. Wish you Good luck for the next time. </p><p style="font-size: 14px" >Team Koneqto</p></td></tr><tr><td style="background-color: #ccc; padding: 10px;">Copy Rights By <a style="color:#3b5998" href=' + fullUrl + '>KONEQTO</a></td></tr></table>';

                    let mailOptions = {
                        from: nodemailer.email,
                        to: emailid,
                        subject: "irrelevantprofile",
                        html: body,
                    };
                    nodemailer.sendMail(mailOptions, function (err, info) {
                        if (err) {
                            uierrorlogger.error("irrelevantprofile sendmail..." + err.body);

                        }
                        else {
                            console.log('Message sent: %s', info.messageId);
                            console.log(info);
                            console.log("Success...." + info);
                            // res.status(200).send({
                            //     success: true,
                            //     message: "Email sent sucessfully"
                            // });
                        }
                    });
                }
                else {
                    console.log("entring into Email does't found case");
                    res.status(200).send({
                        success: true,
                        message: "Email doesn't found, please enter correct email"
                    });

                }
            }

        }).fail(function (err) {
            uierrorlogger.error("api sendmail...fail" + err);

        });
    } catch (error) {
        uierrorlogger.error("Error catch expection api sendmail..." + error);

    }
}
///////////////////////////////////////////////////////////////////////////////////////////////////

router.get('/natureofjob', function (req, res) {
    //console.log("Entering into natureofjob routes method");

    jobpostingdb.natureofjob()
        .then(function (rows) {
            if (rows) {

                res.end(JSON.stringify(rows.rows))


            }
            else {

                //console.log("natureofjob routes" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'natureofjob routes Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'natureofjob routes Method Error' + err
            });
        });

});



router.post('/updatejob', function (req, res) {
    console.log(req.body.loginuserid, req.body.jobpostid, req.body.optional_skills)
    this.picname,
        //req.body.video,
        this.vname,
        jobpostingdb.updatejob(
            req.body.loginuserid,
            req.body.optional_skills,
            req.body.maxsalary,
            req.body.minsalary,
            req.body.gender,
            req.body.notice_period,
            req.body.enddate,
            req.body.phy_status,
            req.body.contactperson,
            this.picname,
            this.vname,
            // req.body.photo,
            // req.body.video,
            req.body.jobpostid)
            .then(function (rows) {
                if (rows) {
                    //console.log("jobposting Sucessful")
                    res.status(200).send({
                        success: true,
                        message: 'Sucessfull'
                    });
                    this.picname = ''
                    this.vname = ''
                }
                else {
                    //console.log("jobposting falied" + err.body)
                    res.status(500).send({
                        success: false,
                        message: 'jobpost Method Error' + err
                    });
                    this.picname = ''
                    this.vname = ''
                }
            }).fail(function (err) {
                uierrorlogger.error(" jobposting failed..." + err.body);
                res.status(500).send({
                    success: false,
                    message: 'jobpost Method Error' + err

                });
                this.picname = ''
                this.vname = ''

            });
});

// edit charity
router.post('/updatecharity', function (req, res) {
    console.log("entering into update charity", req.body.loginuserid,
        req.body.charity_name,
        req.body.charity_address,
        req.body.charity_mobile_no,
        req.body.charity_help_start_date,
        req.body.charity_help_end_date,
        req.body.charity_description,
        req.body.volunteers_name,
        req.body.volunteers_mobile,

        req.body.account_number,
        req.body.account_name,
        req.body.account_type,
        req.body.ifsc_code,
        req.body.firstname, req.body.charity_id, req.body.bankname)

    jobpostingdb.updatecharity(
        req.body.loginuserid,
        req.body.charity_name,
        req.body.charity_address,
        req.body.charity_mobile_no,
        req.body.charity_help_start_date,
        req.body.charity_help_end_date,
        req.body.charity_description,
        req.body.volunteers_name,
        req.body.volunteers_mobile,

        req.body.account_number,
        req.body.account_name,
        req.body.account_type,
        req.body.ifsc_code,
        req.body.firstname, req.body.charity_id, req.body.bankname)
        .then(function (rows) {
            if (rows) {
                //console.log("jobposting Sucessful")
                res.status(200).send({
                    success: true,
                    message: 'Sucessfull'
                });
                // this.picname = ''
                // this.vname = ''
            }
            else {
                //console.log("jobposting falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'updatecharity Method Error' + err
                });

            }
        }).fail(function (err) {
            uierrorlogger.error(" updatecharity failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'updatecharity Method Error' + err

            });


        });
});

// delete charity
router.post('/deletecharity', function (req, res, next) {
    try {
        //console.log("Deletejob data" + req.body.loginuserid, req.body.jobid);
        jobpostingdb.deletecharity(req.query.loginuserid, req.body.charity_id).then(function (rows) {
            if (rows) {
                res.end(JSON.stringify(rows.rows));
            }
        }).fail(function (err) {
            uierrorlogger.error("api deletecharity..." + err.body);
            //next(Errormessage("Create New-Password ", err));
        });
    } catch (error) {
        uierrorlogger.error("Error catch expection api deletecharity..." + error);
        next(Errormessage("deletecharity", error));
    }
});



router.get('/getjobapplicantstatus', function (req, res) {

    jobpostingdb.getjobapplicantstatus(req.query.loginuserid, req.query.jobid, req.query.registration_id)
        .then(function (rows) {
            if (rows) {
                //console.log("jobposting Sucessful")
                res.end(JSON.stringify(rows.rows))
                res.status(200).send({
                    success: true,
                    message: 'Sucessfull'
                });
            }
            else {
                //console.log("jobposting falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'getjobapplicantstatus Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error(" getjobapplicantstatus failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'getjobapplicantstatus Method Error' + err

            });

        });
});
router.get('/getanswer', function (req, res) {

    jobpostingdb.getanswer(req.query.p_registration_id, req.query.posting_id)
        .then(function (rows) {
            if (rows) {

                res.end(JSON.stringify(rows.rows))


            }
            else {

                //console.log("city routes" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'getanswer routes Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'getanswer routes Method Error' + err
            });
        });

});
router.post('/unapplyjob', function (req, res, next) {
    try {
        jobpostingdb.unapplyjob(req.body.loginuserid, req.body.jobid, req.body.registration_id).then(function (rows) {
            if (rows) {
                res.end(JSON.stringify(rows.rows));
            }
        }).fail(function (err) {
            uierrorlogger.error("api unapplyjob..." + err.body);
            //next(Errormessage("Create New-Password ", err));
        });
    } catch (error) {
        uierrorlogger.error("Error catch expection api unapplyjob..." + error);
        next(Errormessage("Create New-Pass.loginword", error));
    }
});

router.get('/checkapplicants', function (req, res) {

    jobpostingdb.checkapplicants(req.query.jobid, req.query.type)
        .then(function (rows) {
            if (rows) {

                res.end(JSON.stringify(rows.rows))


            }
            else {

                //console.log("city routes" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'checkapplicants routes Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'checkapplicants routes Method Error' + err
            });
        });

});

router.get('/loadsalarytype', function (req, res) {

    jobpostingdb.loadsalarytype(req.query.jobtype)
        .then(function (rows) {
            if (rows) {

                res.end(JSON.stringify(rows.rows))


            }
            else {

                //console.log("city routes" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'loadsalarytype routes Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'loadsalarytype routes Method Error' + err
            });
        });

});


router.get('/checkreport', function (req, res) {
    console.log("entering into /checkreport ", req.query.jobid, req.query.registration_id, req.query.loginuserid)

    jobpostingdb.checkreport(req.query.jobid, req.query.registration_id, req.query.loginuserid)
        .then(function (rows) {
            if (rows) {
                res.end(JSON.stringify(rows.rows))
            }
            else {
                //console.log("city routes" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'city routes Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: 'city routes Method Error' + err
            });
        });
});


module.exports = router;