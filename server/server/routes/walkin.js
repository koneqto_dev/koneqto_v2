var express = require('express');
var path = require('path');
var router = express.Router();
var multer = require('multer');
var walkindb = require(path.join(__dirname, '../', 'modules', 'walkindb'));
var uiinfologger = require(path.join(__dirname, '../', '../', 'config')).uiinfologger;
var uierrorlogger = require(path.join(__dirname, '../', '../', 'config')).uierrorlogger;
var http = require("http");
var request = require('request');
var nodemailer = require(path.join(__dirname, '../', '../', 'config')).nodemailer;
var log4js = require('log4js');



router.get('/loadwalkins', function (req, res) {
    console.log("Entering into==>walkin==> loadwalkins routes method");

    walkindb.loadwalkins()
        .then(function (rows) {
            if (rows) {

                res.end(JSON.stringify(rows.rows))
            }
            else {

                console.log("walkin==> loadwalkins routes" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'walkin==> loadwalkins routes Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: '==>walkin==> loadwalkins routes Method Error' + err
            });
        });

});





router.get('/loadadvancejobs', function (req, res) {
    console.log("Entering into==>walkin==> loadadvancejobs routes method", req.query.loginuserid);

    walkindb.loadadvancejobs(req.query.loginuserid)
        .then(function (rows) {
            if (rows) {

                res.end(JSON.stringify(rows.rows))
            }
            else {

                console.log("walkin==> loadadvancejobs  routes" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'walkin==> loadadvancejobs routes Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: '==>walkin==> loadadvancejobs routes Method Error' + err
            });
        });

});





router.get('/profileoverview', function (req, res) {
    console.log("Entering into==>walkin==> profileoverview routes method", req.query.p_user_registration_id);

    walkindb.profileoverview(req.query.p_user_registration_id)
        .then(function (rows) {
            if (rows) {

                res.end(JSON.stringify(rows.rows))
            }
            else {

                console.log("walkin==> profileoverview  routes" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'walkin==> profileoverview routes Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: '==>walkin==> profileoverview routes Method Error' + err
            });
        });

});
router.post('/advancejobsearch', function (req, res) {

    console.log("Entering into tests  advancejobsearch" + req.query.loginuserid, req.body.firstname, req.body.skill_name01, req.body.skill_name02, req.body.skill_name03, req.body.skill_name04, req.body.skill_name1, req.body.currentlocation, req.body.total_exp_in_yrs, req.body.gender, req.body.preferred_location, req.body.marital_status, req.body.employment_type, req.body.notice_period, req.body.qualification, req.body.type_of_interview, req.body.physical_status, req.body.year, req.body.locality);
    walkindb.advancejobsearch(req.query.loginuserid, req.body.firstname, req.body.skill_name01, req.body.skill_name02, req.body.skill_name03, req.body.skill_name04, req.body.skill_name1, req.body.current_location, req.body.total_exp_in_yrs, req.body.gender, req.body.preferred_location, req.body.marital_status, req.body.employment_type, req.body.notice_period, req.body.qualification, req.body.type_of_interview, req.body.physical_status, req.body.year, req.body.locality)
        .then(function (rows) {
            if (rows) {

                res.send(rows.rows);

            }
            else {
                console.log("walkin==>advancejobsearch falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'walkin==>advancejobsearch  Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error(" walkin==>advancejobsearch routes failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'walkin==>advancejobsearch routes Method Error' + err
            });
        });

});


//  sendmypost
router.post('/sendmypost', function (req, res) {
    var send_job_description = req.body.send_job_description;
    var recruiter_name = req.body.recruiter_name
    var usermailid = req.body.profile_id
    var jobid = req.body.postedjobid

    console.log("Entering into tests sendmypost" + req.body.postedjobid, req.body.recruiter_id, req.body.profile_id, req.body.send_job_description);
    walkindb.sendmypost(req.body.postedjobid, req.body.recruiter_id, req.body.profile_id, req.body.send_job_description)
        .then(function (rows) {
            if (rows) {
                res.send(rows.rows);
                sendmypost(send_job_description, recruiter_name, usermailid,jobid);

            }
            else {
                console.log("walkin==>sendmypost falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'walkin==>sendmypost  Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error(" walkin==>sendmypost routes failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'walkin==>sendmypost routes Method Error' + err
            });
        });

});



//  sendmypostemail function
function sendmypost(send_job_description, recruiter_name, usermailid,jobid) {
    try {
        console.log(" sendmypost Email....." + send_job_description, recruiter_name, usermailid,jobid);
        walkindb.getIdforEmail(usermailid,jobid).then(function (rows) {
            if (rows) {
                var emailid = JSON.parse(JSON.stringify(rows.rows))[0].email;
                var name = JSON.parse(JSON.stringify(rows.rows))[0].name;
                var title = JSON.parse(JSON.stringify(rows.rows))[0].job_title;

                console.log("...............=============================>>>>>>>>>>>>>" + usermailid)
                var fullUrl = ' http://koneqto.com';
                var varlenth = JSON.parse(JSON.stringify(rows.rows)).length;
                if (parseInt(varlenth) != 0) {
                    console.log('hieee');
                    var body =
                        '<table align="center" style="width:800px;height:100px;font-size:13px;font-family:arial"><tr><td style="border-bottom: #ccc solid 1px;"><a href=' + fullUrl + '><img width="230" src="http://unicsol.com/wp-content/uploads/2018/05/Koneqto-e1525862695762.png"></a></td></tr>      <tr><td><p style="font-size: 18px;">Dear <b>' + name + ',</b></p><p style="font-size: 14px">Greetings from <b style=" text-transform: capitalize;"> ' + recruiter_name + ',</b></p><p style="font-size: 14px">Hi ' + name + ', Your profile seems like a good fit for the position of <b style=" text-transform: capitalize;"><b>' + title + '</b>.</b></p><table style="border-collapse: collapse;"><tr><td style="vertical-align: -webkit-baseline-middle;"><h4>Description:</h4></td><td><h4 style=" text-transform: capitalize;">' + send_job_description + '</h4><br></td></tr></table><br><p style="font-size: 14px">Regards,</p><p style="font-size: 14px"><b>Team Koneqto.</b></p> </td></tr><tr><td style="background-color: #ccc; padding: 10px;">Copy Rights By <a style="color:#3b5998" href=' + fullUrl + '>KONEQTO</a></td></tr></table>';

                    let mailOptions = {
                        from: nodemailer.email,
                        to: emailid,
                        subject: "Job post",
                        html: body,
                    };
                    nodemailer.sendMail(mailOptions, function (err, info) {
                        if (err) {
                            uierrorlogger.error("jobpostningjs sendmail..." + err.body);

                        }
                        else {
                            console.log('Message sent: %s', info.messageId);
                            console.log(info);
                            console.log("Success...." + info);
                            // res.status(200).send({
                            //     success: true,
                            //     message: "Email sent sucessfully"
                            // });
                        }
                    });
                }
                else {
                    console.log("entring into Email does't found case");
                    res.status(200).send({
                        success: true,
                        message: "Email doesn't found, please enter correct email"
                    });

                }
            }
        }).fail(function (err) {
            uierrorlogger.error("api sendmail...fail" + err);

        });
    } catch (error) {
        uierrorlogger.error("Error catch expection api sendmail..." + error);

    }

}

// router.post('/advancejobsearch', function (req, res) {

//     console.log("Entering into tests  advancejobsearch" + req.query.loginuserid, req.body.firstname, req.body.skill_name, req.body.skill_name1, req.body.current_location, req.body.total_exp_in_yrs, req.body.gender, req.body.preferred_location, req.body.marital_status, req.body.employment_type, req.body.notice_period, req.body.qualification, req.body.type_of_interview, req.body.physical_status, req.body.year, req.body.locality);
//     walkindb.advancejobsearch(req.query.loginuserid, req.body.firstname, req.body.skill_name, req.body.skill_name1, req.body.current_location, req.body.total_exp_in_yrs, req.body.gender, req.body.preferred_location, req.body.marital_status, req.body.employment_type, req.body.notice_period, req.body.qualification, req.body.type_of_interview, req.body.physical_status, req.body.year, req.body.locality)
//         .then(function (rows) {
//             if (rows) {

//                 res.send(rows.rows);

//             }
//             else {
//                 console.log("walkin==>advancejobsearch falied" + err.body)
//                 res.status(500).send({
//                     success: false,
//                     message: 'walkin==>advancejobsearch  Method Error' + err
//                 });
//             }
//         }).fail(function (err) {
//             uierrorlogger.error(" walkin==>advancejobsearch routes failed..." + err.body);
//             res.status(500).send({
//                 success: false,
//                 message: 'walkin==>advancejobsearch routes Method Error' + err
//             });
//         });

// });



router.post('/walkinsearch', function (req, res) {
    console.log("Entering into  walkinsearch" + req.body.skill_name, req.body.location, req.body.functionalarea);
    // p_registration_id ,p_event_name,p_event_type ,p_envet_desription ,p_functional_area,p_event_time
    walkindb.walkinsearch(req.body.skill_name, req.body.location, req.body.functionalarea)
        .then(function (rows) {
            if (rows) {

                res.send(rows.rows);

            }
            else {
                console.log("walkin==>walkinsearch falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'walkin==>walkinsearch  Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error(" walkin==>walkinsearch routes failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'walkin==>walkinsearch routes Method Error' + err
            });
        });

})




router.get('/loadjobsiAnotherloc', function (req, res) {
    console.log("Entering into==>walkin==> loadjobsinotherloc routes method" + req.query.p_user_registration_id);

    walkindb.loadjobsinotherloc(req.query.p_user_registration_id)
        .then(function (rows) {
            if (rows) {

                res.end(JSON.stringify(rows.rows))
            }
            else {

                console.log("walkin==> loadjobsinotherloc routes" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'walkin==> loadjobsinotherloc routes Method Error' + err
                });
            }
        }).fail(function (err) {
            res.status(500).send({
                success: false,
                message: '==>walkin==> loadjobsinotherloc routes Method Error' + err
            });
        });

});


router.post('/oprsearch', function (req, res) {
    console.log("Entering into tests  oprsearch" + req.body.p_user_registration_id, req.body.skills, req.body.location, req.body.experience);

    walkindb.oprsearch(req.body.p_user_registration_id, req.body.skills, req.body.location, req.body.experience)
        .then(function (rows) {
            if (rows) {

                res.send(rows.rows);

            }
            else {
                console.log("walkin==>oprsearch falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'walkin==>oprsearch  Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error(" walkin==>oprsearch routes failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'walkin==>oprsearch routes Method Error' + err
            });
        });

})



router.post('/advanceprofileSearch', function (req, res) {
    console.log("Entering into  advanceprofileSearch" + req.query.loginuserid, req.body.mailid, req.body.phone);
    walkindb.advanceprofileSearch(req.query.loginuserid, req.body.mailid, req.body.phone)
        .then(function (rows) {
            if (rows) {

                res.send(rows.rows);

            } else {
                console.log("walkin==>advanceprofileSearch falied" + err.body)
                res.status(500).send({
                    success: false,
                    message: 'walkin==>advanceprofileSearch  Method Error' + err
                });
            }
        }).fail(function (err) {
            uierrorlogger.error(" walkin==>advanceprofileSearch routes failed..." + err.body);
            res.status(500).send({
                success: false,
                message: 'walkin==>usersearch routes Method Error' + err
            });
        });

})




module.exports = router;