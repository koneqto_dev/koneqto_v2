var express = require('express');
var path = require('path');
var router = express.Router();
var multer = require('multer');
var ratingdb = require(path.join(__dirname, '../', 'modules', 'ratingdb'));
var uiinfologger = require(path.join(__dirname, '../', '../', 'config')).uiinfologger;
var uierrorlogger = require(path.join(__dirname, '../', '../', 'config')).uierrorlogger;
var http = require("http");
var request = require('request');
var log4js = require('log4js');


router.get('/loadcompanylist', function (req, res) {
    console.log("Entering into ratingservice==> loadcompanylist routes method",req.query.p_registration_id);

    ratingdb.loadcompanylist(req.query.p_registration_id)
    .then (function(rows)
    {
         if(rows){
           
               res.end(JSON.stringify(rows.rows))
         }

         else{
          
            console.log("ratingservice==>loadcompanylist routes"+err.body)
            res.status(500).send({ 
                success: false, 
                message:'loadcompanylist routes Method Error'+err
               });
         }
    }).fail(function (err) {
        res.status(500).send({ 
           success: false, 
           message:'ratingservice==>loadcompanylist routes Method Error'+err
          });
});
    
});


router.get('/loadcandidatelist', function (req, res) {
    console.log("Entering into ratingjs==> loadcandidatelist routes method",req.query.p_registration_id);

    ratingdb.loadcandidatelist(req.query.p_registration_id)
    .then (function(rows)
    {
         if(rows){
           
               res.end(JSON.stringify(rows.rows))
         }

         else{
          
            console.log("ratingjs==>loadcandidatelist routes"+err.body)
            res.status(500).send({ 
                success: false, 
                message:'ratingjs==>loadcandidatelist routes Method Error'+err
               });
         }
    }).fail(function (err) {
        res.status(500).send({ 
           success: false, 
           message:'ratingjs==>loadcandidatelist routes Method Error'+err
          });
});
    
});
//user_registration_id,company_registration_id,aspect1,aspect2,aspect3,aspect4,aspect5,description

router.post('/saverating',function(req,res){
    console.log("Entering into  saverating"+req.body.user_registration_id,
    req.body.company_registration_id,
    req.body.aspect1,
    req.body.aspect2,
    req.body.aspect3,
    req.body.aspect4, 
    req.body.aspect5,
    req.body.description     
);
   // p_registration_id ,p_event_name,p_event_type ,p_envet_desription ,p_functional_area,p_event_time
   ratingdb.saverating(req.body.user_registration_id,
        req.body.company_registration_id,
        req.body.aspect1,
        req.body.aspect2,
        req.body.aspect3,
        req.body.aspect4, 
        req.body.aspect5,
        req.body.description)
     .then (function(rows)
     {
          if(rows){
             console.log("ratingjs==>saverating routes Successful")         
             res.status(200).send({ 
                 success: true, 
                 message:'Sucessfull'
                });
          }
          else{
             console.log("ratingjs==>saverating falied"+err.body)
             res.status(500).send({ 
                success: false, 
                message:'ratingjs==>saverating routes Method Error'+err
               });
          }
     }).fail(function (err) {
         uierrorlogger.error("ratingjs==> saverating routes failed..." + err.body);
         res.status(500).send({ 
             success: false, 
             message:'ratingjs==>saverating routes Method Error'+err
            });         
     });     

 })

 router.post('/saveuserrating',function(req,res){
    console.log("Entering into  saveuserrating"+
    req.body.tech_rating,
    req.body.info_rating,
    req.body.behaviour_rating,
    req.body.etiquette_rating,
    req.body.communication_rating,
    req.body.expectations,
    req.body.overall_exp,
    req.body.user,
    req.body.jobid,
    req.body.recruiter_id    
);
   // p_registration_id ,p_event_name,p_event_type ,p_envet_desription ,p_functional_area,p_event_time
   ratingdb.saveuserrating(
    req.body.tech_rating,
    req.body.info_rating,
    req.body.behaviour_rating,
    req.body.etiquette_rating,
    req.body.communication_rating,
    req.body.expectations,
    req.body.overall_exp,
    req.body.user,
    req.body.jobid,
    req.body.recruiter_id)
     .then (function(rows)
     {
          if(rows){
             console.log("ratingjs==>saveuserrating routes Successful")         
             res.status(200).send({ 
                 success: true, 
                 message:'Sucessfull'
                });
          }
          else{
             console.log("ratingjs==>saveuserrating falied"+err.body)
             res.status(500).send({ 
                success: false, 
                message:'ratingjs==>saveuserrating routes Method Error'+err
               });
          }
     }).fail(function (err) {
         uierrorlogger.error("ratingjs==> saveuserrating routes failed..." + err.body);
         res.status(500).send({ 
             success: false, 
             message:'ratingjs==>saverating routes Method Error'+err
            });         
     });     

 })

 router.post('/recruiterrating',function(req,res){
    console.log("Entering into  recruiterrating "+
    req.body.id ,
    req.body.loginuserid ,
    req.body.recruiter_id,
    req.body.infrastructurerec,
    req.body.hostpitalityrec,
    req.body.waiting_timerec,
    req.body.arrangementrec,
    req.body.overall_exprec,
    req.body.r_created_date,
    req.body.rating_for,
    req.body.jobid,
    req.body.infracomp,
    req.body.hostcomp,
    req.body.timecomp,
    req.body.arrangecomp,
    req.body.overall_expcomp,
    req.body.c_created_date,
    req.body.p_rated_for_comp
 
);
created_by=''
   // p_registration_id ,p_event_name,p_event_type ,p_envet_desription ,p_functional_area,p_event_time
   ratingdb.recruiterrating(
    req.body.id ,
    req.body.loginuserid ,
    req.body.recruiter_id,
    req.body.infrastructurerec,
    req.body.hostpitalityrec,
    req.body.waiting_timerec,
    req.body.arrangementrec,
    req.body.overall_exprec,
    req.body.r_created_date,
    created_by,
    req.body.rating_for,
    req.body.jobid,
    req.body.infracomp,
    req.body.hostcomp,
    req.body.timecomp,
    req.body.arrangecomp,
    req.body.overall_expcomp,
    req.body.c_created_date,
    req.body.p_rated_for_comp,
      )
     .then (function(rows)
     {
          if(rows){
             console.log("ratingjs==>recruiterrating routes Successful")         
             res.status(200).send({ 
                 success: true, 
                 message:'Sucessfull'
                });
          }
          else{
             console.log("ratingjs==>recruiterrating falied"+err.body)
             res.status(500).send({ 
                success: false, 
                message:'ratingjs==>recruiterrating routes Method Error'+err
               });
          }
     }).fail(function (err) {
         uierrorlogger.error("ratingjs==> recruiterrating routes failed..." + err.body);
         res.status(500).send({ 
             success: false, 
             message:'ratingjs==>recruiterrating routes Method Error'+err
            });         
     });     

 })


 router.post('/getuserrating',function(req,res){
    console.log("Entering into  getuserrating"+req.body.posting_id,req.body.loginuserid    
);
   // p_registration_id ,p_event_name,p_event_type ,p_envet_desription ,p_functional_area,p_event_time
   ratingdb.loadalluserreviews(req.body.posting_id,req.body.loginuserid  )
     .then (function(rows)
     {
          if(rows){
             console.log("ratingjs==>getuserrating routes Successful")         
             res.send(JSON.stringify(rows.rows));
          }
          else{
             console.log("ratingjs==>getuserrating falied"+err.body)
             res.status(500).send({ 
                success: false, 
                message:'ratingjs==>getuserrating routes Method Error'+err
               });
          }
     }).fail(function (err) {
         uierrorlogger.error("ratingjs==> getuserrating routes failed..." + err.body);
         res.status(500).send({ 
             success: false, 
             message:'ratingjs==>getuserrating routes Method Error'+err
            });         
     });     

 })
 

 router.post('/loadallreviews',function(req,res){
    console.log("Entering into  loadallreviews"+req.body.jobid,req.body.loginuserid    
);
   // p_registration_id ,p_event_name,p_event_type ,p_envet_desription ,p_functional_area,p_event_time
   ratingdb.loadallreviews(req.body.jobid,req.body.loginuserid  )
     .then (function(rows)
     {
          if(rows){
             console.log("ratingjs==>loadallreviews routes Successful")         
             res.send(JSON.stringify(rows.rows));
          }
          else{
             console.log("ratingjs==>loadallreviews falied"+err.body)
             res.status(500).send({ 
                success: false, 
                message:'ratingjs==>loadallreviews routes Method Error'+err
               });
          }
     }).fail(function (err) {
         uierrorlogger.error("ratingjs==> loadallreviews routes failed..." + err.body);
         res.status(500).send({ 
             success: false, 
             message:'ratingjs==>loadallreviews routes Method Error'+err
            });         
     });     

 })


module.exports=router;