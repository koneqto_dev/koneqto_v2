var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
//var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var http = require("http");
var session = require('express-session');
var http = require('http');
var https = require('https');
var fs = require('fs');
var path = require('path');
var methodOverride = require('method-override');
var errorHandler = require('error-handler');
var log4js = require('log4js');
var jwt = require('jsonwebtoken');
var jwt_scretkey = 'uniq-sol-node-jwttoken';
var process = require('process');
var multer = require('multer');
var app = express();
var log = log4js.getLogger("app");
var _ = require('lodash')

var api = require('./server/routes/api');
var profile = require('./server/routes/profile');
var jobposting = require('./server/routes/jobposting');
var friendservice = require('./server/routes/friendservice');
var walkinservice = require('./server/routes/walkin');
var ratingservice = require('./server/routes/rating');
var loginauth = require('./server/routes/loginauth');



var jwt_scretkey = 'jobportal-node-jwttoken';
var authentication = require("./server/routes/auth");


//opentok config
var OpenTok = require('opentok'),
  opentok = new OpenTok('46230262', '5f1e4b3988412b44c680881d3f86c2b5b18c9fdf');
app.use(express.static('public'))
app.use(express.static('public'))
// app.use(bodyParser.json({limit: '100mb', extended: true}))
// app.use(function (req, res, next) { //allow cross origin requests
//   // res.setHeader("Access-Control-Allow-Methods", "POST, PUT, OPTIONS, DELETE, GET");
//   console.log("server connected");
//   res.header("Access-Control-Allow-Origin", "http://localhost:4200");
//   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//   //var loginsessionid = req.session;
//   // res.setHeader('Access-Control-Allow-Methods', '*');
//   //res.setHeader('Access-Control-Allow-Headers', 'Origin', 'Content-Type','X-Auth-Token','Authorization','*');
//   // res.setHeader("Access-Control-Allow-Credentials", "true");
//   next();

// });
app.use(function (req, res, next) {
  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', '*');

  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);

  // Pass to next layer of middleware
  next();
});


app.use(bodyParser({ limit: '500mb' }));
app.use(bodyParser.json({ limit: '500mb' }));
app.use(bodyParser.urlencoded({ limit: '500mb', extended: true }));
app.use(cookieParser());
app.use(methodOverride());
app.use(express.static(path.join(__dirname, 'client')));
app.use(session({ secret: 'your secret here' }));

app.use('/loginauth', loginauth)
app.use('/api', authentication, api);
app.use('/profile', authentication, profile)
app.use('/jobposting', authentication, jobposting)
app.use('/friendservice', authentication, friendservice)
app.use('/walkin', authentication, walkinservice)
app.use('/rating', authentication, ratingservice)


var options = {
  key: fs.readFileSync('certs/koneqto.key'),
  cert: fs.readFileSync('certs/koneqto_com.crt')
};

//to insatntiate server
var config = require('./config');
var port = config.port;
// console.log("Config - port " + port);
// console.log("Connecting string -" + config.connectionString);
log.info("Starting the server....");

var server = http.createServer(app).listen(port, function () {
  // console.log("Express server listening on port " + port);
  log.info("starting the server with port -" + port);
});

//socket io
var io = require('socket.io').listen(server);

var usersCollection = [];
var User = require('./server/modules/loginauthdb');
var pool = require('./config').client;


//opentok tokens and session
app.post('/saveOpenTokSessionToken', (req, res) => {
  opentok.createSession(function (err, session) {
    // if (err) return console.log(err);
    let token = opentok.generateToken(session.sessionId);
    //console.log(token);
    // console.log(req.body.callerId);
    // console.log(req.body.calleeId);
    let sql_query = `INSERT INTO unic_sol.videocall ("caller_id","callee_id","token","session_id") VALUES (${req.body.callerId},${req.body.calleeId},'${token}','${session.sessionId}')`
    pool.query(sql_query, (err, savedSession) => {
      if (err) {
        console.log(err)
      }
      else {
        res.status(200).json({ status: 200, message: "Token and Session saved succesfully", data: savedSession });
      }
    })
  })
});
app.get('/getOpenTokSessionToken/:userId', (req, res) => {
  let sql_query = `SELECT * FROM unic_sol.videocall WHERE ("caller_id" = ${req.params.userId} OR "callee_id" = ${req.params.userId})`;
  pool.query(sql_query, (err, openTokResponse) => {
    if (err) {
      // console.log(err)
    }
    else {
      // console.log(openTokResponse)
      res.status(200).json({ status: 200, message: "success", data: openTokResponse });
    }
  })
})

app.get('/getUserMessages/:fromId/:toId', (req, res) => {
  //console.log("GET USER MESAAGES")
  let fromid = req.params.fromId;
  let toid = req.params.toId
  let sql_query = `SELECT message,message_date,"fromId","toId","fromId" as "fromUserId" FROM unic_sol.lu_user_chat  WHERE  ("fromId"='${fromid}' AND "toId"='${toid}') OR ("fromId"='${toid}' AND "toId"='${fromid}')`;

  pool.query(sql_query, (err, messageResult) => {

    if (err) {
      // console.log(err)
    }
    else {
    //  console.log("messages lis...", messageResult.rows)
      res.json(messageResult.rows)
    }
  })
})
app.post("/listFriends", function (req, res) {
  let ucid = req.body.userId;
  let sq_query = `select * from unic_sol.fn_online_friends('${ucid}')`
  pool.query(sq_query, (err, result) => {
    if (err) {
      // console.log(err)
    }
    else {
      usersCollection = result.rows;
      var clonedArray = usersCollection.slice();
      usersCollection = _.uniqBy(clonedArray, (users) => {
        return users.user_registration_id;
      })
      var i = usersCollection.findIndex(x => x.user_registration_id == req.body.userId);

      clonedArray.splice(i, 0);
      usersCollection = clonedArray;
      res.json(usersCollection);
    }
  })

});
// Socket.io operations
// Socket.io operations
io.on('connection', function (socket) {
  // console.log('A user has connected to the server.');
  let user;
  socket.on('startRoom', function (user) {
    user = user;
    socket.emit('startedRoom', { isJoined: user })


    socket.on('join', function (user_registration_id) {

      // console.log("chat connected with..", user)
      // console.log("socket id  ..", user_registration_id)
      // Same contract as ng-chat.User
      // usersCollection.push({
      //   socket_id: socket.id, // Assigning the socket ID as the user ID in this example
      //   username: user.username,
      //   status: 0, // ng-chat UserStatus.Online,
      //   avatar: null,
      //   user_registration_id: user_registration_id,
      //   first_name: user.first_name,
      //   last_name: user.last_name,
      //   company_name: user.company_name
      // });
      //socket.broadcast.emit("friendsListChanged", usersCollection);
     usersCollection = _.uniqBy(usersCollection, function (e) {
        e.user_registration_id
      });
      var query = "update  unic_sol.lu_user_registration set socket_id=$1 where username=$2";
      pool.query(query, [socket.id, user.username], (err, response) => {
        if (err) {
          // console.log(err)
        }
        else {
          console.log(response)
          socket.broadcast.emit("friendsListChanged", usersCollection);
        }
      })


      // console.log(username + " has joined the chat room.");
      // console.log("socket id.",socket.id)
      // This is the user's unique ID to be used on ng-chat as the connected user.
      socket.emit("generatedUserId", user.user_id);

      // On disconnect remove this socket client from the users collection
      socket.on('disconnect', function () {
        // console.log('User disconnected!');

        var i = usersCollection.findIndex(x => x.user_id == user.user_id);
        usersCollection.splice(i, 1);
        console.log("usersCollection..", usersCollection)
        socket.broadcast.emit("friendsListChanged", usersCollection);
      });
    });
  })
  socket.on("sendMessage", function (message) {
    console.log("Message received:");

    pool.query(`INSERT INTO unic_sol.lu_user_chat(
                "fromId", "toId", message,  "message_date")
                VALUES ('${message.fromUserId}', '${message.toUserId}', '${message.message}', '${message.msgDate}')`, (err, response) => {
        if (err) {
          console.log(err)
        }
        else {
          //  console.log("userCollection..",usersCollection)
          //console.log("msg..", usersCollection.find(x => x.user_registration_id == message.fromUserObj.user_id))
          // console.log("TO User Id..",message.toId)
          console.log(message.fromUserId)
          socket.broadcast.emit("messageReceived", {
            user: message.fromUserObj,
            message: message
          });


          console.log("Message dispatched.");
        }
      })



    // console.log("Message dispatched.");
  });
});





module.exports.server = server;

/** Serving from the same express Server
// No cors required */
app.use(express.static('../Clinet'));
app.use(bodyParser.json()); 